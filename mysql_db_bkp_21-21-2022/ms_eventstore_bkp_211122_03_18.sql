-- MySQL dump 10.13  Distrib 8.0.30, for Linux (x86_64)
--
-- Host: 10.224.102.46    Database: ms_eventstore
-- ------------------------------------------------------
-- Server version	8.0.30-commercial

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `MsAltKey`
--

DROP TABLE IF EXISTS `MsAltKey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `MsAltKey` (
  `alternateKey` varchar(255) NOT NULL,
  `alternateName` varchar(255) NOT NULL,
  `entityId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`alternateKey`,`alternateName`),
  KEY `index_MsAltKey_entityId` (`entityId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MsAltKey`
--

LOCK TABLES `MsAltKey` WRITE;
/*!40000 ALTER TABLE `MsAltKey` DISABLE KEYS */;
/*!40000 ALTER TABLE `MsAltKey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_error`
--

DROP TABLE IF EXISTS `ms_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_error` (
  `errorEventId` varchar(255) NOT NULL,
  `dataEventId` varchar(255) DEFAULT NULL,
  `errorMessage` varchar(1024) DEFAULT NULL,
  `errorSourceTopic` varchar(255) DEFAULT NULL,
  `lastProcessedTime` datetime DEFAULT NULL,
  `payload` longblob,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`errorEventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_error`
--

LOCK TABLES `ms_error` WRITE;
/*!40000 ALTER TABLE `ms_error` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_inbox_events`
--

DROP TABLE IF EXISTS `ms_inbox_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_inbox_events` (
  `eventId` varchar(255) NOT NULL,
  `businessKey` varchar(255) DEFAULT NULL,
  `commandType` varchar(255) DEFAULT NULL,
  `creationTime` datetime DEFAULT NULL,
  `eventDetails` longtext,
  `eventSourceId` varchar(255) DEFAULT NULL,
  `eventType` varchar(255) DEFAULT NULL,
  `organizationId` varchar(255) DEFAULT NULL,
  `payload` longtext,
  `priority` int DEFAULT NULL,
  `processedTime` datetime DEFAULT NULL,
  `sequenceNo` bigint DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tenantId` varchar(255) DEFAULT NULL,
  `userId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`eventId`),
  KEY `inboxBusinessKeyIndex` (`businessKey`),
  KEY `inboxSequenceNoIndex` (`sequenceNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_inbox_events`
--

LOCK TABLES `ms_inbox_events` WRITE;
/*!40000 ALTER TABLE `ms_inbox_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_inbox_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_outbox_events`
--

DROP TABLE IF EXISTS `ms_outbox_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_outbox_events` (
  `eventId` varchar(255) NOT NULL,
  `businessKey` varchar(255) DEFAULT NULL,
  `correlationId` varchar(255) DEFAULT NULL,
  `creationTime` datetime DEFAULT NULL,
  `eventDetails` varchar(255) DEFAULT NULL,
  `eventType` varchar(255) DEFAULT NULL,
  `eventdate` datetime DEFAULT NULL,
  `organizationId` varchar(255) DEFAULT NULL,
  `payload` longtext,
  `priority` int DEFAULT NULL,
  `processedTime` datetime DEFAULT NULL,
  `sequenceNo` bigint DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tenantId` varchar(255) DEFAULT NULL,
  `userId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`eventId`),
  KEY `outboxBusinessKeyIndex` (`businessKey`),
  KEY `outboxSequenceNoIndex` (`sequenceNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_outbox_events`
--

LOCK TABLES `ms_outbox_events` WRITE;
/*!40000 ALTER TABLE `ms_outbox_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_outbox_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_reference_data`
--

DROP TABLE IF EXISTS `ms_reference_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_reference_data` (
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `softDeleteFlag` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`type`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_reference_data`
--

LOCK TABLES `ms_reference_data` WRITE;
/*!40000 ALTER TABLE `ms_reference_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_reference_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_history`
--

DROP TABLE IF EXISTS `schema_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schema_history` (
  `uniqueReference` varchar(255) NOT NULL,
  `createdOn` varchar(255) DEFAULT NULL,
  `results` varchar(255) DEFAULT NULL,
  `script` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uniqueReference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_history`
--

LOCK TABLES `schema_history` WRITE;
/*!40000 ALTER TABLE `schema_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `schema_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'ms_eventstore'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-21 15:42:34
