-- MySQL dump 10.13  Distrib 8.0.30, for Linux (x86_64)
--
-- Host: 10.224.102.46    Database: ms_campaign
-- ------------------------------------------------------
-- Server version	8.0.30-commercial

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Campaign`
--

DROP TABLE IF EXISTS `Campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Campaign` (
  `campaignId` varchar(255) NOT NULL,
  `campaignDescription` varchar(255) DEFAULT NULL,
  `campaignName` varchar(255) DEFAULT NULL,
  `campaignPriority` int DEFAULT NULL,
  `campaignStatus` varchar(255) DEFAULT NULL,
  `campaignType` varchar(255) DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `objectiveType` varchar(255) DEFAULT NULL,
  `productGroupId` varchar(255) DEFAULT NULL,
  `productId` varchar(255) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  PRIMARY KEY (`campaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Campaign`
--

LOCK TABLES `Campaign` WRITE;
/*!40000 ALTER TABLE `Campaign` DISABLE KEYS */;
/*!40000 ALTER TABLE `Campaign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CampaignChannelDetails`
--

DROP TABLE IF EXISTS `CampaignChannelDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CampaignChannelDetails` (
  `Campaign_campaignId` varchar(255) NOT NULL,
  `campaignId` varchar(255) DEFAULT NULL,
  `channelPriority` int DEFAULT NULL,
  `channelSubType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Campaign_campaignId`),
  CONSTRAINT `FKryfig2e97cl6t1iwhpc12cvks` FOREIGN KEY (`Campaign_campaignId`) REFERENCES `Campaign` (`campaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CampaignChannelDetails`
--

LOCK TABLES `CampaignChannelDetails` WRITE;
/*!40000 ALTER TABLE `CampaignChannelDetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `CampaignChannelDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CampaignExtension`
--

DROP TABLE IF EXISTS `CampaignExtension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CampaignExtension` (
  `Campaign_campaignId` varchar(255) NOT NULL,
  `fieldValue` varchar(255) DEFAULT NULL,
  `fieldName` varchar(255) NOT NULL,
  PRIMARY KEY (`Campaign_campaignId`,`fieldName`),
  CONSTRAINT `FKcui2jq6j6n7lh9apu5r989jkx` FOREIGN KEY (`Campaign_campaignId`) REFERENCES `Campaign` (`campaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CampaignExtension`
--

LOCK TABLES `CampaignExtension` WRITE;
/*!40000 ALTER TABLE `CampaignExtension` DISABLE KEYS */;
/*!40000 ALTER TABLE `CampaignExtension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Campaign_channelType`
--

DROP TABLE IF EXISTS `Campaign_channelType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Campaign_channelType` (
  `Campaign_campaignId` varchar(255) NOT NULL,
  `channelType` varchar(255) NOT NULL,
  PRIMARY KEY (`Campaign_campaignId`,`channelType`),
  CONSTRAINT `FKgjpe90ae0b9h1yiwxaretev6h` FOREIGN KEY (`Campaign_campaignId`) REFERENCES `Campaign` (`campaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Campaign_channelType`
--

LOCK TABLES `Campaign_channelType` WRITE;
/*!40000 ALTER TABLE `Campaign_channelType` DISABLE KEYS */;
/*!40000 ALTER TABLE `Campaign_channelType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Campaign_eventTrigger`
--

DROP TABLE IF EXISTS `Campaign_eventTrigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Campaign_eventTrigger` (
  `campaignId` varchar(255) NOT NULL,
  `eventTriggerId` varchar(255) NOT NULL,
  PRIMARY KEY (`campaignId`,`eventTriggerId`),
  KEY `FKs599vc9gd4m6mw9l2lmjq3wat` (`eventTriggerId`),
  CONSTRAINT `FK2riudld9od8jaqp4dpbimas06` FOREIGN KEY (`campaignId`) REFERENCES `Campaign` (`campaignId`),
  CONSTRAINT `FKs599vc9gd4m6mw9l2lmjq3wat` FOREIGN KEY (`eventTriggerId`) REFERENCES `EventTriggers` (`eventTriggerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Campaign_eventTrigger`
--

LOCK TABLES `Campaign_eventTrigger` WRITE;
/*!40000 ALTER TABLE `Campaign_eventTrigger` DISABLE KEYS */;
/*!40000 ALTER TABLE `Campaign_eventTrigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Campaign_profile`
--

DROP TABLE IF EXISTS `Campaign_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Campaign_profile` (
  `campaignId` varchar(255) NOT NULL,
  `profileId` varchar(255) NOT NULL,
  PRIMARY KEY (`campaignId`,`profileId`),
  KEY `FKo371hw64mtb60f1mpt78dbil8` (`profileId`),
  CONSTRAINT `FKa5gpjsxo42dg7c57bcphheu90` FOREIGN KEY (`campaignId`) REFERENCES `Campaign` (`campaignId`),
  CONSTRAINT `FKo371hw64mtb60f1mpt78dbil8` FOREIGN KEY (`profileId`) REFERENCES `Profile` (`profileId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Campaign_profile`
--

LOCK TABLES `Campaign_profile` WRITE;
/*!40000 ALTER TABLE `Campaign_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `Campaign_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DataContext`
--

DROP TABLE IF EXISTS `DataContext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DataContext` (
  `dataContextId` varchar(255) NOT NULL,
  `dataContextName` varchar(255) DEFAULT NULL,
  `dataContextDescription` varchar(255) DEFAULT NULL,
  `dataContextSource` varchar(255) DEFAULT NULL,
  `dataContextServiceName` varchar(255) DEFAULT NULL,
  `dataContextEndPoints` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dataContextId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DataContext`
--

LOCK TABLES `DataContext` WRITE;
/*!40000 ALTER TABLE `DataContext` DISABLE KEYS */;
INSERT INTO `DataContext` VALUES ('DC001','Next Best Product Model flow','DataContext created for Next Best Product Model flow Profiles','Analytics','Dataset_AllCustomersNBP','http://tmnstransact2.southeastasia.cloudapp.azure.com/APIService/odata/Tenant1/Dataset_AllCustomersNBP'),('DC002','Customer Attrition Model flow','DataContext created for Customer Attrition Model flow Profiles','Analytics','Dataset_AllCustomersAttrition','http://40.127.187.34/APIServices/odata/Tenant1/XAIAttritionResultsDetailed'),('DC003','Funds flow','DataContext created for Funds flow Profiles','Analytics','Dataset_AllCustomersFunds','http://tmnstransact2.southeastasia.cloudapp.azure.com/APIService/odata/Tenant1/Dataset_AllCustomers');
/*!40000 ALTER TABLE `DataContext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DataContextExtension`
--

DROP TABLE IF EXISTS `DataContextExtension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DataContextExtension` (
  `DataContext_dataContextId` varchar(255) NOT NULL,
  `fieldValue` varchar(255) DEFAULT NULL,
  `fieldName` varchar(255) NOT NULL,
  PRIMARY KEY (`DataContext_dataContextId`,`fieldName`),
  CONSTRAINT `FK9h94x8iy8my33ue98nlvmpq70` FOREIGN KEY (`DataContext_dataContextId`) REFERENCES `DataContext` (`dataContextId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DataContextExtension`
--

LOCK TABLES `DataContextExtension` WRITE;
/*!40000 ALTER TABLE `DataContextExtension` DISABLE KEYS */;
/*!40000 ALTER TABLE `DataContextExtension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EventTrigger`
--

DROP TABLE IF EXISTS `EventTrigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EventTrigger` (
  `eventTriggerId` varchar(255) NOT NULL,
  `eventCode` varchar(255) DEFAULT NULL,
  `eventDescription` varchar(255) DEFAULT NULL,
  `eventName` varchar(255) DEFAULT NULL,
  `eventSource` varchar(255) DEFAULT NULL,
  `triggerType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`eventTriggerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EventTrigger`
--

LOCK TABLES `EventTrigger` WRITE;
/*!40000 ALTER TABLE `EventTrigger` DISABLE KEYS */;
INSERT INTO `EventTrigger` VALUES ('ET001','SURPLUSFUNDS','Customer has surplus Funds in the selected account','SurplusFunds','Transact/DS','External'),('ET002','OVERDRAWNACCOUNT','The account in consideration is overdrawn','OverdrawnAccount','Transact/DS','External'),('ET003','UPDATEPHONE','Customer updated the phone number','PhoneUpdate','Transact/CRM/DS','Internal'),('ET004','DEPOSITMATURITY','Deposit meets maturity and notify the same to customers','MatureDeposit','Transact/DS','External'),('ET005','PRELOGIN','Offer details display in Prelogin screen (Festivals like New year)','PreLogin','Transact/CRM/DS','Internal'),('ET006','ACCOUNTDASHBOARD','User landed on the account dashboard page','AccountDashboard','Transact/CRM/DS','Internal'),('ET007','APPLY_NEW_ACCOUNT','User landed in open new account page','ApplyNewAccount','Transact/CRM/DS','Internal'),('ET008','OLB_frmStopPayments','Stop Payments Requests','Stop Payments Requests','WEB','Popup'),('ET009','OLB_frmFastAddDBXAccount','Add Infinity Bank Account','Add Infinity Bank Account','WEB','Popup'),('ET010','OLB_frmFastAddExternalAccount','Add External Account','Add External Account','WEB','Popup'),('ET011','OLB_frmFastAddInternationalAccount','Add International Account','Add International Account','WEB','Popup'),('ET012','OLB_frmFastAddRecipient','Add Person-to-Person Recipient','Add Person-to-Person Recipient','WEB','Popup'),('ET013','OLB_frmFastDeActiveRecipient','Deactivate Person-to-Person','Deactivate Person-to-Person','WEB','Popup'),('ET014','OLB_frmFastManagePayee','Manage Recipients','Manage Recipients','WEB','Popup'),('ET015','OLB_frmFastRecipientGateWay','Add Recipient','Add Recipient','WEB','Popup'),('ET016','OLB_frmFastTransfers','Transfer','Transfer','WEB','Popup'),('ET017','OLB_frmFastTransfersActivites','Transfer Activities','Transfer Activities','WEB','Popup'),('ET018','OLB_frmP2PSettings','View Person-to-Person Settings','View Person-to-Person Settings','WEB','Popup'),('ET019','OLB_frmAddBulkTransferFile','Add Bulk Transfer File','Add Bulk Transfer File','WEB','Popup'),('ET020','OLB_frmMakeBulkTransfer','Make Bulk Transfer','Make Bulk Transfer','WEB','Popup'),('ET021','OLB_frmWireTransferAddKonyAccountStep1','Wire Transfer Add Recipient','Wire Transfer Add Recipient','WEB','Popup'),('ET022','OLB_frmWireTransferOneTimePaymentStep1','Wire Transfer Make One-Time Payment','Wire Transfer Make One-Time Payment','WEB','Popup'),('ET023','OLB_frmWireTransfersManageRecipients','Wire Transfer My Recipients','Wire Transfer My Recipients','WEB','Popup'),('ET024','OLB_frmWireTransfersRecent','Wire Transfer History','Wire Transfer History','WEB','Popup'),('ET025','OLB_frmWireTransfersWindow','Make Transfer','Make Transfer','WEB','Popup'),('ET026','OLB_frmProfileManagement','Settings','Settings','WEB','Popup'),('ET027','OLB_frmNAO','Open New Account','Open New Account','WEB','Popup'),('ET028','OLB_frmPersonalFinanceManagement','Personal Finance Management','Personal Finance Management','WEB','Popup'),('ET029','OLB_frmLocateUs','Locate Us','Locate Us','WEB','Popup'),('ET030','OLB_frmOnlineHelp','Help','Help','WEB','Popup'),('ET031','OLB_frmContactUsPrivacyTandC','Contact Us','Contact Us','WEB','Popup'),('ET032','OLB_frmAddPayee1','Add Payee','Add Payee','WEB','Popup'),('ET033','OLB_frmBillPayHistory','My Bills History','My Bills History','WEB','Popup'),('ET034','OLB_frmBulkPayees','Pay a Bill','Pay a Bill','WEB','Popup'),('ET035','OLB_frmMakeOneTimePayee','Make One-Time Payment','Make One-Time Payment','WEB','Popup'),('ET036','OLB_frmManagePayees','My Payee List','My Payee List','WEB','Popup'),('ET037','OLB_frmCardManagement','Card Management','Card Management','WEB','Popup'),('ET038','OLB_frmCustomerFeedback','Customer Feedback','Customer Feedback','WEB','Popup'),('ET039','OLB_frmNotificationsAndMessages','Alerts','Alerts','WEB','Popup'),('ET040','OLB_frmAccountsDetails','View Statements','View Statements','WEB','Popup'),('ET041','OLB_frmDashboard','Accounts Dashboard','Accounts Dashboard','WEB','Popup'),('ET042','OLB_frmScheduledTransactions','Scheduled Transactions','Scheduled Transactions','WEB','Popup'),('ET043','MB_frmUnifiedDashboard','Accounts','Accounts','MOBILE','Popup'),('ET044','MB_frmMMTransferFromAccount','Transfer','Transfer','MOBILE','Popup'),('ET045','MB_frmMMTransfers','Transfer Activities','Transfer Activities','MOBILE','Popup'),('ET046','MB_frmManageRecipientType','Manage Recipients','Manage Recipients','MOBILE','Popup'),('ET047','MB_frmBillPay','My Bills','My Bills','MOBILE','Popup'),('ET048','MB_frmCheckDeposit','Check Deposits','Check Deposits','MOBILE','Popup'),('ET049','MB_frmCardLessHome','Cardless Cash','Cardless Cash','MOBILE','Popup'),('ET050','MB_frmCardManageHome','Card Management','Card Management','MOBILE','Popup'),('ET051','MB_frmNAOSelectProduct','Apply for New Account','Apply for New Account','MOBILE','Popup'),('ET052','MB_frmPFMMyMoney','My Money','My Money','MOBILE','Popup'),('ET053','MB_frmMessages','Messages','Messages','MOBILE','Popup'),('ET054','MB_frmSettings','Settings','Settings','MOBILE','Popup'),('ET055','MB_frmLocationMap','Locate Us','Locate Us','MOBILE','Popup'),('ET056','MB_frmInAppFeedbackRating','Feedback','Feedback','MOBILE','Popup'),('ET057','MB_frmSupport','Support','Support','MOBILE','Popup');
/*!40000 ALTER TABLE `EventTrigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EventTriggers`
--

DROP TABLE IF EXISTS `EventTriggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EventTriggers` (
  `eventTriggerId` varchar(255) NOT NULL,
  `eventCode` varchar(255) DEFAULT NULL,
  `eventDescription` varchar(255) DEFAULT NULL,
  `eventName` varchar(255) DEFAULT NULL,
  `eventSource` varchar(255) DEFAULT NULL,
  `triggerType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`eventTriggerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EventTriggers`
--

LOCK TABLES `EventTriggers` WRITE;
/*!40000 ALTER TABLE `EventTriggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `EventTriggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EventTriggersExtension`
--

DROP TABLE IF EXISTS `EventTriggersExtension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EventTriggersExtension` (
  `EventTriggers_eventTriggerId` varchar(255) NOT NULL,
  `fieldValue` varchar(255) DEFAULT NULL,
  `fieldName` varchar(255) NOT NULL,
  PRIMARY KEY (`EventTriggers_eventTriggerId`,`fieldName`),
  CONSTRAINT `FKcq1rcywagyugn6ceaf9l2jg5v` FOREIGN KEY (`EventTriggers_eventTriggerId`) REFERENCES `EventTriggers` (`eventTriggerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EventTriggersExtension`
--

LOCK TABLES `EventTriggersExtension` WRITE;
/*!40000 ALTER TABLE `EventTriggersExtension` DISABLE KEYS */;
/*!40000 ALTER TABLE `EventTriggersExtension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MsAltKey`
--

DROP TABLE IF EXISTS `MsAltKey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `MsAltKey` (
  `alternateKey` varchar(255) NOT NULL,
  `alternateName` varchar(255) NOT NULL,
  `entityId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`alternateKey`,`alternateName`),
  KEY `index_MsAltKey_entityId` (`entityId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MsAltKey`
--

LOCK TABLES `MsAltKey` WRITE;
/*!40000 ALTER TABLE `MsAltKey` DISABLE KEYS */;
/*!40000 ALTER TABLE `MsAltKey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OfflineTemplate`
--

DROP TABLE IF EXISTS `OfflineTemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `OfflineTemplate` (
  `offlineTemplateId` varchar(255) NOT NULL,
  `channelSubType` varchar(255) DEFAULT NULL,
  `content` longtext,
  `subject` varchar(255) DEFAULT NULL,
  `campaignId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`offlineTemplateId`),
  KEY `FK3u5rjv2ubsdeedg015kcnypsc` (`campaignId`),
  CONSTRAINT `FK3u5rjv2ubsdeedg015kcnypsc` FOREIGN KEY (`campaignId`) REFERENCES `Campaign` (`campaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OfflineTemplate`
--

LOCK TABLES `OfflineTemplate` WRITE;
/*!40000 ALTER TABLE `OfflineTemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `OfflineTemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OfflineTemplateExtension`
--

DROP TABLE IF EXISTS `OfflineTemplateExtension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `OfflineTemplateExtension` (
  `OfflineTemplate_offlineTemplateId` varchar(255) NOT NULL,
  `fieldValue` varchar(255) DEFAULT NULL,
  `fieldName` varchar(255) NOT NULL,
  PRIMARY KEY (`OfflineTemplate_offlineTemplateId`,`fieldName`),
  CONSTRAINT `FK8tsehrwbt90ensuuvon3fjncp` FOREIGN KEY (`OfflineTemplate_offlineTemplateId`) REFERENCES `OfflineTemplate` (`offlineTemplateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OfflineTemplateExtension`
--

LOCK TABLES `OfflineTemplateExtension` WRITE;
/*!40000 ALTER TABLE `OfflineTemplateExtension` DISABLE KEYS */;
/*!40000 ALTER TABLE `OfflineTemplateExtension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OnlineContent`
--

DROP TABLE IF EXISTS `OnlineContent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `OnlineContent` (
  `onlineContentId` varchar(255) NOT NULL,
  `targetURL` varchar(255) DEFAULT NULL,
  `campaignId` varchar(255) DEFAULT NULL,
  `placeholderId` varchar(255) DEFAULT NULL,
  `imageURL` varchar(255) DEFAULT NULL,
  `imageIndex` int DEFAULT NULL,
  `callToActionButtonLabel` varchar(255) DEFAULT NULL,
  `callToActionTargetURL` varchar(255) DEFAULT NULL,
  `showReadLaterButton` varchar(255) DEFAULT NULL,
  `showCloseIcon` varchar(255) DEFAULT NULL,
  `bannerTitle` varchar(255) DEFAULT NULL,
  `bannerDescription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`onlineContentId`),
  KEY `FKefqikrm2ixe8brgn7aahqy7rn` (`campaignId`),
  KEY `FKd0v4wdb4pl89p6jt7i42qylki` (`placeholderId`),
  CONSTRAINT `FKd0v4wdb4pl89p6jt7i42qylki` FOREIGN KEY (`placeholderId`) REFERENCES `PlaceHolder` (`placeholderId`),
  CONSTRAINT `FKefqikrm2ixe8brgn7aahqy7rn` FOREIGN KEY (`campaignId`) REFERENCES `Campaign` (`campaignId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OnlineContent`
--

LOCK TABLES `OnlineContent` WRITE;
/*!40000 ALTER TABLE `OnlineContent` DISABLE KEYS */;
INSERT INTO `OnlineContent` VALUES ('OC001','https://google.com',NULL,'PH001','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-prelogin-1x-1.jpg',1,NULL,NULL,NULL,NULL,NULL,NULL),('OC002','https://google.com',NULL,'PH001','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-prelogin-1x-2.jpg',2,NULL,NULL,NULL,NULL,NULL,NULL),('OC003','https://google.com',NULL,'PH001','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-prelogin-1x-3.jpg',3,NULL,NULL,NULL,NULL,NULL,NULL),('OC004','https://google.com',NULL,'PH002','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-prelogin-2x-1.jpg',1,NULL,NULL,NULL,NULL,NULL,NULL),('OC005','https://google.com',NULL,'PH002','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-prelogin-2x-2.jpg',2,NULL,NULL,NULL,NULL,NULL,NULL),('OC006','https://google.com',NULL,'PH002','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-prelogin-2x-3.jpg',3,NULL,NULL,NULL,NULL,NULL,NULL),('OC007','https://google.com',NULL,'PH003','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-prelogin-3x-1.jpg',1,NULL,NULL,NULL,NULL,NULL,NULL),('OC008','https://google.com',NULL,'PH003','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-prelogin-3x-2.jpg',2,NULL,NULL,NULL,NULL,NULL,NULL),('OC009','https://google.com',NULL,'PH003','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-prelogin-3x-3.jpg',3,NULL,NULL,NULL,NULL,NULL,NULL),('OC010','https://google.com',NULL,'PH010','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-dashboard-1x-1.png',1,NULL,NULL,NULL,NULL,NULL,NULL),('OC011','https://google.com',NULL,'PH010','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-dashboard-1x-2.png',2,NULL,NULL,NULL,NULL,NULL,NULL),('OC012','https://google.com',NULL,'PH010','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-dashboard-1x-3.png',3,NULL,NULL,NULL,NULL,NULL,NULL),('OC013','https://google.com',NULL,'PH011','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-dashboard-2x-1.png',1,NULL,NULL,NULL,NULL,NULL,NULL),('OC014','https://google.com',NULL,'PH011','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-dashboard-2x-2.png',2,NULL,NULL,NULL,NULL,NULL,NULL),('OC015','https://google.com',NULL,'PH011','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-dashboard-2x-3.png',3,NULL,NULL,NULL,NULL,NULL,NULL),('OC016','https://google.com',NULL,'PH012','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-dashboard-3x-1.png',1,NULL,NULL,NULL,NULL,NULL,NULL),('OC017','https://google.com',NULL,'PH012','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-dashboard-3x-2.png',2,NULL,NULL,NULL,NULL,NULL,NULL),('OC018','https://google.com',NULL,'PH012','https://retailbanking1.konycloud.com/dbimages/campaign-mobile-dashboard-3x-3.png',3,NULL,NULL,NULL,NULL,NULL,NULL),('OC019','https://google.com',NULL,'PH007','https://retailbanking1.konycloud.com/dbimages/campaign-web-dashboard-560x308.png',1,NULL,NULL,NULL,NULL,NULL,NULL),('OC020','https://google.com',NULL,'PH008','https://retailbanking1.konycloud.com/dbimages/campaign-web-dashboard-668x728.png',1,NULL,NULL,NULL,NULL,NULL,NULL),('OC021','https://google.com',NULL,'PH009','https://retailbanking1.konycloud.com/dbimages/campaign-web-dashboard-740x304.png',1,NULL,NULL,NULL,NULL,NULL,NULL),('OC022','https://google.com',NULL,'PH004','https://retailbanking1.konycloud.com/dbimages/campaign-web-applyfornewaccount-560x308.png',1,NULL,NULL,NULL,NULL,NULL,NULL),('OC023','https://google.com',NULL,'PH005','https://retailbanking1.konycloud.com/dbimages/campaign-web-applyfornewaccount-668x728.png',1,NULL,NULL,NULL,NULL,NULL,NULL),('OC024','https://google.com',NULL,'PH006','https://retailbanking1.konycloud.com/dbimages/campaign-web-applyfornewaccount-740x304.png',1,NULL,NULL,NULL,NULL,NULL,NULL),('OC025','https://google.com',NULL,'PH013','https://retailbanking1.konycloud.com/dbimages/campaign-web-dashboard-560x308.png',1,NULL,NULL,NULL,NULL,NULL,NULL),('OC026','https://google.com',NULL,'PH014','https://retailbanking1.konycloud.com/dbimages/campaign-web-dashboard-668x728.png',1,NULL,NULL,NULL,NULL,NULL,NULL),('OC027','https://google.com',NULL,'PH015','https://retailbanking1.konycloud.com/dbimages/campaign-web-dashboard-740x304.png',1,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `OnlineContent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OnlineContentExtension`
--

DROP TABLE IF EXISTS `OnlineContentExtension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `OnlineContentExtension` (
  `OnlineContent_onlineContentId` varchar(255) NOT NULL,
  `fieldValue` varchar(255) DEFAULT NULL,
  `fieldName` varchar(255) NOT NULL,
  PRIMARY KEY (`OnlineContent_onlineContentId`,`fieldName`),
  CONSTRAINT `FK4dwbt9w45g5mwjwnjimoer4vk` FOREIGN KEY (`OnlineContent_onlineContentId`) REFERENCES `OnlineContent` (`onlineContentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OnlineContentExtension`
--

LOCK TABLES `OnlineContentExtension` WRITE;
/*!40000 ALTER TABLE `OnlineContentExtension` DISABLE KEYS */;
/*!40000 ALTER TABLE `OnlineContentExtension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PlaceHolder`
--

DROP TABLE IF EXISTS `PlaceHolder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PlaceHolder` (
  `placeholderId` varchar(255) NOT NULL,
  `application` varchar(255) DEFAULT NULL,
  `channelSubType` varchar(255) DEFAULT NULL,
  `placeholderDescription` varchar(255) DEFAULT NULL,
  `placeholderIdentifier` varchar(255) DEFAULT NULL,
  `placeholderName` varchar(255) DEFAULT NULL,
  `imageResolution` varchar(255) DEFAULT NULL,
  `imageScale` varchar(255) DEFAULT NULL,
  `imageSize` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`placeholderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PlaceHolder`
--

LOCK TABLES `PlaceHolder` WRITE;
/*!40000 ALTER TABLE `PlaceHolder` DISABLE KEYS */;
INSERT INTO `PlaceHolder` VALUES ('PH001','RETAIL BANKING','MOBILE','Sample place holder description','PRELOGIN','Homepage','375x106','1x','51200'),('PH002','RETAIL BANKING','MOBILE','Sample place holder description','PRELOGIN','Homepage','750x212','2x','51200'),('PH003','RETAIL BANKING','MOBILE','Sample place holder description','PRELOGIN','Homepage','1125x318','3x','51200'),('PH004','RETAIL BANKING','WEB','Sample place holder description','APPLY_FOR_NEW_ACCOUNT','Apply for new account','560x308','640','51200'),('PH005','RETAIL BANKING','WEB','Sample place holder description','APPLY_FOR_NEW_ACCOUNT','Apply for new account','668x728','1024','51200'),('PH006','RETAIL BANKING','WEB','Sample place holder description','APPLY_FOR_NEW_ACCOUNT','Apply for new account','740x304','1366','51200'),('PH007','RETAIL BANKING','WEB','Sample place holder description','ACCOUNT_DASHBOARD','Account Dashboard','560x308','640','51200'),('PH008','RETAIL BANKING','WEB','Sample place holder description','ACCOUNT_DASHBOARD','Account Dashboard','668x728','1024','51200'),('PH009','RETAIL BANKING','WEB','Sample place holder description','ACCOUNT_DASHBOARD','Account Dashboard','740x304','1366','51200'),('PH010','RETAIL BANKING','MOBILE','Sample place holder description','ACCOUNT_DASHBOARD','Account Dashboard','343x198','1x','51200'),('PH011','RETAIL BANKING','MOBILE','Sample place holder description','ACCOUNT_DASHBOARD','Account Dashboard','686x396','2x','51200'),('PH012','RETAIL BANKING','MOBILE','Sample place holder description','ACCOUNT_DASHBOARD','Account Dashboard','1029x594','3x','51200'),('PH013','RETAIL BANKING','WEB','Sample place holder description','PRELOGIN','Homepage','560x308','640','51200'),('PH014','RETAIL BANKING','WEB','Sample place holder description','PRELOGIN','Homepage','668x728','1024','51200'),('PH015','RETAIL BANKING','WEB','Sample place holder description','PRELOGIN','Homepage','740x304','1366','51200'),('PH016','RETAIL BANKING','MOBILE','Sample place holder description','POPUP','Popup for mobile','560x323','3x','51200'),('PH017','RETAIL BANKING','WEB','Sample place holder description','POPUP','Popup for web','560x323','1366','51200');
/*!40000 ALTER TABLE `PlaceHolder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PlaceHolderExtension`
--

DROP TABLE IF EXISTS `PlaceHolderExtension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PlaceHolderExtension` (
  `PlaceHolder_placeholderId` varchar(255) NOT NULL,
  `fieldValue` varchar(255) DEFAULT NULL,
  `fieldName` varchar(255) NOT NULL,
  PRIMARY KEY (`PlaceHolder_placeholderId`,`fieldName`),
  CONSTRAINT `FKpefp3uuoq2nhwg3vk5ow86d29` FOREIGN KEY (`PlaceHolder_placeholderId`) REFERENCES `PlaceHolder` (`placeholderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PlaceHolderExtension`
--

LOCK TABLES `PlaceHolderExtension` WRITE;
/*!40000 ALTER TABLE `PlaceHolderExtension` DISABLE KEYS */;
/*!40000 ALTER TABLE `PlaceHolderExtension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Profile`
--

DROP TABLE IF EXISTS `Profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Profile` (
  `profileId` varchar(255) NOT NULL,
  `numberOfUsers` int DEFAULT NULL,
  `profileCreationDate` datetime DEFAULT NULL,
  `profileDeactivatedDate` datetime DEFAULT NULL,
  `profileDescription` varchar(255) DEFAULT NULL,
  `profileName` varchar(255) DEFAULT NULL,
  `profileStatus` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`profileId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Profile`
--

LOCK TABLES `Profile` WRITE;
/*!40000 ALTER TABLE `Profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `Profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProfileCondition`
--

DROP TABLE IF EXISTS `ProfileCondition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ProfileCondition` (
  `profileConditionId` varchar(255) NOT NULL,
  `conditionExpression` varchar(255) DEFAULT NULL,
  `dataContextId` varchar(255) DEFAULT NULL,
  `profileId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`profileConditionId`),
  KEY `FKa8ul4wo14mqtucri75sj43k3s` (`dataContextId`),
  KEY `FKj1bq4rvkb04nu14g5oev62mup` (`profileId`),
  CONSTRAINT `FKa8ul4wo14mqtucri75sj43k3s` FOREIGN KEY (`dataContextId`) REFERENCES `DataContext` (`dataContextId`),
  CONSTRAINT `FKj1bq4rvkb04nu14g5oev62mup` FOREIGN KEY (`profileId`) REFERENCES `Profile` (`profileId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProfileCondition`
--

LOCK TABLES `ProfileCondition` WRITE;
/*!40000 ALTER TABLE `ProfileCondition` DISABLE KEYS */;
/*!40000 ALTER TABLE `ProfileCondition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProfileExtension`
--

DROP TABLE IF EXISTS `ProfileExtension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ProfileExtension` (
  `Profile_profileId` varchar(255) NOT NULL,
  `fieldValue` varchar(255) DEFAULT NULL,
  `fieldName` varchar(255) NOT NULL,
  PRIMARY KEY (`Profile_profileId`,`fieldName`),
  CONSTRAINT `FKrs46544ypeffovf17pjtfwi0l` FOREIGN KEY (`Profile_profileId`) REFERENCES `Profile` (`profileId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProfileExtension`
--

LOCK TABLES `ProfileExtension` WRITE;
/*!40000 ALTER TABLE `ProfileExtension` DISABLE KEYS */;
/*!40000 ALTER TABLE `ProfileExtension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_error`
--

DROP TABLE IF EXISTS `ms_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_error` (
  `errorEventId` varchar(255) NOT NULL,
  `dataEventId` varchar(255) DEFAULT NULL,
  `errorMessage` varchar(1024) DEFAULT NULL,
  `errorSourceTopic` varchar(255) DEFAULT NULL,
  `lastProcessedTime` datetime DEFAULT NULL,
  `payload` longblob,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`errorEventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_error`
--

LOCK TABLES `ms_error` WRITE;
/*!40000 ALTER TABLE `ms_error` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_inbox_events`
--

DROP TABLE IF EXISTS `ms_inbox_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_inbox_events` (
  `eventId` varchar(255) NOT NULL,
  `businessKey` varchar(255) DEFAULT NULL,
  `commandType` varchar(255) DEFAULT NULL,
  `creationTime` datetime DEFAULT NULL,
  `eventDetails` longtext,
  `eventSourceId` varchar(255) DEFAULT NULL,
  `eventType` varchar(255) DEFAULT NULL,
  `organizationId` varchar(255) DEFAULT NULL,
  `payload` longtext,
  `priority` int DEFAULT NULL,
  `processedTime` datetime DEFAULT NULL,
  `sequenceNo` bigint DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tenantId` varchar(255) DEFAULT NULL,
  `userId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`eventId`),
  KEY `inboxBusinessKeyIndex` (`businessKey`),
  KEY `inboxSequenceNoIndex` (`sequenceNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_inbox_events`
--

LOCK TABLES `ms_inbox_events` WRITE;
/*!40000 ALTER TABLE `ms_inbox_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_inbox_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_outbox_events`
--

DROP TABLE IF EXISTS `ms_outbox_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_outbox_events` (
  `eventId` varchar(255) NOT NULL,
  `businessKey` varchar(255) DEFAULT NULL,
  `correlationId` varchar(255) DEFAULT NULL,
  `creationTime` datetime DEFAULT NULL,
  `eventDetails` varchar(255) DEFAULT NULL,
  `eventType` varchar(255) DEFAULT NULL,
  `eventdate` datetime DEFAULT NULL,
  `organizationId` varchar(255) DEFAULT NULL,
  `payload` longtext,
  `priority` int DEFAULT NULL,
  `processedTime` datetime DEFAULT NULL,
  `sequenceNo` bigint DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tenantId` varchar(255) DEFAULT NULL,
  `userId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`eventId`),
  KEY `outboxBusinessKeyIndex` (`businessKey`),
  KEY `outboxSequenceNoIndex` (`sequenceNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_outbox_events`
--

LOCK TABLES `ms_outbox_events` WRITE;
/*!40000 ALTER TABLE `ms_outbox_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_outbox_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_reference_data`
--

DROP TABLE IF EXISTS `ms_reference_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_reference_data` (
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `softDeleteFlag` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`type`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_reference_data`
--

LOCK TABLES `ms_reference_data` WRITE;
/*!40000 ALTER TABLE `ms_reference_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_reference_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_history`
--

DROP TABLE IF EXISTS `schema_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schema_history` (
  `uniqueReference` varchar(255) NOT NULL,
  `createdOn` varchar(255) DEFAULT NULL,
  `results` varchar(255) DEFAULT NULL,
  `script` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uniqueReference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_history`
--

LOCK TABLES `schema_history` WRITE;
/*!40000 ALTER TABLE `schema_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `schema_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'ms_campaign'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-21 15:40:33
