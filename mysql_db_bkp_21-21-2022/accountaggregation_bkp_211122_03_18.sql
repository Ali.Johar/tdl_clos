-- MySQL dump 10.13  Distrib 8.0.30, for Linux (x86_64)
--
-- Host: 10.224.102.46    Database: accountaggregation
-- ------------------------------------------------------
-- Server version	8.0.30-commercial

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CustomerProfile`
--

DROP TABLE IF EXISTS `CustomerProfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CustomerProfile` (
  `partyId` varchar(255) NOT NULL,
  `alternateID` varchar(255) DEFAULT NULL,
  `attempt` varchar(255) DEFAULT NULL,
  `secretKey` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tppCustomerId` varchar(255) DEFAULT NULL,
  `updateAt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`partyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CustomerProfile`
--

LOCK TABLES `CustomerProfile` WRITE;
/*!40000 ALTER TABLE `CustomerProfile` DISABLE KEYS */;
INSERT INTO `CustomerProfile` VALUES ('US0010001-100090',NULL,'1','3rAWzQIhXIvnLBvs_2o9St1Q-ehZrXay9LWG9a3iUbU','Success','185528813193529610','1585655934497');
/*!40000 ALTER TABLE `CustomerProfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'accountaggregation'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-21 15:20:55
