-- MySQL dump 10.13  Distrib 8.0.30, for Linux (x86_64)
--
-- Host: 10.224.102.46    Database: arrangement
-- ------------------------------------------------------
-- Server version	8.0.30-commercial

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AccountArrangement`
--

DROP TABLE IF EXISTS `AccountArrangement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AccountArrangement` (
  `id` varchar(255) NOT NULL,
  `closureDate` datetime DEFAULT NULL,
  `overdraftStatus` varchar(255) DEFAULT NULL,
  `processingDate` datetime DEFAULT NULL,
  `Arrangement_arrangementId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDXtd93lyfmpagwgqcs01npmd4yl` (`Arrangement_arrangementId`),
  CONSTRAINT `FKojh2rulh2toyt769t4b6jaq8v` FOREIGN KEY (`Arrangement_arrangementId`) REFERENCES `Arrangement` (`arrangementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AccountArrangement`
--

LOCK TABLES `AccountArrangement` WRITE;
/*!40000 ALTER TABLE `AccountArrangement` DISABLE KEYS */;
/*!40000 ALTER TABLE `AccountArrangement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AccountArrangementExtension`
--

DROP TABLE IF EXISTS `AccountArrangementExtension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AccountArrangementExtension` (
  `AccountArrangement_id` varchar(255) NOT NULL,
  `value` longtext,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`AccountArrangement_id`,`name`),
  CONSTRAINT `FKjhw8880fipg9lpgibaij66jeo` FOREIGN KEY (`AccountArrangement_id`) REFERENCES `AccountArrangement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AccountArrangementExtension`
--

LOCK TABLES `AccountArrangementExtension` WRITE;
/*!40000 ALTER TABLE `AccountArrangementExtension` DISABLE KEYS */;
/*!40000 ALTER TABLE `AccountArrangementExtension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Arrangement`
--

DROP TABLE IF EXISTS `Arrangement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Arrangement` (
  `arrangementId` varchar(255) NOT NULL,
  `accountCategory` varchar(255) DEFAULT NULL,
  `arrangementStatus` varchar(255) DEFAULT NULL,
  `arrangementStatusDate` datetime DEFAULT NULL,
  `coolingDate` datetime DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `creationDate` datetime DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `dormancyStatus` varchar(255) DEFAULT NULL,
  `eStmtEnabled` bit(1) DEFAULT NULL,
  `extArrangementId` varchar(255) NOT NULL,
  `extSysteminfo` varchar(255) DEFAULT NULL,
  `externalIndicator` varchar(255) DEFAULT NULL,
  `iban` varchar(255) DEFAULT NULL,
  `isPortFolio` bit(1) DEFAULT NULL,
  `isPortFolioAccount` bit(1) DEFAULT NULL,
  `linkedReference` varchar(255) DEFAULT NULL,
  `originalContractDate` datetime DEFAULT NULL,
  `portfolioId` varchar(255) DEFAULT NULL,
  `processingDate` datetime DEFAULT NULL,
  `productGroup` varchar(255) DEFAULT NULL,
  `productLine` varchar(255) DEFAULT NULL,
  `relationshipPlan` varchar(255) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `version` int NOT NULL,
  `companyRef` varchar(255) DEFAULT NULL,
  `officerKey` varchar(255) DEFAULT NULL,
  `productIdentifier` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`arrangementId`),
  UNIQUE KEY `UK_iof5k0pk48dqnjmpes997thee` (`extArrangementId`),
  KEY `IDX4deb1pjhac1201dkwlgtd8r4x` (`linkedReference`),
  KEY `IDXiof5k0pk48dqnjmpes997thee` (`extArrangementId`),
  KEY `IDXcah6ww2op6v8ifcwy9gst8b9q` (`linkedReference`,`version`),
  KEY `IDXnwthaopccrln4jme8lueo7aqe` (`extArrangementId`,`version`),
  KEY `IDXbj6aur20g6syoj8qyrsctigwk` (`version`),
  KEY `IDXp3lsfon46hk8xnc0dlayrhg64` (`arrangementId`),
  KEY `IDXksl88nhkdl3i141qilrjecgf2` (`arrangementId`,`version`),
  KEY `IDX90wacfsptdjkf0m2dciavxu4w` (`officerKey`),
  KEY `IDXajyrc743o5aaxn57rd0s4euq` (`productIdentifier`),
  KEY `IDXkwg8v3x08hoa8bkfww6gqimca` (`companyRef`),
  CONSTRAINT `FK8fidyk2ib1g43crb6509impb4` FOREIGN KEY (`companyRef`) REFERENCES `CompanyDetails` (`companyRef`),
  CONSTRAINT `FKi12l31i8tdusj4up3rio7ewpj` FOREIGN KEY (`productIdentifier`) REFERENCES `ProductDetails` (`productIdentifier`),
  CONSTRAINT `FKjjb1vja0xsb2isrwve3iorff4` FOREIGN KEY (`officerKey`) REFERENCES `OfficerDetails` (`officerKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Arrangement`
--

LOCK TABLES `Arrangement` WRITE;
/*!40000 ALTER TABLE `Arrangement` DISABLE KEYS */;
/*!40000 ALTER TABLE `Arrangement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ArrangementBills`
--

DROP TABLE IF EXISTS `ArrangementBills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ArrangementBills` (
  `arrangementId` varchar(255) NOT NULL,
  `billId` varchar(255) NOT NULL,
  `adjustAmount` decimal(19,2) DEFAULT NULL,
  `advancePayment` decimal(19,2) DEFAULT NULL,
  `agingStatus` varchar(255) DEFAULT NULL,
  `billStatus` varchar(255) DEFAULT NULL,
  `deferDate` datetime DEFAULT NULL,
  `delinOsAmount` decimal(19,2) DEFAULT NULL,
  `infoPayType` varchar(255) DEFAULT NULL,
  `originalTotalAmount` decimal(19,2) DEFAULT NULL,
  `outstandingTotalAmount` decimal(19,2) DEFAULT NULL,
  `paymentDate` datetime DEFAULT NULL,
  `paymentIndicator` varchar(255) DEFAULT NULL,
  `repayAmount` decimal(19,2) DEFAULT NULL,
  `settleStatus` varchar(255) DEFAULT NULL,
  `Arrangement_arrangementId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`arrangementId`,`billId`),
  KEY `FK6f0a5psuvc0g3fhctwihw3w5k` (`Arrangement_arrangementId`),
  CONSTRAINT `FK6f0a5psuvc0g3fhctwihw3w5k` FOREIGN KEY (`Arrangement_arrangementId`) REFERENCES `Arrangement` (`arrangementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ArrangementBills`
--

LOCK TABLES `ArrangementBills` WRITE;
/*!40000 ALTER TABLE `ArrangementBills` DISABLE KEYS */;
/*!40000 ALTER TABLE `ArrangementBills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ArrangementBills_billType`
--

DROP TABLE IF EXISTS `ArrangementBills_billType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ArrangementBills_billType` (
  `ArrangementBills_arrangementId` varchar(255) NOT NULL,
  `ArrangementBills_billId` varchar(255) NOT NULL,
  `billDate` datetime DEFAULT NULL,
  `billType` varchar(255) DEFAULT NULL,
  `paymentMethod` varchar(255) DEFAULT NULL,
  `paymentType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ArrangementBills_arrangementId`,`ArrangementBills_billId`),
  KEY `IDX7mt9qc1uanhd08gwcrwj7eqov` (`ArrangementBills_arrangementId`),
  KEY `IDX43mci39ti2jy6fknheb91jwap` (`ArrangementBills_billId`),
  CONSTRAINT `FKmj7eob5wxkebhlmngnng0nk1n` FOREIGN KEY (`ArrangementBills_arrangementId`, `ArrangementBills_billId`) REFERENCES `ArrangementBills` (`arrangementId`, `billId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ArrangementBills_billType`
--

LOCK TABLES `ArrangementBills_billType` WRITE;
/*!40000 ALTER TABLE `ArrangementBills_billType` DISABLE KEYS */;
/*!40000 ALTER TABLE `ArrangementBills_billType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ArrangementBills_property`
--

DROP TABLE IF EXISTS `ArrangementBills_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ArrangementBills_property` (
  `ArrangementBills_arrangementId` varchar(255) NOT NULL,
  `ArrangementBills_billId` varchar(255) NOT NULL,
  `property` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ArrangementBills_arrangementId`,`ArrangementBills_billId`),
  KEY `IDXq9p1c573u87t0al839rpu9i1o` (`ArrangementBills_arrangementId`),
  KEY `IDXiqjev7wtwagfjulqtrftap8yc` (`ArrangementBills_billId`),
  CONSTRAINT `FKjm4h0unmxtcx7vfquw1wqb837` FOREIGN KEY (`ArrangementBills_arrangementId`, `ArrangementBills_billId`) REFERENCES `ArrangementBills` (`arrangementId`, `billId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ArrangementBills_property`
--

LOCK TABLES `ArrangementBills_property` WRITE;
/*!40000 ALTER TABLE `ArrangementBills_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `ArrangementBills_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ArrangementEventProcessed`
--

DROP TABLE IF EXISTS `ArrangementEventProcessed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ArrangementEventProcessed` (
  `arrangementId` varchar(255) NOT NULL,
  `eventName` varchar(255) NOT NULL,
  `processingTime` datetime DEFAULT NULL,
  PRIMARY KEY (`arrangementId`,`eventName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ArrangementEventProcessed`
--

LOCK TABLES `ArrangementEventProcessed` WRITE;
/*!40000 ALTER TABLE `ArrangementEventProcessed` DISABLE KEYS */;
/*!40000 ALTER TABLE `ArrangementEventProcessed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ArrangementExtension`
--

DROP TABLE IF EXISTS `ArrangementExtension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ArrangementExtension` (
  `Arrangement_arrangementId` varchar(255) NOT NULL,
  `value` longtext,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`Arrangement_arrangementId`,`name`),
  KEY `IDXq99ilgws8h659bf1rb5j5y3s6` (`Arrangement_arrangementId`),
  CONSTRAINT `FK6th1uit3ajd5n3rvrixl21cxo` FOREIGN KEY (`Arrangement_arrangementId`) REFERENCES `Arrangement` (`arrangementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ArrangementExtension`
--

LOCK TABLES `ArrangementExtension` WRITE;
/*!40000 ALTER TABLE `ArrangementExtension` DISABLE KEYS */;
/*!40000 ALTER TABLE `ArrangementExtension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ArrangementInterest`
--

DROP TABLE IF EXISTS `ArrangementInterest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ArrangementInterest` (
  `id` varchar(255) NOT NULL,
  `interestProperty` varchar(255) NOT NULL,
  `dividentPaidYtd` decimal(19,2) DEFAULT NULL,
  `effectiveRate` decimal(19,2) DEFAULT NULL,
  `fixedRate` decimal(19,2) DEFAULT NULL,
  `floatingIndex` decimal(19,2) DEFAULT NULL,
  `intRateTierType` varchar(255) DEFAULT NULL,
  `interestAccrued` decimal(19,2) DEFAULT NULL,
  `lastPaidInterest` decimal(19,2) DEFAULT NULL,
  `linkedRate` decimal(19,2) DEFAULT NULL,
  `paymentFrequency` varchar(255) DEFAULT NULL,
  `periodEndingDate` datetime DEFAULT NULL,
  `periodicIndex` decimal(19,2) DEFAULT NULL,
  `Arrangement_arrangementId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`interestProperty`),
  KEY `FKmsw190cjj73rjg29is9ih0n3s` (`Arrangement_arrangementId`),
  CONSTRAINT `FKmsw190cjj73rjg29is9ih0n3s` FOREIGN KEY (`Arrangement_arrangementId`) REFERENCES `Arrangement` (`arrangementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ArrangementInterest`
--

LOCK TABLES `ArrangementInterest` WRITE;
/*!40000 ALTER TABLE `ArrangementInterest` DISABLE KEYS */;
/*!40000 ALTER TABLE `ArrangementInterest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Arrangement_postingRef`
--

DROP TABLE IF EXISTS `Arrangement_postingRef`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Arrangement_postingRef` (
  `Arrangement_arrangementId` varchar(255) NOT NULL,
  `postingRestrict_postingRef` varchar(255) DEFAULT NULL,
  `postingRestrictStartDate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Arrangement_arrangementId`),
  KEY `IDXeq0tv5tcm97cxqa58ujxivk07` (`Arrangement_arrangementId`),
  KEY `IDXil8hs7mifqv0sxwphdkhmvfwu` (`postingRestrict_postingRef`),
  CONSTRAINT `FK9byxkybmrq2e9r77p87bv0rwv` FOREIGN KEY (`postingRestrict_postingRef`) REFERENCES `PostingRestrict` (`postingRef`),
  CONSTRAINT `FKgcuul9k1k3lqyl1ffv4vqnnmx` FOREIGN KEY (`Arrangement_arrangementId`) REFERENCES `Arrangement` (`arrangementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Arrangement_postingRef`
--

LOCK TABLES `Arrangement_postingRef` WRITE;
/*!40000 ALTER TABLE `Arrangement_postingRef` DISABLE KEYS */;
/*!40000 ALTER TABLE `Arrangement_postingRef` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Arrangement_shortTitle`
--

DROP TABLE IF EXISTS `Arrangement_shortTitle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Arrangement_shortTitle` (
  `Arrangement_arrangementId` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `mnemonic` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Arrangement_arrangementId`),
  KEY `IDXnj9a6k7k1swjr0i34b4q459ni` (`Arrangement_arrangementId`),
  CONSTRAINT `FKijh8hrx4xy8102t3n75khycs4` FOREIGN KEY (`Arrangement_arrangementId`) REFERENCES `Arrangement` (`arrangementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Arrangement_shortTitle`
--

LOCK TABLES `Arrangement_shortTitle` WRITE;
/*!40000 ALTER TABLE `Arrangement_shortTitle` DISABLE KEYS */;
/*!40000 ALTER TABLE `Arrangement_shortTitle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CompanyDetails`
--

DROP TABLE IF EXISTS `CompanyDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CompanyDetails` (
  `companyRef` varchar(200) NOT NULL,
  `customerCompanyRef` varchar(500) DEFAULT NULL,
  `leadCompanyRef` varchar(500) DEFAULT NULL,
  `defaultFinancialCompany` varchar(255) DEFAULT NULL,
  `defaultLanguageCode` varchar(255) DEFAULT NULL,
  `processingDate` datetime DEFAULT NULL,
  `version` int NOT NULL,
  PRIMARY KEY (`companyRef`),
  KEY `IDXi8ynwib2l2bcn3vc0l4u34ak0` (`version`),
  KEY `IDXcj7ue9s40xvrlxp6dqn6hxnfw` (`companyRef`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CompanyDetails`
--

LOCK TABLES `CompanyDetails` WRITE;
/*!40000 ALTER TABLE `CompanyDetails` DISABLE KEYS */;
INSERT INTO `CompanyDetails` VALUES ('GB0010001','GB0010001','GB0010001',NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `CompanyDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CompanyDetailsExtension`
--

DROP TABLE IF EXISTS `CompanyDetailsExtension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CompanyDetailsExtension` (
  `CompanyDetails_companyRef` varchar(255) NOT NULL,
  `value` varchar(4000) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`CompanyDetails_companyRef`,`name`),
  CONSTRAINT `FK9eht5tt8g6tdereq2ionta3aj` FOREIGN KEY (`CompanyDetails_companyRef`) REFERENCES `CompanyDetails` (`companyRef`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CompanyDetailsExtension`
--

LOCK TABLES `CompanyDetailsExtension` WRITE;
/*!40000 ALTER TABLE `CompanyDetailsExtension` DISABLE KEYS */;
/*!40000 ALTER TABLE `CompanyDetailsExtension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CompanyDetails_description`
--

DROP TABLE IF EXISTS `CompanyDetails_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CompanyDetails_description` (
  `CompanyDetails_companyRef` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `mnemonic` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CompanyDetails_companyRef`),
  KEY `IDX835ltnq1hgtfmun0mwaamo0rk` (`CompanyDetails_companyRef`),
  CONSTRAINT `FK3qaiumkxd3bpmosidmljomnt8` FOREIGN KEY (`CompanyDetails_companyRef`) REFERENCES `CompanyDetails` (`companyRef`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CompanyDetails_description`
--

LOCK TABLES `CompanyDetails_description` WRITE;
/*!40000 ALTER TABLE `CompanyDetails_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `CompanyDetails_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DepositArrangement`
--

DROP TABLE IF EXISTS `DepositArrangement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DepositArrangement` (
  `id` varchar(255) NOT NULL,
  `maturityDate` datetime DEFAULT NULL,
  `processingDate` datetime DEFAULT NULL,
  `renewalDate` datetime DEFAULT NULL,
  `Arrangement_arrangementId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDXqh8nl2lgguos5ru82tjdy05ft` (`Arrangement_arrangementId`),
  CONSTRAINT `FKf9a4m9kc2imvuvbme7kgv7s02` FOREIGN KEY (`Arrangement_arrangementId`) REFERENCES `Arrangement` (`arrangementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DepositArrangement`
--

LOCK TABLES `DepositArrangement` WRITE;
/*!40000 ALTER TABLE `DepositArrangement` DISABLE KEYS */;
/*!40000 ALTER TABLE `DepositArrangement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DepositArrangementExtension`
--

DROP TABLE IF EXISTS `DepositArrangementExtension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DepositArrangementExtension` (
  `DepositArrangement_id` varchar(255) NOT NULL,
  `value` longtext,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`DepositArrangement_id`,`name`),
  CONSTRAINT `FKgbmv3efflbb3erjeln664jjj8` FOREIGN KEY (`DepositArrangement_id`) REFERENCES `DepositArrangement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DepositArrangementExtension`
--

LOCK TABLES `DepositArrangementExtension` WRITE;
/*!40000 ALTER TABLE `DepositArrangementExtension` DISABLE KEYS */;
/*!40000 ALTER TABLE `DepositArrangementExtension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LendingArrangement`
--

DROP TABLE IF EXISTS `LendingArrangement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `LendingArrangement` (
  `id` varchar(255) NOT NULL,
  `maturityDate` datetime DEFAULT NULL,
  `overdueStatus` varchar(255) DEFAULT NULL,
  `processingDate` datetime DEFAULT NULL,
  `renewalDate` datetime DEFAULT NULL,
  `sanctionedDate` datetime DEFAULT NULL,
  `termAmount` decimal(19,2) DEFAULT NULL,
  `Arrangement_arrangementId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX4xtm4byk7cwxhfne9lh501wp6` (`Arrangement_arrangementId`),
  CONSTRAINT `FKd86ff28qjraj7xkhwrb8rilat` FOREIGN KEY (`Arrangement_arrangementId`) REFERENCES `Arrangement` (`arrangementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LendingArrangement`
--

LOCK TABLES `LendingArrangement` WRITE;
/*!40000 ALTER TABLE `LendingArrangement` DISABLE KEYS */;
/*!40000 ALTER TABLE `LendingArrangement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LendingArrangementExtension`
--

DROP TABLE IF EXISTS `LendingArrangementExtension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `LendingArrangementExtension` (
  `LendingArrangement_id` varchar(255) NOT NULL,
  `value` longtext,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`LendingArrangement_id`,`name`),
  CONSTRAINT `FKix3lxono14ipn2twqs7b1pl4j` FOREIGN KEY (`LendingArrangement_id`) REFERENCES `LendingArrangement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LendingArrangementExtension`
--

LOCK TABLES `LendingArrangementExtension` WRITE;
/*!40000 ALTER TABLE `LendingArrangementExtension` DISABLE KEYS */;
/*!40000 ALTER TABLE `LendingArrangementExtension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MsAltKey`
--

DROP TABLE IF EXISTS `MsAltKey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `MsAltKey` (
  `alternateKey` varchar(255) NOT NULL,
  `alternateName` varchar(255) NOT NULL,
  `entityId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`alternateKey`,`alternateName`),
  KEY `index_MsAltKey_entityId` (`entityId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MsAltKey`
--

LOCK TABLES `MsAltKey` WRITE;
/*!40000 ALTER TABLE `MsAltKey` DISABLE KEYS */;
/*!40000 ALTER TABLE `MsAltKey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OfficerDetails`
--

DROP TABLE IF EXISTS `OfficerDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `OfficerDetails` (
  `officerKey` varchar(200) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `processingDate` datetime DEFAULT NULL,
  `version` int NOT NULL,
  PRIMARY KEY (`officerKey`),
  KEY `IDXdi40rrisjg1fta0h2irqkq32` (`version`),
  KEY `IDXkl2u041alxuvei9vuffxt8n0w` (`officerKey`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OfficerDetails`
--

LOCK TABLES `OfficerDetails` WRITE;
/*!40000 ALTER TABLE `OfficerDetails` DISABLE KEYS */;
INSERT INTO `OfficerDetails` VALUES ('1',NULL,NULL,0),('26',NULL,NULL,0);
/*!40000 ALTER TABLE `OfficerDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PartyArrangement`
--

DROP TABLE IF EXISTS `PartyArrangement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PartyArrangement` (
  `recId` varchar(255) NOT NULL,
  `partyRole` varchar(255) DEFAULT NULL,
  `processingDate` datetime DEFAULT NULL,
  `arrangementId` varchar(255) DEFAULT NULL,
  `partyId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`recId`),
  KEY `IDXrslcipehoott5w8xr11go60jb` (`partyId`),
  KEY `IDX9nav9hct5rfjqyeqiokfaxsmy` (`arrangementId`),
  CONSTRAINT `FK482gofexru5u3vlygkx0qmqm1` FOREIGN KEY (`arrangementId`) REFERENCES `Arrangement` (`arrangementId`),
  CONSTRAINT `FK7itiugt88lp8xyc4jpb1d0bgv` FOREIGN KEY (`partyId`) REFERENCES `PartyDetails` (`partyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PartyArrangement`
--

LOCK TABLES `PartyArrangement` WRITE;
/*!40000 ALTER TABLE `PartyArrangement` DISABLE KEYS */;
/*!40000 ALTER TABLE `PartyArrangement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PartyDetails`
--

DROP TABLE IF EXISTS `PartyDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PartyDetails` (
  `partyId` varchar(255) NOT NULL,
  `processingDate` datetime DEFAULT NULL,
  `version` int NOT NULL,
  PRIMARY KEY (`partyId`),
  KEY `IDXig5b8b82mofyqkrefdtl0nam1` (`version`),
  KEY `IDXsfso8fjvxupp7tx82h2hmx11h` (`partyId`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PartyDetails`
--

LOCK TABLES `PartyDetails` WRITE;
/*!40000 ALTER TABLE `PartyDetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `PartyDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PartyDetailsExtension`
--

DROP TABLE IF EXISTS `PartyDetailsExtension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PartyDetailsExtension` (
  `PartyDetails_partyId` varchar(255) NOT NULL,
  `value` varchar(4000) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`PartyDetails_partyId`,`name`),
  CONSTRAINT `FKd5224rvagrexgygqm2fpurexo` FOREIGN KEY (`PartyDetails_partyId`) REFERENCES `PartyDetails` (`partyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PartyDetailsExtension`
--

LOCK TABLES `PartyDetailsExtension` WRITE;
/*!40000 ALTER TABLE `PartyDetailsExtension` DISABLE KEYS */;
/*!40000 ALTER TABLE `PartyDetailsExtension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PartyDetails_PostingRestrict`
--

DROP TABLE IF EXISTS `PartyDetails_PostingRestrict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PartyDetails_PostingRestrict` (
  `PartyDetails_partyId` varchar(255) NOT NULL,
  `postingRestrict_postingRef` varchar(255) NOT NULL,
  PRIMARY KEY (`PartyDetails_partyId`,`postingRestrict_postingRef`),
  KEY `IDXsnj099y6hus6umvsg1uyh1yd3` (`postingRestrict_postingRef`),
  CONSTRAINT `FKg0m9n1rn4cx1xj8o4k36f7la8` FOREIGN KEY (`postingRestrict_postingRef`) REFERENCES `PostingRestrict` (`postingRef`),
  CONSTRAINT `FKj78vb2xeqorcjojulavo81qpu` FOREIGN KEY (`PartyDetails_partyId`) REFERENCES `PartyDetails` (`partyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PartyDetails_PostingRestrict`
--

LOCK TABLES `PartyDetails_PostingRestrict` WRITE;
/*!40000 ALTER TABLE `PartyDetails_PostingRestrict` DISABLE KEYS */;
/*!40000 ALTER TABLE `PartyDetails_PostingRestrict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PartyRoles`
--

DROP TABLE IF EXISTS `PartyRoles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PartyRoles` (
  `roleId` varchar(255) NOT NULL,
  `isBeneficialOwner` bit(1) DEFAULT NULL,
  `maxTaxLiabPercentage` decimal(19,2) DEFAULT NULL,
  `processingDate` datetime DEFAULT NULL,
  `version` int NOT NULL,
  PRIMARY KEY (`roleId`),
  KEY `IDX11xe0wpwpy11f70v778bgerx9` (`version`),
  KEY `IDXfk8cykw9bew38jtq0bgxi1wdp` (`roleId`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PartyRoles`
--

LOCK TABLES `PartyRoles` WRITE;
/*!40000 ALTER TABLE `PartyRoles` DISABLE KEYS */;
/*!40000 ALTER TABLE `PartyRoles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PartyRoles_description`
--

DROP TABLE IF EXISTS `PartyRoles_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PartyRoles_description` (
  `Role_Id` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `mnemonic` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Role_Id`),
  KEY `IDXc5x4iu0rwenaroga99ldburl5` (`Role_Id`),
  CONSTRAINT `FKafx92dd50rw2hbgcstds7xj2g` FOREIGN KEY (`Role_Id`) REFERENCES `PartyRoles` (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PartyRoles_description`
--

LOCK TABLES `PartyRoles_description` WRITE;
/*!40000 ALTER TABLE `PartyRoles_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `PartyRoles_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PaymentSchedules`
--

DROP TABLE IF EXISTS `PaymentSchedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PaymentSchedules` (
  `id` varchar(255) NOT NULL,
  `property` varchar(255) NOT NULL,
  `scheduleDate` datetime NOT NULL,
  `scheduleType` varchar(255) NOT NULL,
  `cashflowType` varchar(255) DEFAULT NULL,
  `outstandingBalance` decimal(19,2) DEFAULT NULL,
  `scheduleAmount` decimal(19,2) DEFAULT NULL,
  `scheduleCcyAmount` decimal(19,2) DEFAULT NULL,
  `scheduleCurrency` varchar(255) DEFAULT NULL,
  `settledDate` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `Arrangement_arrangementId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`property`,`scheduleDate`,`scheduleType`),
  KEY `FKl4tlq1ko3qvqs6vy58x8cgcq4` (`Arrangement_arrangementId`),
  CONSTRAINT `FKl4tlq1ko3qvqs6vy58x8cgcq4` FOREIGN KEY (`Arrangement_arrangementId`) REFERENCES `Arrangement` (`arrangementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PaymentSchedules`
--

LOCK TABLES `PaymentSchedules` WRITE;
/*!40000 ALTER TABLE `PaymentSchedules` DISABLE KEYS */;
/*!40000 ALTER TABLE `PaymentSchedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PortfolioArrangement`
--

DROP TABLE IF EXISTS `PortfolioArrangement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PortfolioArrangement` (
  `id` varchar(255) NOT NULL,
  `investmentStrategy` varchar(255) DEFAULT NULL,
  `marginLending` bit(1) DEFAULT NULL,
  `processingDate` datetime DEFAULT NULL,
  `recommendedInvestStrategy` varchar(255) DEFAULT NULL,
  `riskProfileExpiryDate` datetime DEFAULT NULL,
  `riskProfileValidityFlag` bit(1) DEFAULT NULL,
  `serviceType` varchar(255) DEFAULT NULL,
  `Arrangement_arrangementId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKb6xdyp7rvcbu7eogsfmxds8mk` (`Arrangement_arrangementId`),
  CONSTRAINT `FKb6xdyp7rvcbu7eogsfmxds8mk` FOREIGN KEY (`Arrangement_arrangementId`) REFERENCES `Arrangement` (`arrangementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PortfolioArrangement`
--

LOCK TABLES `PortfolioArrangement` WRITE;
/*!40000 ALTER TABLE `PortfolioArrangement` DISABLE KEYS */;
/*!40000 ALTER TABLE `PortfolioArrangement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PortfolioArrangementExtension`
--

DROP TABLE IF EXISTS `PortfolioArrangementExtension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PortfolioArrangementExtension` (
  `PortfolioArrangement_id` varchar(255) NOT NULL,
  `value` longtext,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`PortfolioArrangement_id`,`name`),
  CONSTRAINT `FK924lecyw8a8cernv1c4qmc0fv` FOREIGN KEY (`PortfolioArrangement_id`) REFERENCES `PortfolioArrangement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PortfolioArrangementExtension`
--

LOCK TABLES `PortfolioArrangementExtension` WRITE;
/*!40000 ALTER TABLE `PortfolioArrangementExtension` DISABLE KEYS */;
/*!40000 ALTER TABLE `PortfolioArrangementExtension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PostingRestrict`
--

DROP TABLE IF EXISTS `PostingRestrict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PostingRestrict` (
  `postingRef` varchar(200) NOT NULL,
  `restrictionType` varchar(500) DEFAULT NULL,
  `processingDate` datetime DEFAULT NULL,
  `version` int NOT NULL,
  PRIMARY KEY (`postingRef`),
  KEY `IDXau0sv8td1hnhs9tylk2hf3xlm` (`version`),
  KEY `IDXjxrqw9tc803awpow6fyo1dbfy` (`postingRef`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PostingRestrict`
--

LOCK TABLES `PostingRestrict` WRITE;
/*!40000 ALTER TABLE `PostingRestrict` DISABLE KEYS */;
INSERT INTO `PostingRestrict` VALUES ('1','Post No Debits',NULL,0);
/*!40000 ALTER TABLE `PostingRestrict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PostingRestrict_description`
--

DROP TABLE IF EXISTS `PostingRestrict_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PostingRestrict_description` (
  `PostingRestrict_postingRef` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `mnemonic` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PostingRestrict_postingRef`),
  KEY `IDXq9slp2cruei4dwq13p9mvc9tb` (`PostingRestrict_postingRef`),
  CONSTRAINT `FKa8vwwk38m0a38p4dwgvbpqems` FOREIGN KEY (`PostingRestrict_postingRef`) REFERENCES `PostingRestrict` (`postingRef`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PostingRestrict_description`
--

LOCK TABLES `PostingRestrict_description` WRITE;
/*!40000 ALTER TABLE `PostingRestrict_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `PostingRestrict_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProductDetails`
--

DROP TABLE IF EXISTS `ProductDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ProductDetails` (
  `productIdentifier` varchar(200) NOT NULL,
  `processingDate` datetime DEFAULT NULL,
  `version` int NOT NULL,
  PRIMARY KEY (`productIdentifier`),
  KEY `IDXo24r1dnk4biigtcxb86y53lo7` (`version`),
  KEY `IDX4yw32a5415a1y4cd7d48bme85` (`productIdentifier`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProductDetails`
--

LOCK TABLES `ProductDetails` WRITE;
/*!40000 ALTER TABLE `ProductDetails` DISABLE KEYS */;
INSERT INTO `ProductDetails` VALUES ('CURRENT.ACCOUNT',NULL,0),('DEPOSIT.NEGOTIABLE',NULL,0),('NEGOTIABLE.ACCOUNT',NULL,0),('NEGOTIABLE.LOAN',NULL,0),('SAVINGS.ACCOUNT',NULL,0);
/*!40000 ALTER TABLE `ProductDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProductDetails_description`
--

DROP TABLE IF EXISTS `ProductDetails_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ProductDetails_description` (
  `Product_Id` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `mnemonic` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Product_Id`),
  KEY `IDX89t8rs2s51cki5q1re4ipy03n` (`Product_Id`),
  CONSTRAINT `FK99jvwrx5xgdbbkbkmxstbnaah` FOREIGN KEY (`Product_Id`) REFERENCES `ProductDetails` (`productIdentifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProductDetails_description`
--

LOCK TABLES `ProductDetails_description` WRITE;
/*!40000 ALTER TABLE `ProductDetails_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `ProductDetails_description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ScheduleDetails`
--

DROP TABLE IF EXISTS `ScheduleDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ScheduleDetails` (
  `id` varchar(255) NOT NULL,
  `paymentFrequency` varchar(255) NOT NULL,
  `paymentType` varchar(255) NOT NULL,
  `paymentMethod` varchar(255) DEFAULT NULL,
  `Arrangement_arrangementId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`paymentFrequency`,`paymentType`),
  KEY `FKtd6je71q4fhav3nlsf8kkx90` (`Arrangement_arrangementId`),
  CONSTRAINT `FKtd6je71q4fhav3nlsf8kkx90` FOREIGN KEY (`Arrangement_arrangementId`) REFERENCES `Arrangement` (`arrangementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ScheduleDetails`
--

LOCK TABLES `ScheduleDetails` WRITE;
/*!40000 ALTER TABLE `ScheduleDetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `ScheduleDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ScheduleDetails_property`
--

DROP TABLE IF EXISTS `ScheduleDetails_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ScheduleDetails_property` (
  `ScheduleDetails_id` varchar(255) NOT NULL,
  `ScheduleDetails_paymentFrequency` varchar(255) NOT NULL,
  `ScheduleDetails_paymentType` varchar(255) NOT NULL,
  `property` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ScheduleDetails_id`,`ScheduleDetails_paymentFrequency`,`ScheduleDetails_paymentType`),
  KEY `IDXbnxondhu8qrghqctyr52mtdq1` (`ScheduleDetails_id`),
  KEY `IDXc6hsmj3ebo0q7x46s26oh2dwr` (`ScheduleDetails_paymentType`),
  KEY `IDXn0ln5nq8w4543bamqlr569wh2` (`ScheduleDetails_paymentFrequency`),
  CONSTRAINT `FK2xde676p27fxpk3dx4viyv33k` FOREIGN KEY (`ScheduleDetails_id`, `ScheduleDetails_paymentFrequency`, `ScheduleDetails_paymentType`) REFERENCES `ScheduleDetails` (`id`, `paymentFrequency`, `paymentType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ScheduleDetails_property`
--

LOCK TABLES `ScheduleDetails_property` WRITE;
/*!40000 ALTER TABLE `ScheduleDetails_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `ScheduleDetails_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_error`
--

DROP TABLE IF EXISTS `ms_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_error` (
  `errorEventId` varchar(255) NOT NULL,
  `dataEventId` varchar(255) DEFAULT NULL,
  `errorMessage` varchar(1024) DEFAULT NULL,
  `errorSourceTopic` varchar(255) DEFAULT NULL,
  `lastProcessedTime` datetime DEFAULT NULL,
  `payload` longblob,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`errorEventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_error`
--

LOCK TABLES `ms_error` WRITE;
/*!40000 ALTER TABLE `ms_error` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_inbox_events`
--

DROP TABLE IF EXISTS `ms_inbox_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_inbox_events` (
  `eventId` varchar(255) NOT NULL,
  `businessKey` varchar(255) DEFAULT NULL,
  `commandType` varchar(255) DEFAULT NULL,
  `creationTime` datetime DEFAULT NULL,
  `eventDetails` longtext,
  `eventSourceId` varchar(255) DEFAULT NULL,
  `eventType` varchar(255) DEFAULT NULL,
  `organizationId` varchar(255) DEFAULT NULL,
  `payload` longtext,
  `priority` int DEFAULT NULL,
  `processedTime` datetime DEFAULT NULL,
  `sequenceNo` bigint DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tenantId` varchar(255) DEFAULT NULL,
  `userId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`eventId`),
  KEY `inboxBusinessKeyIndex` (`businessKey`),
  KEY `inboxSequenceNoIndex` (`sequenceNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_inbox_events`
--

LOCK TABLES `ms_inbox_events` WRITE;
/*!40000 ALTER TABLE `ms_inbox_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_inbox_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_outbox_events`
--

DROP TABLE IF EXISTS `ms_outbox_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_outbox_events` (
  `eventId` varchar(255) NOT NULL,
  `businessKey` varchar(255) DEFAULT NULL,
  `correlationId` varchar(255) DEFAULT NULL,
  `creationTime` datetime DEFAULT NULL,
  `eventDetails` varchar(255) DEFAULT NULL,
  `eventType` varchar(255) DEFAULT NULL,
  `eventdate` datetime DEFAULT NULL,
  `organizationId` varchar(255) DEFAULT NULL,
  `payload` longtext,
  `priority` int DEFAULT NULL,
  `processedTime` datetime DEFAULT NULL,
  `sequenceNo` bigint DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tenantId` varchar(255) DEFAULT NULL,
  `userId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`eventId`),
  KEY `outboxBusinessKeyIndex` (`businessKey`),
  KEY `outboxSequenceNoIndex` (`sequenceNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_outbox_events`
--

LOCK TABLES `ms_outbox_events` WRITE;
/*!40000 ALTER TABLE `ms_outbox_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_outbox_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_reference_data`
--

DROP TABLE IF EXISTS `ms_reference_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_reference_data` (
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `softDeleteFlag` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`type`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_reference_data`
--

LOCK TABLES `ms_reference_data` WRITE;
/*!40000 ALTER TABLE `ms_reference_data` DISABLE KEYS */;
INSERT INTO `ms_reference_data` VALUES ('LANGUAGE','1','GB',0),('LANGUAGE','2','FR',0),('LANGUAGE','3','DE',0),('LANGUAGE','4','ES',0);
/*!40000 ALTER TABLE `ms_reference_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_history`
--

DROP TABLE IF EXISTS `schema_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schema_history` (
  `uniqueReference` varchar(255) NOT NULL,
  `createdOn` varchar(255) DEFAULT NULL,
  `results` varchar(255) DEFAULT NULL,
  `script` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uniqueReference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_history`
--

LOCK TABLES `schema_history` WRITE;
/*!40000 ALTER TABLE `schema_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `schema_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'arrangement'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-21 15:27:07
