-- MySQL dump 10.13  Distrib 8.0.30, for Linux (x86_64)
--
-- Host: 10.224.102.46    Database: dbxlogs
-- ------------------------------------------------------
-- Server version	8.0.30-commercial

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adminactivity`
--

DROP TABLE IF EXISTS `adminactivity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `adminactivity` (
  `id` varchar(50) NOT NULL,
  `event` varchar(50) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `userRole` varchar(500) DEFAULT NULL,
  `moduleName` varchar(50) DEFAULT NULL,
  `eventData` text,
  `eventts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(50) DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_systemuseractivity_eventts` (`eventts`),
  KEY `idx_systemuseractivity_modulename` (`moduleName`),
  KEY `idx_systemuseractivity_username` (`username`),
  KEY `idx_systemuseractivity_userrole` (`userRole`),
  KEY `idx_systemuseractivity_event` (`event`),
  KEY `idx_systemuseractivity_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adminactivity`
--

LOCK TABLES `adminactivity` WRITE;
/*!40000 ALTER TABLE `adminactivity` DISABLE KEYS */;
/*!40000 ALTER TABLE `adminactivity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admincustomeractivity`
--

DROP TABLE IF EXISTS `admincustomeractivity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admincustomeractivity` (
  `id` varchar(50) NOT NULL,
  `customerId` varchar(50) DEFAULT NULL,
  `adminName` varchar(255) DEFAULT NULL,
  `adminRole` varchar(50) DEFAULT NULL,
  `activityType` varchar(50) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `eventts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(50) DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_admincustomeractivity_adminname` (`adminName`),
  KEY `idx_admincustomeractivity_activitytype` (`activityType`),
  KEY `idx_admincustomeractivity_eventts` (`eventts`),
  KEY `idx_admincustomeractivity_adminrole` (`adminRole`),
  KEY `idx_admincustomeractivity_customerusername` (`customerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admincustomeractivity`
--

LOCK TABLES `admincustomeractivity` WRITE;
/*!40000 ALTER TABLE `admincustomeractivity` DISABLE KEYS */;
/*!40000 ALTER TABLE `admincustomeractivity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archivedauditactivity`
--

DROP TABLE IF EXISTS `archivedauditactivity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `archivedauditactivity` (
  `Id` varchar(255) NOT NULL,
  `EventId` varchar(45) DEFAULT NULL,
  `EventType` varchar(255) DEFAULT NULL,
  `EventSubType` varchar(255) DEFAULT NULL,
  `Status_Id` varchar(255) DEFAULT NULL,
  `sessionId` varchar(255) DEFAULT NULL,
  `AppId` varchar(255) DEFAULT NULL,
  `UserName` varchar(255) DEFAULT NULL,
  `Customer_Id` varchar(255) DEFAULT NULL,
  `partyid` varchar(255) DEFAULT NULL,
  `corecustomerid` varchar(255) DEFAULT NULL,
  `isCSRAssist` tinyint(1) NOT NULL DEFAULT '0',
  `appSessionId` varchar(255) DEFAULT NULL,
  `payeeNickName` varchar(255) DEFAULT NULL,
  `relationshipNumber` varchar(255) DEFAULT NULL,
  `AdminUserName` varchar(255) DEFAULT NULL,
  `AdminUserRole` varchar(255) DEFAULT NULL,
  `Producer` varchar(255) DEFAULT NULL,
  `MoneyMovementRefId` varchar(255) DEFAULT NULL,
  `EventData` json DEFAULT NULL,
  `creditcardnumber` varchar(255) DEFAULT NULL,
  `mfa_State` varchar(255) DEFAULT NULL,
  `mfa_ServiceKey` varchar(255) DEFAULT NULL,
  `mfa_Type` varchar(255) DEFAULT NULL,
  `nonSearchable` json DEFAULT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `deviceModel` varchar(255) DEFAULT NULL,
  `operatingSystem` varchar(255) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `deviceId` varchar(255) DEFAULT NULL,
  `channel` varchar(255) DEFAULT NULL,
  `appVersion` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `ipAddress` varchar(255) DEFAULT NULL,
  `eventts` timestamp NULL DEFAULT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `createdts` timestamp NULL DEFAULT NULL,
  `softdeleteflag` tinyint DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IDX_auditactivity_eventtype` (`EventType`),
  KEY `IDX_auditactivity_eventsubtype` (`EventSubType`),
  KEY `IDX_auditactivity_UserName` (`UserName`),
  KEY `IDX_auditactivity_CustomerId` (`Customer_Id`),
  KEY `IDX_auditactivity_moneymovementid` (`MoneyMovementRefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archivedauditactivity`
--

LOCK TABLES `archivedauditactivity` WRITE;
/*!40000 ALTER TABLE `archivedauditactivity` DISABLE KEYS */;
/*!40000 ALTER TABLE `archivedauditactivity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archivedmoneymovementlog`
--

DROP TABLE IF EXISTS `archivedmoneymovementlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `archivedmoneymovementlog` (
  `Id` varchar(50) NOT NULL,
  `isScheduled` bit(1) NOT NULL DEFAULT b'0',
  `Customer_id` varchar(50) DEFAULT NULL,
  `ExpenseCategory_id` int DEFAULT NULL,
  `Payee_id` int DEFAULT NULL,
  `Bill_id` int DEFAULT NULL,
  `Type_id` int DEFAULT NULL,
  `Reference_id` varchar(50) DEFAULT NULL,
  `fromAccountNumber` varchar(50) DEFAULT NULL,
  `fromAccountBalance` decimal(10,2) DEFAULT NULL,
  `toAccountNumber` varchar(50) DEFAULT NULL,
  `toAccountBalance` decimal(10,2) DEFAULT '0.00',
  `amount` decimal(20,2) DEFAULT '0.00',
  `convertedAmount` decimal(20,2) DEFAULT NULL,
  `transactionCurrency` varchar(45) DEFAULT NULL,
  `baseCurrency` varchar(45) DEFAULT NULL,
  `Status_id` varchar(50) DEFAULT NULL,
  `statusDesc` varchar(50) DEFAULT NULL,
  `notes` varchar(150) DEFAULT '',
  `checkNumber` int DEFAULT '0',
  `imageURL1` text,
  `imageURL2` text,
  `hasDepositImage` tinyint(1) DEFAULT '0',
  `description` varchar(100) DEFAULT ' ',
  `scheduledDate` timestamp NULL DEFAULT NULL,
  `transactionDate` timestamp NULL DEFAULT NULL,
  `createdDate` timestamp NULL DEFAULT NULL,
  `transactionComments` varchar(100) DEFAULT NULL,
  `toExternalAccountNumber` varchar(45) DEFAULT NULL,
  `Person_Id` int DEFAULT NULL,
  `frequencyType` enum('Once','Daily','Weekly','BiWeekly','Monthly','Yearly','Half Yearly','Quarterly','Every Two Weeks') DEFAULT 'Once',
  `numberOfRecurrences` int DEFAULT '0',
  `frequencyStartDate` timestamp NULL DEFAULT NULL,
  `frequencyEndDate` timestamp NULL DEFAULT NULL,
  `checkImage` longtext,
  `checkImageBack` longtext,
  `cashlessOTPValidDate` timestamp NULL DEFAULT NULL,
  `cashlessOTP` varchar(50) DEFAULT NULL,
  `cashlessPhone` varchar(50) DEFAULT NULL,
  `cashlessEmail` varchar(50) DEFAULT NULL,
  `cashlessPersonName` varchar(50) DEFAULT NULL,
  `cashlessMode` varchar(50) DEFAULT NULL,
  `cashlessSecurityCode` varchar(50) DEFAULT NULL,
  `cashWithdrawalTransactionStatus` varchar(50) DEFAULT NULL,
  `cashlessPin` varchar(50) DEFAULT NULL,
  `category` enum('Auto & Transport','Bills & Utilities','Business Services','Education','Entertainment','Fees & Charges','Financial','Food & Dining','Gifts & Donations','Health & Fitness','Home','Income','Investments','Kids','Personal Care','Pets','Shopping','Taxes','Transfer','Travel','Uncategorised') DEFAULT 'Uncategorised',
  `billCategory` enum('Credit Card','Phone','Utilities','Insurance') DEFAULT NULL,
  `recurrenceDesc` varchar(50) DEFAULT NULL,
  `deliverBy` varchar(50) DEFAULT NULL,
  `p2pContact` varchar(50) DEFAULT NULL,
  `p2pRequiredDate` timestamp NULL DEFAULT NULL,
  `requestCreatedDate` varchar(50) DEFAULT NULL,
  `penaltyFlag` bit(1) DEFAULT b'0',
  `payoffFlag` bit(1) DEFAULT b'0',
  `viewReportLink` varchar(150) DEFAULT NULL,
  `isPaypersonDeleted` bit(1) DEFAULT b'0',
  `fee` varchar(50) DEFAULT '0.00',
  `feeCurrency` varchar(45) DEFAULT NULL,
  `feePaidByReceipent` bit(1) DEFAULT NULL,
  `frontImage1` varchar(100) DEFAULT NULL,
  `frontImage2` varchar(100) DEFAULT NULL,
  `backImage1` varchar(100) DEFAULT NULL,
  `backImage2` varchar(100) DEFAULT NULL,
  `checkDesc` varchar(50) DEFAULT NULL,
  `checkNumber1` varchar(50) DEFAULT NULL,
  `checkNumber2` varchar(50) DEFAULT NULL,
  `bankName1` varchar(50) DEFAULT NULL,
  `bankName2` varchar(50) DEFAULT NULL,
  `withdrawlAmount1` varchar(50) DEFAULT '0.00',
  `withdrawlAmount2` varchar(50) DEFAULT '0.00',
  `cashAmount` varchar(50) DEFAULT '0.00',
  `payeeCurrency` varchar(50) DEFAULT 'INR',
  `billid` bigint DEFAULT NULL,
  `isDisputed` bit(1) DEFAULT b'0',
  `disputeDescription` varchar(50) DEFAULT NULL,
  `disputeReason` varchar(50) DEFAULT NULL,
  `disputeStatus` varchar(50) DEFAULT NULL,
  `disputeDate` timestamp NULL DEFAULT NULL,
  `payeeName` varchar(50) DEFAULT NULL,
  `checkDateOfIssue` timestamp NULL DEFAULT NULL,
  `checkReason` varchar(50) DEFAULT NULL,
  `isPayeeDeleted` bit(1) DEFAULT b'0',
  `amountRecieved` varchar(50) DEFAULT '0',
  `requestValidity` timestamp NULL DEFAULT NULL,
  `statementReference` varchar(35) DEFAULT NULL,
  `transCreditDebitIndicator` varchar(45) DEFAULT NULL,
  `bookingDateTime` timestamp NULL DEFAULT NULL,
  `valueDateTime` timestamp NULL DEFAULT NULL,
  `transactionInformation` varchar(500) DEFAULT NULL,
  `addressLine` varchar(70) DEFAULT NULL,
  `transactionAmount` varchar(50) DEFAULT NULL,
  `chargeAmount` varchar(50) DEFAULT NULL,
  `chargeCurrency` varchar(50) DEFAULT NULL,
  `sourceCurrency` varchar(45) DEFAULT NULL,
  `targetCurrency` varchar(50) DEFAULT NULL,
  `unitCurrency` varchar(50) DEFAULT NULL,
  `exchangeRate` varchar(45) DEFAULT NULL,
  `contractIdentification` varchar(35) DEFAULT NULL,
  `quotationDate` timestamp NULL DEFAULT NULL,
  `instructedAmount` varchar(45) DEFAULT NULL,
  `instructedCurrency` varchar(45) DEFAULT NULL,
  `transactionCode` varchar(35) DEFAULT NULL,
  `transactionSubCode` varchar(45) DEFAULT NULL,
  `proprietaryTransactionCode` varchar(35) DEFAULT NULL,
  `proprietaryTransactionIssuer` varchar(35) DEFAULT NULL,
  `balanceCreditDebitIndicator` varchar(45) DEFAULT NULL,
  `balanceType` varchar(45) DEFAULT NULL,
  `balanceAmount` varchar(45) DEFAULT NULL,
  `balanceCurrency` varchar(45) DEFAULT NULL,
  `merchantName` varchar(45) DEFAULT NULL,
  `merchantCategoryCode` varchar(45) DEFAULT NULL,
  `creditorAgentSchemeName` varchar(45) DEFAULT NULL,
  `creditorAgentIdentification` varchar(45) DEFAULT NULL,
  `creditorAgentName` varchar(140) DEFAULT NULL,
  `creditorAgentaddressType` varchar(45) DEFAULT NULL,
  `creditorAgentDepartment` varchar(45) DEFAULT NULL,
  `creditorAgentSubDepartment` varchar(45) DEFAULT NULL,
  `creditorAgentStreetName` varchar(45) DEFAULT NULL,
  `creditorAgentBuildingNumber` varchar(45) DEFAULT NULL,
  `creditorAgentPostCode` varchar(45) DEFAULT NULL,
  `creditorAgentTownName` varchar(45) DEFAULT NULL,
  `creditorAgentCountrySubDivision` varchar(45) DEFAULT NULL,
  `creditorAgentCountry` varchar(45) DEFAULT NULL,
  `creditorAgentAddressLine` varchar(45) DEFAULT NULL,
  `creditorAccountSchemeName` varchar(45) DEFAULT NULL,
  `creditorAccountIdentification` varchar(45) DEFAULT NULL,
  `creditorAccountName` varchar(45) DEFAULT NULL,
  `creditorAccountSeconIdentification` varchar(45) DEFAULT NULL,
  `debtorAgentSchemeName` varchar(45) DEFAULT NULL,
  `debtorAgentIdentification` varchar(45) DEFAULT NULL,
  `debtorAgentName` varchar(45) DEFAULT NULL,
  `debtorAgentAddressType` varchar(45) DEFAULT NULL,
  `debtorAgentDepartment` varchar(45) DEFAULT NULL,
  `debtorAgentSubDepartment` varchar(45) DEFAULT NULL,
  `debtorAgentStreetName` varchar(45) DEFAULT NULL,
  `debtorAgentBuildingNumber` varchar(45) DEFAULT NULL,
  `dedtorAgentPostCode` varchar(45) DEFAULT NULL,
  `debtorAgentTownName` varchar(45) DEFAULT NULL,
  `debtorAgentCountrySubDivision` varchar(45) DEFAULT NULL,
  `debtorAgentCountry` varchar(45) DEFAULT NULL,
  `debtorAgentAddressLine` varchar(45) DEFAULT NULL,
  `debtorAccountSchemeName` varchar(45) DEFAULT NULL,
  `debtorAccountIdentification` varchar(45) DEFAULT NULL,
  `debtorAccountName` varchar(45) DEFAULT NULL,
  `debtorAccountSeconIdentification` varchar(45) DEFAULT NULL,
  `cardInstrumentSchemeName` varchar(45) DEFAULT NULL,
  `cardInstrumentAuthorisationType` varchar(45) DEFAULT NULL,
  `cardInstrumentName` varchar(45) DEFAULT NULL,
  `cardInstrumentIdentification` varchar(45) DEFAULT NULL,
  `IBAN` varchar(45) DEFAULT NULL,
  `sortCode` varchar(45) DEFAULT NULL,
  `FirstPaymentDateTime` timestamp NULL DEFAULT NULL,
  `NextPaymentDateTime` timestamp NULL DEFAULT NULL,
  `FinalPaymentDateTime` timestamp NULL DEFAULT NULL,
  `StandingOrderStatusCode` varchar(6) DEFAULT NULL,
  `FP_Amount` decimal(12,2) DEFAULT NULL,
  `FP_Currency` varchar(45) DEFAULT NULL,
  `NP_Amount` decimal(12,2) DEFAULT NULL,
  `NP_Currency` varchar(45) DEFAULT NULL,
  `FPA_Amount` decimal(12,2) DEFAULT NULL,
  `FPA_Currency` varchar(45) DEFAULT NULL,
  `ConsentId` varchar(45) DEFAULT NULL,
  `Initiation_InstructionIdentification` varchar(45) DEFAULT NULL,
  `Initiation_EndToEndIdentification` varchar(45) DEFAULT NULL,
  `RI_Reference` varchar(45) DEFAULT NULL,
  `RI_Unstructured` varchar(45) DEFAULT NULL,
  `RiskPaymentContextCode` varchar(45) DEFAULT NULL,
  `MerchantCustomerIdentification` varchar(200) DEFAULT NULL,
  `beneficiaryName` varchar(50) DEFAULT NULL,
  `bankName` varchar(50) DEFAULT NULL,
  `swiftCode` varchar(45) DEFAULT NULL,
  `DomesticPaymentId` varchar(45) DEFAULT NULL,
  `linkSelf` varchar(45) DEFAULT NULL,
  `StatusUpdateDateTime` timestamp(6) NULL DEFAULT NULL,
  `dataStatus` varchar(45) DEFAULT NULL,
  `serviceName` varchar(50) DEFAULT NULL,
  `payPersonName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `fromAccountNumber` (`fromAccountNumber`),
  KEY `toAccountNumber` (`toAccountNumber`),
  KEY `fromtoaccountnumber` (`fromAccountNumber`,`toAccountNumber`) COMMENT 'from and two account number',
  KEY `type_id` (`Type_id`) COMMENT 'transaction Type',
  KEY `payee_id` (`Payee_id`) COMMENT 'payee index'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archivedmoneymovementlog`
--

LOCK TABLES `archivedmoneymovementlog` WRITE;
/*!40000 ALTER TABLE `archivedmoneymovementlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `archivedmoneymovementlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auditactivity`
--

DROP TABLE IF EXISTS `auditactivity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auditactivity` (
  `Id` varchar(255) NOT NULL,
  `EventId` varchar(45) DEFAULT NULL,
  `EventType` varchar(255) DEFAULT NULL,
  `EventSubType` varchar(255) DEFAULT NULL,
  `Status_Id` varchar(255) DEFAULT NULL,
  `sessionId` varchar(255) DEFAULT NULL,
  `AppId` varchar(255) DEFAULT NULL,
  `UserName` varchar(255) DEFAULT NULL,
  `Customer_Id` varchar(255) DEFAULT NULL,
  `partyid` varchar(255) DEFAULT NULL,
  `corecustomerid` varchar(255) DEFAULT NULL,
  `isCSRAssist` tinyint(1) NOT NULL DEFAULT '0',
  `appSessionId` varchar(255) DEFAULT NULL,
  `payeeNickName` varchar(255) DEFAULT NULL,
  `relationshipNumber` varchar(255) DEFAULT NULL,
  `AdminUserName` varchar(255) DEFAULT NULL,
  `AdminUserRole` varchar(255) DEFAULT NULL,
  `Producer` varchar(255) DEFAULT NULL,
  `MoneyMovementRefId` varchar(255) DEFAULT NULL,
  `EventData` json DEFAULT NULL,
  `creditcardnumber` varchar(255) DEFAULT NULL,
  `mfa_State` varchar(255) DEFAULT NULL,
  `mfa_ServiceKey` varchar(255) DEFAULT NULL,
  `mfa_Type` varchar(255) DEFAULT NULL,
  `nonSearchable` json DEFAULT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `deviceModel` varchar(255) DEFAULT NULL,
  `operatingSystem` varchar(255) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `deviceId` varchar(255) DEFAULT NULL,
  `channel` varchar(255) DEFAULT NULL,
  `appVersion` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `ipAddress` varchar(255) DEFAULT NULL,
  `eventts` timestamp NULL DEFAULT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `createdts` timestamp NULL DEFAULT NULL,
  `softdeleteflag` tinyint DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IDX_auditactivity_eventtype` (`EventType`),
  KEY `IDX_auditactivity_eventsubtype` (`EventSubType`),
  KEY `IDX_auditactivity_UserName` (`UserName`),
  KEY `IDX_auditactivity_CustomerId` (`Customer_Id`),
  KEY `IDX_auditactivity_moneymovementid` (`MoneyMovementRefId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auditactivity`
--

LOCK TABLES `auditactivity` WRITE;
/*!40000 ALTER TABLE `auditactivity` DISABLE KEYS */;
/*!40000 ALTER TABLE `auditactivity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customeractivity`
--

DROP TABLE IF EXISTS `customeractivity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customeractivity` (
  `id` varchar(50) NOT NULL,
  `sessionId` varchar(100) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `moduleName` varchar(100) DEFAULT NULL,
  `activityType` varchar(50) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `eventts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(50) DEFAULT NULL,
  `channel` varchar(50) DEFAULT NULL,
  `ipAddress` varchar(50) DEFAULT NULL,
  `device` varchar(50) DEFAULT NULL,
  `deviceId` varchar(100) DEFAULT NULL,
  `operatingSystem` varchar(50) DEFAULT NULL,
  `browser` varchar(50) DEFAULT NULL,
  `referenceId` varchar(50) DEFAULT NULL,
  `errorCode` varchar(50) DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customerId` varchar(50) DEFAULT NULL,
  `typeOfMFA` varchar(50) DEFAULT NULL,
  `payeeName` varchar(50) DEFAULT NULL,
  `accountNumber` varchar(50) DEFAULT NULL,
  `relationshipNumber` varchar(50) DEFAULT NULL,
  `phoneNumber` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `bankName` varchar(50) DEFAULT NULL,
  `maskedAccountNumber` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_customeractivity_modulename` (`moduleName`),
  KEY `idx_customeractivity_eventts` (`eventts`),
  KEY `idx_customeractivity_activitytype` (`activityType`),
  KEY `idx_customeractivity_username` (`username`),
  KEY `idx_customeractivity_status` (`status`),
  KEY `idx_customeractivity_channel` (`channel`),
  KEY `idx_customeractivity_operatingsystem` (`operatingSystem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customeractivity`
--

LOCK TABLES `customeractivity` WRITE;
/*!40000 ALTER TABLE `customeractivity` DISABLE KEYS */;
/*!40000 ALTER TABLE `customeractivity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flyway_schema_history`
--

DROP TABLE IF EXISTS `flyway_schema_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flyway_schema_history` (
  `installed_rank` int NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `flyway_schema_history_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flyway_schema_history`
--

LOCK TABLES `flyway_schema_history` WRITE;
/*!40000 ALTER TABLE `flyway_schema_history` DISABLE KEYS */;
INSERT INTO `flyway_schema_history` VALUES (0,NULL,'<< Flyway Schema Creation >>','SCHEMA','`dbxlogs`',NULL,'UBLinfinity','2022-11-15 15:50:07',0,1),(1,'4.0','base','SQL','V4_0__base.sql',-602796576,'UBLinfinity','2022-11-15 15:50:07',189,1),(2,'4.0.0.1','scripts','SQL','V4_0_0_1__scripts.sql',-401542186,'UBLinfinity','2022-11-15 15:50:07',20,1),(3,'4.1.0.0','scripts','SQL','V4_1_0_0__scripts.sql',-1026479102,'UBLinfinity','2022-11-15 15:50:07',62,1),(4,'4.2.0.0','scripts','SQL','V4_2_0_0__scripts.sql',-1001143494,'UBLinfinity','2022-11-15 15:50:07',21,1),(5,'4.2.5.0','scripts','SQL','V4_2_5_0__scripts.sql',-1711697564,'UBLinfinity','2022-11-15 15:50:07',90,1),(6,'4.2.7.0','scripts','SQL','V4_2_7_0__scripts.sql',1207618326,'UBLinfinity','2022-11-15 15:50:07',114,1),(7,'2021.02','scripts','SQL','V2021_02__scripts.sql',-470642973,'UBLinfinity','2022-11-15 15:50:07',91,1);
/*!40000 ALTER TABLE `flyway_schema_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moneymovementlog`
--

DROP TABLE IF EXISTS `moneymovementlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `moneymovementlog` (
  `Id` varchar(50) NOT NULL,
  `isScheduled` bit(1) NOT NULL DEFAULT b'0',
  `Customer_id` varchar(50) DEFAULT NULL,
  `ExpenseCategory_id` int DEFAULT NULL,
  `Payee_id` int DEFAULT NULL,
  `Bill_id` int DEFAULT NULL,
  `Type_id` int DEFAULT NULL,
  `Reference_id` varchar(50) DEFAULT NULL,
  `fromAccountNumber` varchar(50) DEFAULT NULL,
  `fromAccountBalance` decimal(10,2) DEFAULT '0.00',
  `toAccountNumber` varchar(50) DEFAULT NULL,
  `toAccountBalance` decimal(10,2) DEFAULT '0.00',
  `amount` decimal(20,2) DEFAULT '0.00',
  `convertedAmount` decimal(20,2) DEFAULT NULL,
  `transactionCurrency` varchar(45) DEFAULT NULL,
  `baseCurrency` varchar(45) DEFAULT NULL,
  `Status_id` varchar(50) DEFAULT NULL,
  `statusDesc` varchar(50) DEFAULT NULL,
  `notes` varchar(150) DEFAULT '',
  `checkNumber` int DEFAULT '0',
  `imageURL1` text,
  `imageURL2` text,
  `hasDepositImage` tinyint(1) DEFAULT '0',
  `description` varchar(100) DEFAULT ' ',
  `scheduledDate` timestamp NULL DEFAULT NULL,
  `transactionDate` timestamp NULL DEFAULT NULL,
  `createdDate` timestamp NULL DEFAULT NULL,
  `transactionComments` varchar(100) DEFAULT NULL,
  `toExternalAccountNumber` varchar(45) DEFAULT NULL,
  `Person_Id` int DEFAULT NULL,
  `frequencyType` enum('Once','Daily','Weekly','BiWeekly','Monthly','Yearly','Half Yearly','Quarterly','Every Two Weeks') DEFAULT 'Once',
  `numberOfRecurrences` int DEFAULT '0',
  `frequencyStartDate` timestamp NULL DEFAULT NULL,
  `frequencyEndDate` timestamp NULL DEFAULT NULL,
  `checkImage` longtext,
  `checkImageBack` longtext,
  `cashlessOTPValidDate` timestamp NULL DEFAULT NULL,
  `cashlessOTP` varchar(50) DEFAULT NULL,
  `cashlessPhone` varchar(50) DEFAULT NULL,
  `cashlessEmail` varchar(50) DEFAULT NULL,
  `cashlessPersonName` varchar(50) DEFAULT NULL,
  `cashlessMode` varchar(50) DEFAULT NULL,
  `cashlessSecurityCode` varchar(50) DEFAULT NULL,
  `cashWithdrawalTransactionStatus` varchar(50) DEFAULT NULL,
  `cashlessPin` varchar(50) DEFAULT NULL,
  `category` enum('Auto & Transport','Bills & Utilities','Business Services','Education','Entertainment','Fees & Charges','Financial','Food & Dining','Gifts & Donations','Health & Fitness','Home','Income','Investments','Kids','Personal Care','Pets','Shopping','Taxes','Transfer','Travel','Uncategorised') DEFAULT 'Uncategorised',
  `billCategory` enum('Credit Card','Phone','Utilities','Insurance') DEFAULT NULL,
  `recurrenceDesc` varchar(50) DEFAULT NULL,
  `deliverBy` varchar(50) DEFAULT NULL,
  `p2pContact` varchar(50) DEFAULT NULL,
  `p2pRequiredDate` timestamp NULL DEFAULT NULL,
  `requestCreatedDate` varchar(50) DEFAULT NULL,
  `penaltyFlag` bit(1) DEFAULT b'0',
  `payoffFlag` bit(1) DEFAULT b'0',
  `viewReportLink` varchar(150) DEFAULT 'http://pmqa.konylabs.net/KonyWebBanking/view_report.png',
  `isPaypersonDeleted` bit(1) DEFAULT b'0',
  `fee` varchar(50) DEFAULT '0.00',
  `feeCurrency` varchar(45) DEFAULT NULL,
  `feePaidByReceipent` bit(1) DEFAULT NULL,
  `frontImage1` varchar(100) DEFAULT NULL,
  `frontImage2` varchar(100) DEFAULT NULL,
  `backImage1` varchar(100) DEFAULT NULL,
  `backImage2` varchar(100) DEFAULT NULL,
  `checkDesc` varchar(50) DEFAULT NULL,
  `checkNumber1` varchar(50) DEFAULT NULL,
  `checkNumber2` varchar(50) DEFAULT NULL,
  `bankName1` varchar(50) DEFAULT NULL,
  `bankName2` varchar(50) DEFAULT NULL,
  `withdrawlAmount1` varchar(50) DEFAULT '0.00',
  `withdrawlAmount2` varchar(50) DEFAULT '0.00',
  `cashAmount` varchar(50) DEFAULT '0.00',
  `payeeCurrency` varchar(50) DEFAULT 'INR',
  `billid` bigint DEFAULT NULL,
  `isDisputed` bit(1) DEFAULT b'0',
  `disputeDescription` varchar(50) DEFAULT NULL,
  `disputeReason` varchar(50) DEFAULT NULL,
  `disputeStatus` varchar(50) DEFAULT NULL,
  `disputeDate` timestamp NULL DEFAULT NULL,
  `payeeName` varchar(50) DEFAULT NULL,
  `checkDateOfIssue` timestamp NULL DEFAULT NULL,
  `checkReason` varchar(50) DEFAULT NULL,
  `isPayeeDeleted` bit(1) DEFAULT b'0',
  `amountRecieved` varchar(50) DEFAULT '0',
  `requestValidity` timestamp NULL DEFAULT NULL,
  `statementReference` varchar(35) DEFAULT NULL,
  `transCreditDebitIndicator` varchar(45) DEFAULT NULL,
  `bookingDateTime` timestamp NULL DEFAULT NULL,
  `valueDateTime` timestamp NULL DEFAULT NULL,
  `transactionInformation` varchar(500) DEFAULT NULL,
  `addressLine` varchar(70) DEFAULT NULL,
  `transactionAmount` varchar(50) DEFAULT NULL,
  `chargeAmount` varchar(50) DEFAULT NULL,
  `chargeCurrency` varchar(50) DEFAULT NULL,
  `sourceCurrency` varchar(45) DEFAULT NULL,
  `targetCurrency` varchar(50) DEFAULT NULL,
  `unitCurrency` varchar(50) DEFAULT NULL,
  `exchangeRate` varchar(45) DEFAULT NULL,
  `contractIdentification` varchar(35) DEFAULT NULL,
  `quotationDate` timestamp NULL DEFAULT NULL,
  `instructedAmount` varchar(45) DEFAULT NULL,
  `instructedCurrency` varchar(45) DEFAULT NULL,
  `transactionCode` varchar(35) DEFAULT NULL,
  `transactionSubCode` varchar(45) DEFAULT NULL,
  `proprietaryTransactionCode` varchar(35) DEFAULT NULL,
  `proprietaryTransactionIssuer` varchar(35) DEFAULT NULL,
  `balanceCreditDebitIndicator` varchar(45) DEFAULT NULL,
  `balanceType` varchar(45) DEFAULT NULL,
  `balanceAmount` varchar(45) DEFAULT NULL,
  `balanceCurrency` varchar(45) DEFAULT NULL,
  `merchantName` varchar(45) DEFAULT NULL,
  `merchantCategoryCode` varchar(45) DEFAULT NULL,
  `creditorAgentSchemeName` varchar(45) DEFAULT NULL,
  `creditorAgentIdentification` varchar(45) DEFAULT NULL,
  `creditorAgentName` varchar(140) DEFAULT NULL,
  `creditorAgentaddressType` varchar(45) DEFAULT NULL,
  `creditorAgentDepartment` varchar(45) DEFAULT NULL,
  `creditorAgentSubDepartment` varchar(45) DEFAULT NULL,
  `creditorAgentStreetName` varchar(45) DEFAULT NULL,
  `creditorAgentBuildingNumber` varchar(45) DEFAULT NULL,
  `creditorAgentPostCode` varchar(45) DEFAULT NULL,
  `creditorAgentTownName` varchar(45) DEFAULT NULL,
  `creditorAgentCountrySubDivision` varchar(45) DEFAULT NULL,
  `creditorAgentCountry` varchar(45) DEFAULT NULL,
  `creditorAgentAddressLine` varchar(45) DEFAULT NULL,
  `creditorAccountSchemeName` varchar(45) DEFAULT NULL,
  `creditorAccountIdentification` varchar(45) DEFAULT NULL,
  `creditorAccountName` varchar(45) DEFAULT NULL,
  `creditorAccountSeconIdentification` varchar(45) DEFAULT NULL,
  `debtorAgentSchemeName` varchar(45) DEFAULT NULL,
  `debtorAgentIdentification` varchar(45) DEFAULT NULL,
  `debtorAgentName` varchar(45) DEFAULT NULL,
  `debtorAgentAddressType` varchar(45) DEFAULT NULL,
  `debtorAgentDepartment` varchar(45) DEFAULT NULL,
  `debtorAgentSubDepartment` varchar(45) DEFAULT NULL,
  `debtorAgentStreetName` varchar(45) DEFAULT NULL,
  `debtorAgentBuildingNumber` varchar(45) DEFAULT NULL,
  `dedtorAgentPostCode` varchar(45) DEFAULT NULL,
  `debtorAgentTownName` varchar(45) DEFAULT NULL,
  `debtorAgentCountrySubDivision` varchar(45) DEFAULT NULL,
  `debtorAgentCountry` varchar(45) DEFAULT NULL,
  `debtorAgentAddressLine` varchar(45) DEFAULT NULL,
  `debtorAccountSchemeName` varchar(45) DEFAULT NULL,
  `debtorAccountIdentification` varchar(45) DEFAULT NULL,
  `debtorAccountName` varchar(45) DEFAULT NULL,
  `debtorAccountSeconIdentification` varchar(45) DEFAULT NULL,
  `cardInstrumentSchemeName` varchar(45) DEFAULT NULL,
  `cardInstrumentAuthorisationType` varchar(45) DEFAULT NULL,
  `cardInstrumentName` varchar(45) DEFAULT NULL,
  `cardInstrumentIdentification` varchar(45) DEFAULT NULL,
  `IBAN` varchar(45) DEFAULT NULL,
  `sortCode` varchar(45) DEFAULT NULL,
  `FirstPaymentDateTime` timestamp NULL DEFAULT NULL,
  `NextPaymentDateTime` timestamp NULL DEFAULT NULL,
  `FinalPaymentDateTime` timestamp NULL DEFAULT NULL,
  `StandingOrderStatusCode` varchar(6) DEFAULT NULL,
  `FP_Amount` decimal(12,2) DEFAULT NULL,
  `FP_Currency` varchar(45) DEFAULT NULL,
  `NP_Amount` decimal(12,2) DEFAULT NULL,
  `NP_Currency` varchar(45) DEFAULT NULL,
  `FPA_Amount` decimal(12,2) DEFAULT NULL,
  `FPA_Currency` varchar(45) DEFAULT NULL,
  `ConsentId` varchar(45) DEFAULT NULL,
  `Initiation_InstructionIdentification` varchar(45) DEFAULT NULL,
  `Initiation_EndToEndIdentification` varchar(45) DEFAULT NULL,
  `RI_Reference` varchar(45) DEFAULT NULL,
  `RI_Unstructured` varchar(45) DEFAULT NULL,
  `RiskPaymentContextCode` varchar(45) DEFAULT NULL,
  `MerchantCustomerIdentification` varchar(200) DEFAULT NULL,
  `beneficiaryName` varchar(50) DEFAULT NULL,
  `bankName` varchar(50) DEFAULT NULL,
  `swiftCode` varchar(45) DEFAULT NULL,
  `DomesticPaymentId` varchar(45) DEFAULT NULL,
  `linkSelf` varchar(45) DEFAULT NULL,
  `StatusUpdateDateTime` timestamp(6) NULL DEFAULT NULL,
  `dataStatus` varchar(45) DEFAULT NULL,
  `serviceName` varchar(50) DEFAULT NULL,
  `payPersonName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `fromAccountNumber` (`fromAccountNumber`),
  KEY `toAccountNumber` (`toAccountNumber`),
  KEY `fromtoaccountnumber` (`fromAccountNumber`,`toAccountNumber`) COMMENT 'from and two account number',
  KEY `type_id` (`Type_id`) COMMENT 'transaction Type',
  KEY `payee_id` (`Payee_id`) COMMENT 'payee index'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moneymovementlog`
--

LOCK TABLES `moneymovementlog` WRITE;
/*!40000 ALTER TABLE `moneymovementlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `moneymovementlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactionlog`
--

DROP TABLE IF EXISTS `transactionlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transactionlog` (
  `id` varchar(50) NOT NULL,
  `transactionId` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `payeeName` varchar(50) DEFAULT NULL,
  `serviceName` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `fromAccount` varchar(50) DEFAULT NULL,
  `fromAccountType` varchar(50) DEFAULT NULL,
  `toAccount` varchar(50) DEFAULT NULL,
  `toAccountType` varchar(50) DEFAULT NULL,
  `amount` decimal(20,2) DEFAULT NULL,
  `currencyCode` varchar(50) DEFAULT NULL,
  `channel` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `routingNumber` varchar(50) DEFAULT NULL,
  `batchId` varchar(45) DEFAULT NULL,
  `transactionDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fromMobileOrEmail` varchar(50) DEFAULT NULL,
  `toMobileOrEmail` varchar(50) DEFAULT NULL,
  `swiftCode` varchar(50) DEFAULT NULL,
  `internationalRoutingCode` varchar(50) DEFAULT NULL,
  `ibanNumber` varchar(50) DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `module` varchar(255) DEFAULT NULL,
  `customerId` varchar(50) DEFAULT NULL,
  `device` varchar(50) DEFAULT NULL,
  `operatingSystem` varchar(50) DEFAULT NULL,
  `deviceId` varchar(50) DEFAULT NULL,
  `ipAddress` varchar(50) DEFAULT NULL,
  `referenceNumber` varchar(50) DEFAULT NULL,
  `transactionDescription` varchar(1000) DEFAULT NULL,
  `errorCode` varchar(50) DEFAULT NULL,
  `recipientType` varchar(50) DEFAULT NULL,
  `recipientBankName` varchar(50) DEFAULT NULL,
  `recipientAddress` varchar(1000) DEFAULT NULL,
  `recipientBankAddress` varchar(1000) DEFAULT NULL,
  `checkNumber` varchar(50) DEFAULT NULL,
  `cashWithdrawalFor` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_transactionlog_type` (`type`),
  KEY `idx_transactionlog_transactiondate` (`transactionDate`),
  KEY `idx_transactionlog_amount` (`amount`),
  KEY `idx_transactionlog_username` (`username`),
  KEY `idx_transactionlog_transactionid` (`transactionId`),
  KEY `idx_transactionlog_servicename` (`serviceName`),
  KEY `idx_transactionlog_fromaccounttype` (`fromAccountType`),
  KEY `idx_transactionlog_toaccounttype` (`toAccountType`),
  KEY `idx_transactionlog_currency` (`currencyCode`),
  KEY `idx_transactionlog_status` (`status`),
  KEY `idx_transactionlog_frommobileemail` (`fromMobileOrEmail`),
  KEY `idx_transactionlog_tomobileemail` (`toMobileOrEmail`),
  KEY `idx_transactionlog_payeename` (`payeeName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactionlog`
--

LOCK TABLES `transactionlog` WRITE;
/*!40000 ALTER TABLE `transactionlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactionlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'dbxlogs'
--
/*!50003 DROP PROCEDURE IF EXISTS `auditlogs_update_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`UBLinfinity`@`%` PROCEDURE `auditlogs_update_proc`(
in _partyId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _coreCustomerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
 
	IF (_customerId != '') THEN
		UPDATE auditactivity SET Customer_Id=_customerId where partyid = _partyId;
    END IF; 
	
    IF (_coreCustomerId != '') THEN
		UPDATE auditactivity SET corecustomerid=_coreCustomerId where partyid = _partyId;
    END IF; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-21 15:30:47
