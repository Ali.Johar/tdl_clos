-- MySQL dump 10.13  Distrib 8.0.30, for Linux (x86_64)
--
-- Host: 10.224.102.46    Database: document
-- ------------------------------------------------------
-- Server version	8.0.30-commercial

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ApplicationEvidence`
--

DROP TABLE IF EXISTS `ApplicationEvidence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ApplicationEvidence` (
  `appEvidenceId` varchar(255) NOT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `evidenceId` varchar(255) DEFAULT NULL,
  `requirementId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`appEvidenceId`),
  KEY `FKtb1a945nq0piuk39ncm8vvcot` (`evidenceId`),
  KEY `FKn4hre6fyx7kupmksghag96m3g` (`requirementId`),
  CONSTRAINT `FKn4hre6fyx7kupmksghag96m3g` FOREIGN KEY (`requirementId`) REFERENCES `EvidenceRequirement` (`requirementId`),
  CONSTRAINT `FKtb1a945nq0piuk39ncm8vvcot` FOREIGN KEY (`evidenceId`) REFERENCES `Evidence` (`evidenceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ApplicationEvidence`
--

LOCK TABLES `ApplicationEvidence` WRITE;
/*!40000 ALTER TABLE `ApplicationEvidence` DISABLE KEYS */;
/*!40000 ALTER TABLE `ApplicationEvidence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ApplicationFulfilment`
--

DROP TABLE IF EXISTS `ApplicationFulfilment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ApplicationFulfilment` (
  `appFulfilmentId` varchar(255) NOT NULL,
  `applicationId` varchar(255) DEFAULT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`appFulfilmentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ApplicationFulfilment`
--

LOCK TABLES `ApplicationFulfilment` WRITE;
/*!40000 ALTER TABLE `ApplicationFulfilment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ApplicationFulfilment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChannelEntity`
--

DROP TABLE IF EXISTS `ChannelEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ChannelEntity` (
  `channelId` varchar(255) NOT NULL,
  `channelName` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`channelId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChannelEntity`
--

LOCK TABLES `ChannelEntity` WRITE;
/*!40000 ALTER TABLE `ChannelEntity` DISABLE KEYS */;
INSERT INTO `ChannelEntity` VALUES ('core','core-document','Support documents within core banking system'),('infinity','infinity-document','Support documents within Infinity');
/*!40000 ALTER TABLE `ChannelEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DocumentEntity`
--

DROP TABLE IF EXISTS `DocumentEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DocumentEntity` (
  `documentId` varchar(255) NOT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `fileName` varchar(255) DEFAULT NULL,
  `filePath` varchar(255) DEFAULT NULL,
  `fileSize` bigint DEFAULT NULL,
  `mimeType` varchar(255) DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `updatedDate` datetime DEFAULT NULL,
  `evidenceId` varchar(255) DEFAULT NULL,
  `ownerSystem` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`documentId`),
  KEY `FKj5q7sqme28t8g5yg8quyvvlm5` (`evidenceId`),
  KEY `FK4cxxgpbfxfbm7226uha0ikhrh` (`ownerSystem`),
  CONSTRAINT `FK4cxxgpbfxfbm7226uha0ikhrh` FOREIGN KEY (`ownerSystem`) REFERENCES `OwnerSystemEntity` (`ownerSystemId`),
  CONSTRAINT `FKj5q7sqme28t8g5yg8quyvvlm5` FOREIGN KEY (`evidenceId`) REFERENCES `Evidence` (`evidenceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DocumentEntity`
--

LOCK TABLES `DocumentEntity` WRITE;
/*!40000 ALTER TABLE `DocumentEntity` DISABLE KEYS */;
/*!40000 ALTER TABLE `DocumentEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DocumentGroupEntity`
--

DROP TABLE IF EXISTS `DocumentGroupEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DocumentGroupEntity` (
  `id` bigint NOT NULL,
  `documentGroup` varchar(255) DEFAULT NULL,
  `documentId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK71jfutvnw8bf56blnskgom2rv` (`documentId`),
  CONSTRAINT `FK71jfutvnw8bf56blnskgom2rv` FOREIGN KEY (`documentId`) REFERENCES `DocumentEntity` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DocumentGroupEntity`
--

LOCK TABLES `DocumentGroupEntity` WRITE;
/*!40000 ALTER TABLE `DocumentGroupEntity` DISABLE KEYS */;
/*!40000 ALTER TABLE `DocumentGroupEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Evidence`
--

DROP TABLE IF EXISTS `Evidence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Evidence` (
  `evidenceId` varchar(255) NOT NULL,
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `isReusable` bit(1) DEFAULT NULL,
  `isValid` bit(1) DEFAULT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `ownerType` varchar(255) DEFAULT NULL,
  `typeId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`evidenceId`),
  KEY `FKkn9n937bhf94ql8a6e128di7d` (`typeId`),
  CONSTRAINT `FKkn9n937bhf94ql8a6e128di7d` FOREIGN KEY (`typeId`) REFERENCES `EvidenceType` (`typeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Evidence`
--

LOCK TABLES `Evidence` WRITE;
/*!40000 ALTER TABLE `Evidence` DISABLE KEYS */;
/*!40000 ALTER TABLE `Evidence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EvidenceCategory`
--

DROP TABLE IF EXISTS `EvidenceCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EvidenceCategory` (
  `categoryId` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EvidenceCategory`
--

LOCK TABLES `EvidenceCategory` WRITE;
/*!40000 ALTER TABLE `EvidenceCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `EvidenceCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EvidenceCategory_EvidenceType`
--

DROP TABLE IF EXISTS `EvidenceCategory_EvidenceType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EvidenceCategory_EvidenceType` (
  `EvidenceCategory_categoryId` varchar(255) NOT NULL,
  `types_typeId` varchar(255) NOT NULL,
  PRIMARY KEY (`EvidenceCategory_categoryId`,`types_typeId`),
  KEY `FKkmjn8178a2ap5gischudh32qp` (`types_typeId`),
  CONSTRAINT `FKa8ac9n0fitbqgwiydo37togb0` FOREIGN KEY (`EvidenceCategory_categoryId`) REFERENCES `EvidenceCategory` (`categoryId`),
  CONSTRAINT `FKkmjn8178a2ap5gischudh32qp` FOREIGN KEY (`types_typeId`) REFERENCES `EvidenceType` (`typeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EvidenceCategory_EvidenceType`
--

LOCK TABLES `EvidenceCategory_EvidenceType` WRITE;
/*!40000 ALTER TABLE `EvidenceCategory_EvidenceType` DISABLE KEYS */;
/*!40000 ALTER TABLE `EvidenceCategory_EvidenceType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EvidenceProperty`
--

DROP TABLE IF EXISTS `EvidenceProperty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EvidenceProperty` (
  `propertyId` varchar(255) NOT NULL,
  `propKey` varchar(255) DEFAULT NULL,
  `propValue` varchar(255) DEFAULT NULL,
  `evidenceId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`propertyId`),
  KEY `FK969es5b9b4fusef607oatkdm3` (`evidenceId`),
  CONSTRAINT `FK969es5b9b4fusef607oatkdm3` FOREIGN KEY (`evidenceId`) REFERENCES `Evidence` (`evidenceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EvidenceProperty`
--

LOCK TABLES `EvidenceProperty` WRITE;
/*!40000 ALTER TABLE `EvidenceProperty` DISABLE KEYS */;
/*!40000 ALTER TABLE `EvidenceProperty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EvidenceRequirement`
--

DROP TABLE IF EXISTS `EvidenceRequirement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EvidenceRequirement` (
  `requirementId` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `requirementKey` varchar(255) DEFAULT NULL,
  `requirementName` varchar(255) DEFAULT NULL,
  `appFulfilment_appFulfilmentId` varchar(255) DEFAULT NULL,
  `categoryId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`requirementId`),
  KEY `FKo78kq3qnarnek51riegbiiy9m` (`appFulfilment_appFulfilmentId`),
  KEY `FKsia5nkxwhgwnwat4myl3bugir` (`categoryId`),
  CONSTRAINT `FKo78kq3qnarnek51riegbiiy9m` FOREIGN KEY (`appFulfilment_appFulfilmentId`) REFERENCES `ApplicationFulfilment` (`appFulfilmentId`),
  CONSTRAINT `FKsia5nkxwhgwnwat4myl3bugir` FOREIGN KEY (`categoryId`) REFERENCES `EvidenceCategory` (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EvidenceRequirement`
--

LOCK TABLES `EvidenceRequirement` WRITE;
/*!40000 ALTER TABLE `EvidenceRequirement` DISABLE KEYS */;
/*!40000 ALTER TABLE `EvidenceRequirement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EvidenceRequirementProperty`
--

DROP TABLE IF EXISTS `EvidenceRequirementProperty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EvidenceRequirementProperty` (
  `propertyId` varchar(255) NOT NULL,
  `propDescription` varchar(255) DEFAULT NULL,
  `propKey` varchar(255) DEFAULT NULL,
  `propValue` varchar(255) DEFAULT NULL,
  `requirementId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`propertyId`),
  KEY `FKi75ogixfiu5u3m3ntm82v3vv7` (`requirementId`),
  CONSTRAINT `FKi75ogixfiu5u3m3ntm82v3vv7` FOREIGN KEY (`requirementId`) REFERENCES `EvidenceRequirement` (`requirementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EvidenceRequirementProperty`
--

LOCK TABLES `EvidenceRequirementProperty` WRITE;
/*!40000 ALTER TABLE `EvidenceRequirementProperty` DISABLE KEYS */;
/*!40000 ALTER TABLE `EvidenceRequirementProperty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EvidenceRequirement_EvidenceType`
--

DROP TABLE IF EXISTS `EvidenceRequirement_EvidenceType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EvidenceRequirement_EvidenceType` (
  `EvidenceRequirement_requirementId` varchar(255) NOT NULL,
  `allowedEvidenceTypes_typeId` varchar(255) NOT NULL,
  PRIMARY KEY (`EvidenceRequirement_requirementId`,`allowedEvidenceTypes_typeId`),
  KEY `FK8myt2yd5o5x0ren18ry24i41b` (`allowedEvidenceTypes_typeId`),
  CONSTRAINT `FK8myt2yd5o5x0ren18ry24i41b` FOREIGN KEY (`allowedEvidenceTypes_typeId`) REFERENCES `EvidenceType` (`typeId`),
  CONSTRAINT `FKp9rg0unn3uttasr69b5vv5e2w` FOREIGN KEY (`EvidenceRequirement_requirementId`) REFERENCES `EvidenceRequirement` (`requirementId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EvidenceRequirement_EvidenceType`
--

LOCK TABLES `EvidenceRequirement_EvidenceType` WRITE;
/*!40000 ALTER TABLE `EvidenceRequirement_EvidenceType` DISABLE KEYS */;
/*!40000 ALTER TABLE `EvidenceRequirement_EvidenceType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EvidenceType`
--

DROP TABLE IF EXISTS `EvidenceType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EvidenceType` (
  `typeId` varchar(255) NOT NULL,
  `allowMultiple` bit(1) DEFAULT NULL,
  `defaultValidPeriod` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`typeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EvidenceType`
--

LOCK TABLES `EvidenceType` WRITE;
/*!40000 ALTER TABLE `EvidenceType` DISABLE KEYS */;
/*!40000 ALTER TABLE `EvidenceType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EvidenceTypeProperty`
--

DROP TABLE IF EXISTS `EvidenceTypeProperty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EvidenceTypeProperty` (
  `id` int NOT NULL,
  `mandatory` bit(1) DEFAULT NULL,
  `propDescription` varchar(255) DEFAULT NULL,
  `propKey` varchar(255) DEFAULT NULL,
  `typeId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5yexojtilwswo59mjy77fbev5` (`typeId`),
  CONSTRAINT `FK5yexojtilwswo59mjy77fbev5` FOREIGN KEY (`typeId`) REFERENCES `EvidenceType` (`typeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EvidenceTypeProperty`
--

LOCK TABLES `EvidenceTypeProperty` WRITE;
/*!40000 ALTER TABLE `EvidenceTypeProperty` DISABLE KEYS */;
/*!40000 ALTER TABLE `EvidenceTypeProperty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EvidenceType_EvidenceCategory`
--

DROP TABLE IF EXISTS `EvidenceType_EvidenceCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EvidenceType_EvidenceCategory` (
  `EvidenceType_typeId` varchar(255) NOT NULL,
  `categories_categoryId` varchar(255) NOT NULL,
  PRIMARY KEY (`EvidenceType_typeId`,`categories_categoryId`),
  KEY `FKqjt9syx68laiyshb9mk4ryieg` (`categories_categoryId`),
  CONSTRAINT `FKnaux5jdx9wfjddl0s0qsn5lhm` FOREIGN KEY (`EvidenceType_typeId`) REFERENCES `EvidenceType` (`typeId`),
  CONSTRAINT `FKqjt9syx68laiyshb9mk4ryieg` FOREIGN KEY (`categories_categoryId`) REFERENCES `EvidenceCategory` (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EvidenceType_EvidenceCategory`
--

LOCK TABLES `EvidenceType_EvidenceCategory` WRITE;
/*!40000 ALTER TABLE `EvidenceType_EvidenceCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `EvidenceType_EvidenceCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MsAltKey`
--

DROP TABLE IF EXISTS `MsAltKey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `MsAltKey` (
  `alternateKey` varchar(255) NOT NULL,
  `alternateName` varchar(255) NOT NULL,
  `entityId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`alternateKey`,`alternateName`),
  KEY `index_MsAltKey_entityId` (`entityId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MsAltKey`
--

LOCK TABLES `MsAltKey` WRITE;
/*!40000 ALTER TABLE `MsAltKey` DISABLE KEYS */;
/*!40000 ALTER TABLE `MsAltKey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OwnerSystemEntity`
--

DROP TABLE IF EXISTS `OwnerSystemEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `OwnerSystemEntity` (
  `ownerSystemId` varchar(255) NOT NULL,
  `ownerSystemName` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `channel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ownerSystemId`),
  KEY `FK4k0td7d39mufr9ghqij07wup5` (`channel`),
  CONSTRAINT `FK4k0td7d39mufr9ghqij07wup5` FOREIGN KEY (`channel`) REFERENCES `ChannelEntity` (`channelId`),
  CONSTRAINT `OwnerSystemEntity_ibfk_1` FOREIGN KEY (`channel`) REFERENCES `ChannelEntity` (`channelId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OwnerSystemEntity`
--

LOCK TABLES `OwnerSystemEntity` WRITE;
/*!40000 ALTER TABLE `OwnerSystemEntity` DISABLE KEYS */;
INSERT INTO `OwnerSystemEntity` VALUES ('arrangement','arrangement','Arrangement documents.','core'),('corporate-los','corporate-los','Corporate LOS Fabric App.','infinity'),('digital-banking','digital-banking','Infinity Digital Banking Apps are a set of apps used by Retail and Business banking users to perform various operations for their banking needs.','infinity'),('holding','holding','Holding documents.','core'),('party','party','Party documents.','core'),('retail','retail','Retail onboarding and Retail lending journeys use this owner system id. Infinity Origination, Workspace and Infinity Assist apps use this owner system id while dealing with Retail applications.','infinity'),('sme','sme','SME onboarding and SME lending journeys use this owner system id. Infinity Origination, Workspace and Infinity Assist apps use this owner system id while dealing with SME applications.','infinity');
/*!40000 ALTER TABLE `OwnerSystemEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SearchMetadataEntity`
--

DROP TABLE IF EXISTS `SearchMetadataEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `SearchMetadataEntity` (
  `searchMetadataId` bigint NOT NULL,
  `searchKey` varchar(255) DEFAULT NULL,
  `searchValue` varchar(255) DEFAULT NULL,
  `documentId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`searchMetadataId`),
  KEY `FKsvl148ptghihio8jlwgku8567` (`documentId`),
  CONSTRAINT `FKsvl148ptghihio8jlwgku8567` FOREIGN KEY (`documentId`) REFERENCES `DocumentEntity` (`documentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SearchMetadataEntity`
--

LOCK TABLES `SearchMetadataEntity` WRITE;
/*!40000 ALTER TABLE `SearchMetadataEntity` DISABLE KEYS */;
/*!40000 ALTER TABLE `SearchMetadataEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint NOT NULL,
  PRIMARY KEY (`next_val`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (1);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_error`
--

DROP TABLE IF EXISTS `ms_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_error` (
  `errorEventId` varchar(255) NOT NULL,
  `dataEventId` varchar(255) DEFAULT NULL,
  `errorMessage` varchar(1024) DEFAULT NULL,
  `errorSourceTopic` varchar(255) DEFAULT NULL,
  `lastProcessedTime` datetime DEFAULT NULL,
  `payload` longblob,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`errorEventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_error`
--

LOCK TABLES `ms_error` WRITE;
/*!40000 ALTER TABLE `ms_error` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_inbox_events`
--

DROP TABLE IF EXISTS `ms_inbox_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_inbox_events` (
  `eventId` varchar(255) NOT NULL,
  `businessKey` varchar(255) DEFAULT NULL,
  `commandType` varchar(255) DEFAULT NULL,
  `creationTime` datetime DEFAULT NULL,
  `eventDetails` longtext,
  `eventSourceId` varchar(255) DEFAULT NULL,
  `eventType` varchar(255) DEFAULT NULL,
  `organizationId` varchar(255) DEFAULT NULL,
  `payload` longtext,
  `priority` int DEFAULT NULL,
  `processedTime` datetime DEFAULT NULL,
  `sequenceNo` bigint DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tenantId` varchar(255) DEFAULT NULL,
  `userId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`eventId`),
  KEY `inboxBusinessKeyIndex` (`businessKey`),
  KEY `inboxSequenceNoIndex` (`sequenceNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_inbox_events`
--

LOCK TABLES `ms_inbox_events` WRITE;
/*!40000 ALTER TABLE `ms_inbox_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_inbox_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_outbox_events`
--

DROP TABLE IF EXISTS `ms_outbox_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_outbox_events` (
  `eventId` varchar(255) NOT NULL,
  `businessKey` varchar(255) DEFAULT NULL,
  `correlationId` varchar(255) DEFAULT NULL,
  `creationTime` datetime DEFAULT NULL,
  `eventDetails` varchar(255) DEFAULT NULL,
  `eventType` varchar(255) DEFAULT NULL,
  `eventdate` datetime DEFAULT NULL,
  `organizationId` varchar(255) DEFAULT NULL,
  `payload` longtext,
  `priority` int DEFAULT NULL,
  `processedTime` datetime DEFAULT NULL,
  `sequenceNo` bigint DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tenantId` varchar(255) DEFAULT NULL,
  `userId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`eventId`),
  KEY `outboxBusinessKeyIndex` (`businessKey`),
  KEY `outboxSequenceNoIndex` (`sequenceNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_outbox_events`
--

LOCK TABLES `ms_outbox_events` WRITE;
/*!40000 ALTER TABLE `ms_outbox_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_outbox_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ms_reference_data`
--

DROP TABLE IF EXISTS `ms_reference_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ms_reference_data` (
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `softDeleteFlag` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`type`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ms_reference_data`
--

LOCK TABLES `ms_reference_data` WRITE;
/*!40000 ALTER TABLE `ms_reference_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_reference_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_history`
--

DROP TABLE IF EXISTS `schema_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schema_history` (
  `uniqueReference` varchar(255) NOT NULL,
  `createdOn` varchar(255) DEFAULT NULL,
  `results` varchar(255) DEFAULT NULL,
  `script` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uniqueReference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_history`
--

LOCK TABLES `schema_history` WRITE;
/*!40000 ALTER TABLE `schema_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `schema_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'document'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-21 15:31:32
