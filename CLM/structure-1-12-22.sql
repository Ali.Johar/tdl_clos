-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: 119.160.107.35    Database: dbxdb
-- ------------------------------------------------------
-- Server version	5.7.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `UBL_CLM_APPLICATION_LOG`
--

DROP TABLE IF EXISTS `UBL_CLM_APPLICATION_LOG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UBL_CLM_APPLICATION_LOG` (
  `ApplicationLogId` bigint(20) NOT NULL AUTO_INCREMENT,
  `ApplicationID` varchar(100) DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  `Action` varchar(200) DEFAULT NULL,
  `Comment` varchar(3000) DEFAULT NULL,
  `UserId` varchar(45) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ApplicationLogId`)
) ENGINE=InnoDB AUTO_INCREMENT=220 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UBL_CLM_CUSTOMER_ACCOUNT_DATA`
--

DROP TABLE IF EXISTS `UBL_CLM_CUSTOMER_ACCOUNT_DATA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UBL_CLM_CUSTOMER_ACCOUNT_DATA` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ApplicationID` varchar(100) DEFAULT NULL,
  `PurposeOfAccountId` varchar(30) DEFAULT NULL,
  `PurposeOfAccountIdDesc` varchar(45) DEFAULT NULL,
  `MonthlyIncome` varchar(20) DEFAULT NULL,
  `SourceOfFundId` varchar(30) DEFAULT NULL,
  `SourceOfFundIdDesc` varchar(45) DEFAULT NULL,
  `MonthlyTransaction` varchar(20) DEFAULT NULL,
  `SDMIsAtmId` varchar(20) DEFAULT NULL,
  `SDMIsDirectBankingId` varchar(20) DEFAULT NULL,
  `SDMIsThirdPartyId` varchar(20) DEFAULT NULL,
  `SDMIsElectronicCommunicationId` varchar(20) DEFAULT NULL,
  `SDMIsCardId` varchar(20) DEFAULT NULL,
  `SDMIsInternetMobileBankingId` varchar(20) DEFAULT NULL,
  `SDMIsAtmIdValue` varchar(200) DEFAULT NULL,
  `SDMIsDirectBankingIdValue` varchar(200) DEFAULT NULL,
  `SDMIsElectronicCommunicationIdValue` varchar(200) DEFAULT NULL,
  `SDMIsCardIdValue` varchar(200) DEFAULT NULL,
  `SDMIsInternetMobileBankingIdValue` varchar(200) DEFAULT NULL,
  `SDMIsThirdPartyIdValue` varchar(200) DEFAULT NULL,
  `RIMId` varchar(20) DEFAULT NULL,
  `RIMIdValue` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `ApplicationID` (`ApplicationID`),
  CONSTRAINT `UBL_CLM_CUSTOMER_ACCOUNT_DATA_ibfk_1` FOREIGN KEY (`ApplicationID`) REFERENCES `UBL_CLM_CUSTOMER_DATA` (`ApplicationID`)
) ENGINE=InnoDB AUTO_INCREMENT=2594 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UBL_CLM_CUSTOMER_DATA`
--

DROP TABLE IF EXISTS `UBL_CLM_CUSTOMER_DATA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UBL_CLM_CUSTOMER_DATA` (
  `id` varchar(50) NOT NULL,
  `ApplicationID` varchar(100) DEFAULT NULL,
  `CustomerType` varchar(200) DEFAULT NULL,
  `AccountType` varchar(200) DEFAULT NULL,
  `IdDocumentType` varchar(200) DEFAULT NULL,
  `IdDocumentTypeValue` varchar(200) DEFAULT NULL,
  `IdDocumentNumber` varchar(200) DEFAULT NULL,
  `NoofFailedBio` varchar(20) DEFAULT NULL,
  `UBL_CLM_CUSTOMER_DATAcol` varchar(20) DEFAULT NULL,
  `EmerContactAddress` varchar(200) DEFAULT NULL,
  `BioType` varchar(100) DEFAULT NULL,
  `BioStatus` varchar(20) DEFAULT NULL,
  `BioData` json DEFAULT NULL,
  `BioReferenceNumber` varchar(20) DEFAULT NULL,
  `BioIssuanceDate` varchar(20) DEFAULT NULL,
  `BioAccountTypeValue` varchar(20) DEFAULT NULL,
  `BioProvince` varchar(20) DEFAULT NULL,
  `BankingPreference` varchar(20) DEFAULT NULL,
  `AccountOpeningType` varchar(20) DEFAULT NULL,
  `AccountCategory` varchar(20) DEFAULT NULL,
  `BranchNameKey` varchar(20) DEFAULT NULL,
  `BranchNameValue` varchar(20) DEFAULT NULL,
  `OperatingInsKey` varchar(20) DEFAULT NULL,
  `OperatingInsValue` varchar(20) DEFAULT NULL,
  `ProfitCenterKey` varchar(20) DEFAULT NULL,
  `ProfitCenterValue` varchar(20) DEFAULT NULL,
  `ProductId` varchar(20) DEFAULT NULL,
  `ProductIdValue` varchar(80) DEFAULT NULL,
  `CustTitle` varchar(50) DEFAULT NULL,
  `CustName` varchar(50) DEFAULT NULL,
  `CustShortName` varchar(30) DEFAULT NULL,
  `AccountTitle` varchar(200) DEFAULT NULL,
  `CustIdIssuanceDate` varchar(20) DEFAULT NULL,
  `CustIdExpiryDate` varchar(20) DEFAULT NULL,
  `CustDOB` varchar(20) DEFAULT NULL,
  `CustCityOfBirthId` varchar(20) DEFAULT NULL,
  `CustCityOfBirthIdValue` varchar(80) DEFAULT NULL,
  `CustFatherOrHusbName` varchar(50) DEFAULT NULL,
  `CustMotherName` varchar(50) DEFAULT NULL,
  `CustMAritalStatusId` varchar(20) DEFAULT NULL,
  `CustMAritalStatusIdValue` varchar(80) DEFAULT NULL,
  `CustEducationId` varchar(20) DEFAULT NULL,
  `CustEducationIdValue` varchar(80) DEFAULT NULL,
  `CustNationalityId` varchar(20) DEFAULT NULL,
  `CustNationalityIdValue` varchar(80) DEFAULT NULL,
  `CustONationalityId` varchar(20) DEFAULT NULL,
  `CustONationalityIdValue` varchar(80) DEFAULT NULL,
  `CustCountryOfResId` varchar(20) DEFAULT NULL,
  `CustCountryOfResIdValue` varchar(80) DEFAULT NULL,
  `CustCityOfResId` varchar(20) DEFAULT NULL,
  `CustCityOfResIdValue` varchar(80) DEFAULT NULL,
  `CustStateProvinceOfResId` varchar(20) DEFAULT NULL,
  `CustStateProvinceOfResIdValue` varchar(80) DEFAULT NULL,
  `CustZakatExcemptionId` varchar(20) DEFAULT NULL,
  `CustZakatExcemption` varchar(20) DEFAULT NULL,
  `CustZakatExcemptionReasonId` varchar(20) DEFAULT NULL,
  `CustZakatExcemptionReason` varchar(50) DEFAULT NULL,
  `CustClientCategoryId` varchar(20) DEFAULT NULL,
  `CustClientCategoryIdValue` varchar(80) DEFAULT NULL,
  `CustCiMobileIdValue` varchar(80) DEFAULT NULL,
  `CustCurStreet` varchar(50) DEFAULT NULL,
  `CustCurCity` varchar(20) DEFAULT NULL,
  `CustCurProvince` varchar(20) DEFAULT NULL,
  `CustCurCountry` varchar(20) DEFAULT NULL,
  `CustPerStreet` varchar(50) DEFAULT NULL,
  `CustPerCity` varchar(20) DEFAULT NULL,
  `CustPerProvince` varchar(20) DEFAULT NULL,
  `CustPerCountry` varchar(20) DEFAULT NULL,
  `CustPerSameAsCur` varchar(20) DEFAULT NULL,
  `CustMailAddType` varchar(20) DEFAULT NULL,
  `CustOthStreet` varchar(50) DEFAULT NULL,
  `CustOthCity` varchar(20) DEFAULT NULL,
  `CustOthProvince` varchar(20) DEFAULT NULL,
  `CustOthCountry` varchar(20) DEFAULT NULL,
  `CustIsPEP` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `EmerContactName` varchar(45) DEFAULT NULL,
  `EmerContactRelationId` varchar(15) DEFAULT NULL,
  `EmerContactRelationIdValue` varchar(80) DEFAULT NULL,
  `EmerContactIdentityDocumentNumber` varchar(20) DEFAULT NULL,
  `EmerContactMobile` varchar(20) DEFAULT NULL,
  `EmpOccupationId` varchar(20) DEFAULT NULL,
  `EmpOccupationIdDesc` varchar(80) DEFAULT NULL,
  `EmpName` varchar(45) DEFAULT NULL,
  `EmpDesignation` varchar(20) DEFAULT NULL,
  `EmpSourceOfIncomeId` varchar(20) DEFAULT NULL,
  `EmpSourceOfIncomeIdDesc` varchar(80) DEFAULT NULL,
  `EmpSourceOfWealthId` varchar(20) DEFAULT NULL,
  `EmpSourceOfWealthIdValue` varchar(80) DEFAULT NULL,
  `EmpIsUSACitezen` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `EmpIsCOBUSA` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `EMPIsUSAAddPh` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `EMPIsMandateUSAAdd` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `EmpIsCRS` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `CDDMonthlyCredit` varchar(20) DEFAULT NULL,
  `CDDPurposeOfAccountId` varchar(20) DEFAULT NULL,
  `CDDPurposeOfAccountIdDesc` varchar(80) DEFAULT NULL,
  `CDDNumberOfTransactionId` varchar(20) DEFAULT NULL,
  `CDDNumberOfTransactionIdDesc` varchar(80) DEFAULT NULL,
  `CDDExpectedInwardRemittance` varchar(20) DEFAULT NULL,
  `CDDOFFTT` varchar(20) DEFAULT NULL,
  `CDDProfitCenterId` varchar(20) DEFAULT NULL,
  `CDDProfitCenterIdDesc` varchar(80) DEFAULT NULL,
  `CDDNumberOfCRTransactionId` varchar(20) DEFAULT NULL,
  `CDDNumberOfCRTransactionIdDesc` varchar(80) DEFAULT NULL,
  `CDDNumberOfDRTransactionId` varchar(20) DEFAULT NULL,
  `CDDNumberOfDRTransactionIdDesc` varchar(20) DEFAULT NULL,
  `CDDMonthlyAggregateCR` varchar(20) DEFAULT NULL,
  `CDDMonthlyAggregateDR` varchar(20) DEFAULT NULL,
  `CDDMonthlyAggregateDRTransactionIdValue` varchar(80) DEFAULT NULL,
  `CDDEMPRMCode` varchar(20) DEFAULT NULL,
  `CDDRelInitiationMethod` varchar(20) DEFAULT NULL,
  `CDDReferralEmpCode` varchar(20) DEFAULT NULL,
  `CDDIsAccountForSOE` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `PEPOrganizationId` varchar(20) DEFAULT NULL,
  `PEPOrganizationIdDesc` varchar(80) DEFAULT NULL,
  `PEPRealtionshipId` varchar(20) DEFAULT NULL,
  `PEPRealtionshipIdDesc` varchar(80) DEFAULT NULL,
  `PEPNameofOrganization` varchar(20) DEFAULT NULL,
  `PEPValidDeclaration` varchar(20) DEFAULT NULL,
  `PEPSourceOfFund` varchar(20) DEFAULT NULL,
  `PEPReputationId` varchar(20) DEFAULT NULL,
  `PEPReputationIdDesc` varchar(80) DEFAULT NULL,
  `SBPrincipleName` varchar(45) DEFAULT NULL,
  `SBPDocumentNumber` varchar(20) DEFAULT NULL,
  `SBNTN` varchar(20) DEFAULT NULL,
  `SBPCDCAccount` varchar(20) DEFAULT NULL,
  `SBAgentName` varchar(20) DEFAULT NULL,
  `SBADocumentNumber` varchar(20) DEFAULT NULL,
  `SBANTN` varchar(20) DEFAULT NULL,
  `SBACDCAccount` varchar(30) DEFAULT NULL,
  `SEVOBId` varchar(20) DEFAULT NULL,
  `SEVOBIdDesc` varchar(80) DEFAULT NULL,
  `SEDesignation` varchar(30) DEFAULT NULL,
  `SENOBId` varchar(20) DEFAULT NULL,
  `SENOBIdDesc` varchar(80) DEFAULT NULL,
  `SECounterPart` varchar(20) DEFAULT NULL,
  `SEPositionInBusiness` varchar(20) DEFAULT NULL,
  `AGRINOBId` varchar(20) DEFAULT NULL,
  `AGRINOBIdDesc` varchar(80) DEFAULT NULL,
  `AGRILandSize` varchar(20) DEFAULT NULL,
  `AGRILandUnit` varchar(20) DEFAULT NULL,
  `AGRILandLocation` varchar(20) DEFAULT NULL,
  `AGRILandOwnerShipId` varchar(20) DEFAULT NULL,
  `AGRILandOwnerShipIdDesc` varchar(80) DEFAULT NULL,
  `AGRILandUploadDocument` varchar(50) DEFAULT NULL,
  `NRInterContactNo` varchar(20) DEFAULT NULL,
  `NRInterAddress` varchar(20) DEFAULT NULL,
  `NRCountryResidentId` varchar(20) DEFAULT NULL,
  `NRCountryResidentIdValue` varchar(80) DEFAULT NULL,
  `NRPurposeOfAccountId` varchar(20) DEFAULT NULL,
  `NRPurposeOfAccountIdValue` varchar(80) DEFAULT NULL,
  `NRIdentityDocumentTypeId` varchar(20) DEFAULT NULL,
  `NRIdentityDocumentTypeIdValue` varchar(80) DEFAULT NULL,
  `NRExpiryDate` varchar(20) DEFAULT NULL,
  `NRDocumentNumber` varchar(20) DEFAULT NULL,
  `NRFBBankName` varchar(20) DEFAULT NULL,
  `NRFBAccountNo` varchar(30) DEFAULT NULL,
  `NRFBHaveIdentityDocument` varchar(45) DEFAULT NULL,
  `CRSCountryofTaxId` varchar(20) DEFAULT NULL,
  `CRSCountryofTaxIdValue` varchar(80) DEFAULT NULL,
  `CRSNoNTNReason` varchar(50) DEFAULT NULL,
  `CRSExplantion` varchar(50) DEFAULT NULL,
  `IsFSCardRequird` tinyint(1) DEFAULT NULL,
  `FSCardTypeId` varchar(20) DEFAULT NULL,
  `FSCardTypeIdValue` varchar(80) DEFAULT NULL,
  `FSNameOnCard` varchar(50) DEFAULT NULL,
  `FSEStatementFreqId` varchar(20) DEFAULT NULL,
  `FSEStatementFreqIdValue` varchar(80) DEFAULT NULL,
  `FSGoGreen` varchar(20) DEFAULT NULL,
  `FSisSMSEnabled` varchar(20) DEFAULT NULL,
  `FSIsChequeBookNeeded` varchar(20) DEFAULT NULL,
  `FSNoOfChequeBook` varchar(20) DEFAULT NULL,
  `FSNoofLeaves` varchar(20) DEFAULT NULL,
  `DocAnotherDocument` varchar(100) DEFAULT NULL,
  `DocAdditionalDocument` varchar(100) DEFAULT NULL,
  `DocIdentityDocumentFront` varchar(100) DEFAULT NULL,
  `DocIdentityDocumentBack` varchar(100) DEFAULT NULL,
  `DocEvidenceOfIncome` varchar(100) DEFAULT NULL,
  `DocSelfie` varchar(100) DEFAULT NULL,
  `DocSignatureCard` varchar(100) DEFAULT NULL,
  `DocZakatExemptionForm` varchar(100) DEFAULT NULL,
  `PageName` varchar(100) DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `CreatedDate` varchar(50) DEFAULT NULL,
  `ModifiedBy` varchar(45) DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL,
  `ATA` varchar(45) DEFAULT NULL,
  `NtbEtb` varchar(45) DEFAULT NULL,
  `CustCiEmail` varchar(80) DEFAULT NULL,
  `CustCiLandline` varchar(20) DEFAULT NULL,
  `CustCiMobile` varchar(20) DEFAULT NULL,
  `CustCiMobileId` varchar(20) DEFAULT NULL,
  `CustomerId` varchar(45) DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  `BranchNameId` varchar(45) DEFAULT NULL,
  `BranchNameIdValue` varchar(45) DEFAULT NULL,
  `LastActionUserId` varchar(45) DEFAULT NULL,
  `IsLock` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `CDDPensionFrom` varchar(45) DEFAULT NULL,
  `CDDPensionTypeId` varchar(45) DEFAULT NULL,
  `CDDPensionTypeIdValue` varchar(45) DEFAULT NULL,
  `CDDPensLifeCertificate` tinyint(4) DEFAULT NULL,
  `CDDPensNonMarriageCertificate` tinyint(4) DEFAULT NULL,
  `CDDNameofGuadian` varchar(45) DEFAULT NULL,
  `CDDRelationShipwithGuardianId` varchar(45) DEFAULT NULL,
  `CDDRelationShipwithGuardianIdValue` varchar(45) DEFAULT NULL,
  `CDDRemitterName` varchar(45) DEFAULT NULL,
  `CDDContactNumber` varchar(48) DEFAULT NULL,
  `IsPrimaryCustomer` tinyint(1) DEFAULT NULL,
  `CustCountryOfBirthId` varchar(45) DEFAULT NULL,
  `CustCountryOfBirthIdValue` varchar(45) DEFAULT NULL,
  `CustGenderId` varchar(20) DEFAULT NULL,
  `CustGenderIdValue` varchar(45) DEFAULT NULL,
  `BranchCityId` varchar(20) DEFAULT NULL,
  `BranchCityIdValue` varchar(45) DEFAULT NULL,
  `EmpStatusId` varchar(20) DEFAULT NULL,
  `EmpStatusIdValue` varchar(45) DEFAULT NULL,
  `EmpIndustryId` varchar(20) DEFAULT NULL,
  `EmpIndustryIdValue` varchar(45) DEFAULT NULL,
  `CRAInitOnboardingRespId` varchar(10) DEFAULT NULL,
  `CRARuleBasedScore` varchar(3) DEFAULT NULL,
  `CRAModelBasedScore` varchar(3) DEFAULT NULL,
  `CRARiskStatus` varchar(3) DEFAULT NULL,
  `CDDIFTT` varchar(45) DEFAULT NULL,
  `CDDExpectedAggregatedCreditMonth` varchar(45) DEFAULT NULL,
  `EmpEmploymentSector` varchar(50) DEFAULT NULL,
  `SubmittedDate` datetime DEFAULT NULL,
  `containerId` varchar(50) DEFAULT NULL,
  `processInstanceId` varchar(45) DEFAULT NULL,
  `taskInstanceId` varchar(45) DEFAULT NULL,
  `custAccountNumber` varchar(45) DEFAULT NULL,
  `mobileNetworkId` varchar(20) DEFAULT NULL,
  `mobileNetworkIdValue` varchar(80) DEFAULT NULL,
  `UBL_CLM_CUSTOMER_DATAcol1` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_ApplicationID_UBL_CLM_CUSTOMER_DATA` (`ApplicationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UBL_CLM_CUSTOMER_DATA_EDD`
--

DROP TABLE IF EXISTS `UBL_CLM_CUSTOMER_DATA_EDD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UBL_CLM_CUSTOMER_DATA_EDD` (
  `EDDId` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerDataId` varchar(50) NOT NULL,
  `PEPName` varchar(45) DEFAULT NULL,
  `PEPDesignation` varchar(45) DEFAULT NULL,
  `PEPIsHoldValidDeclaration` tinyint(1) DEFAULT NULL,
  `PEPIsSourceOfFundUpdate` tinyint(1) DEFAULT NULL,
  `PEPReputation` varchar(1) DEFAULT NULL,
  `PEPIsAssociate` tinyint(1) DEFAULT NULL,
  `PEPRelationship` varchar(20) DEFAULT NULL,
  `SEIsPhysicalVerification` tinyint(1) DEFAULT NULL,
  `SEDateOfLastVisit` varchar(20) DEFAULT NULL,
  `SEIsNatureOfBusinessUpdated` tinyint(1) DEFAULT NULL,
  `SEIsCounterPartiesUpdated` tinyint(1) DEFAULT NULL,
  `AGRIsHoldValidDeclaration` tinyint(1) DEFAULT NULL,
  `AGRIsCropCultivation` tinyint(1) DEFAULT NULL,
  `AGRIsHorticultureOrchard` tinyint(1) DEFAULT NULL,
  `AGRIsFisheries` tinyint(1) DEFAULT NULL,
  `AGRIsPoultry` tinyint(1) DEFAULT NULL,
  `AGRIsFertilizerPesticide` tinyint(1) DEFAULT NULL,
  `AGRIsOther` tinyint(1) DEFAULT NULL,
  `AGRWhatOther` varchar(20) DEFAULT NULL,
  `AGRIsLivestock` tinyint(1) DEFAULT NULL,
  `AGRIsMeasurementType` varchar(30) DEFAULT NULL,
  `AGRIsMeasurementUnit` varchar(10) DEFAULT NULL,
  `AGRIsLandSituatedLocation` varchar(100) DEFAULT NULL,
  `AGRIsPropertyOwnership` tinyint(1) DEFAULT NULL,
  `NRInternationalContactNo` varchar(20) DEFAULT NULL,
  `NRInternationalAddress` varchar(20) DEFAULT NULL,
  `NRIsCountryOfResidence` tinyint(1) DEFAULT NULL,
  `NRPurposeOfAccountOpening` varchar(100) DEFAULT NULL,
  `NRIsForeignAccountDetails` tinyint(1) DEFAULT NULL,
  `NRBankName` varchar(50) DEFAULT NULL,
  `NRAccountCIF` varchar(100) DEFAULT NULL,
  `NRISForeignId` tinyint(1) DEFAULT NULL,
  `NRForeignIdName` varchar(20) DEFAULT NULL,
  `NRForeignIdExp` varchar(20) DEFAULT NULL,
  `CRSIsCountryOfResidence` tinyint(1) DEFAULT NULL,
  `CRSTIN` varchar(20) DEFAULT NULL,
  `CRSExplanation` varchar(500) DEFAULT NULL,
  `BUSIsSpecificNature` tinyint(1) DEFAULT NULL,
  `BUSIsSignificantPosition` tinyint(1) DEFAULT NULL,
  `MANMandate` varchar(12) DEFAULT NULL,
  `MANRelationship` varchar(100) DEFAULT NULL,
  `MANIsExpiry` tinyint(1) DEFAULT NULL,
  `MANExpiry` varchar(20) DEFAULT NULL,
  `MANNoOfChanges` varchar(2) DEFAULT NULL,
  `NGOIsValidLicence` tinyint(1) DEFAULT NULL,
  `NGOLicenceIssuingAuthority` varchar(100) DEFAULT NULL,
  `NGOLicenceValidity` varchar(12) DEFAULT NULL,
  `NGOIsDocumentHold` tinyint(1) DEFAULT NULL,
  `NGOIsHeldResidence` tinyint(1) DEFAULT NULL,
  `NGOIsCIFUpdated` tinyint(1) DEFAULT NULL,
  `NGOFundUtilizedFor` varchar(100) DEFAULT NULL,
  `NGOFirstDonor` varchar(100) DEFAULT NULL,
  `NGOFirstDonorcnic` varchar(20) DEFAULT NULL,
  `NGOSecondDonor` varchar(100) DEFAULT NULL,
  `NGOSecondDonorcnic` varchar(20) DEFAULT NULL,
  `NGOGeoraphicalArea` varchar(100) DEFAULT NULL,
  `NGOIsReputation` tinyint(1) DEFAULT NULL,
  `NGOIsApprovalFromSeniorManagement` tinyint(1) DEFAULT NULL,
  `HFIsValidDeclaration` tinyint(1) DEFAULT NULL,
  `HFIsFundProvidedBySelf` tinyint(1) DEFAULT NULL,
  `HFRelationshipType` varchar(100) DEFAULT NULL,
  `HFRelationshipName` varchar(100) DEFAULT NULL,
  `HFRelationshipcnic` varchar(20) DEFAULT NULL,
  `NATStayPurpose` varchar(100) DEFAULT NULL,
  `NATOpeningPurpose` varchar(100) DEFAULT NULL,
  `NATValidityOfVisa` varchar(100) DEFAULT NULL,
  `HMReason` varchar(100) DEFAULT NULL,
  `CASHJustification` varchar(100) DEFAULT NULL,
  `IBIsCustomerDeclarationObtain` tinyint(1) DEFAULT NULL,
  `IBIsKYCUpdated` tinyint(1) DEFAULT NULL,
  `OBIsRealtionshipOtherBank` tinyint(1) DEFAULT NULL,
  `OBIBANAccount` varchar(30) DEFAULT NULL,
  `OBAccounTitle` varchar(45) DEFAULT NULL,
  `OBBranchName` varchar(45) DEFAULT NULL,
  `OBBranchCode` varchar(10) DEFAULT NULL,
  `SBIsPrincipleAgent` varchar(1) DEFAULT NULL,
  `SBNamePrincipleAgent` varchar(45) DEFAULT NULL,
  `SBNTN` varchar(20) DEFAULT NULL,
  `SBCDCAccount` varchar(30) DEFAULT NULL,
  `SBAgentPrincipleName` varchar(45) DEFAULT NULL,
  `SBAgentcnicType` varchar(1) DEFAULT NULL,
  `SBAgentcnicNumber` varchar(20) DEFAULT NULL,
  `SBAgentNTNNumber` varchar(20) DEFAULT NULL,
  `SBCDCAccountType` varchar(20) DEFAULT NULL,
  `SBCDCAccountNumber` varchar(20) DEFAULT NULL,
  `NRCountryOfResidenceId` varchar(20) DEFAULT NULL,
  `NRCountryOfResidenceIdValue` varchar(80) DEFAULT NULL,
  `CRSCountryOfResidenceId` varchar(20) DEFAULT NULL,
  `CRSCountryOfResidenceIdValue` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`EDDId`),
  KEY `CustomerDataId` (`CustomerDataId`),
  CONSTRAINT `UBL_CLM_CUSTOMER_DATA_EDD_ibfk_1` FOREIGN KEY (`CustomerDataId`) REFERENCES `UBL_CLM_CUSTOMER_DATA` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3330 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UBL_CLM_DISCREPANCY_DATA`
--

DROP TABLE IF EXISTS `UBL_CLM_DISCREPANCY_DATA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UBL_CLM_DISCREPANCY_DATA` (
  `id` varchar(100) COLLATE latin1_bin NOT NULL,
  `ApplicationID` varchar(100) COLLATE latin1_bin DEFAULT NULL,
  `DiscrepantDetails` varchar(10000) COLLATE latin1_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UBL_CLM_ROLE`
--

DROP TABLE IF EXISTS `UBL_CLM_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UBL_CLM_ROLE` (
  `RoleKey` varchar(2) NOT NULL,
  `RoleDescription` varchar(20) DEFAULT NULL,
  `IsActive` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`RoleKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UBL_CLM_Status`
--

DROP TABLE IF EXISTS `UBL_CLM_Status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UBL_CLM_Status` (
  `Id` varchar(50) NOT NULL,
  `Description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UBL_CLM_USER_ROLE`
--

DROP TABLE IF EXISTS `UBL_CLM_USER_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UBL_CLM_USER_ROLE` (
  `UserRoleId` int(11) NOT NULL,
  `RoleKey` varchar(2) DEFAULT NULL,
  `UserId` varchar(45) DEFAULT NULL,
  `IsActive` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`UserRoleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ubl_clm_lov_data`
--

DROP TABLE IF EXISTS `ubl_clm_lov_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ubl_clm_lov_data` (
  `LOVKey` varchar(500) NOT NULL,
  `LOVData` json DEFAULT NULL,
  `LOVDetails` varchar(500) DEFAULT NULL,
  `IsActive` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`LOVKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'dbxdb'
--
/*!50003 DROP FUNCTION IF EXISTS `func_escape_input_for_in_operator` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` FUNCTION `func_escape_input_for_in_operator`(
  _input TEXT
) RETURNS text CHARSET utf8
    DETERMINISTIC
BEGIN
    DECLARE result TEXT;
    DECLARE element TEXT;
    DECLARE ctr LONG;
    SET ctr = 1;
    SET result = '';
    
    readinput: LOOP
		SET element = func_split_str(_input, ',', ctr);
		
		IF element = '' THEN
			LEAVE readinput;
		END IF;
        
        IF result != '' THEN
			set result = concat ( result ,',');
		END IF;
		
        set result = concat ( result , quote(element));        
        SET ctr = ctr + 1;
		ITERATE readinput;
    END LOOP readinput;
    
    RETURN (result);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `func_split_str` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` FUNCTION `func_split_str`(
  X TEXT,
  delim TEXT,
  pos INT
) RETURNS text CHARSET utf8
    READS SQL DATA
    DETERMINISTIC
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(X, delim, pos),
       LENGTH(SUBSTRING_INDEX(X, delim, pos -1)) + 1),
       delim, '') ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `account_action_approvers_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `account_action_approvers_proc`(
in _contractId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _cif varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _accountIds text CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _approvalActionList varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _featureId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET SESSION group_concat_max_len = 1000000;

SET @customerIdList = (SELECT group_concat(DISTINCT `customeraction`.`Customer_id` SEPARATOR ",") from (`customeraction`)
						where 
							`customeraction`.`isAllowed` = '0'
						and `customeraction`.`Action_id` = _approvalActionList
                        and FIND_IN_SET(`customeraction`.`Account_id`, _accountIds)
                        and `customeraction`.`contractId` = _contractId
                        and `customeraction`.`coreCustomerId` = _cif);

SET @customerIdList = IF(@customerIdList is null, '', @customerIdList);
SET @NumberOfAccounts = LENGTH(_accountIds) - LENGTH(REPLACE(_accountIds, ',', '')) + 1;

SET @customerIdListWithNoAccountAccess = (SELECT group_concat(DISTINCT Customer_id SEPARATOR ",") from
                                            ( SELECT Customer_id, Account_id
                                            from customeraccounts
                                            where FIND_IN_SET(Account_id, _accountIds)
                                            group by Customer_id having
                                            count(Account_id) != @NumberOfAccounts ) AS tempcustomeraccounts);
										  
SET @customerIdListWithNoAccountAccess = IF(@customerIdListWithNoAccountAccess is null, '', @customerIdListWithNoAccountAccess);
SELECT 
	DISTINCT (`customer`.`id` ) AS id , (`customer`.`username`) AS userName , (`membergroup`.`Name`) AS groupId,
										(`customer`.`FirstName`) AS firstName , (`customer`.`LastName`) AS lastName
from 
	(`customer`
LEFT JOIN `contractcustomers` ON (`contractcustomers`.`customerId` = `customer`.`id` and 
`contractcustomers`.`contractId` = _contractId and `contractcustomers`.`coreCustomerId` = _cif)
LEFT JOIN `customergroup` ON (`customergroup`.`Customer_id` = `customer`.`id`)
LEFT JOIN `customeraction` ON (`customeraction`.`Customer_id` = `customer`.`id`)
LEFT JOIN `membergroup` ON (`membergroup`.`id` = `customergroup`.`Group_id`)
LEFT JOIN `groupactionlimit` ON (`groupactionlimit`.`Group_id` = `customergroup`.`Group_id`)
INNER JOIN `customeraccounts` ON (`customeraccounts`.`Customer_id` = `customer`.`id`)
LEFT JOIN `contractfeatures` ON (`contractfeatures`.`contractId` = _contractId and `contractfeatures`.`coreCustomerId` = _cif))
	where 
        `contractfeatures`.`contractId` = _contractId
        and `contractfeatures`.`coreCustomerId` = _cif
        and `contractfeatures`.`featureId` = _featureId
		and FIND_IN_SET(`customeraccounts`.`Account_id`,  _accountIds)
		and `customer`.`Status_id` = 'SID_CUS_ACTIVE'
        and NOT FIND_IN_SET(`customer`.`id`,  @customerIdList)
		and NOT FIND_IN_SET(`customer`.`id`,  @customerIdListWithNoAccountAccess)
		and FIND_IN_SET(`groupactionlimit`.`Action_id`,_approvalActionList) > 0
		and FIND_IN_SET(`customeraction`.`Action_id`,_approvalActionList) > 0;      

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `achtransactions_fetch_records_subrecords_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `achtransactions_fetch_records_subrecords_proc`(
    IN `_transactionId` INT 
)
BEGIN
    SELECT 
        
        `achTransaction`.`transaction_id`,
        `achTransaction`.`fromAccount`,
        `achTransaction`.`effectiveDate`,
        `achTransaction`.`requestId`,
        `achTransaction`.`createdby`,
        `achTransaction`.`createdts` AS `createdOn`,
        `achTransaction`.`maxAmount`,
        `achTransaction`.`status`,
        `achTransaction`.`transactionType_id`,
        `achTransaction`.`templateType_id`,
        `achTransaction`.`companyId`,
        `achTransaction`.`templateRequestType_id`,
        `achTransaction`.`templateName`,
        `achTransaction`.`confirmationNumber`,
        `achTransaction`.`actedBy`,
        `achTransaction`.`template_id`,
        `achTransaction`.`totalAmount`,
        `achTransaction`.`featureActionId`,
        `bbTemplateRequestType`.`templateRequestTypeName` AS `templateRequestType`,
        `achTransactionRecord`.`transactionRecord_id`,
        `achTransactionRecord`.`toAccountNumber`,
        `achTransactionRecord`.`toAccountType`,
        `achTransactionRecord`.`abatrcNumber`,
        `achTransactionRecord`.`detail_id`,
        `achTransactionRecord`.`amount`,
        `achTransactionRecord`.`additionalInfo`,
        `achTransactionRecord`.`eIN`,
        `achTransactionRecord`.`isZeroTaxDue`,
        `achTransactionRecord`.`taxType_id`,
        `achTransactionRecord`.`templateRequestType_id`,
        `achTransactionRecord`.`transaction_id` as `transaction_id_reclevel`,
        `achTransactionRecord`.`record_Name`,
        `achTransactionSubRecord`.`transcationSubRecord_id`,
        `achTransactionSubRecord`.`amount` as `subrecordamount`,
        `achTransactionSubRecord`.`taxSubCategory_id`,
        `bbTaxType`.`taxType`,
        `bbTaxSubtype`.`taxSubType`,
        `achTransactionSubRecord`.`transactionRecord_id` as `transactionRecord_id_subreclevel` 
      
        FROM (((((`achtransaction` as `achTransaction`
      LEFT JOIN `achtransactionrecord` as `achTransactionRecord` 
      ON `achTransaction`.`transaction_id` = `achTransactionRecord`.`transaction_id`)
      LEFT JOIN `achtransactionsubrecord` as `achTransactionSubRecord`
      ON `achTransactionRecord`.`transactionRecord_id` = `achTransactionSubRecord`.`transactionRecord_id`)
      LEFT JOIN `bbtaxtype` as `bbTaxType`
      ON `achTransactionRecord`.`taxType_id` = `bbTaxType`.`id`)
      LEFT JOIN `bbtaxsubtype` as `bbTaxSubtype`
      ON `achTransactionSubRecord`.`taxSubCategory_id` = `bbTaxSubtype`.`id`)
      LEFT JOIN `bbtemplaterequesttype` as `bbTemplateRequestType`
      ON `achTransaction`.`templateRequestType_id` = `bbTemplateRequestType`.`templateRequestType_id`)
      WHERE 
        `achTransaction`.`transaction_id` = _transactionId
      ORDER BY `achTransaction`.`transaction_id`,`achTransactionRecord`.`transactionRecord_id`,`achTransactionSubRecord`.`transactionRecord_id`;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ach_file_record_subrecord_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `ach_file_record_subrecord_create_proc`(
IN _recordvalues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _subRecordvalues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	DECLARE index1 INTEGER DEFAULT 0;
	set @numOfRecords = LENGTH(_recordvalues) - LENGTH(REPLACE(_recordvalues, '|', '')) + 1;
	insertRecords : LOOP
		set index1 = index1 + 1;
		IF index1 = @numOfRecords + 1 THEN 
			LEAVE insertRecords;
		else
			set @recordsData = SUBSTRING_INDEX(SUBSTRING_INDEX(_recordvalues, '|', index1), '|', -1 );
			SET @query = CONCAT('INSERT INTO achfilerecord(achFileId,offsetAccountNumber,offsetAmount,offsetTransactionType,effectiveDate,requestType,totalCreditAmount,totalDebitAmount,transactionType) VALUES (',@recordsData,');');
			PREPARE sql_query FROM @query;
			EXECUTE sql_query;
			SET @id = LAST_INSERT_ID();
			IF _subRecordvalues != '' THEN 
				SET @subRecordsData = SUBSTRING_INDEX(SUBSTRING_INDEX(_subRecordvalues, '|', index1), '|', -1 );
				IF @subRecordsData != ';' THEN
					SET @numberOfSubRecords = LENGTH(@subRecordsData) - LENGTH(REPLACE(@subRecordsData,';','')) + 1;
					SET @subRecordIndex = 1;
					createSubRecords : LOOP
						IF @subRecordIndex = @numberOfSubRecords + 1 THEN
							LEAVE createSubRecords;
						else
							set @subRecordData = SUBSTRING_INDEX(SUBSTRING_INDEX(@subRecordsData, ';', @subRecordIndex), ';', -1 );
							set @query = concat('INSERT INTO achfilesubrecord(amount,receiverAccountNumber,receiverAccountType,receiverName,receiverTransactionType,achFileRecordId) VALUES (',@subRecordData,',',@id,');');
							PREPARE sql_query FROM @query;
							EXECUTE sql_query;
						SET @subRecordIndex = @subRecordIndex + 1;
						END IF;
					END LOOP createSubRecords;
				END IF;
			END IF;
	    END IF;
	END LOOP insertRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ach_template_record_subrecord_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `ach_template_record_subrecord_create_proc`(
IN _recordvalues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _subRecordvalues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	DECLARE index1 INTEGER DEFAULT 0;
	set @numOfRecords = LENGTH(_recordvalues) - LENGTH(REPLACE(_recordvalues, '|', '')) + 1;
	insertRecords : LOOP
		set index1 = index1 + 1;
		IF index1 = @numOfRecords + 1 THEN 
			LEAVE insertRecords;
		else
			set @recordsData = SUBSTRING_INDEX(SUBSTRING_INDEX(_recordvalues, '|', index1), '|', -1 );
			SET @query = CONCAT('INSERT INTO bbtemplaterecord(record_Name,toAccountNumber,abatrcNumber,detail_id,amount,additionalInfo,ein,isZeroTaxDue,template_id,taxType_id,templateRequestType_id,toAccountType) VALUES (',@recordsData,');');
			PREPARE sql_query FROM @query;
			EXECUTE sql_query;
			SET @id = LAST_INSERT_ID();
			IF _subRecordvalues != '' THEN 
				SET @subRecordsData = SUBSTRING_INDEX(SUBSTRING_INDEX(_subRecordvalues, '|', index1), '|', -1 );
				IF @subRecordsData != ';' THEN
					SET @numberOfSubRecords = LENGTH(@subRecordsData) - LENGTH(REPLACE(@subRecordsData,';','')) + 1;
					SET @subRecordIndex = 1;
					createSubRecords : LOOP
						IF @subRecordIndex = @numberOfSubRecords + 1 THEN
							LEAVE createSubRecords;
						else
							set @subRecordData = SUBSTRING_INDEX(SUBSTRING_INDEX(@subRecordsData, ';', @subRecordIndex), ';', -1 );
							set @query = concat('INSERT INTO bbtemplatesubrecord(amount,taxSubCategory_id,templateRecord_id) VALUES (',@subRecordData,',',@id,');');
							PREPARE sql_query FROM @query;
							EXECUTE sql_query;
						SET @subRecordIndex = @subRecordIndex + 1;
						END IF;
					END LOOP createSubRecords;
				END IF;
			END IF;
	    END IF;
	END LOOP insertRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ach_transaction_record_subrecord_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `ach_transaction_record_subrecord_create_proc`(
IN _recordvalues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _subRecordvalues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	DECLARE index1 INTEGER DEFAULT 0;
	set @numOfRecords = LENGTH(_recordvalues) - LENGTH(REPLACE(_recordvalues, '|', '')) + 1;
	insertRecords : LOOP
		set index1 = index1 + 1;
		IF index1 = @numOfRecords + 1 THEN 
			LEAVE insertRecords;
		else
			set @recordsData = SUBSTRING_INDEX(SUBSTRING_INDEX(_recordvalues, '|', index1), '|', -1 );
			SET @query = CONCAT('INSERT INTO achtransactionrecord(record_Name,toAccountNumber,abatrcNumber,detail_id,amount,additionalInfo,eIN,isZeroTaxDue,transaction_id,taxType_id,templateRequestType_id,toAccountType) VALUES (',@recordsData,');');
			PREPARE sql_query FROM @query;
			EXECUTE sql_query;
			SET @id = LAST_INSERT_ID();
			IF _subRecordvalues != '' THEN 
				SET @subRecordsData = SUBSTRING_INDEX(SUBSTRING_INDEX(_subRecordvalues, '|', index1), '|', -1 );
                IF @subRecordsData != ';' THEN 
					SET @numberOfSubRecords = LENGTH(@subRecordsData) - LENGTH(REPLACE(@subRecordsData,';','')) + 1;
					SET @subRecordIndex = 1;
					createSubRecords : LOOP
						IF @subRecordIndex = @numberOfSubRecords + 1 THEN
							LEAVE createSubRecords;
						else
							set @subRecordData = SUBSTRING_INDEX(SUBSTRING_INDEX(@subRecordsData, ';', @subRecordIndex), ';', -1 );
							set @query = concat('INSERT INTO achtransactionsubrecord(amount,taxSubCategory_id,transactionRecord_id) VALUES (',@subRecordData,',',@id,');');
							PREPARE sql_query FROM @query;
							EXECUTE sql_query;
						SET @subRecordIndex = @subRecordIndex + 1;
						END IF;
					END LOOP createSubRecords;
                END IF;
			END IF;
	    END IF;
	END LOOP insertRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `action_limits_update_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `action_limits_update_proc`(
  IN _action varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
  IN _minTxLimit decimal(20,2),
  IN _maxTxLimit decimal(20,2),
  IN _dailyLimit decimal(20,2),
  IN _weeklyLimit decimal(20,2)
)
BEGIN
  UPDATE actionlimit SET value = _minTxLimit where Action_id = _action AND LimitType_id = 'MIN_TRANSACTION_LIMIT'; 
  UPDATE actionlimit SET value = _maxTxLimit where Action_id = _action AND LimitType_id = 'MAX_TRANSACTION_LIMIT'; 
  UPDATE actionlimit SET value = _dailyLimit where Action_id = _action AND LimitType_id = 'DAILY_LIMIT';
  UPDATE actionlimit SET value = _weeklyLimit where Action_id = _action AND LimitType_id = 'WEEKLY_LIMIT';
  UPDATE groupactionlimit SET value = _maxTxLimit where Action_id = _action AND LimitType_id = 'MAX_TRANSACTION_LIMIT' AND value > _maxTxLimit; 
  UPDATE groupactionlimit SET value = _dailyLimit where Action_id = _action AND LimitType_id = 'DAILY_LIMIT' AND value > _dailyLimit;
  UPDATE groupactionlimit SET value = _weeklyLimit where Action_id = _action AND LimitType_id = 'WEEKLY_LIMIT' AND value > _weeklyLimit; 
  UPDATE organisationactionlimit SET value = _maxTxLimit where Action_id = _action AND LimitType_id = 'MAX_TRANSACTION_LIMIT' AND value > _maxTxLimit; 
  UPDATE organisationactionlimit SET value = _dailyLimit where Action_id = _action AND LimitType_id = 'DAILY_LIMIT' AND value > _dailyLimit;
  UPDATE organisationactionlimit SET value = _weeklyLimit where Action_id = _action AND LimitType_id = 'WEEKLY_LIMIT' AND value > _weeklyLimit; 
  UPDATE customeraction SET value = _maxTxLimit where Action_id = _action AND LimitType_id = 'PRE_APPROVED_TRANSACTION_LIMIT' AND value > _maxTxLimit; 
  UPDATE customeraction SET value = _dailyLimit where Action_id = _action AND LimitType_id = 'PRE_APPROVED_DAILY_LIMIT' AND value > _dailyLimit;
  UPDATE customeraction SET value = _weeklyLimit where Action_id = _action AND LimitType_id = 'PRE_APPROVED_WEEKLY_LIMIT' AND value > _weeklyLimit; 
  UPDATE customeraction SET value = _maxTxLimit where Action_id = _action AND LimitType_id = 'AUTO_DENIED_TRANSACTION_LIMIT' AND value > _maxTxLimit; 
  UPDATE customeraction SET value = _dailyLimit where Action_id = _action AND LimitType_id = 'AUTO_DENIED_DAILY_LIMIT' AND value > _dailyLimit;
  UPDATE customeraction SET value = _weeklyLimit where Action_id = _action AND LimitType_id = 'AUTO_DENIED_WEEKLY_LIMIT' AND value > _weeklyLimit;
  UPDATE customeraction SET value = _maxTxLimit where Action_id = _action AND LimitType_id = 'MAX_TRANSACTION_LIMIT' AND value > _maxTxLimit; 
  UPDATE customeraction SET value = _dailyLimit where Action_id = _action AND LimitType_id = 'DAILY_LIMIT' AND value > _dailyLimit;
  UPDATE customeraction SET value = _weeklyLimit where Action_id = _action AND LimitType_id = 'WEEKLY_LIMIT' AND value > _weeklyLimit; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `alert_history_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `alert_history_proc`(
	IN _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
  )
BEGIN
 SELECT
        `alerthistory`.`id` AS `alerthistory_id`,
        `alerthistory`.`EventId` AS `alerthistory_EventId`,
        `alerthistory`.`AlertSubTypeId` AS `alerthistory_AlertSubTypeId`,
        `alerthistory`.`AlertTypeId` AS `alerthistory_AlertTypeId`,
        `alerthistory`.`AlertCategoryId` AS `alerthistory_AlertCategoryId`,
        `alerthistory`.`Customer_Id` AS `alerthistory_Customer_Id`,
        `alerthistory`.`LanguageCode` AS `alerthistory_LanguageCode`,
        `alerthistory`.`ChannelId` AS `alerthistory_ChannelId`,
        `alerthistory`.`Status` AS `alerthistory_Status`,
        `alerthistory`.`Subject` AS `alerthistory_Subject`,
        `alerthistory`.`Message` AS `alerthistory_Message`,
        `alerthistory`.`SenderName` AS `alerthistory_SenderName`,
        `alerthistory`.`SenderEmail` AS `alerthistory_SenderEmail`,
        `alerthistory`.`ReferenceNumber` AS `alerthistory_ReferenceNumber`,
        `alerthistory`.`createdts` AS `alerthistory_sentDate`,
        `alerthistory`.`softdeleteflag` AS `alerthistory_softdeleteflag`,
        `alertsubtype`.`Name` AS `alertsubtype_Name`,
        `alertsubtype`.`Description` AS `alertsubtype_Description`,
        `channeltext`.`Description` AS `channeltext_Description`
    FROM `alerthistory`
        JOIN `alertsubtype`ON (`alertsubtype`.`id`=`alerthistory`.`AlertSubTypeId`)
		JOIN `channeltext` ON (`channeltext`.`channelID` = `alerthistory`.`ChannelId`)
        WHERE  (`alerthistory`.`Customer_Id` = _customerId AND `channeltext`.`LanguageCode` = 'en-US') ORDER BY `alerthistory`.`DispatchDate` desc  LIMIT 800;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `approvalmatrixtemplate_cleanup_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `approvalmatrixtemplate_cleanup_proc`(
IN _actionIds TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _cif VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _limitTypeId TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
MAINLABEL: BEGIN
	UPDATE approvalmatrix SET softdeleteflag = 1 WHERE contractId= _contractId AND 
													 coreCustomerId = _cif AND
													 FIND_IN_SET(actionId, _actionIds) AND
													 FIND_IN_SET(limitTypeId, _limitTypeId) AND
													 softdeleteflag = 0;										
    
    UPDATE approvalmatrixtemplate SET softdeleteflag = 1 WHERE contractId= _contractId AND 
																coreCustomerId = _cif AND
                                                                FIND_IN_SET(actionId, _actionIds)AND
																FIND_IN_SET(limitTypeId, _limitTypeId) AND
																softdeleteflag = 0;				
									
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `approvalmatrixtemplate_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `approvalmatrixtemplate_create_proc`(
IN _matrixValues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _matrixApprover TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _isGroupMatrix INT
)
BEGIN
	
	DECLARE index1 INTEGER DEFAULT 0;
	DECLARE index2 INTEGER DEFAULT 0;
    
    set @length = LENGTH(_matrixValues) - LENGTH(REPLACE(_matrixValues, ',', '')) + 1;
	getValues: LOOP
			set index1 = index1 + 1;
			IF index1 = @length + 1 THEN 
				LEAVE getValues;
			else
				set @matrixRecord = SUBSTRING_INDEX( SUBSTRING_INDEX(_matrixValues, ',', index1), ',', -1 );
				set @matrixComma = REPLACE(@matrixRecord, ';', ',');
				set @query = concat('INSERT INTO approvalmatrixtemplate(contractId,coreCustomerId,actionId,approvalruleId,limitTypeId,lowerlimit,upperlimit,isGroupMatrix) VALUES (',@matrixComma,');');
				prepare sql_query from @query;
				execute sql_query;
				
				SET @id = LAST_INSERT_ID();
                
                IF _isGroupMatrix = 0 THEN
					set @customerIds = SUBSTRING_INDEX( SUBSTRING_INDEX(_matrixApprover, ',', index1), ',', -1 );
					IF @customerIds IS NOT NULL AND @customerIds != '' THEN
						set @customerIdsComma = REPLACE(@customerIds, ';', ',');
							set @length2 = LENGTH(@customerIdsComma) - LENGTH(REPLACE(@customerIdsComma, ',', '')) + 1;
							set index2 = 0;
							getCustomerIds: LOOP
								set index2 = index2 + 1;
								IF index2 = @length2 + 1 THEN 
									LEAVE getCustomerIds;
								else
									set @customerId = SUBSTRING_INDEX( SUBSTRING_INDEX(@customerIdsComma, ',', index2), ',', -1 );
									INSERT INTO customerapprovalmatrixtemplate(customerId,approvalMatrixId) values (@customerId,@id);							
									ITERATE getCustomerIds;
								END IF;
							END LOOP getCustomerIds;
						END IF;
					ITERATE  getValues;
				ELSEIF _isGroupMatrix = 1 THEN
					set @sigValues = SUBSTRING_INDEX( SUBSTRING_INDEX(_matrixApprover, '#', index1), '#', -1 );
					SET @id = LAST_INSERT_ID();
					SET @groupList = SUBSTRING_INDEX(@sigValues, ';', 1 );
					SET @groupRule = SUBSTRING_INDEX(@sigValues, ';', -1 );
					INSERT INTO signatorygroupmatrixtemplate(approvalMatrixId, groupList, groupRule) values (@id, @groupList, @groupRule);
               END IF; 
			END IF;
	END LOOP getValues;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `approvalmatrixtemplate_default_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `approvalmatrixtemplate_default_create_proc`(
IN _actionIds TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _cif VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _approvalMode INT
)
MAINLABEL: BEGIN
 DECLARE accountList TEXT DEFAULT "";
 DECLARE limitTypeId_1 varchar(255) DEFAULT "DAILY_LIMIT";
 DECLARE limitTypeId_2 varchar(255) DEFAULT "MAX_TRANSACTION_LIMIT";
 DECLARE limitTypeId_3 varchar(255) DEFAULT "WEEKLY_LIMIT";
 DECLARE actionIndex INTEGER DEFAULT 0;
 DECLARE isGroupMatrix INT DEFAULT 0;
 DECLARE typeId TEXT DEFAULT "";
 
 IF _actionIds IS NULL OR  _actionIds = '' THEN
	LEAVE MAINLABEL;
END IF;

IF _contractId IS NULL OR  _contractId = '' THEN
	LEAVE MAINLABEL;
END IF;
	
IF _cif IS NULL OR  _cif = '' THEN
	LEAVE MAINLABEL;
END IF;
 
IF _approvalMode = 0 THEN
	SET isGroupMatrix = 0;
ELSEIF _approvalMode = 1 THEN
	SET isGroupMatrix = 1;
END IF;

set @numOfActions = LENGTH(_actionIds) - LENGTH(REPLACE(_actionIds, ',', '')) + 1;
set actionIndex = 0; 
getAction: LOOP
	set actionIndex = actionIndex + 1;
	IF actionIndex = @numOfActions + 1 THEN
		LEAVE getAction;
	Else
		set @actionId = SUBSTRING_INDEX(SUBSTRING_INDEX(_actionIds, ',', actionIndex), ',', -1 );
		SELECT Type_id INTO typeId FROM featureaction WHERE id = @actionId;
		IF typeId = "MONETARY" THEN			
			INSERT INTO approvalmatrixtemplate (contractId, actionId, limitTypeId,coreCustomerId, approvalruleId, isGroupMatrix) VALUES
			(_contractId, @actionId, limitTypeId_1,_cif,'NO_APPROVAL',isGroupMatrix);
            INSERT INTO approvalmatrixtemplate (contractId, actionId, limitTypeId,coreCustomerId, approvalruleId, isGroupMatrix) VALUES
			(_contractId, @actionId,limitTypeId_2,_cif,'NO_APPROVAL',isGroupMatrix);
            INSERT INTO approvalmatrixtemplate (contractId, actionId, limitTypeId,coreCustomerId, approvalruleId, isGroupMatrix) VALUES
			(_contractId, @actionId, limitTypeId_3,_cif,'NO_APPROVAL',isGroupMatrix);
		ELSEIF typeId = "NON_MONETARY" THEN
			INSERT INTO approvalmatrixtemplate (contractId, actionId, limitTypeId,coreCustomerId, approvalruleId, isGroupMatrix) VALUES
			(_contractId, @actionId, "NON_MONETARY_LIMIT",_cif,'NO_APPROVAL',isGroupMatrix);
        END IF;
	END IF;
 END LOOP getAction;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `approvalmatrix_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `approvalmatrix_create_proc`(
IN _matrixValues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _approverIds TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

	DECLARE index1 INTEGER DEFAULT 0;
	DECLARE index2 INTEGER DEFAULT 0;
	set @length = LENGTH(_matrixValues) - LENGTH(REPLACE(_matrixValues, ',', '')) + 1;
	getValues: LOOP
			set index1 = index1 + 1;
			IF index1 = @length + 1 THEN 
				LEAVE getValues;
			else
				set @matrixRecord = SUBSTRING_INDEX( SUBSTRING_INDEX(_matrixValues, ',', index1), ',', -1 );
				set @matrixComma = REPLACE(@matrixRecord, ';', ',');
				set @query = concat('INSERT INTO approvalmatrix(name,contractId,coreCustomerId,actionId,accountId,approvalruleId,limitTypeId,lowerlimit,upperlimit) VALUES (',@matrixComma,');');
				prepare sql_query from @query;
				execute sql_query;
				
				SET @id = LAST_INSERT_ID();
				set @customerIds = SUBSTRING_INDEX( SUBSTRING_INDEX(_approverIds, ',', index1), ',', -1 );
                IF @customerIds IS NOT NULL AND @customerIds != '' THEN
					set @customerIdsComma = REPLACE(@customerIds, ';', ',');
						set @length2 = LENGTH(@customerIdsComma) - LENGTH(REPLACE(@customerIdsComma, ',', '')) + 1;
						set index2 = 0;
						getCustomerIds: LOOP
							set index2 = index2 + 1;
							IF index2 = @length2 + 1 THEN 
								LEAVE getCustomerIds;
							else
								set @customerId = SUBSTRING_INDEX( SUBSTRING_INDEX(@customerIdsComma, ',', index2), ',', -1 );
								INSERT INTO customerapprovalmatrix(customerId,approvalMatrixId) values (@customerId,@id);							
								ITERATE getCustomerIds;
							END IF;
						END LOOP getCustomerIds;
					END IF;
				ITERATE  getValues;
			END IF;
	END LOOP getValues;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `approvalmatrix_default_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `approvalmatrix_default_create_proc`(
IN _actionIds TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _accountIds TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _cif VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
MAINLABEL: BEGIN
 DECLARE accountList TEXT DEFAULT "";
 DECLARE limitTypeId_1 varchar(255) DEFAULT "DAILY_LIMIT";
 DECLARE limitTypeId_2 varchar(255) DEFAULT "MAX_TRANSACTION_LIMIT";
 DECLARE limitTypeId_3 varchar(255) DEFAULT "WEEKLY_LIMIT";
 DECLARE accountIndex INTEGER DEFAULT 0;
 DECLARE actionIndex INTEGER DEFAULT 0;
 DECLARE typeId TEXT DEFAULT "";
 
IF _actionIds IS NULL OR  _actionIds = '' THEN
	LEAVE MAINLABEL;
END IF;
	
IF _contractId IS NULL OR  _contractId = '' THEN
	LEAVE MAINLABEL;
END IF;
	
IF _cif IS NULL OR  _cif = '' THEN
	LEAVE MAINLABEL;
END IF;

IF _accountIds IS NULL OR  _accountIds = '' THEN
	LEAVE MAINLABEL;
END IF;
 
set @numOfAccounts = LENGTH(_accountIds) - LENGTH(REPLACE(_accountIds, ',', '')) + 1;
set @numOfActions = LENGTH(_actionIds) - LENGTH(REPLACE(_actionIds, ',', '')) + 1;
getAccount: LOOP
	set accountIndex = accountIndex + 1;
	IF accountIndex = @numOfAccounts + 1 THEN 
		LEAVE getAccount;
	Else
		set @accountId = SUBSTRING_INDEX(SUBSTRING_INDEX(_accountIds, ',', accountIndex), ',', -1 );
		set actionIndex = 0; 
		 getAction: LOOP
            set actionIndex = actionIndex + 1;
            IF actionIndex = @numOfActions + 1 THEN
				LEAVE getAction;
			Else
				set @actionId = SUBSTRING_INDEX(SUBSTRING_INDEX(_actionIds, ',', actionIndex), ',', -1 );
                SELECT Type_id INTO typeId FROM featureaction WHERE id = @actionId;
                IF typeId = "MONETARY" THEN			
					INSERT INTO approvalmatrix (contractId, name, accountId, actionId, limitTypeId,coreCustomerId, approvalruleId) VALUES
					(_contractId, concat(@actionId, "_", @accountId, "_", limitTypeId_1, "_", _contractId), @accountId, @actionId, limitTypeId_1,_cif,'NO_APPROVAL');
					INSERT INTO approvalmatrix (contractId, name, accountId, actionId, limitTypeId,coreCustomerId, approvalruleId) VALUES
					(_contractId, concat(@actionId, "_", @accountId, "_", limitTypeId_2, "_", _contractId), @accountId,@actionId,limitTypeId_2,_cif,'NO_APPROVAL');
					INSERT INTO approvalmatrix (contractId, name, accountId, actionId, limitTypeId,coreCustomerId, approvalruleId) VALUES
					(_contractId, concat(@actionId, "_", @accountId, "_", limitTypeId_3, "_", _contractId),@accountId, @actionId, limitTypeId_3,_cif,'NO_APPROVAL');
                ELSEIF typeId = "NON_MONETARY" THEN
					INSERT INTO approvalmatrix (contractId, name, accountId, actionId, limitTypeId,coreCustomerId, approvalruleId) VALUES
                    (_contractId, concat(@actionId, "_", @accountId, "_", "NON_MONETARY_LIMIT", "_", _contractId), @accountId, @actionId, "NON_MONETARY_LIMIT",_cif,'NO_APPROVAL');
				END IF;
			END IF;
		 END LOOP getAction;
        set accountList = CONCAT(@accountId,",",accountList);
	END IF;
END LOOP getAccount;  

SET accountList = (select SUBSTRING(accountList FROM 1 FOR (CHAR_LENGTH(accountList)-1)));
select accountList;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `approvalmatrix_default_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `approvalmatrix_default_delete_proc`(
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _cif VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _filterColumnIds TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _filterColumnName VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
 
 IF _filterColumnName = "actionId" THEN
	 DELETE FROM approvalmatrix where contractId = _contractId and coreCustomerId = _cif and FIND_IN_SET(actionId,_filterColumnIds) COLLATE utf8_general_ci;
 ELSEIF _filterColumnName = "accountId" THEN
 	 DELETE FROM approvalmatrix where contractId = _contractId and coreCustomerId = _cif and FIND_IN_SET(accountId,_filterColumnIds) COLLATE utf8_general_ci;
 ELSEIF _filterColumnName = "cif" THEN
 	 DELETE FROM approvalmatrix where contractId = _contractId and FIND_IN_SET(coreCustomerId,_filterColumnIds) COLLATE utf8_general_ci;
 END IF;
 	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `approvalmatrix_fetch_grouprecords_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `approvalmatrix_fetch_grouprecords_proc`(
    IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _cif VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _accountId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _limitTypeId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _actions TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
    )
BEGIN 
    IF _cif = "" THEN
    SET _cif = "%";
    END IF;
    
    IF _accountId = "" THEN
    SET _accountId = "%";
    END IF;
    
    IF _limitTypeId = "" THEN
    SET _limitTypeId = "%";
    END IF;
    
    SELECT 
    	
      	`approvalMatrix`.`id`,
        `approvalMatrix`.`contractId`,
         `approvalMatrix`.`accountId`,
         `approvalMatrix`.`limitTypeId`,
         `featureAction`.`id` AS `actionId`,
         `featureAction`.`name` AS `actionName`,
         `featureAction`.`description` AS `actionDescription`,
         `featureAction`.`Feature_id` AS `featureId`,
		 `featureAction`.`Type_id` AS `actionType`,
         `feature`.`name` AS `featureName`,
         `feature`.`Status_id` AS `fifeaturestatus`,
         `approvalRule`.`id` AS `approvalruleId`,
         `approvalRule`.`numberOfApprovals`,
         `approvalRule`.`name` AS `approvalRuleName`,
         `approvalMatrix`.`lowerlimit`,
         `approvalMatrix`.`upperlimit`,
		 `signatoryGroupMatrix`.`groupList` AS `groupList`,
		 `signatoryGroupMatrix`.`groupRule` AS `groupRule`,
         `contractcorecustomers`.`coreCustomerId` AS `cifId`,
         `contractcorecustomers`.`coreCustomerName` AS `cifName`,
         `approvalMatrix`.`invalid`,
         `approvalMatrix`.`isGroupMatrix`
        FROM ((((((`approvalmatrix` AS `approvalMatrix`
        LEFT JOIN
        `signatorygroupmatrix` AS `signatoryGroupMatrix`
        ON `approvalMatrix`.`id` = `signatoryGroupMatrix`.`approvalMatrixId`)
        LEFT JOIN
        `featureaction` AS `featureAction`
        ON `approvalMatrix`.`actionId` = `featureAction`.`id`)
        LEFT JOIN
        `approvalrule` AS `approvalRule`
        ON `approvalMatrix`.`approvalruleId` = `approvalRule`.`id`) 
      LEFT JOIN
        `feature` AS `feature`
        ON `featureAction`.`Feature_id` = `feature`.`id`)
      LEFT JOIN
        `contractfeatures` AS `contractfeatures`
        ON `feature`.`id` = `contractfeatures`.`featureId`
          and  `approvalMatrix`.`contractId` = `contractfeatures`.`contractId`
          and `approvalMatrix`.`coreCustomerId` = `contractfeatures`.`coreCustomerId`)
      LEFT JOIN
        `contractcorecustomers` AS `contractcorecustomers`
        ON `approvalMatrix`.`contractId`  = `contractcorecustomers`.`contractId` AND `approvalMatrix`.`coreCustomerId` = `contractcorecustomers`.`coreCustomerId`)          
    WHERE 
    `approvalMatrix`.`contractId` = `_contractId` AND
    `approvalMatrix`.`coreCustomerId` LIKE `_cif` AND
    `approvalMatrix`.`accountId` LIKE `_accountId` AND 
    `approvalMatrix`.`isGroupMatrix` = 1 AND
    FIND_IN_SET(`approvalMatrix`.`actionId`,`_actions`) > 0 AND 
    `approvalMatrix`.`limitTypeId` LIKE `_limitTypeId` AND 
    `approvalMatrix`.`softdeleteflag` = 0 AND
	 `featureAction`.`approveFeatureAction` is not null AND
	 `featureAction`.`approveFeatureAction` != '' AND
	`featureAction`.`status` = 'SID_ACTION_ACTIVE'
        ORDER BY `approvalMatrix`.`contractId`,`approvalMatrix`.`coreCustomerId`,`approvalMatrix`.`accountId`,`approvalMatrix`.`limitTypeId`,`approvalMatrix`.`actionId` ,`approvalMatrix`.`lowerlimit`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `approvalmatrix_fetch_records_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `approvalmatrix_fetch_records_proc`(
    IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _cif VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _accountId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _limitTypeId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _actions TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
    )
BEGIN 
    IF _cif = "" THEN
    SET _cif = "%";
    END IF;
    
    IF _accountId = "" THEN
    SET _accountId = "%";
    END IF;
    
    IF _limitTypeId = "" THEN
    SET _limitTypeId = "%";
    END IF;
    
    SELECT 
    	
      	`approvalMatrix`.`id`,
        `approvalMatrix`.`contractId`,
         `approvalMatrix`.`accountId`,
         `approvalMatrix`.`limitTypeId`,
         `featureAction`.`id` AS `actionId`,
         `featureAction`.`name` AS `actionName`,
         `featureAction`.`description` AS `actionDescription`,
         `featureAction`.`Feature_id` AS `featureId`,
		 `featureAction`.`Type_id` AS `actionType`,
         `feature`.`name` AS `featureName`,
         `feature`.`Status_id` AS `fifeaturestatus`,
         `approvalRule`.`id` AS `approvalruleId`,
         `approvalRule`.`numberOfApprovals`,
         `approvalRule`.`name` AS `approvalRuleName`,
         `approvalMatrix`.`lowerlimit`,
         `approvalMatrix`.`upperlimit`,
         `customer`.`id` AS `customerId`,
         `customer`.`FirstName` AS `firstName`,
         `customer`.`LastName` AS `lastName`,
         `contractcorecustomers`.`coreCustomerId` AS `cifId`,
         `contractcorecustomers`.`coreCustomerName` AS `cifName`,
         `approvalMatrix`.`invalid`,
         `approvalMatrix`.`isGroupMatrix`
        FROM (((((((`approvalmatrix` AS `approvalMatrix`
        LEFT JOIN
        `customerapprovalmatrix` AS `customerApprovalMatrix`
        ON `approvalMatrix`.`id` = `customerApprovalMatrix`.`approvalMatrixId`)
        LEFT JOIN
        `customer` AS `customer`
        ON `customerApprovalMatrix`.`customerId` = `customer`.`id`)
        LEFT JOIN
        `featureaction` AS `featureAction`
        ON `approvalMatrix`.`actionId` = `featureAction`.`id`)
        LEFT JOIN
        `approvalrule` AS `approvalRule`
        ON `approvalMatrix`.`approvalruleId` = `approvalRule`.`id`) 
      LEFT JOIN
        `feature` AS `feature`
        ON `featureAction`.`Feature_id` = `feature`.`id`)
      LEFT JOIN
        `contractfeatures` AS `contractfeatures`
        ON `feature`.`id` = `contractfeatures`.`featureId`
          and  `approvalMatrix`.`contractId` = `contractfeatures`.`contractId`
          and `approvalMatrix`.`coreCustomerId` = `contractfeatures`.`coreCustomerId`)
      LEFT JOIN
        `contractcorecustomers` AS `contractcorecustomers`
        ON `approvalMatrix`.`contractId`  = `contractcorecustomers`.`contractId` AND `approvalMatrix`.`coreCustomerId` = `contractcorecustomers`.`coreCustomerId`)          
    WHERE 
    `approvalMatrix`.`contractId` = `_contractId` AND
    `approvalMatrix`.`coreCustomerId` LIKE `_cif` AND
    `approvalMatrix`.`accountId` LIKE `_accountId` AND 
    FIND_IN_SET(`approvalMatrix`.`actionId`,`_actions`) > 0 AND 
    `approvalMatrix`.`limitTypeId` LIKE `_limitTypeId` AND 
    `approvalMatrix`.`softdeleteflag` = 0 AND
	 `featureAction`.`approveFeatureAction` is not null AND
	 `featureAction`.`approveFeatureAction` != '' AND
	`featureAction`.`status` = 'SID_ACTION_ACTIVE'
        ORDER BY `approvalMatrix`.`contractId`,`approvalMatrix`.`coreCustomerId`,`approvalMatrix`.`accountId`,`approvalMatrix`.`limitTypeId`,`approvalMatrix`.`actionId` ,`approvalMatrix`.`lowerlimit`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `approvalmatrix_signatorygroupmatrixcreate_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `approvalmatrix_signatorygroupmatrixcreate_proc`(
IN _matrixValues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _signatorymatrixValues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

	DECLARE index1 INTEGER DEFAULT 0;
	DECLARE index2 INTEGER DEFAULT 0;
	set @length = LENGTH(_matrixValues) - LENGTH(REPLACE(_matrixValues, ',', '')) + 1;
	getValues: LOOP
			set index1 = index1 + 1;
			IF index1 = @length + 1 THEN 
				LEAVE getValues;
			else
				set @matrixRecord = SUBSTRING_INDEX( SUBSTRING_INDEX(_matrixValues, ',', index1), ',', -1 );
				set @matrixComma = REPLACE(@matrixRecord, ';', ',');
				set @query = concat('INSERT INTO approvalmatrix(name,contractId,coreCustomerId,actionId,accountId,approvalruleId,isGroupMatrix,limitTypeId,lowerlimit,upperlimit) VALUES (',@matrixComma,');');
				prepare sql_query from @query;
				execute sql_query;
				
				set @sigValues = SUBSTRING_INDEX( SUBSTRING_INDEX(_signatorymatrixValues, '#', index1), '#', -1 );
				SET @id = LAST_INSERT_ID();
				SET @groupList = SUBSTRING_INDEX(@sigValues, ';', 1 );
				SET @groupRule = SUBSTRING_INDEX(@sigValues, ';', -1 );
				INSERT INTO signatorygroupmatrix(approvalMatrixId, groupList, groupRule) values (@id, @groupList, @groupRule);
                
				ITERATE  getValues;
			END IF;
	END LOOP getValues;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `approvalmatrix_update_softdeleteflag_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `approvalmatrix_update_softdeleteflag_proc`(
	IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _cif VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _accountIds TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _actionId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _limitTypeId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
 UPDATE approvalmatrix SET softdeleteflag = 1 WHERE contractId= _contractId AND 
													 coreCustomerId = _cif AND
													 FIND_IN_SET(accountId, _accountIds) AND 
													 actionId = _actionId AND 
													 limitTypeId = _limitTypeId COLLATE utf8_general_ci;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `approvalrequest_counts_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `approvalrequest_counts_proc`(
	in _customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    in _approveActionList TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
    in _createActionList TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
	)
MAINLABEL:BEGIN

		SET SESSION group_concat_max_len = 100000000;
		
	    SELECT 0 as count, 'ACHTransactionsForMyApproval' as TransactionType
	    UNION 
	    SELECT 0 as count, 'ACHFilesForMyApproval' as TransactionType
	    UNION
        SELECT 0 as count, 'GeneralTransactionsForMyApproval' as TransactionType
	    UNION
	    SELECT 0 as count, 'myRequestsWaiting' as TransactionType
	    UNION 
	    SELECT 0 as count, 'myRequestsRejected' as TransactionType
	    UNION 
	    SELECT 0 as count, 'myRequestsApproved' as TransactionType;
	            
	    SET @companyId = (SELECT Organization_Id FROM customer WHERE id =_customerId);
        
        IF @companyId is NULL THEN      
			SET @companyId = "";
		END IF;
        
        IF _approveActionList is NULL THEN      
			SET _approveActionList = "";
		END IF;
        
        IF _createActionList is NULL THEN      
			SET _createActionList = "";
		END IF;
        
        SET @features = (SELECT group_concat(Feature_id SEPARATOR ",") FROM featureaction WHERE FIND_IN_SET(id,_approveActionList) > 0);
        
        IF @features is NULL THEN      
			SET @features = "";
		END IF;
        
        SET @createApproveActions = (SELECT group_concat(id SEPARATOR ",") FROM featureaction WHERE FIND_IN_SET(Feature_id, @features) > 0 AND (id LIKE "%_CREATE" OR id LIKE "%_UPLOAD"));
        
        IF @createApproveActions is NULL THEN      
			SET @createApproveActions = "";
		END IF;
	    
	    SET @customerMatrixIds = (SELECT group_concat(approvalMatrixId SEPARATOR ",") FROM customerapprovalmatrix where customerId = _customerId);
	    
	    IF @customerMatrixIds is NULL THEN      
			SET @customerMatrixIds = "";
		END IF;
		
		SET @alreadyApprovedIds = (select GROUP_CONCAT(DISTINCT(requestId) SEPARATOR ",") from bbactedrequest where createdby = _customerId AND action = 'Approved');
        
        IF @alreadyApprovedIds is NULL THEN      
			SET @alreadyApprovedIds = "";
		END IF;
        
        SET @approvalRequestIds = (SELECT group_concat(DISTINCT(requestId) SEPARATOR ",") FROM requestapprovalmatrix WHERE FIND_IN_SET(requestapprovalmatrix.approvalMatrixId,  @customerMatrixIds) AND NOT FIND_IN_SET(requestapprovalmatrix.requestId, @alreadyApprovedIds));
	    
        IF @approvalRequestIds is NULL THEN      
			SET @approvalRequestIds = "";
		END IF;
        
	    set @select_statement = concat("select count(requestId) as count,
								TransactionType from (
									(select 
										DISTINCT(bbrequest.requestId),
										if(bbrequest.featureActionId like 'ACH_FILE%', 'ACHFilesForMyApproval' ,if(bbrequest.featureActionId like 'ACH%','ACHTransactionsForMyApproval', 'GeneralTransactionsForMyApproval')) as TransactionType,
										bbrequest.createdby,
										bbrequest.companyId,
										bbrequest.status
									FROM 
										(`bbrequest` 
										LEFT JOIN `requestapprovalmatrix` ON (bbrequest.requestId = requestapprovalmatrix.requestId ))
									WHERE FIND_IN_SET(bbrequest.requestId, \"", @approvalRequestIds,"\") AND bbrequest.companyId = ", @companyId
	                                ," AND 
	                                FIND_IN_SET(bbrequest.featureActionId, \"", @createApproveActions, "\") AND
	                                bbrequest.status = 'Pending') as tablea) 
	                                group BY TransactionType"); 
	                                
		set @select_statement = concat(@select_statement, " UNION select count(requestId) as count,
								TransactionType from (
									(select 
										DISTINCT(bbrequest.requestId),
										if(bbrequest.status = 'Pending', 'myRequestsWaiting', if(bbrequest.status = 'Rejected', 'myRequestsRejected', if(bbrequest.status = 'Approved', 'myRequestsApproved', 'myRequestsWithdrawn'))) as TransactionType,
										bbrequest.createdby,
										bbrequest.companyId,
										bbrequest.status
									FROM 
										`bbrequest` WHERE bbrequest.companyId = ", @companyId," AND 
	                                    FIND_IN_SET(bbrequest.featureActionId, \"", _createActionList, "\") AND
	                                    bbrequest.createdby = ", quote(_customerId),") as tableb) 
	                                group BY TransactionType");
		
	    IF @select_statement is NULL THEN      
			LEAVE MAINLABEL;
		END IF;
        
	    -- select @select_statement;
		PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `approval_matrix_manual_cleanup_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `approval_matrix_manual_cleanup_proc`(
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _coreCustomerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

UPDATE approvalmatrix set softdeleteflag = '1' where coreCustomerId = _coreCustomerId;

SET @customerAccounts = (SELECT group_concat(distinct accountId SEPARATOR ",") from contractaccounts WHERE (contractId = _contractId AND coreCustomerId=_coreCustomerId));

SET @customerActions = (SELECT group_concat(distinct actionId SEPARATOR ",") from contractactionlimit LEFT JOIN featureaction ON (featureaction.id = contractactionlimit.actionId) WHERE (contractId = _contractId AND coreCustomerId=_coreCustomerId) AND featureaction.approveFeatureAction IS NOT NULL);

call approvalmatrix_default_create_proc(@customerActions, _contractId, @customerAccounts, _coreCustomerId);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `archived_request_search_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `archived_request_search_proc`(
  in _dateInitialPoint char(50), 
  in _dateFinalPoint char(50), 
  in _requestStatusID char(50), 
  in _requestCategory char(50), 
  in _offset char(50), 
  in _sortCriteria char(50), 
  in _sortOrder char(50), 
  in _requestAssignedTo char(50), 
  in _searchKey char(50), 
  in _messageRepliedBy char(50), 
  in _recordsPerPage varchar(50),
  in _queryType char(50)
)
BEGIN 
SET 
  @selectClause := IF(
    IFNULL(_queryType, '') = "count", 
    'count(acr.id) AS cnt', 
    'acr.id AS customerrequest_id'
  );
SET 
  @stmt := CONCAT(
    'SELECT ', @selectClause, ' FROM archivedcustomerrequest acr '
  );
SET 
  @whereclause := ' WHERE true';
SET 
  @joinCustomer := 0;
IF _dateInitialPoint != '' 
AND _dateFinalPoint != '' THEN 
SET 
  @whereclause = CONCAT(
    @whereclause, " 
    AND TIMESTAMP(DATE(acr.createdts)) >= ", 
    quote(_dateInitialPoint), " 
    AND ", 
    " TIMESTAMP(DATE(acr.createdts)) <= ", quote(_dateFinalPoint)
  );
ELSEIF _dateInitialPoint != '' THEN 
SET 
  @whereclause = CONCAT(
    @whereclause, " 
    AND TIMESTAMP(DATE(acr.createdts)) = ", 
    quote(_dateInitialPoint)
  );
ELSEIF _dateFinalPoint != '' THEN 
SET 
  @whereclause = CONCAT(
    @whereclause, " 
    AND TIMESTAMP(DATE(acr.createdts)) = ", 
    quote(_dateFinalPoint)
  );
END IF;
IF _requestStatusID != '' THEN 
SET 
  @whereclause = CONCAT(
    @whereclause, " 
    AND acr.Status_id IN 
    (
      ", 
    func_escape_input_for_in_operator(_requestStatusID), "
    )
    "
  );
END IF;
IF _requestCategory != '' THEN 
SET 
  @whereclause = CONCAT(
    @whereclause, " 
    AND acr.RequestCategory_id = ", 
    quote(_requestCategory)
  );
END IF;
IF _requestAssignedTo != '' THEN 
SET 
  @whereclause = CONCAT(
    @whereclause, " 
    AND acr.AssignedTo = ", 
    quote(_requestAssignedTo)
  );
END IF;
IF _messageRepliedBy != '' THEN 
SET 
  @stmt = CONCAT(
    @stmt, 'LEFT JOIN archivedrequestmessage ON (acr.id = archivedrequestmessage.CustomerRequest_id) '
  );
SET 
  @whereclause = CONCAT(
    @whereclause, " 
  AND archivedrequestmessage.RepliedBy_id = ", 
    quote(_messageRepliedBy)
  );
END IF;
IF _searchKey != '' THEN 
SET 
  @joinCustomer = 1;
SET _searchKey=CONCAT("%",_searchKey,"%");
SET 
  @whereclause = CONCAT(
    @whereclause, " 
  AND 
  (
    acr.Customer_id LIKE ", 
    quote(_searchKey), " OR acr.id LIKE ", 
    quote(_searchKey), " OR customer.UserName LIKE ", 
    quote(_searchKey), ")"
  );
END IF;
IF _queryType != 'count' THEN IF _sortCriteria = 'customer_Fullname' THEN 
SET 
  @joinCustomer = 1;
END IF;

SET @sortColumn='acr.lastmodifiedts';
IF( _sortCriteria = 'customerrequest_Customer_id') THEN
    SET @sortColumn = 'acr.Customer_id';
ELSEIF( _sortCriteria = 'customerrequest_AssignedTo') THEN
    SET @sortColumn = 'acr.AssignedTo';
ELSEIF( _sortCriteria = 'customerrequest_createdts') THEN
    SET @sortColumn = 'acr.createdts';
ELSEIF( _sortCriteria = 'customerrequest_RequestCategory_id') THEN
    SET @sortColumn = 'acr.RequestCategory_id';
ELSEIF( _sortCriteria = 'customer_Fullname') THEN
    SET @sortColumn = 'CONCAT(customer.FirstName,customer.LastName)';
ELSEIF( _sortCriteria = 'customerrequest_Status_id') THEN
    SET @sortColumn = 'acr.Status_id';
ELSEIF( _sortCriteria = 'customerrequest_AssignedTo_Name') THEN
    SET @sortColumn = 'CONCAT(systemuser.FirstName,systemuser.LastName)';
END IF;

SET 
  @whereclause = CONCAT(
    @whereclause, 
    " 
  ORDER BY
    ", 
    IF(
      @sortColumn = '', 'acr.lastmodifiedts', 
      @sortColumn
    ), 
    ' ', 
    IF(
      @sortColumn = '' 
      OR _sortOrder = '', 
      'DESC', 
      _sortOrder
    )
  );
SET 
  @whereclause = CONCAT(
    @whereclause, 
    " LIMIT ", 
    IF(_offset = '', '0', _offset),
    ',',
    IF(_recordsPerPage = '', '10', _recordsPerPage)
  );
END IF;
IF @joinCustomer = 1 THEN 
SET 
  @stmt = CONCAT(
    @stmt, 'LEFT JOIN customer ON (acr.Customer_id = customer.id) '
  );
END IF;
IF _sortCriteria = 'customerrequest_AssignedTo_Name' THEN 
SET 
  @stmt = CONCAT(
    @stmt, 'LEFT JOIN systemuser ON (acr.AssignedTo = systemuser.id) '
  );
END IF;
SET 
  @stmt = CONCAT(@stmt, @whereclause);
PREPARE stmt 
FROM 
  @stmt;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `authorizationCheckForRejectAndWithdrawl_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `authorizationCheckForRejectAndWithdrawl_proc`(
IN _requestId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _companyId TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _featureactionlist TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

	SET SESSION group_concat_max_len = 100000000;
	
	SET @features = (SELECT group_concat(Feature_id SEPARATOR ",") FROM featureaction WHERE FIND_IN_SET(id,_featureactionlist) > 0);
	IF @features is NULL THEN      
		SET @features = "";
	END IF;
	
	SET @createActions = (SELECT group_concat(id SEPARATOR ",") FROM featureaction WHERE FIND_IN_SET(Feature_id, @features) > 0 AND approveFeatureAction is not NULL);
	
	IF @createActions is NULL THEN      
		SET @createActions = "";
	END IF;  
	
	SELECT * FROM bbrequest WHERE requestId = _requestId AND FIND_IN_SET(companyId, _companyId) AND FIND_IN_SET(featureActionId, @createActions);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `auto_reject_invalid_pending_requests_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `auto_reject_invalid_pending_requests_proc`( 
	IN _customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
MAINLABEL:BEGIN
	
		SET SESSION group_concat_max_len = 100000000;

        SET @oldMatrixIds = (SELECT group_concat(approvalMatrixId SEPARATOR ",") FROM customerapprovalmatrix where customerId = _customerId);
        
        IF @oldMatrixIds is NULL THEN      
			LEAVE MAINLABEL;
		END IF;
        
        SET @newMatrixIds = (SELECT group_concat(approvalMatrixId SEPARATOR ",") 
								FROM customerapprovalmatrix 
                                LEFT JOIN
								`approvalmatrix` ON (`customerapprovalmatrix`.`approvalMatrixId` = `approvalmatrix`.`id`)
                                INNER JOIN
									(SELECT DISTINCT Account_id,
                                    featureaction.Feature_id as featureId,
									(select id from featureaction where featureaction.Feature_id = featureId and featureaction.Type_id = 'MONETARY') as Action_id
									FROM 
									customeraction 
                                    INNER JOIN featureaction ON (customeraction.Action_id = featureaction.id)
									WHERE Customer_id = _customerId
									AND isAllowed = '1' 
									AND Account_id is NOT null
									AND Action_id like '%_APPROVE') as 
								`can` ON ((`approvalmatrix`.`actionId` = `can`.`Action_id`) 
											AND (`approvalmatrix`.`accountId` = `can`.`Account_id`))
								where customerId = _customerId);
		
        IF @newMatrixIds is NULL THEN      
			SET @newMatrixIds = "";
		END IF;
         
		SET @invalidMatrixIds = ( SELECT GROUP_CONCAT(id SEPARATOR ",") 
									FROM `approvalmatrix` 
										WHERE FIND_IN_SET(`approvalmatrix`.`id`, @oldMatrixIds)
                                        AND NOT FIND_IN_SET(`approvalmatrix`.`id`, @newMatrixIds)
                                        );
								
        IF @invalidMatrixIds is NULL THEN 
			LEAVE MAINLABEL;
		END IF;
        
        SET @invalidRequestIds = ( SELECT GROUP_CONCAT(DISTINCT(`bbrequest`.`requestId`) SEPARATOR ",") 
									FROM 
                                    `bbrequest`
                                    LEFT JOIN 
									`requestapprovalmatrix` ON (`bbrequest`.`requestId` = `requestapprovalmatrix`.`requestId`)
                                    INNER JOIN 
                                    (SELECT 
										`approvalmatrix`.`id` as approvalMatrixId,
										`approvalrule`.`numberOfApprovals` as numberOfApprovalsRequired,
										(SELECT count(DISTINCT(customerId)) FROM customerapprovalmatrix WHERE approvalMatrixId = `approvalmatrix`.`id`) as numberOfApprovers
										FROM 
										`approvalmatrix`
										LEFT JOIN
										`approvalrule` ON (`approvalmatrix`.`approvalruleId` = `approvalrule`.`id`)
									) as `temp_request_table` ON (`requestapprovalmatrix`.`approvalMatrixId` = `temp_request_table`.`approvalMatrixId`)
									WHERE  
                                    FIND_IN_SET(`requestapprovalmatrix`.`approvalMatrixId`, @invalidMatrixIds)
                                    AND 
                                    ( temp_request_table.numberOfApprovalsRequired = '-1' 
										OR 
									  temp_request_table.numberOfApprovalsRequired >= temp_request_table.numberOfApprovers )
									);
		
		SET SQL_SAFE_UPDATES = 0;
		
		SET @select_statement = concat("
										DELETE FROM customerapprovalmatrix WHERE approvalMatrixId in (",@invalidMatrixIds,") AND customerId = '",_customerId,"'
									");
        PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
        
		SET @select_statement = concat("
										UPDATE approvalmatrix SET invalid = '1' WHERE approvalmatrix.id in  (",@invalidMatrixIds,")
									");
        PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
        
        IF @invalidRequestIds is NULL THEN   
        	SET SQL_SAFE_UPDATES = 1;   
			LEAVE MAINLABEL;
		END IF;
        
		SET @select_statement = concat("
										UPDATE bbrequest SET status = 'Rejected' WHERE bbrequest.requestId in  (",@invalidRequestIds,")
									");
		PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
		
		SET @select_statement = concat("
										UPDATE billpaytransfers 
												INNER JOIN bbrequest ON billpaytransfers.requestId = bbrequest.requestId
                                                SET billpaytransfers.status =  'Rejected'
                                                WHERE billpaytransfers.requestId in (",@invalidRequestIds,")
									");
		PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
        
        SET @select_statement = concat("
										UPDATE ownaccounttransfers 
												INNER JOIN bbrequest ON ownaccounttransfers.requestId = bbrequest.requestId
                                                SET ownaccounttransfers.status =  'Rejected'
                                                WHERE ownaccounttransfers.requestId in (",@invalidRequestIds,")
									");
		PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
        
        SET @select_statement = concat("
										UPDATE interbankfundtransfers 
												INNER JOIN bbrequest ON interbankfundtransfers.requestId = bbrequest.requestId
                                                SET interbankfundtransfers.status =  'Rejected'
                                                WHERE interbankfundtransfers.requestId in (",@invalidRequestIds,")
									");
		PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
        
        SET @select_statement = concat("
										UPDATE intrabanktransfers 
												INNER JOIN bbrequest ON intrabanktransfers.requestId = bbrequest.requestId
                                                SET intrabanktransfers.status =  'Rejected'
                                                WHERE intrabanktransfers.requestId in (",@invalidRequestIds,")
									");
		PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
        
        SET @select_statement = concat("
										UPDATE p2ptransfers 
												INNER JOIN bbrequest ON p2ptransfers.requestId = bbrequest.requestId
                                                SET p2ptransfers.status =  'Rejected'
                                                WHERE p2ptransfers.requestId in (",@invalidRequestIds,")
									");
		PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
        
        SET @select_statement = concat("
										UPDATE wiretransfers 
												INNER JOIN bbrequest ON wiretransfers.requestId = bbrequest.requestId
                                                SET wiretransfers.status =  'Rejected'
                                                WHERE wiretransfers.requestId in (",@invalidRequestIds,")
									");
		PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
        
        SET @select_statement = concat("
										UPDATE internationalfundtransfers 
												INNER JOIN bbrequest ON internationalfundtransfers.requestId = bbrequest.requestId
                                                SET internationalfundtransfers.status =  'Rejected'
                                                WHERE internationalfundtransfers.requestId in (",@invalidRequestIds,")
									");
		PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
        
        SET @select_statement = concat("
										UPDATE achtransaction 
												INNER JOIN bbrequest ON achtransaction.requestId = bbrequest.requestId
                                                SET achtransaction.status =  'Rejected'
                                                WHERE achtransaction.requestId in (",@invalidRequestIds,")
									");
		PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
        
        SET @select_statement = concat("
										UPDATE achfile 
												INNER JOIN bbrequest ON achfile.requestId = bbrequest.requestId
                                                SET achfile.status =  'Rejected'
                                                WHERE achfile.requestId in (",@invalidRequestIds,")
									");
		PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
        
        SET @companyId = (SELECT Organization_Id FROM customer WHERE id =_customerId);
        IF @companyId is NULL THEN      
			LEAVE MAINLABEL;
		END IF;
        
        SET @numOfParams = 0;
        IF LENGTH(@invalidRequestIds) > 0 THEN
			SET @numOfParams = LENGTH(@invalidRequestIds) - LENGTH(REPLACE(@invalidRequestIds, ',', '')) + 1;
		END IF;
        
        SET @select_statement = "";
		SET @idx = 1;
		LogAction:LOOP
			IF @idx > @numOfParams THEN 
				LEAVE LogAction;
			END IF;
			
			SET @requestId = SUBSTRING_INDEX(SUBSTRING_INDEX(@invalidRequestIds, ',', @idx), ',', -1 );
            
            SET @select_statement = concat("
										INSERT INTO bbactedrequest (`requestId`, `companyId`, `comments`, `status`, `action`)
											VALUES ('",@requestId,"' , '",@companyId,"' , 'Rejected by system as one of the approver lost his permission', 'Rejected', 'Rejected');
									");
          	PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
            
			SET @idx = @idx + 1;
			 
		END LOOP LogAction;
		
		SET SQL_SAFE_UPDATES = 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bbrequest_updatecounter_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `bbrequest_updatecounter_proc`(
	IN _requestId BIGINT,
	IN `_counter` INT)
BEGIN
	SET _counter = if(_counter = NULL OR _counter = "" , 0 , _counter);
   UPDATE bbrequest
   SET `receivedSets` = `receivedSets` + _counter
  	WHERE `requestID` = `_requestId`;
  	
  	SELECT * FROM `bbrequest` WHERE `requestID` = `_requestId`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bbrequest_updatestatus_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `bbrequest_updatestatus_proc`(
	IN _requestId BIGINT, IN _status VARCHAR(50))
BEGIN
  	
  	UPDATE bbrequest
   SET `status` = `_status`
   WHERE	`requestID` = `_requestId`;
  	
  	SELECT * FROM `bbrequest` WHERE `requestID` = `_requestId`;
  	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bulkpayment_request_po_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `bulkpayment_request_po_create_proc`(
IN _povalues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
	DECLARE index1 INTEGER DEFAULT 0;
    SET @numOfPOs = LENGTH(_povalues) - LENGTH(REPLACE(_povalues, '|', '')) + 1;
		insertPOs : LOOP
        SET index1 = index1 + 1;
        IF index1 = @numOfPOs + 1 THEN
            LEAVE insertPOs;
        ELSE
        SET @posData = SUBSTRING_INDEX(SUBSTRING_INDEX(_povalues, '|', index1), '|', -1 );
            SET @query = CONCAT('INSERT INTO bulkpaymentrequestpos(paymentrequestPOId,paymentrequestId,paymentOrderId,templateId,confirmationNumber,recipientName,accountNumber,bankName,swift,featureActionId,companyId,roleId,status,currency,amount,feesPaidBy,paymentReference,debitAccountIBAN,beneficiaryIBAN,beneficiaryName,beneficiaryNickName,beneficiaryAddress,accountWithBankBIC,customer,paymentMethod,accType,createdby)
            VALUES (',@posData,');');
            PREPARE sql_query FROM @query;
            EXECUTE sql_query;
         END IF;
    END LOOP insertPOs;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bulkpayment_template_po_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `bulkpayment_template_po_create_proc`(
IN _povalues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
	DECLARE index1 INTEGER DEFAULT 0;
    SET @numOfPOs = LENGTH(_povalues) - LENGTH(REPLACE(_povalues, '|', '')) + 1;
		insertPOs : LOOP
        SET index1 = index1 + 1;
        IF index1 = @numOfPOs + 1 THEN
            LEAVE insertPOs;
        ELSE
        SET @posData = SUBSTRING_INDEX(SUBSTRING_INDEX(_povalues, '|', index1), '|', -1 );
            SET @query = CONCAT('INSERT INTO bulkpaymenttemplatepos(paymentOrderId,templateId,confirmationNumber,recipientName,accountNumber,featureActionId,companyId,roleId,status,createdby,beneficiaryName,paymentMethod,currency,amount,feesPaidBy,paymentReference,swift,beneficiaryNickName,beneficiaryAddress,accType,beneficiaryType,addToExistingFlag,beneficiaryIBAN,bankName)
            VALUES (',@posData,');');
            PREPARE sql_query FROM @query;
            EXECUTE sql_query;
         END IF;
    END LOOP insertPOs;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bulkwiretemplate_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `bulkwiretemplate_create_proc`(
	IN `_bulkwiretemplateValues` TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `_bulkwiretemplatelineitemValues` TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

	DECLARE index1 INTEGER DEFAULT 0;
	
	DECLARE EXIT HANDLER for SQLEXCEPTION
	 BEGIN
	  GET DIAGNOSTICS CONDITION 1 @text = MESSAGE_TEXT;
	  SELECT @text as ErrorMessage;
	 END;
	
	SET @query1 = CONCAT('INSERT INTO bulkwiretemplate(bulkWireTemplateId,bulkWireTemplateName,noOfTransactions,noOfDomesticTransactions,noOfInternationalTransactions,createdBy,modifiedBy,company_id,createdts,lastmodifiedts,defaultFromAccount,defaultCurrency) VALUES (',_bulkwiretemplateValues,');');
	PREPARE sql_query FROM @query1;
	EXECUTE sql_query;
	
	SET @id = TRIM(BOTH '"' FROM (SELECT SUBSTRING_INDEX(_bulkwiretemplateValues, ",", 1)));
	
	SET @query2 = CONCAT('INSERT INTO bulkwiretemplatelineitems(bulkWireTemplateID,createdts,lastmodifiedts,swiftCode,bulkWireTransferType,transactionType,internationalRoutingNumber,recipientName,recipientAddressLine1,recipientAddressLine2,recipientCity,recipientState,recipientCountryName,recipientZipCode,recipientBankName,recipientBankAddress1,recipientBankAddress2,recipientBankZipCode,recipientBankcity,recipientBankstate,accountNickname,recipientAccountNumber,routingNumber,createdby,modifiedBy,payeeId,templateRecipientCategory) values ',_bulkwiretemplatelineitemValues,';');	
	PREPARE sql_query2 FROM @query2;
	EXECUTE sql_query2;

SELECT * FROM bulkwiretemplate WHERE bulkWireTemplateID = @id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bulkwiretemplate_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `bulkwiretemplate_delete_proc`(
	IN `_bulkwiretemplateID` VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `_bulkwiretemplatelineitemIDs` TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

	IF _bulkwiretemplateID IS NOT NULL AND _bulkwiretemplateID != '' THEN
		IF _bulkwiretemplatelineitemIDs IS NOT NULL AND _bulkwiretemplatelineitemIDs != '' THEN
			UPDATE bulkwiretemplatelineitems SET softdeleteflag = 1 WHERE FIND_IN_SET(bulkWireTemplateLineItemID, _bulkwiretemplatelineitemIDs) COLLATE utf8_general_ci;
			SELECT COUNT(*) FROM bulkwiretemplatelineitems WHERE bulkWireTemplateID = _bulkwiretemplateID AND bulkWireTransferType = 'Domestic' AND softdeleteflag = 0  COLLATE utf8_general_ci INTO @DomCount;
			SELECT COUNT(*) FROM bulkwiretemplatelineitems WHERE bulkWireTemplateID = _bulkwiretemplateID AND bulkWireTransferType = 'International' AND softdeleteflag = 0  COLLATE utf8_general_ci INTO @InternationalCount;
			SET @totalCount = @DomCount + @InternationalCount;
			UPDATE bulkwiretemplate SET noOfTransactions = @totalCount, noOfDomesticTransactions = @DomCount, noOfInternationalTransactions = @InternationalCount WHERE bulkWireTemplateID = _bulkwiretemplateID COLLATE utf8_general_ci;
			SELECT * FROM bulkwiretemplate WHERE bulkWireTemplateID = _bulkwiretemplateID COLLATE utf8_general_ci;
			
		ELSE
			UPDATE bulkwiretemplate SET deleteUniqueValue = _bulkwiretemplateID WHERE bulkWireTemplateID = _bulkwiretemplateID COLLATE utf8_general_ci;
			UPDATE bulkwiretemplate SET softdeleteflag = 1 WHERE bulkWireTemplateID = _bulkwiretemplateID COLLATE utf8_general_ci;
			UPDATE bulkwiretemplatelineitems SET softdeleteflag = 1 WHERE bulkWireTemplateID = _bulkwiretemplateID COLLATE utf8_general_ci;
			SELECT "SUCCESS";
		END IF;		
		
	ELSE
		SELECT "FAILED";
	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `bulkwiretemplate_update_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `bulkwiretemplate_update_proc`(
	IN `_bulkwiretemplateValues` TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `_update_bulkwiretemplatelineitemValues` TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `_insert_bulkwiretemplatelineitemValues` TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

	DECLARE EXIT HANDLER for SQLEXCEPTION
	 BEGIN
	  GET DIAGNOSTICS CONDITION 1 @text = MESSAGE_TEXT;
	  SELECT @text as ErrorMessage;
	 END;
	 
	IF _bulkwiretemplateValues IS NULL OR _bulkwiretemplateValues = '' THEN
		SELECT "_bulkwiretemplateValues CANNOT BE NULL OR EMPTY";
	ELSE
		SET @query0 = CONCAT('INSERT INTO bulkwiretemplate(bulkWireTemplateID,bulkWireTemplateName,createdBy,modifiedBy,lastmodifiedts,defaultFromAccount,defaultCurrency) VALUES (',_bulkwiretemplateValues,') 
			ON DUPLICATE KEY UPDATE bulkWireTemplateName = VALUES(bulkWireTemplateName), modifiedBy = VALUES(modifiedBy), lastmodifiedts = VALUES(lastmodifiedts), defaultFromAccount = VALUES(defaultFromAccount), defaultCurrency = VALUES(defaultCurrency);');
			PREPARE sql_query0 FROM @query0;
			EXECUTE sql_query0;

		IF _update_bulkwiretemplatelineitemValues IS NOT NULL AND _update_bulkwiretemplatelineitemValues != '' THEN
			SET @query1 = CONCAT('INSERT INTO bulkwiretemplatelineitems(bulkWireTemplateLineItemID,bulkWireTemplateID,lastmodifiedts,swiftCode,bulkWireTransferType,transactionType,internationalRoutingNumber,recipientName,recipientAddressLine1,recipientAddressLine2,recipientCity,recipientState,recipientCountryName,recipientZipCode,recipientBankName,recipientBankAddress1,recipientBankAddress2,recipientBankZipCode,recipientBankcity,recipientBankstate,accountNickname,recipientAccountNumber,routingNumber,createdby,modifiedBy,payeeId,templateRecipientCategory) values ',_update_bulkwiretemplatelineitemValues,'
			ON DUPLICATE KEY UPDATE lastmodifiedts = VALUES(lastmodifiedts), swiftCode = VALUES(swiftCode), bulkWireTransferType = VALUES(bulkWireTransferType), transactionType = VALUES(transactionType), internationalRoutingNumber = VALUES(internationalRoutingNumber), recipientName = VALUES(recipientName), recipientAddressLine1 = VALUES(recipientAddressLine1), recipientAddressLine2 = VALUES(recipientAddressLine2), recipientCity = VALUES(recipientCity), recipientState = VALUES(recipientState), recipientCountryName = VALUES(recipientCountryName), recipientZipCode = VALUES(recipientZipCode), recipientBankName = VALUES(recipientBankName), recipientBankAddress1 = VALUES(recipientBankAddress1), recipientBankAddress2 = VALUES(recipientBankAddress2), recipientBankZipCode = VALUES(recipientBankZipCode), recipientBankcity = VALUES(recipientBankcity), recipientBankstate = VALUES(recipientBankstate), accountNickname = VALUES(accountNickname), recipientAccountNumber = VALUES(recipientAccountNumber), routingNumber = VALUES(routingNumber), createdby = VALUES(createdby), modifiedBy = VALUES(modifiedBy), payeeId = VALUES(payeeId), templateRecipientCategory = VALUES(templateRecipientCategory);');
			
			PREPARE sql_query1 FROM @query1;
			EXECUTE sql_query1;
		END IF;
		
		
		IF _insert_bulkwiretemplatelineitemValues IS NOT NULL AND _insert_bulkwiretemplatelineitemValues != '' THEN
			SET @query2 = CONCAT('INSERT INTO bulkwiretemplatelineitems(bulkWireTemplateID,createdts,lastmodifiedts,swiftCode,bulkWireTransferType,transactionType,internationalRoutingNumber,recipientName,recipientAddressLine1,recipientAddressLine2,recipientCity,recipientState,recipientCountryName,recipientZipCode,recipientBankName,recipientBankAddress1,recipientBankAddress2,recipientBankZipCode,recipientBankcity,recipientBankstate,accountNickname,recipientAccountNumber,routingNumber,createdby,modifiedBy,payeeId,templateRecipientCategory) values ',_insert_bulkwiretemplatelineitemValues,';');	
			PREPARE sql_query2 FROM @query2;
			EXECUTE sql_query2;
		END IF;
	
			
		SET @bulkWiretemplateID = TRIM(BOTH '"' FROM (SELECT SUBSTRING_INDEX(_bulkwiretemplateValues, ",", 1)));
		
		SELECT COUNT(*) FROM bulkwiretemplatelineitems WHERE bulkWireTemplateID = @bulkWiretemplateID AND bulkWireTransferType = 'Domestic' AND softdeleteflag = 0 INTO @DomCount;
		SELECT COUNT(*) FROM bulkwiretemplatelineitems WHERE bulkWireTemplateID = @bulkWiretemplateID AND bulkWireTransferType = 'International' AND softdeleteflag = 0 INTO @InternationalCount;
		SET @totalCount = @DomCount + @InternationalCount;
		
		UPDATE bulkwiretemplate SET noOfTransactions = @totalCount, noOfDomesticTransactions = @DomCount, noOfInternationalTransactions = @InternationalCount WHERE bulkWireTemplateID = @bulkWiretemplateID;
	
		SELECT * FROM bulkwiretemplate WHERE bulkWireTemplateID = @bulkWiretemplateID;
	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `businesstyperole_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `businesstyperole_get_proc`(
	in _businessTypeId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

select groupbusinesstype.BusinessType_id as businessTypeId,
	   membergroup.id as groupId,
       membergroup.Name as groupName,
       membergroup.Description as groupDescription,
       groupbusinesstype.isDefaultGroup as isDefaultGroup
from 
groupbusinesstype 
LEFT JOIN membergroup ON ( groupbusinesstype.Group_id = membergroup.id)
where
groupbusinesstype.BusinessType_id = _businessTypeId;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `businesstype_defaultgroup_update_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `businesstype_defaultgroup_update_proc`(
IN _businessTypeId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _groupId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _isDefault varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
IF(_isDefault = '0') THEN
    UPDATE groupbusinesstype SET `isDefaultGroup` = false where `BusinessType_id` = _businessTypeId AND `Group_id` = _groupId;
ELSE
    UPDATE groupbusinesstype SET `isDefaultGroup` = false where `BusinessType_id` = _businessTypeId;
    UPDATE groupbusinesstype SET `isDefaultGroup` = true where `BusinessType_id` = _businessTypeId AND `Group_id` = _groupId;     
END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `campaign_c360_count_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `campaign_c360_count_proc`()
BEGIN

SELECT
	campaignplaceholder.channel, campaignplaceholder.screen, campaignplaceholder.image_resolution, 
	count(*) AS campaign_count
FROM defaultcampaignspecification LEFT JOIN campaignplaceholder 
ON (defaultcampaignspecification.campaignplaceholder_id = campaignplaceholder.id)
GROUP BY campaignplaceholder.id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `campaign_c360_customergroup_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `campaign_c360_customergroup_delete_proc`(
	IN _groupId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN

DECLARE cursorFinished INTEGER DEFAULT 0;

DECLARE customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci;
DECLARE customerGroupCursor CURSOR FOR 
	SELECT Customer_id FROM customergroup WHERE Group_id = _groupId ;
    
DECLARE CONTINUE HANDLER FOR NOT FOUND SET cursorFinished = 1;

OPEN customerGroupCursor;
    getPlaceholder : LOOP
        FETCH customerGroupCursor INTO customerId;
        IF 
			cursorFinished = 1 THEN LEAVE getPlaceholder;
        END IF;
        DELETE FROM customergroup 
			WHERE Customer_id = customerId 
            AND Group_id = _groupId;
    END LOOP getPlaceholder;
CLOSE customerGroupCursor;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `campaign_c360_datacontextsandattributes_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `campaign_c360_datacontextsandattributes_get_proc`()
BEGIN
	SELECT 
		`model`.`id` AS `modelId`,
		`model`.`name` AS `modelName`,
		`model`.`endpoint_url` AS `endpoint`,
		`attr`.`id` AS `attributeid`,
		`attr`.`endpoint_attribute_id` AS `attributendpoint`,
		`attr`.`name` AS `attributename`,
		`attr`.`attributetype` AS `attributetype`,
		`attr`.`range` AS `range`,
		`attr`.`helptext` AS `helptext`,
		`attr`.`criterias` AS `criterias`,
		`attr`.`options` AS `options`
		 from attribute attr
		inner join modelattribute ma on attr.id = ma.attribute_id
		inner join model model on ma.model_id = model.id
		order by model.name,attr.name desc;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `campaign_c360_default_specification_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `campaign_c360_default_specification_get_proc`()
BEGIN

SELECT  
campaignplaceholder.id,
campaignplaceholder.channel, campaignplaceholder.screen, 
campaignplaceholder.image_resolution, campaignplaceholder.image_scale, 
defaultcampaignspecification.image_index,
defaultcampaignspecification.image_url, defaultcampaignspecification.destination_url
FROM defaultcampaignspecification 
LEFT JOIN campaignplaceholder ON (defaultcampaignspecification.campaignplaceholder_id = campaignplaceholder.id);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `campaign_c360_default_specification_manage_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `campaign_c360_default_specification_manage_proc`(
	IN _channel VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _screen VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _imageIndex INT(2),
    IN _imageResolution VARCHAR(10) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _imageURL VARCHAR(200) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _destinationURL VARCHAR(200) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _createdOrModifiedBy VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

DECLARE placeholderId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci;
DECLARE currentImageURL VARCHAR(200) CHARACTER SET UTF8 COLLATE utf8_general_ci;

SET placeholderId = (
	SELECT id FROM campaignplaceholder 
    WHERE channel = _channel
	AND screen = _screen
    AND image_resolution = _imageResolution
);

SET currentImageURL = (
	SELECT image_url FROM defaultcampaignspecification 
    WHERE campaignplaceholder_id = placeholderId
    AND image_index = _imageIndex
);

IF currentImageURL IS NULL THEN
	INSERT INTO defaultcampaignspecification 
    (campaignplaceholder_id, image_index, image_url, destination_url, createdby) 
    VALUES (placeholderId, _imageIndex, _imageURL, _destinationURL, _createdOrModifiedBy);
END IF;

IF currentImageURL IS NOT NULL THEN
	UPDATE defaultcampaignspecification 
    SET image_url = image_url, destination_url = _destinationURL, modifiedby = _createdOrModifiedBy
    WHERE campaignplaceholder_id = placeholderId 
    AND image_index = _imageIndex;
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `campaign_c360_group_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `campaign_c360_group_get_proc`(
IN _campaignId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SELECT membergroup.id AS group_id, membergroup.Name AS group_name,membergroup.Description AS group_desc,
	groupattribute.admin_attributes AS attributes, groupattribute.customer_count AS customer_count
	FROM membergroup 
	LEFT JOIN campaigngroup ON (membergroup.id = campaigngroup.group_id)
    LEFT JOIN groupattribute ON (membergroup.id = groupattribute.group_id)
	WHERE campaigngroup.campaign_id = _campaignId;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `campaign_c360_model_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `campaign_c360_model_get_proc`(
	IN _attributeId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SELECT model.id, model.name, model.endpoint_url FROM model 
LEFT JOIN modelattribute ON (model.id = modelattribute.model_id)
WHERE modelattribute.attribute_id = _attributeId;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `campaign_c360_priority_update_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `campaign_c360_priority_update_proc`(
in _priorityStart INT(100),
in _priorityEnd INT(100)
)
BEGIN

SET sql_safe_updates = 0;
  
UPDATE campaign 
	SET priority = priority + 1 
    WHERE priority >= _priorityStart AND priority < _priorityEnd;

SET sql_safe_updates = 1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `campaign_c360_specification_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `campaign_c360_specification_delete_proc`(
	IN _campaignId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _channel VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _screen VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

DECLARE cursorFinished INTEGER DEFAULT 0;

DECLARE campaignPlaceholderId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci;
DECLARE campaignPlaceholderCursor CURSOR FOR 
	SELECT id FROM campaignplaceholder WHERE channel = _channel AND screen = _screen;
    
DECLARE CONTINUE HANDLER FOR NOT FOUND SET cursorFinished = 1;

OPEN campaignPlaceholderCursor;
    getPlaceholder : LOOP
        FETCH campaignPlaceholderCursor INTO campaignPlaceholderId;
        IF 
			cursorFinished = 1 THEN LEAVE getPlaceholder;
        END IF;
        DELETE FROM campaignspecification 
			WHERE campaign_id = _campaignId 
            AND campaignplaceholder_id = campaignPlaceholderId;
    END LOOP getPlaceholder;
CLOSE campaignPlaceholderCursor;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `campaign_c360_specification_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `campaign_c360_specification_get_proc`(
in _campaignId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SELECT campaignspecification.campaign_id, 
campaignplaceholder.channel, campaignplaceholder.screen, 
campaignplaceholder.image_resolution, campaignplaceholder.image_scale,
campaignspecification.image_url, campaignspecification.destination_url
FROM campaignspecification 
LEFT JOIN campaignplaceholder ON (campaignspecification.campaignplaceholder_id = campaignplaceholder.id)
WHERE campaignspecification.campaign_id = _campaignId;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `campaign_c360_specification_manage_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `campaign_c360_specification_manage_proc`(
	IN _campaignId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _channel VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _screen VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _imageResolution VARCHAR(10) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _imageURL VARCHAR(200) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _destinationURL VARCHAR(200) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _userId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	
DECLARE placeholderId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci;
DECLARE existingImageURL VARCHAR(200) CHARACTER SET UTF8 COLLATE utf8_general_ci;

SET placeholderId = (
	SELECT id FROM campaignplaceholder 
    WHERE channel = _channel
	AND screen = _screen
    AND image_resolution = _imageResolution
);

SET existingImageURL = (
	SELECT image_url FROM campaignspecification 
    WHERE campaign_id = _campaignId
    AND campaignplaceholder_id = placeholderId
);

IF existingImageURL IS NULL THEN
	SET @mainQuery = CONCAT(
		"insert into campaignspecification ",
        "(`campaign_id`, `campaignplaceholder_id`, `image_url`, ");
        
	IF _destinationURL != '' THEN 
		SET @mainQuery = CONCAT(@mainQuery, "`destination_url`, ");
	END IF;
    
    SET @mainQuery = CONCAT(@mainQuery, 
        "`createdby`) values (",
		quote(_campaignId), ", ",
		quote(placeholderId), ", ",
		quote(_imageURL), ", ");
        
	IF _destinationURL != '' THEN 
		SET @mainQuery = CONCAT(@mainQuery, quote(_destinationURL), ", ");
	END IF;
        
	SET @mainQuery = CONCAT(@mainQuery, quote(_userId), ")");
	
	PREPARE queryStatement FROM @mainQuery;
	EXECUTE queryStatement;
	DEALLOCATE PREPARE queryStatement;
END IF;

IF existingImageURL IS NOT NULL THEN
	UPDATE campaignspecification 
    SET image_url = _imageURL, modifiedby = _userId, destination_url = _destinationURL
	WHERE campaign_id = _campaignId
	AND campaignplaceholder_id = placeholderId;
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `campaign_dbp_count_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `campaign_dbp_count_proc`()
BEGIN

SELECT 
campaignplaceholder_groupby_id.channel, campaignplaceholder_groupby_id.screen, 
campaignplaceholder_groupby_id.campaign_count
FROM (
	SELECT 
		campaignplaceholder.channel, campaignplaceholder.screen, 
		count(*) AS campaign_count
	FROM defaultcampaignspecification LEFT JOIN campaignplaceholder 
	ON (defaultcampaignspecification.campaignplaceholder_id = campaignplaceholder.id)
	GROUP BY campaignplaceholder.id
	) 
AS campaignplaceholder_groupby_id 
GROUP BY campaignplaceholder_groupby_id.channel, campaignplaceholder_groupby_id.screen,
campaignplaceholder_groupby_id.campaign_count;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `campaign_dbp_display_count_update_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `campaign_dbp_display_count_update_proc`(
    IN _campaignId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _campaignPlaceholderId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _imageIndex INT(2)
)
BEGIN

DECLARE displayCount INT(8);

IF _campaignId = 'DEFAULT_CAMPAIGN' THEN
	SET displayCount = (
		SELECT (display_count + 1) FROM defaultcampaignspecification 
		WHERE campaignplaceholder_id = _campaignPlaceholderId
        AND image_index = _imageIndex
	);

	UPDATE defaultcampaignspecification SET display_count = displayCount
	WHERE campaignplaceholder_id = _campaignPlaceholderId
    AND image_index = _imageIndex;
END IF;

IF _campaignId != 'DEFAULT_CAMPAIGN' THEN
	SET displayCount = (
		SELECT (display_count + 1) FROM campaignspecification 
		WHERE campaign_id = _campaignId 
		AND campaignplaceholder_id = _campaignPlaceholderId
	);

	UPDATE campaignspecification SET display_count = displayCount
	WHERE campaign_id = _campaignId 
    AND campaignplaceholder_id = _campaignPlaceholderId;
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `campaign_dbp_specification_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `campaign_dbp_specification_get_proc`(
	IN _currentTimestamp VARCHAR(16) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _scale VARCHAR(10) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _username VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _channel VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _screen VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

DECLARE placeholderId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci;
DECLARE customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci;

SET placeholderId = (
	SELECT id FROM campaignplaceholder 
		WHERE channel = _channel
		AND screen = _screen
		AND image_scale = _scale
);
SET customerId = (
	SELECT id FROM customer 
		WHERE UserName = _username
);

SELECT campaignspecification.campaign_id, campaignspecification.campaignplaceholder_id,
		campaignspecification.image_url, campaignspecification.destination_url,
		1 AS image_index, campaign.priority
		FROM campaignspecification 
		LEFT JOIN campaign ON (campaignspecification.campaign_id = campaign.id) 
		WHERE campaignspecification.campaignplaceholder_id = placeholderId 
        AND campaign.status_id = 'SID_SCHEDULED_ACTIVE_COMPLETED'
		AND campaignspecification.campaign_id IN 
			(SELECT campaign_id FROM campaigngroup WHERE group_id IN 
				(SELECT Group_id FROM customergroup WHERE Customer_id = customerId))
		AND campaign.start_datetime <= STR_TO_DATE(_currentTimestamp, '%m/%d/%Y %H:%i:%s') 
		AND campaign.end_datetime >= STR_TO_DATE(_currentTimestamp, '%m/%d/%Y %H:%i:%s')
        AND campaign.id NOT IN 
			(SELECT campaign_id FROM custcompletedcampaign WHERE Customer_id = customerId)
UNION ALL
SELECT 'DEFAULT_CAMPAIGN' AS campaign_id, campaignplaceholder_id, 
		image_url, destination_url, image_index, 1000 AS priority
		FROM defaultcampaignspecification 
		WHERE campaignplaceholder_id = placeholderId 
ORDER BY priority, image_index;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `campaign_dbp_specification_prelogin_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `campaign_dbp_specification_prelogin_get_proc`(
	IN _currentTimestamp VARCHAR(16) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _scale VARCHAR(10) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _deviceId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

DECLARE placeholderId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci;
DECLARE customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci;
SET placeholderId = (
	SELECT id FROM campaignplaceholder 
		WHERE channel = 'MOBILE'
		AND screen = 'PRE_LOGIN'
		AND image_scale = _scale
);
SET customerId = (
	SELECT Customer_id FROM customerdevice 
    WHERE id = _deviceId
	AND Channel_id = 'CH_ID_MOB' 
    ORDER BY LastLoginTime DESC LIMIT 1
);

SELECT campaignspecification.campaign_id, campaignspecification.campaignplaceholder_id,
		campaignspecification.image_url, campaignspecification.destination_url,
		1 AS image_index, campaign.priority
		FROM campaignspecification 
		LEFT JOIN campaign ON (campaignspecification.campaign_id = campaign.id) 
		WHERE campaignspecification.campaignplaceholder_id = placeholderId 
        AND campaign.status_id = 'SID_SCHEDULED_ACTIVE_COMPLETED'
		AND campaign.id IN (
			SELECT campaign_id FROM campaigngroup WHERE group_id IN 
				(SELECT Group_id FROM customergroup WHERE Customer_id = customerId))
		AND campaign.start_datetime <= STR_TO_DATE(_currentTimestamp, '%m/%d/%Y %H:%i:%s') 
		AND campaign.end_datetime >= STR_TO_DATE(_currentTimestamp, '%m/%d/%Y %H:%i:%s')
        AND campaign.id NOT IN 
			(SELECT campaign_id FROM custcompletedcampaign WHERE Customer_id = customerId)
UNION ALL
SELECT 'DEFAULT_CAMPAIGN' AS campaign_id, campaignplaceholder_id, 
		image_url, destination_url, image_index, 1000 AS priority
		FROM defaultcampaignspecification 
		WHERE campaignplaceholder_id = placeholderId 
ORDER BY priority, image_index;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `combinedaccess_deleteAndUpdatePreferences` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `combinedaccess_deleteAndUpdatePreferences`(newCustomerId varchar(150) CHARACTER SET UTF8 COLLATE utf8_general_ci, accountId_Del varchar(150) CHARACTER SET UTF8 COLLATE utf8_general_ci, accountType_Del varchar(150) CHARACTER SET UTF8 COLLATE utf8_general_ci,deactivatedCustomerId varchar(150) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
 DECLARE newCustomerId  varchar(150) CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT newCustomerId;
 DECLARE accountId_Del varchar(150) CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT accountId_Del;
 DECLARE accountType_Del varchar(150) CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT accountType_Del;
 DECLARE deactivatedCustomerId  varchar(150) CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT deactivatedCustomerId;
DECLARE exit handler for sqlexception
  BEGIN
    -- ERROR
  ROLLBACK;
  RESIGNAL;
END;
 
DECLARE exit handler for sqlwarning
 BEGIN
    -- WARNING
 ROLLBACK;
 RESIGNAL;
END;
START TRANSACTION;
 delete from dbxcustomeralertentitlement  where  dbxcustomeralertentitlement.Customer_id = deactivatedCustomerId and dbxcustomeralertentitlement.AccountId = accountId_Del and dbxcustomeralertentitlement.AccountType = accountType_Del;
 delete from customeralertswitch where  customeralertswitch.Customer_id =  deactivatedCustomerId and customeralertswitch.AccountID = accountId_Del  and customeralertswitch.AccountType = accountType_Del;
 delete from customeralertcategorychannel where customeralertcategorychannel.Customer_id = deactivatedCustomerId and customeralertcategorychannel.AccountId = accountId_Del and customeralertcategorychannel.AccountType = accountType_Del;
 update  dbxcustomeralertentitlement  set dbxcustomeralertentitlement.Customer_id = newCustomerId where dbxcustomeralertentitlement.Customer_id =   deactivatedCustomerId;
 update  customeralertswitch  set customeralertswitch.Customer_id = newCustomerId where customeralertswitch.Customer_id =   deactivatedCustomerId;
 update  customeralertcategorychannel  set customeralertcategorychannel.Customer_id = newCustomerId where customeralertcategorychannel.Customer_id = deactivatedCustomerId ;
COMMIT;
 END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `combinedaccess_updatePreferences_Delink` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `combinedaccess_updatePreferences_Delink`(newCustomerId varchar(150) CHARACTER SET UTF8 COLLATE utf8_general_ci, accountIds varchar(200) CHARACTER SET UTF8 COLLATE utf8_general_ci,combinedCustomerId varchar(150) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
 DECLARE newCustomerId  varchar(150) CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT newCustomerId;
 DECLARE accountIds varchar(200) CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT accountIds;
 DECLARE combinedCustomerId  varchar(150) CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT combinedCustomerId;
DECLARE exit handler for sqlexception
  BEGIN
    -- ERROR
  ROLLBACK;
  RESIGNAL;
END;
 
DECLARE exit handler for sqlwarning
 BEGIN
    -- WARNING
 ROLLBACK;
 RESIGNAL;
END;
START TRANSACTION;
 update  dbxcustomeralertentitlement  set dbxcustomeralertentitlement.Customer_id = newCustomerId where dbxcustomeralertentitlement.Customer_id = combinedCustomerId and FIND_IN_SET(dbxcustomeralertentitlement.AccountId ,accountIds);
 update  customeralertswitch  set customeralertswitch.Customer_id = newCustomerId where customeralertswitch.Customer_id = combinedCustomerId and FIND_IN_SET(customeralertswitch.AccountID,accountIds);
 update  customeralertcategorychannel  set customeralertcategorychannel.Customer_id = newCustomerId where customeralertcategorychannel.Customer_id = combinedCustomerId and FIND_IN_SET(customeralertcategorychannel.AccountId,accountIds);
COMMIT;
 END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `contract_actionlimits_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `contract_actionlimits_create_proc`(
  IN _queryInput LONGTEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
      
      DECLARE tempLimitValue varchar(255) DEFAULT "";
      
      SET SESSION group_concat_max_len = 100000000;
      set @index = 0;
      set @numOfRecords = LENGTH(_queryInput) - LENGTH(REPLACE(_queryInput, '|', '')) + 1;
      set @serviceDefinitionId = "";
      insertRecords : LOOP
          set @index = @index + 1;
          IF @index = @numOfRecords + 1 THEN 
            LEAVE insertRecords;
          else
            set @recordsData = concat('\"',UUID(),'\"',',', SUBSTRING_INDEX(SUBSTRING_INDEX(_queryInput, '|', @index), '|', -1 ));
            
            set @contractId = SUBSTRING_INDEX(SUBSTRING_INDEX(@recordsData, '\",',2 ), '\"', -1 );
            set @customerId = SUBSTRING_INDEX(SUBSTRING_INDEX(@recordsData, '\",',3 ), ',\"', -1 );
            set @featureId = SUBSTRING_INDEX(SUBSTRING_INDEX(@recordsData, '\",',4), ',\"', -1 );
            set @actionId = SUBSTRING_INDEX(SUBSTRING_INDEX(@recordsData, '\",',5), ',\"', -1 );
            set @isNewAction = SUBSTRING_INDEX(SUBSTRING_INDEX(@recordsData, '\",',6), ',\"', -1 );
            set @limitId = SUBSTRING_INDEX(SUBSTRING_INDEX(@recordsData, '\",',7), ',\"', -1 );
            set @limitValue = SUBSTRING_INDEX(SUBSTRING_INDEX(@recordsData, ',\"',-1 ), '\"', 1 );
            
             SET @recordsDataWithoutLimits = concat(SUBSTRING_INDEX(@recordsData, '\",',6 ),'\"');
             
              IF isnull(@serviceDefinitionId) OR @serviceDefinitionId = "" THEN
                SET @serviceDefinitionId = (SELECT servicedefinitionId from contract WHERE id = @contractId);
                     END IF;
             
                     IF @limitId != '@' AND  @limitValue != '@' THEN 
              SET @limitAtFI = (SELECT actionlimit.value FROM actionlimit WHERE actionlimit.Action_id = @actionId AND actionlimit.LimitType_id = @limitId);
      
              SET @limitATServiceDefinition = (SELECT servicedefinitionactionlimit.value FROM servicedefinitionactionlimit WHERE servicedefinitionactionlimit.actionId = @actionId AND servicedefinitionactionlimit.limitTypeId = @limitId
                                                   AND servicedefinitionactionlimit.serviceDefinitionId = @serviceDefinitionId );
                       SET tempLimitValue = LEAST(@limitAtFI,@limitATServiceDefinition,@limitValue);
            END IF;
            
            IF !isnull(tempLimitValue) AND tempLimitValue !='' THEN
               SET @limitValue = tempLimitValue;
                     END IF;
            

            SET @contarctFeatures = (select group_concat(contractfeatures.id SEPARATOR ",") from contractfeatures where contractId=@contractId AND 
                                                               coreCustomerId =@customerId AND featureId = @featureId);
                     
            
             
             SET @serviceDefinitionActions = (select group_concat(servicedefinitionactionlimit.id SEPARATOR ",") from servicedefinitionactionlimit where actionId=@actionId AND 
                                                               serviceDefinitionId =@serviceDefinitionId);
             
                      SET @existingActionLimitRecords = (select group_concat(contractactionlimit.id SEPARATOR ",") from contractactionlimit where contractId=@contractId AND 
                                                               coreCustomerId =@customerId AND featureId= @featureId AND actionId= @actionId AND 
                                      limitTypeId = @limitId );
             
             SET @existingActionRecords = (select group_concat(contractactionlimit.id SEPARATOR ",") from contractactionlimit where contractId=@contractId AND 
                                                               coreCustomerId =@customerId AND featureId= @featureId AND actionId= @actionId);
                                     
                     IF !isnull(@contarctFeatures) AND @contarctFeatures != "" AND !isnull(@serviceDefinitionActions) AND @serviceDefinitionActions != ""  THEN
                IF !isnull(@existingActionLimitRecords) AND @existingActionLimitRecords != "" THEN
                    SET @query = concat('UPDATE contractactionlimit SET value = ','\'',@limitValue,'\'','WHERE id = ','\'',@existingActionLimitRecords,'\'',';'); 
                           ELSE 
                                  IF (@limitId = '@' OR @limitValue = '@') AND (isnull(@existingActionRecords) OR @existingActionRecords = "") THEN
                                       SET @query = concat('INSERT IGNORE INTO contractactionlimit(id,contractId,coreCustomerId,featureId,actionId, isNewAction) VALUES (',@recordsDataWithoutLimits,');');
                               ELSE  IF @limitId != '@' AND  @limitValue != '@' AND !isnull(tempLimitValue) AND tempLimitValue !='' THEN    
                                         SET @query = concat('INSERT IGNORE INTO contractactionlimit(id,contractId,coreCustomerId,featureId,actionId, isNewAction, limitTypeId,value) VALUES (',@recordsData,');');
                                  END IF;
                           END IF;
                     END IF;
            END IF;
            PREPARE stmt FROM @query; EXECUTE stmt; DEALLOCATE PREPARE stmt;
           END IF;
      END LOOP insertRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `contract_action_limit_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `contract_action_limit_update`(
  IN _contractActionLimit LONGTEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
  
)
BEGIN
  DECLARE index1 INTEGER DEFAULT 0;
       set @numOfRecords = LENGTH(_contractActionLimit) - LENGTH(REPLACE(_contractActionLimit, '|', '')) + 1;
  updateRecords : LOOP 
     set index1 = index1 + 1;
              IF index1 = @numOfRecords + 1 THEN 
                     LEAVE updateRecords;
              else
                  set @contractValues = SUBSTRING_INDEX(SUBSTRING_INDEX(_contractActionLimit, '|', index1), '|', -1 );
                     set @contractId = SUBSTRING_INDEX(SUBSTRING_INDEX(@contractValues, '\",',1 ), '\"', -1 );
                     set @coreCustomerId = SUBSTRING_INDEX(SUBSTRING_INDEX(@contractValues, '\",',2), ',\"', -1 );
                     set @featureId = SUBSTRING_INDEX(SUBSTRING_INDEX(@contractValues, '\",',3), ',\"', -1 );
                     set @actionId = SUBSTRING_INDEX(SUBSTRING_INDEX(@contractValues, '\",',4), ',\"', -1 );
                     set @isNewAction = SUBSTRING_INDEX(SUBSTRING_INDEX(@contractValues, '\",',5), ',\"', -1 );
                     set @limitTypeId = SUBSTRING_INDEX(SUBSTRING_INDEX(@contractValues, '\",',6), ',\"', -1 );
                     set @limitValue = SUBSTRING_INDEX(SUBSTRING_INDEX(@contractValues, ',\"',-1 ), '\"', 1 );
            set @num = cast(@limitValue AS DECIMAL(20,2));
                     UPDATE contractactionlimit SET value = @num where contractId = @contractId AND coreCustomerId = @coreCustomerId AND featureId = @featureId AND actionId = @actionId AND limitTypeId = @limitTypeId; 
              END if;
              
       END LOOP updateRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `contract_communication_address_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `contract_communication_address_delete_proc`(
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN

DELETE FROM `contractcommunication` WHERE  contractId = _contractId ;
    
DELETE FROM `contractaddress` WHERE  contractId = _contractId ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `contract_corecustomers_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `contract_corecustomers_delete_proc`(
IN _contractCustomersList TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN

DELETE FROM `customergroup` 
	WHERE  contractId = _contractId AND FIND_IN_SET(coreCustomerId,_contractCustomersList);

DELETE FROM `customeraction` 
	WHERE  contractId = _contractId AND FIND_IN_SET(coreCustomerId,_contractCustomersList);
    
DELETE FROM `customeraccounts` 
	WHERE  contractId = _contractId AND FIND_IN_SET(coreCustomerId,_contractCustomersList);
    
DELETE FROM `contractcustomers` 
	WHERE  contractId = _contractId AND FIND_IN_SET(coreCustomerId,_contractCustomersList);

DELETE FROM `contractcorecustomers` 
	WHERE  contractId = _contractId AND FIND_IN_SET(coreCustomerId,_contractCustomersList);
    
DELETE FROM `contractaccounts` 
	WHERE  contractId = _contractId AND FIND_IN_SET(coreCustomerId,_contractCustomersList);
    
DELETE FROM `contractfeatures` 
	WHERE  contractId = _contractId AND FIND_IN_SET(coreCustomerId,_contractCustomersList);
    
DELETE FROM `contractactionlimit` 
	WHERE  contractId = _contractId AND FIND_IN_SET(coreCustomerId,_contractCustomersList);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `contract_corecustomer_accounts_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `contract_corecustomer_accounts_delete_proc`(
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _coreCustomerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _accountsCSV TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
DELETE FROM `customeraction` 
	WHERE  contractId = _contractId AND FIND_IN_SET(Account_id,_accountsCSV) AND coreCustomerId=_coreCustomerId ; 
    
DELETE FROM `customeraccounts` 
	WHERE  contractId = _contractId AND FIND_IN_SET(Account_id,_accountsCSV) AND coreCustomerId=_coreCustomerId ;

DELETE FROM `contractaccounts` 
	WHERE  contractId = _contractId AND FIND_IN_SET(accountId,_accountsCSV) AND coreCustomerId=_coreCustomerId ;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `contract_corecustomer_actions_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `contract_corecustomer_actions_delete_proc`(
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _coreCustomerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _actionsCSV TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
DELETE FROM `customeraction` 
	WHERE  contractId = _contractId AND FIND_IN_SET(Action_id,_actionsCSV) AND coreCustomerId = _coreCustomerId ; 
        
DELETE FROM `contractactionlimit` 
	WHERE  contractId = _contractId AND FIND_IN_SET(actionId,_actionsCSV) AND coreCustomerId = _coreCustomerId ;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `contract_corecustomer_details_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `contract_corecustomer_details_get_proc`(
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _coreCustomerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET @customerAccounts = (SELECT group_concat(accountId SEPARATOR ",") from contractaccounts WHERE (contractId = _contractId AND coreCustomerId=_coreCustomerId));

SET @customerFeatures = (SELECT group_concat(featureId SEPARATOR ",") from contractfeatures WHERE (contractId = _contractId AND coreCustomerId=_coreCustomerId));

SET @customerActions = (SELECT group_concat(actionId SEPARATOR ",") from contractactionlimit WHERE (contractId = _contractId AND coreCustomerId=_coreCustomerId));

select @customerAccounts As coreCustomerAccounts;
select @customerFeatures As coreCustomerFeatures;
select @customerActions As coreCustomerActions;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `contract_corecustomer_features_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `contract_corecustomer_features_delete_proc`(
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _coreCustomerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _featuresCSV TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
DELETE FROM `customeraction` 
	WHERE  contractId = _contractId AND FIND_IN_SET(featureId,_featuresCSV) AND coreCustomerId=_coreCustomerId ; 
    
DELETE FROM `contractfeatures` 
	WHERE  contractId = _contractId AND FIND_IN_SET(featureId,_featuresCSV) AND coreCustomerId=_coreCustomerId ;
    
DELETE FROM `contractactionlimit` 
	WHERE  contractId = _contractId AND FIND_IN_SET(featureId,_featuresCSV) AND coreCustomerId=_coreCustomerId ;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `contract_features_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `contract_features_create_proc`(
IN _features MEDIUMTEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _serviceTypeId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _defaultActionsEnabled VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
 DECLARE finished INTEGER DEFAULT 0;
 DECLARE featureId varchar(255) DEFAULT "";
 DECLARE featuresList TEXT DEFAULT "";
 DECLARE featureActionId varchar(255) DEFAULT "" ;
 DECLARE entryStatus INTEGER DEFAULT 0 ;
 DECLARE limitId varchar(255) DEFAULT "";
 DECLARE tempLimitValue varchar(255) DEFAULT "";

 DECLARE features CURSOR 
		FOR (select id from feature where FIND_IN_SET(id COLLATE utf8_general_ci,@features_list COLLATE utf8_general_ci));
DECLARE actions CURSOR 
		FOR (select id from featureaction where FIND_IN_SET(featureaction.id COLLATE utf8_general_ci,@validServicedefinitionActions COLLATE utf8_general_ci));
 DECLARE limits CURSOR 
		FOR (select LimitType_id from actionlimit where actionlimit.Action_id COLLATE utf8_general_ci = featureActionId COLLATE utf8_general_ci);
 DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;
        
 SET SESSION group_concat_max_len = 100000000;
 
SET @features_list = (select group_concat(feature.id SEPARATOR ",") from feature JOIN featureroletype ON (featureroletype.Feature_id =feature.id  and featureroletype.RoleType_id = _serviceTypeId) 
                      where (FIND_IN_SET(feature.id COLLATE utf8_general_ci,_features COLLATE utf8_general_ci))) ; 
SET @features_list = IF(@features_list is null, '', @features_list);

OPEN features; 
 getFeature: LOOP
        FETCH features INTO featureId;
        IF finished = 1 THEN 
            LEAVE getFeature;
	    else
            SET @id = (SELECT LEFT(UUID(), 50));
			INSERT INTO contractfeatures(id,contractId,coreCustomerId,featureId) VALUES
		    (@id,_contractId,_customerId,featureId);
			set featuresList = CONCAT(featureId,",",featuresList);
			ITERATE  getFeature;
        END IF;
END LOOP getFeature;
CLOSE features;
  
SET featuresList = (select SUBSTRING(featuresList FROM 1 FOR (CHAR_LENGTH(featuresList)-1)));
select featuresList;

SET finished = 0;

SET @validFIActions = (SELECT group_concat(distinct featureaction.id SEPARATOR ",") FROM featureaction WHERE
                        FIND_IN_SET(featureaction.Feature_id COLLATE utf8_general_ci,featuresList COLLATE utf8_general_ci) AND
                        featureaction.status = 'SID_ACTION_ACTIVE');

SET @servicedefinitionId = (SELECT servicedefinitionId from contract WHERE id  = _contractId);

SET @validServicedefinitionActions = (SELECT group_concat(distinct servicedefinitionactionlimit.actionId SEPARATOR ",") FROM servicedefinitionactionlimit WHERE
                                    servicedefinitionactionlimit.serviceDefinitionId = @servicedefinitionId AND
                                    FIND_IN_SET(servicedefinitionactionlimit.actionId COLLATE utf8_general_ci,@validFIActions COLLATE utf8_general_ci));
                                    
If !ISNULL(_defaultActionsEnabled) AND _defaultActionsEnabled = 'true' THEN

OPEN actions; 
getAction: LOOP
        SET entryStatus = 0;
        FETCH actions INTO featureActionId;
        SET @featureId = (SELECT Feature_id FROM featureaction WHERE id COLLATE utf8_general_ci = featureActionId COLLATE utf8_general_ci);
        IF finished = 1 THEN 
            LEAVE getAction;
	    else
            OPEN limits; 
			getlimit: LOOP
            FETCH limits INTO limitId;
			IF finished = 1 THEN 
               LEAVE getlimit;
			else
               
               SET @limitvalue = (SELECT value FROM actionlimit WHERE Action_id COLLATE utf8_general_ci = featureActionId COLLATE utf8_general_ci 
               AND LimitType_id COLLATE utf8_general_ci = limitId COLLATE utf8_general_ci);
			   SET @limitATServiceDefinition = (SELECT servicedefinitionactionlimit.value FROM servicedefinitionactionlimit WHERE 
                                                  servicedefinitionactionlimit.actionId COLLATE utf8_general_ci = featureActionId COLLATE utf8_general_ci AND
                                                  servicedefinitionactionlimit.limitTypeId COLLATE utf8_general_ci = limitId COLLATE utf8_general_ci AND
                                                   servicedefinitionactionlimit.serviceDefinitionId COLLATE utf8_general_ci = @servicedefinitionId COLLATE utf8_general_ci);
			  SET tempLimitValue = LEAST(@limitvalue,@limitATServiceDefinition);
              
              if(!isnull(tempLimitValue) AND tempLimitValue !='') THEN
                 SET @limitvalue = tempLimitValue;
              END IF;
				
			   SET @id = (SELECT LEFT(UUID(), 50));
               INSERT IGNORE INTO contractactionlimit(id,contractId,coreCustomerId,contractactionlimit.featureId,actionId,limitTypeId,value) VALUES
		        (@id,_contractId,_customerId,@featureId,featureActionId,limitId,@limitvalue);
                
                SET entryStatus = 1;
			   ITERATE  getlimit;
			END IF;
                END LOOP getlimit;
                CLOSE limits;
                
            SET finished = 0;
            IF entryStatus = 0 THEN
                SET @id = (SELECT LEFT(UUID(), 50));
                INSERT IGNORE INTO contractactionlimit(id,contractId,coreCustomerId,contractactionlimit.featureId,actionId) VALUES
		        (@id,_contractId,_customerId,@featureId,featureActionId);
            END IF;
			ITERATE  getAction;
        END IF;
END LOOP getAction;
CLOSE actions;

END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `contract_search_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `contract_search_proc`(
in _contractId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _contractName varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _coreCustomerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _coreCustomerName varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _email varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _phoneCountryCode varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _phoneNumber varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _country varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _serviceDefinitionId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

	SET @isWhereAppened = false;
    SET @shouldAndAppend = false;
	SET @select_statement = CONCAT("SELECT contract.id as contractId , contract.name as contractName , contract.servicedefinitionId as serviceDefinitionId , servicedefinition.name as serviceDefinitionName, emailcommunication.value as email , contractcorecustomers.coreCustomerName as coreCustomerName FROM contract  LEFT JOIN servicedefinition ON ( servicedefinition.id = contract.servicedefinitionId) LEFT JOIN contractcorecustomers ON (contractcorecustomers.contractId = contract.id) LEFT JOIN contractcommunication emailcommunication ON (emailcommunication.contractId = contract.id and emailcommunication.typeId = 'COMM_TYPE_EMAIL') LEFT JOIN contractcommunication phonecommunication ON (phonecommunication.contractId = contract.id and phonecommunication.typeId = 'COMM_TYPE_PHONE')
    LEFT JOIN contractaddress ON (contractaddress.contractId = contract.id)  LEFT JOIN address ON (address.id = contractaddress.addressId)");
	
    IF(_contractId != '') THEN
		SET @select_statement = CONCAT(@select_statement , " where contract.id = ",quote(_contractId));
        SET @isWhereAppened = true;
        SET @shouldAndAppend = true;
    END IF;
	
    IF(_contractName != '') THEN
		IF(!@isWhereAppened) THEN
			SET @select_statement = CONCAT(@select_statement , " where");
            SET @isWhereAppened = true;
		END IF;
        IF(@isWhereAppened and @shouldAndAppend) THEN
			SET @select_statement = CONCAT(@select_statement , " and");
		END IF;
		SET _contractName = REPLACE(_contractName , "'","''");
        SET @select_statement = CONCAT(@select_statement , " contract.name like '%",_contractName,"%'");
        SET @shouldAndAppend = true;
    END IF;
    
    IF(_serviceDefinitionId != '') THEN
		IF(!@isWhereAppened) THEN
			SET @select_statement = CONCAT(@select_statement , " where");
            SET @isWhereAppened = true;
		END IF;
        IF(@isWhereAppened and @shouldAndAppend) THEN
			SET @select_statement = CONCAT(@select_statement , " and");
		END IF;
        SET @select_statement = CONCAT(@select_statement , " contract.servicedefinitionId = ",quote(_serviceDefinitionId));
        SET @shouldAndAppend = true;
    END IF;
    
    IF(_coreCustomerId != '') THEN
		IF(!@isWhereAppened) THEN
			SET @select_statement = CONCAT(@select_statement , " where");
            SET @isWhereAppened = true;
		END IF;
        IF(@isWhereAppened and @shouldAndAppend) THEN
			SET @select_statement = CONCAT(@select_statement , " and");
		END IF;
        SET @select_statement = CONCAT(@select_statement , " contractcorecustomers.coreCustomerId =",quote(_coreCustomerId));
        SET @shouldAndAppend = true;
    END IF;
    
    IF(_coreCustomerName != '') THEN
		IF(!@isWhereAppened) THEN
			SET @select_statement = CONCAT(@select_statement , " where");
            SET @isWhereAppened = true;
		END IF;
        IF(@isWhereAppened and @shouldAndAppend) THEN
			SET @select_statement = CONCAT(@select_statement , " and");
		END IF;
        SET @select_statement = CONCAT(@select_statement , " contractcorecustomers.coreCustomerName like '%",_coreCustomerName,"%'");
        SET @shouldAndAppend = true;
    END IF;
    
    IF(_country != '') THEN
		IF(!@isWhereAppened) THEN
			SET @select_statement = CONCAT(@select_statement , " where");
            SET @isWhereAppened = true;
		END IF;
        IF(@isWhereAppened and @shouldAndAppend) THEN
			SET @select_statement = CONCAT(@select_statement , " and");
		END IF;
        SET @select_statement = CONCAT(@select_statement , " address.country =",quote(_country));
        SET @shouldAndAppend = true;
    END IF;
    
    IF(_email != '') THEN
		IF(!@isWhereAppened) THEN
			SET @select_statement = CONCAT(@select_statement , " where");
            SET @isWhereAppened = true;
		END IF;
        IF(@isWhereAppened and @shouldAndAppend) THEN
			SET @select_statement = CONCAT(@select_statement , " and");
		END IF;
        SET @select_statement = CONCAT(@select_statement , " emailcommunication.value =",quote(_email));
        SET @shouldAndAppend = true;
    END IF;
    
    IF(_phoneCountryCode != '' and _phoneNumber!= '') THEN
		IF(!@isWhereAppened) THEN
			SET @select_statement = CONCAT(@select_statement , " where ");
            SET @isWhereAppened = true;
		END IF;
        IF(@isWhereAppened and @shouldAndAppend) THEN
			SET @select_statement = CONCAT(@select_statement , " and");
		END IF;
        SET @select_statement = CONCAT(@select_statement , " phonecommunication.value = ",quote(_phoneNumber)," and phonecommunication.phoneCountryCode = ",quote(_phoneCountryCode));
        SET @shouldAndAppend = true;
    END IF;
    
    SET @select_statement = CONCAT(@select_statement , ";");
    
PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `contract_users_details_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `contract_users_details_get_proc`(
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _backendType VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET @customers = (SELECT group_concat(DISTINCT customerId SEPARATOR ",") from contractcustomers WHERE contractId = _contractId);

SELECT distinct customer.id AS customerId,FirstName AS firstName,MiddleName AS middleName, 
 LastName AS lastName, UserName AS userName, Status_id AS statusId, DateOfBirth AS dateOfBirth , Ssn AS Ssn ,
 backendidentifier.BackendId AS primaryCoreCustomerId,
 Value AS Email 
 FROM customer 
 LEFT JOIN customercommunication ON (customer.id = customercommunication.Customer_id AND customercommunication.Type_id = 'COMM_TYPE_EMAIL' AND customercommunication.isPrimary = '1') 
 LEFT JOIN backendidentifier ON (backendidentifier.Customer_id = customer.id AND backendidentifier.BackendType = _backendType)
 WHERE FIND_IN_SET(customer.id ,@customers) group by customer.id ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `corecustomeraccounts_details_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `corecustomeraccounts_details_get_proc`(
IN _coreCustomerIdList TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

	SET SESSION group_concat_max_len = 1000000;
    
	SET @corecustomersList = (SELECT group_concat(contractcustomers.coreCustomerId SEPARATOR ',') from contractcustomers WHERE contractcustomers.customerId = _customerId AND FIND_IN_SET(contractcustomers.coreCustomerId,_coreCustomerIdList));

	SET @selectstatement = concat("SELECT 
           `contractcorecustomers`.`coreCustomerId` AS coreCustomerId , 
           `contractcorecustomers`.`coreCustomerName` AS coreCustomerName ,
           `customeraccounts`.`email` AS email ,
           `customeraccounts`.`Customer_id` AS customerId ,
           `customeraccounts`.`FavouriteStatus` AS favouriteStatus ,
           IF ((`customeraccounts`.`EStatementmentEnable`) = '1' ,'true' , 'false') AS `eStatementEnable`,
           IF ((`contractcorecustomers`.`isBusiness`) = '1' ,'true' , 'false') AS `isBusinessAccount`,
           `contractaccounts`.`accountId` AS accountId
           from (`contractcorecustomers`) 
		   JOIN (`contractaccounts`) ON (`contractcorecustomers`.`coreCustomerId` = `contractaccounts`.`coreCustomerId`)
           JOIN (`customeraccounts`) ON (`customeraccounts`.`Account_id` = `contractaccounts`.`accountId`)
           where FIND_IN_SET(`contractcorecustomers`.`coreCustomerId`, '",@corecustomersList,"') AND `customeraccounts`.`Customer_id` = '",_customerId,"'");
           
           PREPARE stmt FROM @selectstatement; EXECUTE stmt; DEALLOCATE PREPARE stmt;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customeractionlimits_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customeractionlimits_create_proc`(
IN _customerActionsCSV TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _accountsCSV TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _businessTypeId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _groupId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

 DECLARE finished INTEGER DEFAULT 0 ;
 DECLARE featureActionId varchar(255) DEFAULT "" ;
 DECLARE actionslist TEXT DEFAULT "" ;
 DECLARE limitId varchar(255) DEFAULT ""  ;
 DECLARE entryStatus INTEGER DEFAULT 0 ;
 DECLARE accountId varchar(255) DEFAULT "" ;
 DECLARE actualLimitId varchar(255) DEFAULT "" ;

DECLARE accounts CURSOR
         FOR (SELECT Account_id FROM accounts WHERE FIND_IN_SET(Account_id,_accountsCSV));
DECLARE actions CURSOR 
      FOR (select id from featureaction where FIND_IN_SET(id,@validActionsList));
DECLARE limits CURSOR 
		FOR (select LimitType_id from actionlimit where Action_id COLLATE utf8_general_ci = featureActionId COLLATE utf8_general_ci );
DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;

IF(ISNULL(_groupId) OR _groupId='' ) THEN
SET @groupId = (SELECT Group_id FROM groupbusinesstype WHERE BusinessType_id COLLATE utf8_general_ci = _businessTypeId 
                       AND isDefaultGroup = true);
ELSE
SET @groupId = _groupId ;
END IF;
 
SET @groupId = (SELECT id FROM membergroup WHERE id = @groupId AND Type_id = 'TYPE_ID_BUSINESS' 
                       AND Status_id = 'SID_ACTIVE' );
                       
SET @validActionsList = (SELECT group_concat(distinct Action_id SEPARATOR ",") FROM groupactionlimit WHERE Group_id COLLATE utf8_general_ci =@groupId COLLATE utf8_general_ci 
                       AND (FIND_IN_SET(Action_id,_customerActionsCSV)));
                       
OPEN accounts; 
getAccount : LOOP
FETCH accounts INTO accountId;
IF finished = 1 THEN 
	LEAVE getAccount;
ELSE
  OPEN actions; 
  getAction: LOOP
        SET entryStatus = 0;
        FETCH actions INTO featureActionId;
        IF finished = 1 THEN 
            LEAVE getAction;
	    else
            OPEN limits; 
			getlimit: LOOP
            FETCH limits INTO limitId;
			IF finished = 1 THEN 
               LEAVE getlimit;
			else
               SET @limitvalue = (SELECT value FROM actionlimit WHERE Action_id COLLATE utf8_general_ci = featureActionId  
               AND LimitType_id COLLATE utf8_general_ci = limitId );
               
			   if (limitId='MAX_TRANSACTION_LIMIT') THEN 
			   SET actualLimitId = 'AUTO_DENIED_TRANSACTION_LIMIT';
			   ELSEIF (limitId='MIN_TRANSACTION_LIMIT') THEN 
               SET actualLimitId = 'PRE_APPROVED_TRANSACTION_LIMIT';
               ELSEIF (limitId='DAILY_LIMIT') THEN 
               SET actualLimitId = 'PRE_APPROVED_DAILY_LIMIT';
                SET @id = (SELECT LEFT(UUID(), 50));
			   INSERT IGNORE INTO customeractionlimits(id,RoleType_id,Customer_id,Action_id,Account_id,isAllowed,LimitType_id,value) VALUES
		        (@id,'TYPE_ID_BUSINESS',_customerId,featureActionId,accountId,true,actualLimitId,0.00);
                SET actualLimitId = 'AUTO_DENIED_DAILY_LIMIT';
               ELSEIF (limitId='WEEKLY_LIMIT') THEN 
               SET actualLimitId = 'PRE_APPROVED_WEEKLY_LIMIT';
                SET @id = (SELECT LEFT(UUID(), 50));
			   INSERT IGNORE INTO customeractionlimits(id,RoleType_id,Customer_id,Action_id,Account_id,isAllowed,LimitType_id,value) VALUES
		        (@id,'TYPE_ID_BUSINESS',_customerId,featureActionId,accountId,true,actualLimitId,0.00);
               SET actualLimitId = 'AUTO_DENIED_WEEKLY_LIMIT';
               END IF;
               
               
               
			   SET @id = (SELECT LEFT(UUID(), 50));
			   INSERT IGNORE INTO customeractionlimits(id,RoleType_id,Customer_id,Action_id,Account_id,isAllowed,LimitType_id,value) VALUES
		        (@id,'TYPE_ID_BUSINESS',_customerId,featureActionId,accountId,true,actualLimitId,@limitvalue);
                SET entryStatus = 1;
			   ITERATE  getlimit;
			END IF;
                END LOOP getlimit;
                CLOSE limits;
                
            SET finished = 0;
            IF entryStatus = 0 THEN
                  SET @id = (SELECT LEFT(UUID(), 50));
			      INSERT IGNORE INTO customeractionlimits(id,RoleType_id,Customer_id,Action_id,Account_id,isAllowed) VALUES
		        (@id,'TYPE_ID_BUSINESS',_customerId,featureActionId,accountId,true);
            END IF;
			set actionslist = CONCAT(featureActionId,",",actionslist);
			ITERATE  getAction;
        END IF;
  END LOOP getAction;
  CLOSE actions;
  ITERATE  getAccount;
 END IF;
END LOOP getAccount;
CLOSE accounts;

SET actionslist = (select SUBSTRING(actionslist FROM 1 FOR (CHAR_LENGTH(actionslist)-1)));

select actionslist;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customeractions_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customeractions_create_proc`(
IN _customerActionsCSV TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _accountsCSV TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _businessTypeId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _groupId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

 DECLARE finished INTEGER DEFAULT 0 ;
 DECLARE featureActionId varchar(255) DEFAULT "" ;
 DECLARE actionslist TEXT DEFAULT "" ;
 DECLARE limitId varchar(255) DEFAULT ""  ;
 DECLARE entryStatus INTEGER DEFAULT 0 ;
 DECLARE accountId varchar(255) DEFAULT "" ;
 DECLARE actualLimitId varchar(255) DEFAULT "" ;

DECLARE accounts CURSOR
         FOR (SELECT Account_id FROM accounts WHERE FIND_IN_SET(Account_id,_accountsCSV));
DECLARE actions CURSOR 
      FOR (select id from featureaction where FIND_IN_SET(id,@validActionsList));
DECLARE limits CURSOR 
		FOR (select LimitType_id from actionlimit where Action_id COLLATE utf8_general_ci = featureActionId COLLATE utf8_general_ci );
DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;

IF(ISNULL(_groupId) OR _groupId='' ) THEN
SET @groupId = (SELECT Group_id FROM groupbusinesstype WHERE BusinessType_id COLLATE utf8_general_ci = _businessTypeId 
                       AND isDefaultGroup = true);
ELSE
SET @groupId = _groupId ;
END IF;
 
SET @groupId = (SELECT id FROM membergroup WHERE id = @groupId AND Type_id = 'TYPE_ID_BUSINESS' 
                       AND Status_id = 'SID_ACTIVE' );
                       
SET @validActionsList = (SELECT group_concat(distinct Action_id SEPARATOR ",") FROM groupactionlimit WHERE Group_id COLLATE utf8_general_ci =@groupId COLLATE utf8_general_ci 
                       AND (FIND_IN_SET(Action_id,_customerActionsCSV)));
                       
OPEN accounts; 
getAccount : LOOP
FETCH accounts INTO accountId;
IF finished = 1 THEN 
	LEAVE getAccount;
ELSE
  OPEN actions; 
  getAction: LOOP
        SET entryStatus = 0;
        FETCH actions INTO featureActionId;
        IF finished = 1 THEN 
            LEAVE getAction;
	    else
            OPEN limits; 
			getlimit: LOOP
            FETCH limits INTO limitId;
			IF finished = 1 THEN 
               LEAVE getlimit;
			else
               SET @limitvalue = (SELECT value FROM actionlimit WHERE Action_id COLLATE utf8_general_ci = featureActionId  
               AND LimitType_id COLLATE utf8_general_ci = limitId );
               
			   if (limitId='MAX_TRANSACTION_LIMIT') THEN 
			   SET actualLimitId = 'AUTO_DENIED_TRANSACTION_LIMIT';
			   ELSEIF (limitId='MIN_TRANSACTION_LIMIT') THEN 
               SET actualLimitId = 'PRE_APPROVED_TRANSACTION_LIMIT';
               ELSEIF (limitId='DAILY_LIMIT') THEN 
               SET actualLimitId = 'PRE_APPROVED_DAILY_LIMIT';
                SET @id = (SELECT LEFT(UUID(), 50));
			   INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,Action_id,Account_id,isAllowed,LimitType_id,value) VALUES
		        (@id,'TYPE_ID_BUSINESS',_customerId,featureActionId,accountId,true,actualLimitId,0.00);
                SET actualLimitId = 'AUTO_DENIED_DAILY_LIMIT';
               ELSEIF (limitId='WEEKLY_LIMIT') THEN 
               SET actualLimitId = 'PRE_APPROVED_WEEKLY_LIMIT';
                SET @id = (SELECT LEFT(UUID(), 50));
			   INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,Action_id,Account_id,isAllowed,LimitType_id,value) VALUES
		        (@id,'TYPE_ID_BUSINESS',_customerId,featureActionId,accountId,true,actualLimitId,0.00);
               SET actualLimitId = 'AUTO_DENIED_WEEKLY_LIMIT';
               END IF;
               
               
               
			   SET @id = (SELECT LEFT(UUID(), 50));
			   INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,Action_id,Account_id,isAllowed,LimitType_id,value) VALUES
		        (@id,'TYPE_ID_BUSINESS',_customerId,featureActionId,accountId,true,actualLimitId,@limitvalue);
                SET entryStatus = 1;
			   ITERATE  getlimit;
			END IF;
                END LOOP getlimit;
                CLOSE limits;
                
            SET finished = 0;
            IF entryStatus = 0 THEN
                  SET @id = (SELECT LEFT(UUID(), 50));
			      INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,Action_id,Account_id,isAllowed) VALUES
		        (@id,'TYPE_ID_BUSINESS',_customerId,featureActionId,accountId,true);
            END IF;
			set actionslist = CONCAT(featureActionId,",",actionslist);
			ITERATE  getAction;
        END IF;
  END LOOP getAction;
  CLOSE actions;
  ITERATE  getAccount;
 END IF;
END LOOP getAccount;
CLOSE accounts;

SET actionslist = (select SUBSTRING(actionslist FROM 1 FOR (CHAR_LENGTH(actionslist)-1)));

select actionslist;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customeraction_save_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customeraction_save_proc`(
  IN _queryInput MEDIUMTEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
      set @index = 0;
      set @numOfRecords = LENGTH(_queryInput) - LENGTH(REPLACE(_queryInput, '|', '')) + 1;
      insertRecords : LOOP
          set @index = @index + 1;
          IF @index = @numOfRecords + 1 THEN 
            LEAVE insertRecords;
          else
            set @recordsData = concat('\'',UUID(),'\'',',', SUBSTRING_INDEX(SUBSTRING_INDEX(_queryInput, '|', @index), '|', -1 ));
           	set @query = concat('INSERT INTO customeraction(id,RoleType_id,Customer_id,coreCustomerId,contractId,featureId,action_id,account_id,isAllowed,limitGroupId, limitType_id,value) VALUES (',@recordsData,');');
            PREPARE stmt FROM @query; EXECUTE stmt; DEALLOCATE PREPARE stmt;
           END IF;
      END LOOP insertRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customeralertchannel_deletebulk_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customeralertchannel_deletebulk_proc`(
IN _deleteRecords TEXT )
begin
	
  DECLARE index1 INTEGER DEFAULT 0;
 	
  DECLARE EXIT HANDLER for SQLEXCEPTION
    BEGIN
      GET DIAGNOSTICS CONDITION 1 @text = MESSAGE_TEXT;
      SELECT @text as errmsg;
   END;	
    set @numOfRecords = LENGTH(_deleteRecords) - LENGTH(REPLACE(_deleteRecords, '|', '')) ;
    deleteRecords : LOOP
     set index1 = index1 + 1;
        IF index1 = @numOfRecords + 1 THEN
            LEAVE deleteRecords;
        else
            set @rowValues = SUBSTRING_INDEX(SUBSTRING_INDEX(_deleteRecords, '|', index1), '|', -1 );
            set @customer_id = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',1 ), '\"', -1 );
            set @alertCategoryId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',2), ',\"', -1 );
            set @alertTypeId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',3), ',\"', -1 );
            set @alertSubTypeId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',4), ',\"', -1 );
            set @accountId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',5), ',\"', -1 );
            set @accountType = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',6), ',\"', -1 );
		    set @channelId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, ',\"',-1 ), '\"', 1 );
		                   			
		  SET @query  =  CONCAT('delete from `customeralertchannel` where ' ,'`customerId` = ',quote(@customer_id),'  and `alertCategoryId` = ',quote(@alertCategoryId),'  and `alertTypeId` = ',quote(@alertTypeId),'  and `alertSubTypeId` = ',quote(@alertSubTypeId),'  and `accountId` = ',quote(@accountId),'  and `accountType` = ',quote(@accountType),'  and `channelId` = ',quote(@channelId),' ;');		
	
		 PREPARE stmt FROM @query; EXECUTE stmt; DEALLOCATE PREPARE stmt;	  			 
        END if;       
    END LOOP deleteRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customeralertchannel_insertbulk_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customeralertchannel_insertbulk_proc`(
IN _recordvalues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
begin
	
	DECLARE EXIT HANDLER for SQLEXCEPTION
     BEGIN
      GET DIAGNOSTICS CONDITION 1 @text = MESSAGE_TEXT;
      SELECT @text as errmsg;
     END;

	IF  _recordvalues is not null AND _recordvalues !='' THEN
	  SET @query =  CONCAT('insert into  `customeralertchannel` ( `customerId`, `alertCategoryId`, `alertTypeId`, `alertSubTypeId`, `accountId`, `accountType`,`channelId`, `createdby`) values  ',_recordvalues,';');	       
	  PREPARE sql_query FROM @query; EXECUTE sql_query; DEALLOCATE PREPARE sql_query;
    End IF ;
	   
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customeralertchannel_sync` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customeralertchannel_sync`(
								in operationType varchar(20) CHARACTER SET UTF8 COLLATE utf8_general_ci,
								in changedLevel varchar(20) CHARACTER SET UTF8 COLLATE utf8_general_ci,
								in channelsStr varchar(255) CHARACTER SET UTF8 COLLATE utf8_general_ci,                                                        
                                in filterValue varchar(255) CHARACTER SET UTF8 COLLATE utf8_general_ci )
BEGIN
SET @preference = (select `alertPreferenceView` from `customerviewalertconfiguration`);

IF operationType is null THEN
     SET operationType = "";
ELSEIF operationType = 'edit' and changedLevel is not null THEN 
SET @channels_list = ( select group_concat(channel.id SEPARATOR ',') from `channel` where (FIND_IN_SET(`channel`.`id`,channelsStr))) ; 
SET @channels_list = IF(@channels_list is null, '', @channels_list);

	IF (@preference = changedLevel AND changedLevel LIKE 'CATEGORY') THEN
	   DELETE FROM `customeralertchannel` where `alertCategoryId` = filterValue AND FIND_IN_SET(`customeralertchannel`.`channelId`, @channels_list);
	ELSEIF (@preference = changedLevel AND changedLevel LIKE 'GROUP') THEN
	   DELETE FROM `customeralertchannel` where `alertTypeId` = filterValue AND FIND_IN_SET(`customeralertchannel`.`channelId`, @channels_list);
	ELSEIF (@preference = changedLevel AND changedLevel LIKE 'ALERT') THEN
	  DELETE FROM `customeralertchannel` where `alertSubTypeId` = filterValue AND FIND_IN_SET(`customeralertchannel`.`channelId`, @channels_list);
	END IF;
ELSEIF operationType = 'reassign' THEN
  IF (@preference != 'CATEGORY' ) THEN
   DELETE FROM `customeralertchannel` where `alertTypeId` = filterValue;
  END IF;
 END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customeralertchannel_updatebulk_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customeralertchannel_updatebulk_proc`( 
IN _updateRecords TEXT)
begin
	
 DECLARE index1 INTEGER DEFAULT 0;	
  
    set @numOfRecords = LENGTH(_updateRecords) - LENGTH(REPLACE(_updateRecords, '|', ''));
  updateRecords : LOOP
     set index1 = index1 + 1;
        IF index1 = @numOfRecords + 1 THEN
            LEAVE updateRecords;
        else
            set @rowValues = SUBSTRING_INDEX(SUBSTRING_INDEX(_updateRecords, '|', index1), '|', -1 );
            set @customer_id = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',1 ), '\"', -1 );
            set @alertCategoryId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',2), ',\"', -1 );
            set @alertTypeId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',3), ',\"', -1 );
            set @alertSubTypeId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',4), ',\"', -1 );
            set @accountId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',5), ',\"', -1 );
			set @accountType = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',6), ',\"', -1 );
		    set @channelId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',7), ',\"', -1 );		  
            set @modifiedby = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, ',\"',-1 ), '\"', 1 );
			           
           	SET @queryu = CONCAT('update `customeralertchannel` set  `modifiedby` =', quote(@modifiedby),' where `customerId` = ',quote(@customer_id),'  and `alertCategoryId` = ',quote(@alertCategoryId),'  and `alertTypeId` = ',quote(@alertTypeId),'  and `alertSubTypeId` = ',quote(@alertSubTypeId),'  and `accountId` = ',quote(@accountId),'  and `accountType` = ',quote(@accountType),'  and `channelId` = ',quote(@channelId),' ;');		
		     PREPARE stmt FROM @queryu; EXECUTE stmt; DEALLOCATE PREPARE stmt;
					 
        END if;       
    END LOOP updateRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customeralertfrequency_deletebulk_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customeralertfrequency_deletebulk_proc`( 
IN _deleteRecords TEXT)
begin
	
 DECLARE index1 INTEGER DEFAULT 0;	
  
    set @numOfRecords = LENGTH(_deleteRecords) - LENGTH(REPLACE(_deleteRecords, '|', ''));
  deleteRecords : LOOP
     set index1 = index1 + 1;
        IF index1 = @numOfRecords + 1 THEN
            LEAVE deleteRecords;
        else
            set @rowValues = SUBSTRING_INDEX(SUBSTRING_INDEX(_deleteRecords, '|', index1), '|', -1 );
            set @customer_id = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',1 ), '\"', -1 );
            set @alertCategoryId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',2), ',\"', -1 );
            set @alertTypeId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',3), ',\"', -1 );
            set @alertSubTypeId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',4), ',\"', -1 );
            set @accountId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',5), ',\"', -1 );
			set @accountType = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, ',\"',-1 ), '\"', 1 );
			
            SET @query = CONCAT('delete from `customeralertfrequency` where ' ,'`customerId` = ',quote(@customer_id),'  and `alertCategoryId` = ',quote(@alertCategoryId),'  and `alertTypeId` = ',quote(@alertTypeId),'  and `alertSubTypeId` = ',quote(@alertSubTypeId),'  and `accountId` = ',quote(@accountId),'  and `accountType` = ',quote(@accountType),' ;');		
			PREPARE sql_query FROM @query; EXECUTE sql_query; DEALLOCATE PREPARE sql_query;
				 
        END if;       
    END LOOP deleteRecords;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customeralertfrequency_insertbulk_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customeralertfrequency_insertbulk_proc`(
IN _recordvalues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
begin
	
	DECLARE EXIT HANDLER for SQLEXCEPTION
     BEGIN
      GET DIAGNOSTICS CONDITION 1 @text = MESSAGE_TEXT;
      SELECT @text as errmsg;
     END;

	IF  _recordvalues is not null AND _recordvalues !='' THEN
	 SET @query =  CONCAT( 'INSERT INTO `customeralertfrequency` (`customerId`, `alertCategoryId`, `alertTypeId`, `alertSubTypeId`, `accountId`, `accountType`,`alertFrequencyId`, `frequencyValue`, `frequencyTime`, `createdby`) values  ',_recordvalues,';');    	         
    select @query;
	PREPARE sql_query FROM @query; EXECUTE sql_query; DEALLOCATE PREPARE sql_query;	
	End IF;	   
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customeralertFrequency_sync` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customeralertFrequency_sync`( 
							in operationType varchar(20) CHARACTER SET UTF8 COLLATE utf8_general_ci,
							in filterValue varchar(255) CHARACTER SET UTF8 COLLATE utf8_general_ci  )
BEGIN
SET @preference = (select `alertPreferenceView` from `customerviewalertconfiguration`);
IF (operationType is null) THEN
  SET operationType = "";
ELSEIF (operationType LIKE 'edit' && @preference = 'ALERT') THEN
   DELETE FROM `customeralertfrequency` where `alertSubTypeId` = filterValue; 
ELSEIF (operationType LIKE 'reassign') THEN
  IF (@preference != 'CATEGORY') THEN
   DELETE FROM `customeralertfrequency` where `alertTypeId` = filterValue;
	END IF;
END IF;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customeralertfrequency_updatebulk_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customeralertfrequency_updatebulk_proc`(
IN _updateRecords TEXT)
begin
	
 DECLARE index1 INTEGER DEFAULT 0;	
  
    set @numOfRecords = LENGTH(_updateRecords) - LENGTH(REPLACE(_updateRecords, '|', ''));
  updateRecords : LOOP
     set index1 = index1 + 1;
        IF index1 = @numOfRecords + 1 THEN
            LEAVE updateRecords;
        else
            set @rowValues = SUBSTRING_INDEX(SUBSTRING_INDEX(_updateRecords, '|', index1), '|', -1 );
            set @customer_id = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',1 ), '\"', -1 );
            set @alertCategoryId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',2), ',\"', -1 );
            set @alertTypeId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',3), ',\"', -1 );
            set @alertSubTypeId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',4), ',\"', -1 );
            set @accountId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',5), ',\"', -1 );
			set @accountType = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',6), ',\"', -1 );
		    set @alertFrequencyId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',7), ',\"', -1 );		  
		    set @frequencyValue = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',8), ',\"', -1 );	
		    set @frequencyTime = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',9), ',\"', -1 );	
            set @modifiedby = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, ',\"',-1 ), '\"', 1 );
			
            IF STRCMP(@frequencyValue, 'null') != 0 or STRCMP(@frequencyValue, 'NULL') != 0 Then  
                   SET @frequencyValue = quote(@frequencyValue);	
		    END IF;  
		   
		   IF STRCMP(@frequencyTime, 'null') != 0 or STRCMP(@frequencyTime, 'NULL') != 0 Then  
                   SET @frequencyTime = quote(@frequencyTime);	
		    END IF;
           
           	SET @queryu = CONCAT('UPDATE `customeralertfrequency` set  `modifiedby` =', quote(@modifiedby),',`alertFrequencyId` = ',quote(@alertFrequencyId),',`frequencyValue`=',@frequencyValue,',`frequencyTime`=',@frequencyTime,' where `customerId` = ',quote(@customer_id),'  and `alertCategoryId` = ',quote(@alertCategoryId),'  and `alertTypeId` = ',quote(@alertTypeId),'  and `alertSubTypeId` = ',quote(@alertSubTypeId),'  and `accountId` = ',quote(@accountId),'  and `accountType` = ',quote(@accountType),' ;');		
			PREPARE sql_query FROM @queryu; EXECUTE sql_query; DEALLOCATE PREPARE sql_query;	
          
				 
        END if;       
    END LOOP updateRecords;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_accounts_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_accounts_delete_proc`(in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
DELETE FROM customeraccounts where customeraccounts.Customer_id = _customerId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_actions_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_actions_delete`(in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
DELETE FROM customeraction where customeraction.Customer_id = _customerId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_actions_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_actions_proc`(
in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _actionId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET SESSION group_concat_max_len = 100000000;

SET @orgId = (SELECT Organization_Id from customer where id=_customerId);

IF @orgId is null THEN 
  SET @orgId = "";
END IF;

SET @active_features = (SELECT group_concat(id SEPARATOR ",") from feature where Status_id = 'SID_FEATURE_ACTIVE');

IF @active_features is null THEN 
  SET @active_features = "";
END IF;

SET @active_actions = (SELECT group_concat(id SEPARATOR ",") from featureaction where FIND_IN_SET(Feature_id, @active_features));

IF @active_actions is null THEN 
  SET @active_actions = "";
END IF;

SET @organization_active_features = (SELECT group_concat(featureId SEPARATOR ",") from organisationfeatures where (featureStatus is null OR featureStatus = 'SID_FEATURE_ACTIVE') AND FIND_IN_SET(featureId, @active_features) AND organisationId = @orgId);

IF @organization_active_features is null THEN 
  SET @organization_active_features = "";
END IF;

SET @organization_active_actions = (SELECT group_concat(id SEPARATOR ",") from featureaction where FIND_IN_SET(Feature_id, @organization_active_features));

IF @organization_active_actions is null THEN 
  SET @organization_active_actions = "";
END IF;

SET @business_customer_enabled_actions = (SELECT group_concat(Action_id SEPARATOR ",") from customeraction where isAllowed = '1' AND Customer_id =_customerId AND FIND_IN_SET(Action_id, @organization_active_actions));

IF @business_customer_enabled_actions is null THEN 
    SET @business_customer_enabled_actions = "";
END IF;

SET @customer_disabled_actions = (SELECT group_concat(Action_id SEPARATOR ",") from customeraction where isAllowed = '0' AND Account_id is NULL AND Customer_id =_customerId);

IF @customer_disabled_actions is null THEN 
    SET @customer_disabled_actions = "";
END IF;

SET @customer_groups = (SELECT group_concat(Group_id SEPARATOR ",") from customergroup where Customer_id = _customerId);

IF @customer_groups is null THEN 
  SET @customer_groups = "";
END IF;

SET @group_actions = (SELECT group_concat(Action_id SEPARATOR ",") from groupactionlimit where FIND_IN_SET(Group_id, @customer_groups));

IF @group_actions is null THEN 
  SET @group_actions = "";
END IF;

IF @orgId = "" THEN 
  SET @active_feature_condition = concat("`feature`.`Status_id` = 'SID_FEATURE_ACTIVE' AND FIND_IN_SET(`featureaction`.`id`, '",@group_actions,"') ");
  ELSE 
    SET @active_feature_condition = concat("FIND_IN_SET(`featureaction`.`id`, '",@organization_active_actions,"') AND FIND_IN_SET(`featureaction`.`id`, '",@group_actions,"') ");
END IF;

set @select_statement = concat("SELECT 
        `customeraction`.`Customer_id` AS `Customer_id`,
        `customeraction`.`Account_id` AS `Account_id`,
        IF(`customeraction`.`isAllowed` = '1', 'true', 'false') AS `isAllowed`,
        `featureaction`.`id` AS `Action_id`,
        IF(`featureaction`.`isAccountLevel` = '1', 'true', 'false') AS `isAccountLevel`,
        `feature`.`Status_id` AS `Feature_Status_id`,
        `feature`.`id` AS `Feature_id`,
        `customeraction`.`RoleType_id` AS `RoleType_id`,
      `customeraction`.`LimitType_id` AS `LimitType_id`,
        `customeraction`.`value` AS `value`
    FROM
        (`customeraction`
      LEFT JOIN `featureaction` ON (`featureaction`.`id` = `customeraction`.`Action_id`)
      LEFT JOIN `feature` ON (`feature`.`id` = `featureaction`.`Feature_id`))
    where `customeraction`.`Customer_id` = ", quote(_customerId)," and `featureaction`.`status` = 'SID_ACTION_ACTIVE' and ",@active_feature_condition);
    
    IF(_actionId != '') THEN
    set @select_statement =  concat(@select_statement ," and `customeraction`.`Action_id` = ",quote(_actionId));
  END IF;
  
    set @select_statement =  concat(@select_statement ," 
    UNION SELECT 
        `customergroup`.`Customer_id` AS `Customer_id`,
        NULL AS `Account_id`,
        IF(`membergroup`.`Type_id` = 'TYPE_ID_BUSINESS', 
          IF(FIND_IN_SET(`featureaction`.`id`, '",@business_customer_enabled_actions,"'), 'true', 'false') , 'true') AS `isAllowed`,
        `featureaction`.`id` AS `Action_id`,
        IF(`featureaction`.`isAccountLevel` = '1', 'true', 'false') AS `isAccountLevel`,
        `feature`.`Status_id` AS `Feature_Status_id`,
        `feature`.`id` AS `Feature_id`,
        `membergroup`.`Type_id` AS `RoleType_id`,
        `groupactionlimit`.`LimitType_id` AS `LimitType_id`,
        `groupactionlimit`.`value` AS `value`
    FROM
        (`customergroup`
        LEFT JOIN `membergroup` ON (`membergroup`.`id` = `customergroup`.`Group_id`)
        LEFT JOIN `groupactionlimit` ON (`groupactionlimit`.`Group_id` = `customergroup`.`Group_id`)
        LEFT JOIN `featureaction` ON (`featureaction`.`id` = `groupactionlimit`.`Action_id`)
        LEFT JOIN `feature` ON (`feature`.`id` = `featureaction`.`Feature_id`))
    where `customergroup`.`Customer_id` =", quote(_customerId), " and `feature`.`Status_id` = 'SID_FEATURE_ACTIVE'
      and NOT FIND_IN_SET(`groupactionlimit`.`Action_id`, '",@customer_disabled_actions,"')
        and `featureaction`.`status` = 'SID_ACTION_ACTIVE'
      and ", @active_feature_condition );
    
    IF(_actionId != '') THEN
      set @select_statement =  concat(@select_statement ," and `groupactionlimit`.`Action_id` = ",quote(_actionId));
    END IF;
    
    -- select @select_statement;
    PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_action_group_action_limits_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_action_group_action_limits_proc`(
in _customerID varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
SELECT 
        `customeractionlimit`.`Customer_id` AS `Customer_id`,
        `customeractionlimit`.`LimitType_id` AS `Customer_LimitType_id`,
        CONCAT('', `customeractionlimit`.`Value`) AS `Customer_Limit_Value`,
        NULL AS `Group_LimitType_id`,
        NULL AS `Group_Limit_Value`,
        `service`.`id` AS `Action_id`,
        `service`.`Name` AS `Action_Name`,
        `service`.`code` AS `Action_Code`,
        `service`.`Description` AS `Action_Description`,
        `service`.`Status_id` AS `Action_Status_id`,
        `app`.`id` AS `App_id`,
        `app`.`Name` AS `App_Name`,
        `appactionlimit`.`LimitType_id` AS `LimitType_id`,
        CONCAT('', `appactionlimit`.`Value`) AS `Limit_Value`,
        `feature`.`id` AS `Feature_id`,
        `feature`.`Name` AS `Feature_Name`,
        `feature`.`code` AS `Feature_Code`,
        `feature`.`Description` AS `Feature_Description`,
        `feature`.`Status_id` AS `Feature_Status_id`
    FROM
        (((((`customeractionlimit`
        JOIN `app` ON ((`appaction`.`App_id` = `app`.`id`)))
		JOIN `service` ON ((`service`.`id` = `customeractionlimit`.`Action_id`)))
		LEFT JOIN `feature` ON ((`feature`.`id` = `service`.`Feature_id`)))
        LEFT JOIN `appaction` ON ((`service`.`id` = `appaction`.`Action_id`)))
        LEFT JOIN `appactionlimit` ON ((`appaction`.`id` = `appactionlimit`.`AppAction_id`))) 
	where `customeractionlimit`.`Customer_id` = _customerID
    UNION SELECT 
        `customergroup`.`Customer_id` AS `Customer_id`,
        NULL AS `Customer_LimitType_id`,
        NULL AS `Customer_Limit_Value`,
        `groupactionlimit`.`LimitType_id` AS `Group_LimitType_id`,
        CONCAT('', `groupactionlimit`.`Value`) AS `Group_Limit_Value`,
        `service`.`id` AS `Action_id`,
        `service`.`Name` AS `Action_Name`,
        `service`.`code` AS `Action_Code`,
        `service`.`Description` AS `Action_Description`,
        `service`.`Status_id` AS `Action_Status_id`,
        `app`.`id` AS `App_id`,
        `app`.`Name` AS `App_Name`,
        `appactionlimit`.`LimitType_id` AS `LimitType_id`,
        CONCAT('', `appactionlimit`.`Value`) AS `Limit_Value`,
        `feature`.`id` AS `Feature_id`,
        `feature`.`Name` AS `Feature_Name`,
        `feature`.`code` AS `Feature_Code`,
        `feature`.`Description` AS `Feature_Description`,
        `feature`.`Status_id` AS `Feature_Status_id`
    FROM
        ((((((`customergroup`
        JOIN `service` ON ((`service`.`id` = `groupactionlimit`.`Action_id`)))
		JOIN `app` ON ((`appaction`.`App_id` = `app`.`id`)))
        LEFT JOIN `groupactionlimit` ON ((`groupactionlimit`.`Group_id` = `customergroup`.`Group_id`)))
        LEFT JOIN `feature` ON ((`feature`.`id` = `service`.`Feature_id`)))
        LEFT JOIN `appaction` ON ((`service`.`id` = `appaction`.`Action_id`)))
        LEFT JOIN `appactionlimit` ON ((`appaction`.`id` = `appactionlimit`.`AppAction_id`)))
	where `customergroup`.`Customer_id` = _customerID and `service`.`Status_id` != 'SID_INACTIVE' and `feature`.`Status_id` != 'SID_INACTIVE';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_action_limits_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_action_limits_delete`(in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
DELETE FROM customeractionlimits where customeractionlimits.Customer_id = _customerId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_action_limits_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_action_limits_delete_proc`(in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
DELETE FROM customeraction where customeraction.Customer_id = _customerId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_action_limits_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_action_limits_proc`(
in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _actionId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET SESSION group_concat_max_len = 100000000;

SET @orgId = (SELECT Organization_Id from customer where id=_customerId);

IF @orgId is null THEN 
  SET @orgId = "";
END IF;

SET @active_features = (SELECT group_concat(id SEPARATOR ",") from feature where Status_id = 'SID_FEATURE_ACTIVE');

IF @active_features is null THEN 
  SET @active_features = "";
END IF;

SET @active_actions = (SELECT group_concat(id SEPARATOR ",") from featureaction where FIND_IN_SET(Feature_id, @active_features));

IF @active_actions is null THEN 
  SET @active_actions = "";
END IF;

SET @organization_active_features = (SELECT group_concat(featureId SEPARATOR ",") from organisationfeatures where (featureStatus is null OR featureStatus = 'SID_FEATURE_ACTIVE') AND FIND_IN_SET(featureId, @active_features) AND organisationId = @orgId);

IF @organization_active_features is null THEN 
  SET @organization_active_features = "";
END IF;

SET @organization_active_actions = (SELECT group_concat(id SEPARATOR ",") from featureaction where FIND_IN_SET(Feature_id, @organization_active_features));

IF @organization_active_actions is null THEN 
  SET @organization_active_actions = "";
END IF;

SET @business_customer_enabled_actions = (SELECT group_concat(Action_id SEPARATOR ",") from customeractionlimits where isAllowed = '1' AND Customer_id =_customerId AND FIND_IN_SET(Action_id, @organization_active_actions));

IF @business_customer_enabled_actions is null THEN 
  	SET @business_customer_enabled_actions = "";
END IF;

SET @customer_disabled_actions = (SELECT group_concat(Action_id SEPARATOR ",") from customeractionlimits where isAllowed = '0' AND Account_id is NULL AND Customer_id =_customerId);

IF @customer_disabled_actions is null THEN 
  	SET @customer_disabled_actions = "";
END IF;

SET @customer_groups = (SELECT group_concat(Group_id SEPARATOR ",") from customergroup where Customer_id = _customerId);

IF @customer_groups is null THEN 
  SET @customer_groups = "";
END IF;

SET @group_actions = (SELECT group_concat(Action_id SEPARATOR ",") from groupactionlimit where FIND_IN_SET(Group_id, @customer_groups));

IF @group_actions is null THEN 
  SET @group_actions = "";
END IF;

IF @orgId = "" THEN 
  SET @active_feature_condition = concat("`feature`.`Status_id` = 'SID_FEATURE_ACTIVE' AND FIND_IN_SET(`featureaction`.`id`, '",@group_actions,"') ");
  ELSE 
    SET @active_feature_condition = concat("FIND_IN_SET(`featureaction`.`id`, '",@organization_active_actions,"') AND FIND_IN_SET(`featureaction`.`id`, '",@group_actions,"') ");
END IF;

set @select_statement = concat("SELECT 
        `customeractionlimits`.`Customer_id` AS `Customer_id`,
        `customeractionlimits`.`Account_id` AS `Account_id`,
        IF(`customeractionlimits`.`isAllowed` = '1', 'true', 'false') AS `isAllowed`,
        `featureaction`.`id` AS `Action_id`,
        IF(`featureaction`.`isAccountLevel` = '1', 'true', 'false') AS `isAccountLevel`,
        `feature`.`Status_id` AS `Feature_Status_id`,
        `feature`.`id` AS `Feature_id`,
        `customeractionlimits`.`RoleType_id` AS `RoleType_id`,
    	`customeractionlimits`.`LimitType_id` AS `LimitType_id`,
        `customeractionlimits`.`value` AS `value`
    FROM
        (`customeractionlimits`
    	LEFT JOIN `featureaction` ON (`featureaction`.`id` = `customeractionlimits`.`Action_id`)
    	LEFT JOIN `feature` ON (`feature`.`id` = `featureaction`.`Feature_id`))
  	where `customeractionlimits`.`Customer_id` = ", quote(_customerId)," and ",@active_feature_condition);
    
    IF(_actionId != '') THEN
    set @select_statement =  concat(@select_statement ," and `customeractionlimits`.`Action_id` = ",quote(_actionId));
  END IF;
  
    set @select_statement =  concat(@select_statement ," 
  	UNION SELECT 
        `customergroup`.`Customer_id` AS `Customer_id`,
        NULL AS `Account_id`,
        IF(`membergroup`.`Type_id` = 'TYPE_ID_BUSINESS', 
      		IF(FIND_IN_SET(`featureaction`.`id`, '",@business_customer_enabled_actions,"'), 'true', 'false') , 'true') AS `isAllowed`,
        `featureaction`.`id` AS `Action_id`,
        IF(`featureaction`.`isAccountLevel` = '1', 'true', 'false') AS `isAccountLevel`,
        `feature`.`Status_id` AS `Feature_Status_id`,
        `feature`.`id` AS `Feature_id`,
        `membergroup`.`Type_id` AS `RoleType_id`,
        `groupactionlimit`.`LimitType_id` AS `LimitType_id`,
        `groupactionlimit`.`value` AS `value`
    FROM
        (`customergroup`
        LEFT JOIN `membergroup` ON (`membergroup`.`id` = `customergroup`.`Group_id`)
        LEFT JOIN `groupactionlimit` ON (`groupactionlimit`.`Group_id` = `customergroup`.`Group_id`)
        LEFT JOIN `featureaction` ON (`featureaction`.`id` = `groupactionlimit`.`Action_id`)
        LEFT JOIN `feature` ON (`feature`.`id` = `featureaction`.`Feature_id`))
  	where `customergroup`.`Customer_id` =", quote(_customerId), " and `feature`.`Status_id` = 'SID_FEATURE_ACTIVE'
    	and NOT FIND_IN_SET(`groupactionlimit`.`Action_id`, '",@customer_disabled_actions,"')
    	and ", @active_feature_condition );
    
    IF(_actionId != '') THEN
    	set @select_statement =  concat(@select_statement ," and `groupactionlimit`.`Action_id` = ",quote(_actionId));
  	END IF;
    
    -- select @select_statement;
  	PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_action_save_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_action_save_proc`(
  IN _queryInput MEDIUMTEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
      set @index = 0;
      set @numOfRecords = LENGTH(_queryInput) - LENGTH(REPLACE(_queryInput, '|', '')) + 1;
      insertRecords : LOOP
          set @index = @index + 1;
          IF @index = @numOfRecords + 1 THEN 
            LEAVE insertRecords;
          else
            set @recordsData = concat('\'',UUID(),'\'',',', SUBSTRING_INDEX(SUBSTRING_INDEX(_queryInput, '|', @index), '|', -1 ));
           	set @query = concat('INSERT INTO customeraction(id,RoleType_id,Customer_id,contractId,coreCustomerId,featureId,action_id,account_id,isAllowed,limitGroupId,limitType_id,value) VALUES (',@recordsData,');');
            PREPARE stmt FROM @query; EXECUTE stmt; DEALLOCATE PREPARE stmt;
           END IF;
      END LOOP insertRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_alert_preferences_reset_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_alert_preferences_reset_proc`()
BEGIN
 truncate table dbxcustomeralertentitlement;
 truncate table customeralertchannel;
 truncate table customeralertfrequency;
 truncate table customeralertswitch;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_basic_info_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_basic_info_proc`(
in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SELECT accountLockoutThreshold,accountLockoutTime into @accountLockoutThreshold,@accountLockoutTime from passwordlockoutsettings ;

SELECT 
        `customer`.`UserName` AS `Username`,
        `customer`.`FirstName` AS `FirstName`,
        `customer`.`MiddleName` AS `MiddleName`,
        `customer`.`LastName` AS `LastName`,
        CONCAT(IFNULL(`customer`.`FirstName`, ''),
                ' ',
                IFNULL(`customer`.`MiddleName`, ''),
                ' ',
                IFNULL(`customer`.`LastName`, '')) AS `Name`,
        `customer`.`Salutation` AS `Salutation`,
        `customer`.`id` AS `Customer_id`,
        `customer`.`Ssn` AS `SSN`,
        `customer`.`createdts` AS `CustomerSince`,
        `customer`.`Gender` AS `Gender`,
        `customer`.`DateOfBirth` AS `DateOfBirth`,
        `customer`.`isEnrolledFromSpotlight` AS `isEnrolledFromSpotlight`,
        IF(`customer`.`Status_id`= 'SID_CUS_SUSPENDED',`customer`.`Status_id`, 
        IF(`customer`.`lockCount`+1 >= @accountLockoutThreshold, 'SID_CUS_LOCKED',`customer`.`Status_id`)) AS `CustomerStatus_id`,
        `customerstatus`.`Description` AS `CustomerStatus_name`,
        `customer`.`MaritalStatus_id` AS `MaritalStatus_id`,
        `maritalstatus`.`Description` AS `MaritalStatus_name`,
        `customer`.`SpouseName` AS `SpouseName`,
        `customer`.`DrivingLicenseNumber` AS `DrivingLicenseNumber`,
        `customer`.`lockedOn` AS `lockedOn`,
        `customer`.`lockCount` AS `lockCount`,
        `customer`.`EmployementStatus_id` AS `EmployementStatus_id`,
        `employementstatus`.`Description` AS `EmployementStatus_name`,
        (SELECT 
                GROUP_CONCAT(`customerflagstatus`.`Status_id`, ' '
                        SEPARATOR ',')
            FROM
                `customerflagstatus`
            WHERE
                (`customerflagstatus`.`Customer_id` = `customer`.`id`)) AS `CustomerFlag_ids`,
        (SELECT 
                GROUP_CONCAT(`status`.`Description`, ' '
                        SEPARATOR ',')
            FROM
                `status`
            WHERE
                `status`.`id` IN (SELECT 
                        `customerflagstatus`.`Status_id`
                    FROM
                        `customerflagstatus`
                    WHERE
                        (`customerflagstatus`.`Customer_id` = `customer`.`id`))) AS `CustomerFlag`,
        `customer`.`IsEnrolledForOlb` AS `IsEnrolledForOlb`,
        `customer`.`isEnrolled` AS `isEnrolled`,
        `customer`.`IsStaffMember` AS `IsStaffMember`,
        `customer`.`Location_id` AS `Branch_id`,
        `location`.`Name` AS `Branch_name`,
        `location`.`Code` AS `Branch_code`,
        `customer`.`IsOlbAllowed` AS `IsOlbAllowed`,
        `customer`.`IsAssistConsented` AS `IsAssistConsented`,
        `customer`.`isEagreementSigned` AS `isEagreementSigned`,
	`customer`.`isCombinedUser` AS `isCombinedUser`,
	IFNULL(`customer`.`combinedUserId`, '') AS `combinedUserId`,	
        IF((`customer`.`isCombinedUser` = '1'),
            'TYPE_ID_RETAIL,TYPE_ID_BUSINESS',
            `customer`.`CustomerType_id`) AS `CustomerType_id`,
        IF((`customer`.`isCombinedUser` = '1'),
            'Retail Banking,Business Banking',
            `customertype`.`Name`) AS `CustomerType_Name`,
        IF((`customer`.`isCombinedUser` = '1'),
            'Retail and Business Banking User',
            `customertype`.`Description`) AS `CustomerType_Description`,        
        
		`customer`.`Organization_Id` AS `organisation_id`,
        `organisation`.`BusinessType_id` AS `BusinessType_id`,
        `businesstype`.`name` AS `BusinessType`,        
        `organisation`.`Name` AS `organisation_name`,
	(SELECT `customercommunication`.`Value` FROM
                `customercommunication`
            WHERE
                `customercommunication`.`Type_id` = 'COMM_TYPE_PHONE' AND  `customercommunication`.`isPrimary` = 1
                AND `customercommunication`.`Customer_id` = `customer`.`id`) AS `PrimaryPhoneNumber`,
        (SELECT `customercommunication`.`Value` FROM
                `customercommunication`
            WHERE
                `customercommunication`.`Type_id` = 'COMM_TYPE_EMAIL'  AND  `customercommunication`.`isPrimary` = 1
                AND `customercommunication`.`Customer_id` = `customer`.`id`) AS `PrimaryEmailAddress`,                
        (SELECT `customercommunication`.`Value` FROM
                `customercommunication`
            WHERE
                `customercommunication`.`Type_id` = 'COMM_TYPE_PHONE' AND `customercommunication`.`isTypeBusiness` = '1'
                AND `customercommunication`.`Customer_id` = `customer`.`id`) AS `BusinessPrimaryPhoneNumber`,
        (SELECT `customercommunication`.`Value` FROM
                `customercommunication`
            WHERE
                `customercommunication`.`Type_id` = 'COMM_TYPE_EMAIL' AND `customercommunication`.`isTypeBusiness` = '1'
                AND `customercommunication`.`Customer_id` = `customer`.`id`) AS `BusinessPrimaryEmailAddress`,
        `customer`.`DocumentsSubmitted` AS `DocumentsSubmitted`,
        `customer`.`ApplicantChannel` AS `ApplicantChannel`,
        `customer`.`Product` AS `Product`,
        `customer`.`Reason` AS `Reason`,
        @accountLockoutTime AS accountLockoutTime
    FROM
        (((((((`customer`
        LEFT JOIN `location` ON ((`customer`.`Location_id` = `location`.`id`)))
        LEFT JOIN `organisation` ON ((`customer`.`Organization_Id` = `organisation`.`id`)))
        LEFT JOIN `businesstype` ON ((`businesstype`.`id` = `organisation`.`BusinessType_id`)))
        JOIN `customertype` ON ((`customer`.`CustomerType_id` = `customertype`.`id`)))
        LEFT JOIN `status` `customerstatus` ON ((`customer`.`Status_id` = `customerstatus`.`id`)))
        LEFT JOIN `status` `maritalstatus` ON ((`customer`.`MaritalStatus_id` = `maritalstatus`.`id`)))
        LEFT JOIN `status` `employementstatus` ON ((`customer`.`EmployementStatus_id` = `employementstatus`.`id`)))
    WHERE `customer`.`id` = _customerId
    LIMIT 1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_contract_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_contract_delete_proc`(in customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in contractId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in coreCustomerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
begin
       
       SET @contract_statement = 'DELETE FROM contractcustomers where';
       SET @suspended_statement = 'DELETE FROM suspendedcustomers where';
       SET @accounts_statement = 'DELETE FROM customeraccounts where ';
       SET @excluded_accounts_statement = 'DELETE FROM excludedcustomeraccounts where ';
       SET @group_statement = 'DELETE FROM customergroup where ';
       SET @action_statement = 'DELETE FROM customeraction where ';
       SET @limitgroup_statement = 'DELETE FROM customerlimitgrouplimits where ';
       SET @excluded_action_statement = 'DELETE FROM excludedcustomeraction where ';
       

       SET @where_clause = '';
       SET @where_clause1 = '';
       if(customerId != '') THEN
              SET @where_clause = CONCAT(@where_clause , '`customerId` = ');
              SET @where_clause = CONCAT(@where_clause , quote(customerId));
              SET @where_clause1 = CONCAT(@where_clause1 , '`Customer_id` = ');
              SET @where_clause1 = CONCAT(@where_clause1 , quote(customerId));
       END IF;
       
       IF(contractId != '') THEN
              IF(@where_clause != '') THEN
                     SET @where_clause = CONCAT(@where_clause , ' AND ');
                     SET @where_clause1 = CONCAT(@where_clause1 , ' AND ');
              END if;
              SET @where_clause = CONCAT(@where_clause , '`contractId` = ');
              SET @where_clause = CONCAT(@where_clause , quote(contractId));
              SET @where_clause1 = CONCAT(@where_clause1 , '`contractId` = ');
              SET @where_clause1 = CONCAT(@where_clause1 , quote(contractId));
       END IF;
       
       IF(coreCustomerId != '') THEN
              IF(@where_clause != '') THEN
                     SET @where_clause = CONCAT(@where_clause , ' AND ');
                     SET @where_clause1 = CONCAT(@where_clause1 , ' AND ');
              END if;
              SET @where_clause = CONCAT(@where_clause , '`coreCustomerId` = ');
              SET @where_clause = CONCAT(@where_clause , quote(coreCustomerId));
              SET @where_clause1 = CONCAT(@where_clause1 , '`coreCustomerId` = ');
              SET @where_clause1 = CONCAT(@where_clause1 , quote(coreCustomerId));
       END IF;
       
       IF(@where_clause != '') THEN     
              SET @contract_statement = CONCAT(@contract_statement , @where_clause);
              SET @suspended_statement = CONCAT(@suspended_statement , @where_clause);
              SET @accounts_statement = CONCAT(@accounts_statement , @where_clause1); 
              SET @excluded_accounts_statement = CONCAT(@excluded_accounts_statement , @where_clause1);
              SET @group_statement = CONCAT(@group_statement , @where_clause1); 
              SET @action_statement = CONCAT(@action_statement , @where_clause1);     
              SET @limitgroup_statement = CONCAT(@limitgroup_statement , @where_clause1);    
              SET @excluded_action_statement = CONCAT(@excluded_action_statement , @where_clause1);
                     
       
              PREPARE stmt FROM @contract_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
              PREPARE stmt FROM @suspended_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
              PREPARE stmt FROM @accounts_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
              PREPARE stmt FROM @excluded_accounts_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
              PREPARE stmt FROM @group_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
              PREPARE stmt FROM @action_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
              PREPARE stmt FROM @limitgroup_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
              PREPARE stmt FROM @excluded_action_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
       END if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_eagreement_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_eagreement_get_proc`(
in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	select membergroup.isEAgreementActive AS isEAgreementActive from 
	membergroup 
    LEFT JOIN customergroup on (customergroup.Group_id = membergroup.id)
    where
    customergroup.Customer_id = _customerId AND membergroup.Type_id = 'TYPE_ID_BUSINESS';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_entitlements_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_entitlements_proc`(
in _customerID varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	SELECT 
		`service`.`id` AS `serviceId`,
        `service`.`Name` AS `serviceName`,
        `service`.`Description` AS `serviceDesc`,
        `service`.`Notes` AS `serviceNotes`,
        IF(`customerentitlement`.`MaxTransactionLimit` != '' && !ISNULL(`customerentitlement`.`MaxTransactionLimit`) 
        ,`customerentitlement`.`MaxTransactionLimit`, `service`.`MaxTransferLimit`) AS `maxTransferLimit`,
        `service`.`MinTransferLimit` AS `minTransferLimit`,
        `service`.`DisplayName` AS `displayName`,
        `service`.`DisplayDescription` AS `displayDesc`
	FROM `customerentitlement`
		JOIN `service` ON (`service`.`id` = `customerentitlement`.`Service_id`)
	WHERE `customerentitlement`.`Customer_id` = _customerID
    UNION
    SELECT 
		`service`.`id` AS `serviceId`,
        `service`.`Name` AS `serviceName`,
        `service`.`Description` AS `serviceDesc`,
        `service`.`Notes` AS `serviceNotes`,
        `service`.`MaxTransferLimit` AS `maxTransferLimit`,
        `service`.`MinTransferLimit` AS `minTransferLimit`,
        `service`.`DisplayName` AS `displayName`,
        `service`.`DisplayDescription` AS `displayDesc`
	FROM `customergroup`
    	JOIN `groupentitlement` ON (`groupentitlement`.`Group_id` = `customergroup`.`Group_id`)
		JOIN `service` ON (`service`.`id` = `groupentitlement`.`Service_id`)
	WHERE `customergroup`.`Customer_id` = _customerID;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_group_actions_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_group_actions_proc`(
in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _actionId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET SESSION group_concat_max_len = 100000000;

SET @isCombinedUser = (SELECT isCombinedUser from customer where id=_customerId);

IF @isCombinedUser = '1' THEN
 
SET @customer_disabled_actions = (SELECT group_concat(Action_id SEPARATOR ",") from customeraction where isAllowed = '0' AND Account_id is NULL AND Customer_id =_customerId);

IF @customer_disabled_actions is null THEN 
      SET @customer_disabled_actions = "";
END IF;

SET @customer_groups = (SELECT group_concat(Group_id SEPARATOR ",") from customergroup where Customer_id = _customerId and Group_id IN (select id from membergroup where Type_id = 'TYPE_ID_RETAIL') );

IF @customer_groups is null THEN 
  SET @customer_groups = "";
END IF;

SET @group_actions = (SELECT group_concat(Action_id SEPARATOR ",") from groupactionlimit where FIND_IN_SET(Group_id, @customer_groups));

IF @group_actions is null THEN 
  SET @group_actions = "";
END IF;

SET @active_feature_condition = concat("`feature`.`Status_id` = 'SID_FEATURE_ACTIVE' AND FIND_IN_SET(`featureaction`.`id`, '",@group_actions,"') ");

set @select_statement = concat("SELECT
        `customergroup`.`Customer_id` AS `Customer_id`,
        NULL AS `Account_id`,
        'true' AS `isAllowed`,
        `featureaction`.`id` AS `Action_id`,
        `feature`.`Status_id` AS `Feature_Status_id`,
        `feature`.`id` AS `Feature_id`,
		 IF(`featureaction`.`isAccountLevel` = '1', 'true', 'false') AS `isAccountLevel`,
        `membergroup`.`Type_id` AS `RoleType_id`,
        `groupactionlimit`.`LimitType_id` AS `LimitType_id`,
        `groupactionlimit`.`value` AS `value`
    FROM
        (`customergroup`
        LEFT JOIN `membergroup` ON (`membergroup`.`id` = `customergroup`.`Group_id` and `membergroup`.`Type_id` = 'TYPE_ID_RETAIL')
        LEFT JOIN `groupactionlimit` ON (`groupactionlimit`.`Group_id` = `customergroup`.`Group_id`)
        LEFT JOIN `featureaction` ON (`featureaction`.`id` = `groupactionlimit`.`Action_id`)
        LEFT JOIN `featureactionroletype` ON (`featureactionroletype`.`Action_id` = `featureaction`.`id` and `featureactionroletype`.`RoleType_id`= 'TYPE_ID_RETAIL')
        LEFT JOIN `feature` ON (`feature`.`id` = `featureaction`.`Feature_id`))
      where `customergroup`.`Customer_id` =", quote(_customerId), " and `feature`.`Status_id` = 'SID_FEATURE_ACTIVE'
        and NOT FIND_IN_SET(`groupactionlimit`.`Action_id`, '",@customer_disabled_actions,"')
        and ", @active_feature_condition);
    
    IF(_actionId != '') THEN
        set @select_statement =  concat(@select_statement ," and `groupactionlimit`.`Action_id` = ",quote(_actionId));
      END IF;
    
    -- select @select_statement;
      PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_group_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_group_delete_proc`(in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
DELETE FROM customergroup where customergroup.Customer_id = _customerId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_group_org_actionlimits_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_group_org_actionlimits_proc`(
in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _organisationId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _isOnlyPremissions varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
SET SESSION group_concat_max_len = 10000000;

SET @organization_actions = (SELECT GROUP_CONCAT(DISTINCT organisationactionlimit.Action_id SEPARATOR ",")  FROM organisationactionlimit WHERE 
							organisationactionlimit.Organisation_id=_organisationId and 
							organisationactionlimit.Action_id NOT IN (
									SELECT featureaction.id FROM
									featureaction
									LEFT JOIN feature on (featureaction.Feature_id = feature.id)
									LEFT JOIN organisationfeatures on (organisationfeatures.featureId = feature.id)
									WHERE
									organisationfeatures.organisationId = _organisationId
									AND organisationfeatures.featureStatus='SID_FEATURE_SUSPENDED'
									OR feature.Status_id != 'SID_FEATURE_ACTIVE'
							));
                                    
SET @organization_actions = concat("'", IF(@organization_actions IS NULL, '', @organization_actions), "'");

SET @business_customer_enabled_actions = (SELECT GROUP_CONCAT(Action_id SEPARATOR "','") FROM customeraction WHERE isAllowed = '1' AND Customer_id =_customerId AND FIND_IN_SET(Action_id, @organization_actions));

IF @business_customer_enabled_actions IS NOT NULL THEN 
  	SET @business_customer_enabled_actions = concat("'", IF(@business_customer_enabled_actions is null, '', @business_customer_enabled_actions), "'");
END IF;

SET @groups = (SELECT GROUP_CONCAT(customergroup.Group_id SEPARATOR ",") FROM customergroup WHERE Customer_id=_customerId);

IF(@groups != '' ) THEN
    SET @groups =  concat(@groups,",");
END IF;

SET @list = (SELECT @groups);

SET @select_statement = '';
	groupIdIteration : LOOP
		IF LENGTH(TRIM(@list)) = 0 OR @list IS NULL THEN
		LEAVE groupIdIteration;
	  END IF;
		SET @next = SUBSTRING_INDEX(@list,',',1);
		SET @nextlen = LENGTH(@next);
		SET @value = TRIM(@next);
		
		set @roleType = (SELECT membergroup.Type_id FROM membergroup WHERE membergroup.id= @value );

		IF @roleType = ('TYPE_ID_BUSINESS') THEN
			IF _isOnlyPremissions = 'true' THEN
				SELECT DISTINCT customeraction.Action_id AS actionId FROM customeraction WHERE isAllowed = '1' AND Customer_id =_customerId AND FIND_IN_SET(Action_id, @organization_actions)
                AND (customeraction.Action_id IN (SELECT DISTINCT groupactionlimit.Action_id FROM groupactionlimit WHERE groupactionlimit.Group_id = @value));
			ELSE
			SET @select_statement =  concat(@select_statement , "SELECT DISTINCT
								`feature`.`id` as `featureId`,
								`feature`.`name` AS `featureName`,
								`feature`.`description` AS `featureDescription`,
								`featureaction`.`id` as `actionId`,
								`featureaction`.`Type_id` as `actionType`,
								`featureaction`.`description` AS `actionDescription`,
								`featureaction`.`name` AS `actionName`,
								IF(`featureaction`.`id` in (", @business_customer_enabled_actions,"), 'true', 'false') AS `isActionAllowed`,
								IF(`featureaction`.`isAccountLevel`= 1 , 'true','false') AS `isAccountLevel`,
								if(`customeraction`.`isAllowed` = 1 , 'true','false') as `isAllowedForCustomer`,
								`customeraction`.`Account_id` as `accountId`,
								`customeraction`.`LimitType_id` as `limitTypeId`,
								`customeraction`.`value` as `value`
						   FROM
							(`customeraction` 
							LEFT JOIN `customergroup` ON (`customergroup`.`Customer_id` = `customeraction`.`Customer_id`)
							LEFT JOIN `membergroup` ON (`membergroup`.`id` = `customergroup`.`Group_id`)
							LEFT JOIN `featureaction` ON (`featureaction`.`id` = `customeraction`.`Action_id`)
							LEFT JOIN `feature` ON (`feature`.`id` = `featureaction`.`Feature_id`))
						where 
							`customeraction`.`Customer_id` = ",quote(_customerId),"
							and (`customeraction`.`Action_id` in (
							select DISTINCT `groupactionlimit`.`Action_id` from 
							`groupactionlimit` where 
							`groupactionlimit`.`Group_id` = ",quote(@value),"))
							and (`featureaction`.`id` in (", @business_customer_enabled_actions,"))");
			PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
            END IF;
		END IF;
	SET @list = INSERT(@list,1,@nextlen + 1,'');  
	END LOOP;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_group_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_group_proc`(
IN _customerIds LONGTEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _groupId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _numberOfRows BIGINT,
IN _userId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
DECLARE INSERTED_RECORDS_COUNT BIGINT DEFAULT 0;
DECLARE INDEXVALUE BIGINT DEFAULT 1;
DECLARE CustomerId LONGTEXT CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT NULL;
DECLARE buffer LONGTEXT CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT NULL;
set buffer ="";
iterator:
LOOP
	IF LENGTH(TRIM(_customerIds)) = 0 OR _customerIds IS NULL OR INDEXVALUE > _numberOfRows THEN
		LEAVE iterator;
	END IF;
	SET CustomerId = func_split_str(_customerIds,",",INDEXVALUE);
    IF EXISTS(select * from customergroup where Customer_id=CustomerId and Group_id=_groupId) THEN
		SET INDEXVALUE = INDEXVALUE+1;
    else
		INSERT IGNORE INTO customergroup(Customer_id,Group_id,createdby) values(CustomerId,_groupId,_userId);
		IF(ROW_COUNT() = 0) THEN
			SET buffer=concat(buffer,",",CustomerId);
		END IF;
		SET INSERTED_RECORDS_COUNT= INSERTED_RECORDS_COUNT+ ROW_COUNT();
		SET INDEXVALUE = INDEXVALUE+1;
    end if;
END LOOP;
SET buffer = right(buffer,LENGTH(buffer)-1);
SELECT buffer AS 'FAILED_RECORDS';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_group_unlink_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_group_unlink_proc`(
IN roleId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
Delete from customergroup where Group_id = roleId;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_requests_assign_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_requests_assign_proc`(
IN _requestIds TEXT  CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _csrID varchar(200)  CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
		SET @updateclause = concat( "UPDATE `customerrequest` SET customerrequest.AssignedTo = ",quote(_csrID)," where customerrequest.id in (",func_escape_input_for_in_operator(_requestIds),")");
     	PREPARE stmt FROM @updateclause; EXECUTE stmt; DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_request_archived_message_search_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_request_archived_message_search_proc`(
	IN _customerID varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _customerName varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _customerFirstName varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _customerMiddleName varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _customerLastName varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _customerUsername varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _messageRepliedBy varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _requestSubject varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _requestAssignedTo varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _requestCategory varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _requestID varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _requestStatusID varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _dateInitialPoint varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _dateFinalPoint varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
    )
BEGIN
 SET @queryStatement = "
		SELECT 
        `archivedcustomerrequest`.`id` AS `customerrequest_id`,
        `archivedcustomerrequest`.`RequestCategory_id` AS `customerrequest_RequestCategory_id`,
        `archivedcustomerrequest`.`lastupdatedbycustomer` AS `customerrequest_lastupdatedbycustomer`,
        `requestcategory`.`Name` AS `requestcategory_Name`,
        `archivedcustomerrequest`.`Customer_id` AS `customerrequest_Customer_id`,
        `customer`.`FirstName` AS `customer_FirstName`,
        `customer`.`MiddleName` AS `customer_MiddleName`,
        CONCAT(`customer`.`FirstName`,
                `customer`.`LastName`) AS `customer_Fullname`,
        CONCAT(`systemuser`.`FirstName`,
                `systemuser`.`LastName`) AS `customerrequest_AssignedTo_Name`,
        `customer`.`LastName` AS `customer_LastName`,
        `customer`.`UserName` AS `customer_Username`,
        `customer`.`Salutation` AS `customer_Salutation`,
        `customer`.`Gender` AS `customer_Gender`,
        `customer`.`DateOfBirth` AS `customer_DateOfBirth`,
        `customer`.`Status_id` AS `customer_Status_id`,
        `customer`.`Ssn` AS `customer_Ssn`,
        `customer`.`MaritalStatus_id` AS `customer_MaritalStatus_id`,
        `customer`.`SpouseName` AS `customer_SpouseName`,
        `customer`.`EmployementStatus_id` AS `customer_EmployementStatus_id`,
        `customer`.`IsEnrolledForOlb` AS `customer_IsEnrolledForOlb`,
        `customer`.`IsStaffMember` AS `customer_IsStaffMember`,
        `customer`.`Location_id` AS `customer_Location_id`,
        `customer`.`PreferredContactMethod` AS `customer_PreferredContactMethod`,
        `customer`.`PreferredContactTime` AS `customer_PreferredContactTime`,
        `archivedcustomerrequest`.`Priority` AS `customerrequest_Priority`,
        `archivedcustomerrequest`.`Status_id` AS `customerrequest_Status_id`,
        `archivedcustomerrequest`.`AssignedTo` AS `customerrequest_AssignedTo`,
        `archivedcustomerrequest`.`RequestSubject` AS `customerrequest_RequestSubject`,
        `archivedcustomerrequest`.`Accountid` AS `customerrequest_Accountid`,
        `archivedcustomerrequest`.`createdby` AS `customerrequest_createdby`,
        `archivedcustomerrequest`.`modifiedby` AS `customerrequest_modifiedby`,
        `archivedcustomerrequest`.`createdts` AS `customerrequest_createdts`,
        `archivedcustomerrequest`.`lastmodifiedts` AS `customerrequest_lastmodifiedts`,
        `archivedcustomerrequest`.`synctimestamp` AS `customerrequest_synctimestamp`,
        `archivedcustomerrequest`.`softdeleteflag` AS `customerrequest_softdeleteflag`,
        `archivedrequestmessage`.`id` AS `requestmessage_id`,
        `archivedrequestmessage`.`RepliedBy` AS `requestmessage_RepliedBy`,
        `archivedrequestmessage`.`RepliedBy_Name` AS `requestmessage_RepliedBy_Name`,
        `archivedrequestmessage`.`MessageDescription` AS `requestmessage_MessageDescription`,
        `archivedrequestmessage`.`ReplySequence` AS `requestmessage_ReplySequence`,
        `archivedrequestmessage`.`IsRead` AS `requestmessage_IsRead`,
        `archivedrequestmessage`.`createdby` AS `requestmessage_createdby`,
        `archivedrequestmessage`.`modifiedby` AS `requestmessage_modifiedby`,
        `archivedrequestmessage`.`createdts` AS `requestmessage_createdts`,
        `archivedrequestmessage`.`lastmodifiedts` AS `requestmessage_lastmodifiedts`,
        `archivedrequestmessage`.`synctimestamp` AS `requestmessage_synctimestamp`,
        `archivedrequestmessage`.`softdeleteflag` AS `requestmessage_softdeleteflag`,
        `archivedmessageattachment`.`id` AS `messageattachment_id`,
        `archivedmessageattachment`.`AttachmentType_id` AS `messageattachment_AttachmentType_id`,
        `archivedmessageattachment`.`Media_id` AS `messageattachment_Media_id`,
        `archivedmessageattachment`.`createdby` AS `messageattachment_createdby`,
        `archivedmessageattachment`.`modifiedby` AS `messageattachment_modifiedby`,
        `archivedmessageattachment`.`createdts` AS `messageattachment_createdts`,
        `archivedmessageattachment`.`lastmodifiedts` AS `messageattachment_lastmodifiedts`,
        `archivedmessageattachment`.`softdeleteflag` AS `messageattachment_softdeleteflag`,
        `archivedmedia`.`id` AS `media_id`,
        `archivedmedia`.`Name` AS `media_Name`,
        `archivedmedia`.`Size` AS `media_Size`,
        `archivedmedia`.`Type` AS `media_Type`,
        `archivedmedia`.`Description` AS `media_Description`,
        `archivedmedia`.`Url` AS `media_Url`,
        `archivedmedia`.`createdby` AS `media_createdby`,
        `archivedmedia`.`modifiedby` AS `media_modifiedby`,
        `archivedmedia`.`lastmodifiedts` AS `media_lastmodifiedts`,
        `archivedmedia`.`synctimestamp` AS `media_synctimestamp`,
        `archivedmedia`.`softdeleteflag` AS `media_softdeleteflag`
    FROM
        (`archivedcustomerrequest`
		JOIN `archivedrequestmessage` ON (`archivedcustomerrequest`.`id` = `archivedrequestmessage`.`CustomerRequest_id`)
		JOIN `customer` ON (`archivedcustomerrequest`.`Customer_id` = `customer`.`id`)
		JOIN `requestcategory` ON (`archivedcustomerrequest`.`RequestCategory_id` = `requestcategory`.`id`)
        LEFT JOIN `archivedmessageattachment` ON (`archivedrequestmessage`.`id` = `archivedmessageattachment`.`RequestMessage_id`)
        LEFT JOIN `archivedmedia` ON (`archivedmessageattachment`.`Media_id` = `archivedmedia`.`id`)
        LEFT JOIN `systemuser` ON (`archivedcustomerrequest`.`AssignedTo` = `systemuser`.`id`)) 
	WHERE true ";

IF _customerID != '' THEN
		set @queryStatement = concat(@queryStatement,"customer.id = ", quote(_customerID));
  END if;

  IF _customerFirstName != '' THEN
		set @queryStatement = concat(@queryStatement," and customer.FirstName = ", quote(_customerFirstName));
  END if;

  IF _customerMiddleName != '' THEN
		set @queryStatement = concat(@queryStatement," and customer.MiddleName = ", quote(_customerMiddleName));
  END if;

  IF _customerLastName != '' THEN
		set @queryStatement = concat(@queryStatement," and customer.LastName = ", quote(_customerLastName));
  END if;

  IF _customerUsername != '' THEN
		set @queryStatement = concat(@queryStatement," and customer.LastName = ", quote(_customerUsername));
  END if;

  IF _messageRepliedBy != '' THEN
		set @queryStatement = concat(@queryStatement," and archivedrequestmessage.RepliedBy = ", quote(_messageRepliedBy));
  END if;

  IF _requestSubject != '' THEN
		set @queryStatement = concat(@queryStatement," and archivedcustomerrequest.RequestSubject = ", quote(_requestSubject));
  END if;

  IF _requestAssignedTo != '' THEN
		set @queryStatement = concat(@queryStatement," and archivedcustomerrequest.AssignedTo = ", quote(_requestAssignedTo));
  END if;

 IF _requestCategory != '' THEN
		set @queryStatement = concat(@queryStatement," and archivedcustomerrequest.RequestCategory_id = ", quote(_requestCategory));
  END if;

   IF _requestID != '' THEN
		set @queryStatement = concat(@queryStatement," and archivedcustomerrequest.id = ", quote(_requestID));
  END if;

   IF _requestStatusID != '' THEN
		set @queryStatement = concat(@queryStatement," and archivedcustomerrequest.Status_id = ", quote(_requestStatusID));
  END if;
  
   IF _customerName != '' THEN
		set @queryStatement = concat(@queryStatement," and  CONCAT(`customer`.`FirstName`,`customer`.`LastName`).Status_id = ", quote(_customerName));
  END if;
  
IF  _dateInitialPoint != '' THEN
     IF LOCATE("=", _dateInitialPoint)!=0 THEN
    	set @queryStatement = concat(@queryStatement," and archivedrequestmessage.createdts = ", quote(_dateInitialPoint));
     ELSEIF LOCATE(">", _dateInitialPoint)!=0 THEN
        set @queryStatement = concat(@queryStatement," and archivedrequestmessage.createdts > ", quote(_dateInitialPoint));
     ELSEIF LOCATE("<", _dateInitialPoint)!=0 THEN
        set @queryStatement = concat(@queryStatement," and archivedrequestmessage.createdts < ", quote(_dateInitialPoint));
	 ELSEIF _dateFinalPoint != '' THEN
        set @queryStatement = concat(@queryStatement," and archivedrequestmessage.createdts > ", quote(_dateInitialPoint), " and requestmessage.createdts < ", quote(_dateFinalPoint));
	 END IF;	
 END IF;	
		
 		set @queryStatement = concat(@queryStatement," ORDER BY `archivedrequestmessage`.`ReplySequence` DESC");
 		PREPARE stmt FROM @queryStatement;
		execute stmt;
		DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_request_archive_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_request_archive_proc`()
BEGIN
#Proc to Faclitate Archiving and Purging of Requests.
#Author : Aditya Mankal (KH2322) 

#NOT Found Handlers - START
DECLARE customer_request_cursor_done BOOLEAN DEFAULT FALSE;
DECLARE customer_requestmessage_cursor_done BOOLEAN DEFAULT FALSE;  
DECLARE customer_requestmessageattachment_cursor_done BOOLEAN DEFAULT FALSE;
#NOT Found Handlers - END

DECLARE curr_request_id VARCHAR(50) DEFAULT '';
DECLARE curr_requestmessage_id VARCHAR(50) DEFAULT '';
DECLARE curr_media_id VARCHAR(50) DEFAULT '';
DECLARE curr_messageattachment_id VARCHAR(50) DEFAULT 0;
DECLARE media_count INT(10) DEFAULT 0;
DECLARE customer_request_cursor CURSOR FOR SELECT id FROM customerrequest WHERE Status_id = 'SID_RESOLVED' and TIMESTAMPDIFF(SQL_TSI_SECOND ,lastmodifiedts, now())>=15552000;#6months
DECLARE CONTINUE HANDLER FOR NOT FOUND SET customer_request_cursor_done = TRUE,customer_requestmessage_cursor_done=TRUE,customer_requestmessageattachment_cursor_done=TRUE;
	OPEN customer_request_cursor;
	customer_customerrequest_loop:REPEAT 
	FETCH customer_request_cursor INTO curr_request_id;
	IF NOT customer_request_cursor_done THEN
		INSERT INTO archivedcustomerrequest SELECT *FROM customerrequest where id=curr_request_id;
		UPDATE archivedcustomerrequest SET `Status_id`='SID_ARCHIVED' WHERE id=curr_request_id;
			select curr_request_id;
			
			REQUESTMESSAGEBLOCK: BEGIN
			  DECLARE customer_requestmessage_cursor CURSOR FOR SELECT id FROM requestmessage WHERE CustomerRequest_id = curr_request_id;
				OPEN customer_requestmessage_cursor;
				   customer_requestmessage_loop:REPEAT 
					FETCH customer_requestmessage_cursor INTO curr_requestmessage_id;
					IF NOT customer_requestmessage_cursor_done THEN
						INSERT INTO archivedrequestmessage SELECT *FROM requestmessage where id = curr_requestmessage_id;
						
						  MESSAGEATTACHMENTBLOCK:BEGIN
							   DECLARE customer_messageattachment_cursor CURSOR FOR SELECT id FROM messageattachment WHERE RequestMessage_id =curr_requestmessage_id;								
								OPEN customer_messageattachment_cursor;
								  customer_messageattachment_loop:REPEAT 
									  FETCH customer_messageattachment_cursor INTO curr_messageattachment_id;
								      IF NOT customer_requestmessageattachment_cursor_done THEN
									  SELECT Media_id from messageattachment where id = curr_messageattachment_id into curr_media_id;
									  
									  SELECT count(id) from archivedmedia where id = curr_media_id into media_count;
									  IF media_count = 0 THEN #Attempt to INSERT only in case of a non archived attachment
										INSERT INTO archivedmedia SELECT *FROM media where id = curr_media_id;
									  END IF;
									  INSERT INTO archivedmessageattachment SELECT *FROM messageattachment where id = curr_messageattachment_id;
								   
									  DELETE FROM messageattachment where id = curr_messageattachment_id;
									  SELECT count(id) from messageattachment where Media_id = curr_media_id into media_count;
									  IF media_count = 0 THEN #Attempt to delete when no other message is pointing to the media as an attachment
										DELETE FROM media where id = curr_media_id;
									  END IF;
								   
								  END IF;
								  UNTIL customer_requestmessageattachment_cursor_done 
								  END REPEAT customer_messageattachment_loop;
								  SET customer_request_cursor_done=FALSE;
								  SET customer_requestmessage_cursor_done=FALSE;
								  SET customer_requestmessageattachment_cursor_done=FALSE;									  
								CLOSE customer_messageattachment_cursor;
							END MESSAGEATTACHMENTBLOCK;
							
							DELETE FROM requestmessage where id = curr_requestmessage_id;
					END IF;	
				  UNTIL customer_requestmessage_cursor_done 
				  END REPEAT customer_requestmessage_loop;
				  SET customer_request_cursor_done=FALSE;
				  SET customer_requestmessage_cursor_done=FALSE;
				  SET customer_requestmessageattachment_cursor_done=FALSE;
				CLOSE customer_requestmessage_cursor;
			END REQUESTMESSAGEBLOCK;
			
		 DELETE FROM customerrequest where id = curr_request_id;
        END IF;
	UNTIL customer_request_cursor_done 
	END REPEAT customer_customerrequest_loop;
	SET customer_request_cursor_done=FALSE;
	SET customer_requestmessage_cursor_done=FALSE;
	SET customer_requestmessageattachment_cursor_done=FALSE;	
	CLOSE customer_request_cursor;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_request_message_search_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_request_message_search_proc`(
	IN _customerID varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _customerName varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _customerFirstName varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _customerMiddleName varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _customerLastName varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _customerUsername varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _messageRepliedBy varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _requestSubject varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _requestAssignedTo varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _requestCategory varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _requestID varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _requestStatusID varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _dateInitialPoint varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _dateFinalPoint varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
    )
BEGIN
 SET @queryStatement = "
		SELECT 
        `customerrequest`.`id` AS `customerrequest_id`,
        `customerrequest`.`RequestCategory_id` AS `customerrequest_RequestCategory_id`,
        `customerrequest`.`lastupdatedbycustomer` AS `customerrequest_lastupdatedbycustomer`,
        `requestcategory`.`Name` AS `requestcategory_Name`,
        `customerrequest`.`Customer_id` AS `customerrequest_Customer_id`,
        `customer`.`FirstName` AS `customer_FirstName`,
        `customer`.`MiddleName` AS `customer_MiddleName`,
        CONCAT(`customer`.`FirstName`,
                `customer`.`LastName`) AS `customer_Fullname`,
        CONCAT(`systemuser`.`FirstName`,
                `systemuser`.`LastName`) AS `customerrequest_AssignedTo_Name`,
        `customer`.`LastName` AS `customer_LastName`,
        `customer`.`UserName` AS `customer_Username`,
        `customer`.`Salutation` AS `customer_Salutation`,
        `customer`.`Gender` AS `customer_Gender`,
        `customer`.`DateOfBirth` AS `customer_DateOfBirth`,
        `customer`.`Status_id` AS `customer_Status_id`,
        `customer`.`Ssn` AS `customer_Ssn`,
        `customer`.`MaritalStatus_id` AS `customer_MaritalStatus_id`,
        `customer`.`SpouseName` AS `customer_SpouseName`,
        `customer`.`EmployementStatus_id` AS `customer_EmployementStatus_id`,
        `customer`.`IsEnrolledForOlb` AS `customer_IsEnrolledForOlb`,
        `customer`.`IsStaffMember` AS `customer_IsStaffMember`,
        `customer`.`Location_id` AS `customer_Location_id`,
        `customer`.`PreferredContactMethod` AS `customer_PreferredContactMethod`,
        `customer`.`PreferredContactTime` AS `customer_PreferredContactTime`,
        `customerrequest`.`Priority` AS `customerrequest_Priority`,
        `customerrequest`.`Status_id` AS `customerrequest_Status_id`,
        `customerrequest`.`AssignedTo` AS `customerrequest_AssignedTo`,
        `customerrequest`.`RequestSubject` AS `customerrequest_RequestSubject`,
        `customerrequest`.`Accountid` AS `customerrequest_Accountid`,
        `customerrequest`.`createdby` AS `customerrequest_createdby`,
        `customerrequest`.`modifiedby` AS `customerrequest_modifiedby`,
        `customerrequest`.`createdts` AS `customerrequest_createdts`,
        `customerrequest`.`lastmodifiedts` AS `customerrequest_lastmodifiedts`,
        `customerrequest`.`synctimestamp` AS `customerrequest_synctimestamp`,
        `customerrequest`.`softdeleteflag` AS `customerrequest_softdeleteflag`,
        `requestmessage`.`id` AS `requestmessage_id`,
        `requestmessage`.`isPriorityMessage` AS `requestmessage_isPriorityMessage`,
        `requestmessage`.`RepliedBy` AS `requestmessage_RepliedBy`,
        `requestmessage`.`RepliedBy_Name` AS `requestmessage_RepliedBy_Name`,
        `requestmessage`.`MessageDescription` AS `requestmessage_MessageDescription`,
        `requestmessage`.`ReplySequence` AS `requestmessage_ReplySequence`,
        `requestmessage`.`IsRead` AS `requestmessage_IsRead`,
        `requestmessage`.`createdby` AS `requestmessage_createdby`,
        `requestmessage`.`modifiedby` AS `requestmessage_modifiedby`,
        `requestmessage`.`createdts` AS `requestmessage_createdts`,
        `requestmessage`.`lastmodifiedts` AS `requestmessage_lastmodifiedts`,
        `requestmessage`.`synctimestamp` AS `requestmessage_synctimestamp`,
        `requestmessage`.`softdeleteflag` AS `requestmessage_softdeleteflag`,
        `messageattachment`.`id` AS `messageattachment_id`,
        `messageattachment`.`AttachmentType_id` AS `messageattachment_AttachmentType_id`,
        `messageattachment`.`Media_id` AS `messageattachment_Media_id`,
        `messageattachment`.`createdby` AS `messageattachment_createdby`,
        `messageattachment`.`modifiedby` AS `messageattachment_modifiedby`,
        `messageattachment`.`createdts` AS `messageattachment_createdts`,
        `messageattachment`.`lastmodifiedts` AS `messageattachment_lastmodifiedts`,
        `messageattachment`.`softdeleteflag` AS `messageattachment_softdeleteflag`,
        `media`.`id` AS `media_id`,
        `media`.`Name` AS `media_Name`,
        `media`.`Size` AS `media_Size`,
        `media`.`Type` AS `media_Type`,
        `media`.`Description` AS `media_Description`,
        `media`.`Url` AS `media_Url`,
        `media`.`createdby` AS `media_createdby`,
        `media`.`modifiedby` AS `media_modifiedby`,
        `media`.`lastmodifiedts` AS `media_lastmodifiedts`,
        `media`.`synctimestamp` AS `media_synctimestamp`,
        `media`.`softdeleteflag` AS `media_softdeleteflag`
    FROM
        (`customerrequest`
		JOIN `requestmessage` ON (`customerrequest`.`id` = `requestmessage`.`CustomerRequest_id`)
		JOIN `customer` ON (`customerrequest`.`Customer_id` = `customer`.`id`)
		JOIN `requestcategory` ON (`customerrequest`.`RequestCategory_id` = `requestcategory`.`id`)
        LEFT JOIN `systemuser` ON (`customerrequest`.`AssignedTo` = `systemuser`.`id`)
        LEFT JOIN `messageattachment` ON (`requestmessage`.`id` = `messageattachment`.`RequestMessage_id`)
        LEFT JOIN `media` ON (`messageattachment`.`Media_id` = `media`.`id`))
	WHERE true ";

IF _customerID != '' THEN
		SET @queryStatement = concat(@queryStatement," and customer.id = ", quote(_customerID));
  END if;

  IF _customerFirstName != '' THEN
		SET @queryStatement = concat(@queryStatement," and customer.FirstName = ", quote(_customerFirstName));
  END if;

  IF _customerMiddleName != '' THEN
		SET @queryStatement = concat(@queryStatement," and customer.MiddleName = ", quote(_customerMiddleName));
  END if;

  IF _customerLastName != '' THEN
		SET @queryStatement = concat(@queryStatement," and customer.LastName = ", quote(_customerLastName));
  END if;

  IF _customerUsername != '' THEN
		SET @queryStatement = concat(@queryStatement," and customer.LastName = ", quote(_customerUsername));
  END if;

  IF _messageRepliedBy != '' THEN
		SET @queryStatement = concat(@queryStatement," and requestmessage.RepliedBy = ", quote(_messageRepliedBy));
  END if;

  IF _requestSubject != '' THEN
		SET @queryStatement = concat(@queryStatement," and customerrequest.RequestSubject = ", quote(_requestSubject));
  END if;

  IF _requestAssignedTo != '' THEN
		SET @queryStatement = concat(@queryStatement," and customerrequest.AssignedTo = ", quote(_requestAssignedTo));
  END if;

 IF _requestCategory != '' THEN
		SET @queryStatement = concat(@queryStatement," and customerrequest.RequestCategory_id = ", quote(_requestCategory));
  END if;

   IF _requestID != '' THEN
		SET @queryStatement = concat(@queryStatement," and customerrequest.id = ", quote(_requestID));
  END if;

   IF _requestStatusID != '' THEN
		SET @queryStatement = concat(@queryStatement," and customerrequest.Status_id = ", quote(_requestStatusID));
  END if;
  
   IF _customerName != '' THEN
		SET @queryStatement = concat(@queryStatement," and  CONCAT(`customer`.`FirstName`,`customer`.`LastName`).Status_id = ", quote(_customerName));
  END if;
  
IF  _dateInitialPoint != '' THEN
     IF LOCATE("=", _dateInitialPoint)!=0 THEN
    	SET @queryStatement = concat(@queryStatement," and requestmessage.createdts = ", quote(_dateInitialPoint));
     ELSEIF LOCATE(">", _dateInitialPoint)!=0 THEN
        SET @queryStatement = concat(@queryStatement," and requestmessage.createdts > ", quote(_dateInitialPoint));
     ELSEIF LOCATE("<", _dateInitialPoint)!=0 THEN
        SET @queryStatement = concat(@queryStatement," and requestmessage.createdts < ", quote(_dateInitialPoint));
	 ELSEIF _dateFinalPoint != '' THEN
        SET @queryStatement = concat(@queryStatement," and requestmessage.createdts > ", quote(_dateInitialPoint), " and requestmessage.createdts < ", quote(_dateFinalPoint));
	 END IF;	
 END IF;	
 		
 		SET @queryStatement = concat(@queryStatement," ORDER BY `requestmessage`.`ReplySequence` DESC");
		PREPARE stmt FROM @queryStatement;
		execute stmt;
		DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_request_search_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_request_search_proc`(
  in _dateInitialPoint varchar(50), 
  in _dateFinalPoint varchar(50), 
  in _requestStatusID varchar(50), 
  in _requestCategory varchar(50), 
  in _offset varchar(50), 
  in _sortCriteria varchar(50), 
  in _sortOrder varchar(50), 
  in _requestAssignedTo varchar(50), 
  in _searchKey varchar(50), 
  in _messageRepliedBy varchar(50), 
  in _recordsPerPage varchar(50),
  in _queryType varchar(50)
)
BEGIN 
SET 
  @selectClause := IF(
    IFNULL(_queryType, '') = "count", 
    'count(cr.id) AS cnt', 
    'cr.id AS customerrequest_id'
  );
SET 
  @stmt := CONCAT(
    'SELECT ', @selectClause, ' FROM customerrequest cr '
  );
SET 
  @whereclause := ' WHERE true';
SET 
  @joinCustomer := 0;
IF _dateInitialPoint != '' 
AND _dateFinalPoint != '' THEN 
SET 
  @whereclause = CONCAT(
    @whereclause, " 
  AND TIMESTAMP(DATE(cr.createdts)) >= ", 
    quote(_dateInitialPoint), "
  AND ", 
    " TIMESTAMP(DATE(cr.createdts)) <= ",quote(_dateFinalPoint)
  );
ELSEIF _dateInitialPoint != '' THEN 
SET 
  @whereclause = CONCAT(
    @whereclause, " 
  AND TIMESTAMP(DATE(cr.createdts)) = ", 
    quote(_dateInitialPoint)
  );
ELSEIF _dateFinalPoint != '' THEN 
SET 
  @whereclause = CONCAT(
    @whereclause, " 
  AND TIMESTAMP(DATE(cr.createdts)) = ", 
    quote(_dateFinalPoint)
  );
END IF;
IF _requestStatusID != '' THEN 
SET 
  @whereclause = CONCAT(
    @whereclause, " 
  AND cr.Status_id IN 
  (
    ", 
   func_escape_input_for_in_operator(_requestStatusID), "
  )
  "
  );
END IF;
IF _requestCategory != '' THEN 
SET 
  @whereclause = CONCAT(
    @whereclause, " 
  AND cr.RequestCategory_id = ", 
    quote(_requestCategory)
  );
END IF;
IF _requestAssignedTo != '' THEN 
SET 
  @whereclause = CONCAT(
    @whereclause, " 
  AND cr.AssignedTo = ", 
    quote(_requestAssignedTo)
  );
END IF;
IF _messageRepliedBy != '' THEN 
SET 
  @stmt = CONCAT(
    @stmt, 'JOIN requestmessage ON (cr.id = requestmessage.CustomerRequest_id) '
  );
SET 
  @whereclause = CONCAT(
    @whereclause, " 
  AND requestmessage.RepliedBy_id = ", 
    quote(_messageRepliedBy)
  );
END IF;
IF _searchKey != '' THEN 
SET 
  @joinCustomer = 1;
SET _searchKey=CONCAT("%",_searchKey,"%");
SET 
  @whereclause = CONCAT(
    @whereclause, " 
  AND 
  (
    cr.Customer_id LIKE ", 
    quote(_searchKey), " OR cr.id LIKE ", 
    quote(_searchKey), " OR customer.UserName LIKE ", 
    quote(_searchKey), ")"
  );
END IF;
IF _queryType != 'count' THEN IF _sortCriteria = 'customer_Fullname' THEN 
SET 
  @joinCustomer = 1;
END IF;

SET @sortColumn='cr.lastmodifiedts';
IF( _sortCriteria = 'customerrequest_Customer_id') THEN
    SET @sortColumn = 'cr.Customer_id';
ELSEIF( _sortCriteria = 'customerrequest_AssignedTo') THEN
    SET @sortColumn = 'cr.AssignedTo';
ELSEIF( _sortCriteria = 'customerrequest_createdts') THEN
    SET @sortColumn = 'cr.createdts';
ELSEIF( _sortCriteria = 'customerrequest_RequestCategory_id') THEN
    SET @sortColumn = 'cr.RequestCategory_id';
ELSEIF( _sortCriteria = 'customer_Fullname') THEN
    SET @sortColumn = 'CONCAT(customer.FirstName,customer.LastName)';
ELSEIF( _sortCriteria = 'customerrequest_Status_id') THEN
    SET @sortColumn = 'cr.Status_id';
ELSEIF( _sortCriteria = 'customerrequest_AssignedTo_Name') THEN
    SET @sortColumn = 'CONCAT(systemuser.FirstName,systemuser.LastName)';
END IF;
  
  
SET 
  @whereclause = CONCAT(
    @whereclause, 
    " 
ORDER BY
  ", 
    IF(
      @sortColumn = '', 'cr.lastmodifiedts', 
      @sortColumn
    ), 
    ' ', 
    IF(
      @sortColumn = '' 
      OR _sortOrder = '', 
      'DESC', 
      _sortOrder
    )
  );

SET 
  @whereclause = CONCAT(
    @whereclause, 
    " LIMIT ", 
    IF(_offset = '', '0', _offset),
    ',',
    IF(_recordsPerPage = '', '10', _recordsPerPage)
  );
END IF;
IF @joinCustomer = 1 THEN 
SET 
  @stmt = CONCAT(
    @stmt, 'JOIN customer ON (cr.Customer_id = customer.id) '
  );
END IF;
IF _sortCriteria = 'customerrequest_AssignedTo_Name' THEN 
SET 
  @stmt = CONCAT(
    @stmt, 'LEFT JOIN systemuser ON (cr.AssignedTo = systemuser.id) '
  );
END IF;

SET 
  @stmt = CONCAT(@stmt, @whereclause);
PREPARE stmt 
FROM 
  @stmt;

 -- select @stmt;  
 EXECUTE stmt;
 DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_search_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_search_proc`( 
in _searchType varchar(60),
in _id varchar(50), 
in _name varchar(50),  
in _SSN varchar(50),
in _username varchar(50), 
in _phone varchar(100), 
in _email varchar(100),
in _dateOfBirth varchar(100),
in _IsStaffMember varchar(10),
in _cardorAccountnumber varchar(50),
in _TIN varchar(50),
in _group varchar(40), 
in _IDType varchar(50),
in _IDValue varchar(50),
in _companyId varchar(50),
in _requestID varchar(50),
in _branchIDS varchar(2000), 
in _productIDS varchar(2000), 
in _cityIDS varchar(2000), 
in _entitlementIDS varchar(2000), 
in _groupIDS varchar(2000), 
in _customerStatus varchar(50), 
in _before varchar(20), 
in _after varchar(20), 
in _sortVariable varchar(100), 
in _sortDirection varchar(4),  
in _pageOffset bigint, 
in _pageSize bigint)
BEGIN
	
    DECLARE _maxLockCount varchar(50);
	IF _searchType LIKE 'GROUP_SEARCH%' then
		set @search_select_statement = "SELECT 
					customer.id, customer.FirstName, customer.MiddleName, customer.LastName,
					concat(IFNULL(customer.FirstName,''), ' ', IFNULL(customer.MiddleName,''), ' ', IFNULL(customer.LastName,'')) as name,
					customer.UserName as Username, customer.isCombinedUser as isCombinedUser,IFNULL(customer.combinedUserId, '') as combinedUserId, customer.Salutation, customer.Gender, customer.IsStaffMember,
					IF((`customer`.`isCombinedUser` = '1'),'TYPE_ID_RETAIL,TYPE_ID_BUSINESS',`customer`.`CustomerType_id`) AS `CustomerTypeId`,
					customer.modifiedby, customer.lastmodifiedts, customer.Status_id, PrimaryEmail.value AS PrimaryEmail,
					GROUP_CONCAT(customergroup.Group_id) as assigned_group_ids,
					address.City_id, city.Name As City_name,
					address.addressLine1 As addressLine1,
					address.addressLine2 As addressLine2,
					city.Name As city,
					address.zipCode As zipCode,
					country.Name As county,
					customer.isEnrolled as isEnrolled,
					customer.Location_id AS branch_id,
					location.Name AS branch_name";
		set @search_count_statement = "SELECT count(distinct customer.id) as SearchMatchs";
		set @queryStatement = concat("
				FROM customer
                    JOIN (
						SELECT
							customer.id
						FROM
							customer ",
							IF( _groupIDS != '' OR _entitlementIDS != '', " LEFT JOIN customergroup ON (customergroup.Customer_id=customer.id)", ""),
							IF( _cityIDS != '', " LEFT JOIN customeraddress ON (customer.id=customeraddress.Customer_id AND customeraddress.isPrimary=1 and customeraddress.Type_id='ADR_TYPE_HOME')
							LEFT JOIN address ON (customeraddress.Address_id = address.id)
							LEFT JOIN city ON (address.City_id = city.id)", ""),
							IF( _entitlementIDS != '', " LEFT JOIN customerentitlement ON (customerentitlement.Customer_id=customer.id) ", ""),
							IF( _productIDS != '', " LEFT JOIN customerproduct ON (customerproduct.Customer_id=customer.id) ", ""));
			set @whereclause = "WHERE true";
			
            IF _username != "" then
				
				set @whereclause = concat(@whereclause, " AND (customer.firstname like concat(",quote(_username),",'%')");
				
				
				set @whereclause = concat(@whereclause," OR customer.username like concat(",quote(_username)," ,'%')");
				
				
				set @whereclause = concat(@whereclause," OR customer.id like concat(", quote(_username) ,",'%')) ");
			end if;
            
            
			if _IsStaffMember != "" then
				if _IsStaffMember = "true" then
					set @whereclause = concat(@whereclause," AND customer.IsStaffMember = '1'");
				else
					set @whereclause = concat(@whereclause," AND customer.IsStaffMember = '0'");
				end if;
			end if;
            
			
			if _entitlementIDS != "" then
				set @whereclause = concat(@whereclause," AND (customerentitlement.Service_id in (",func_escape_input_for_in_operator(_entitlementIDS),") 
							OR customergroup.Group_id in ( select Group_id from groupentitlement where Service_id in (",func_escape_input_for_in_operator(_entitlementIDS)," )))");
			end if;
			
			
			if _groupIDS != "" then
				set @whereclause = concat(@whereclause," AND customergroup.Group_id in (",func_escape_input_for_in_operator(_groupIDS),") ");
			end if;
			
			
			if _productIDS != "" then
				set @whereclause = concat(@whereclause," AND customerproduct.Product_id in (",func_escape_input_for_in_operator(_productIDS),")");
			end if;
			
			
			if _branchIDS != "" then
				set @whereclause = concat(@whereclause, " AND customer.Location_id in (",func_escape_input_for_in_operator(_branchIDS),")");
			end if;
			
			
			if _customerStatus != "" then
				set @whereclause = concat(@whereclause, " AND customer.Status_id = ", quote(_customerStatus));
			end if;
			
			
			if _cityIDS != "" then
				set @whereclause = concat(@whereclause, " AND city.Name in (",func_escape_input_for_in_operator(_cityIDS),")");
			end if;
			
			
			if _before != "" and _after != "" then
				set @whereclause = concat(@whereclause, " AND date(customer.createdts) >= date ", quote(_before) ," and date(customer.createdts) <= date ", quote(_after));
			else 
				if _before != "" then
					set @whereclause = concat(@whereclause, " AND date(customer.createdts) <= date ", quote(_before));
				elseif _after != "" then
					set @whereclause = concat(@whereclause, " AND date(customer.createdts) >= date ", quote(_after));
				end if;
			end if;
			
			set @queryStatement = concat(@queryStatement,@whereclause,
            ") paginatedCustomers ON (paginatedCustomers.id=customer.id)
					
					LEFT JOIN customercommunication PrimaryEmail ON (PrimaryEmail.Customer_id=paginatedCustomers.id AND PrimaryEmail.isPrimary=1 AND PrimaryEmail.Type_id='COMM_TYPE_EMAIL')
					LEFT JOIN customergroup ON (customergroup.Customer_id=paginatedCustomers.id)
					LEFT JOIN customeraddress ON (paginatedCustomers.id=customeraddress.Customer_id AND customeraddress.isPrimary=1 and customeraddress.Type_id='ADR_TYPE_HOME')
					LEFT JOIN address ON (customeraddress.Address_id = address.id)
					LEFT JOIN city ON (city.id = address.City_id)
					LEFT JOIN country ON (city.Country_id = country.id)
					LEFT JOIN location ON (location.id=customer.Location_id)  
			");
			
			IF _searchType = 'GROUP_SEARCH' THEN
                set @queryStatement2 = concat(@search_count_statement, @queryStatement);
				set @queryStatement = concat(@search_select_statement, @queryStatement, " group by paginatedCustomers.id ");
				IF _sortVariable = "DEFAULT" OR _sortVariable = "" THEN
					set @queryStatement = concat(@queryStatement, " ORDER BY FirstName");
	            ELSEIF _sortVariable != "" THEN
					set @queryStatement = concat(@queryStatement, " ORDER BY ",_sortVariable);
	            end if;
	            IF _sortDirection != "" THEN
					set @queryStatement = concat(@queryStatement, " ",_sortDirection);
	            end if;
	            set @queryStatement = concat(@queryStatement, " LIMIT ",_pageOffset,",",_pageSize);

			ELSEIF _searchType = 'GROUP_SEARCH_TOTAL_COUNT' then
				set @queryStatement = concat(@search_count_statement, @queryStatement);
			END IF;


	ELSEIF _searchType LIKE 'CUSTOMER_SEARCH%' then
			
			SELECT accountLockoutThreshold from passwordlockoutsettings where id='PLOCKID1' INTO _maxLockCount;
            
            SET @search_select_statement = concat("SELECT customer.id, customer.FirstName, customer.MiddleName, customer.LastName,customer.DateOfBirth,
				concat(IFNULL(customer.FirstName,''), ' ', IFNULL(customer.MiddleName,''), ' ', IFNULL(customer.LastName,'')) as name,
				customer.UserName as Username,customer.isEnrolledFromSpotlight as isEnrolledFromSpotlight,customer.isCombinedUser as isCombinedUser,IFNULL(customer.combinedUserId, '') as combinedUserId, customer.Salutation, customer.Gender,CONCAT('****', RIGHT(customer.Ssn, 4)) as Ssn,
                IF((`customer`.`isCombinedUser` = '1'),'TYPE_ID_RETAIL,TYPE_ID_BUSINESS',`customer`.`CustomerType_id`) AS `CustomerTypeId`, 
                company.id as CompanyId, company.Name as CompanyName,
                organisationemployees.isAuthSignatory as isAuthSignatory, 
                IF(IFNULL(customer.lockCount,0) >= ",_maxLockCount,", 'SID_CUS_LOCKED',customer.Status_id) as Status_id,
				PrimaryPhone.value AS PrimaryPhoneNumber,
				PrimaryEmail.value AS PrimaryEmailAddress,
                GROUP_CONCAT(membergroup.Name) as groups,
				address.addressLine1 As addressLine1,
				address.addressLine2 As addressLine2,
				city.Name As city,
				address.zipCode As zipCode,
				country.Name As county,
				customer.isEnrolled as isEnrolled,
                customer.ApplicantChannel, customer.createdts");
            SET @search_count_statement = "SELECT count(distinct customer.id) as SearchMatchs";
			SET @queryStatement = concat("
			FROM customer
			JOIN (
				SELECT
					customer.id
				FROM customer ",
					IF( _phone != '', " JOIN customercommunication PrimaryPhone ON (PrimaryPhone.Customer_id=customer.id AND PrimaryPhone.isPrimary=1 AND PrimaryPhone.Type_id='COMM_TYPE_PHONE') ",""),
					IF(_email != '', "  JOIN customercommunication PrimaryEmail ON (PrimaryEmail.Customer_id=customer.id AND PrimaryEmail.isPrimary=1 AND PrimaryEmail.Type_id='COMM_TYPE_EMAIL') ",""),
                    IF( _TIN != '', " LEFT JOIN organisationmembership ON (customer.Organization_id = organisationmembership.Organization_id)", ""),
                    IF( _cardorAccountnumber != '', " LEFT JOIN card ON (customer.id = card.User_id)
                    LEFT JOIN accounts ON (customer.id = accounts.User_id)
                    LEFT JOIN customeraccounts ON (customer.id = customeraccounts.Customer_id)", ""));

                SET @queryStatement = concat(@queryStatement," WHERE true");
                
                IF _id != '' THEN
					set @queryStatement = concat(@queryStatement," and customer.id = ", quote(_id));
                end if;
                
                 IF _name != '' THEN
					set @queryStatement = concat(@queryStatement," and customer.LastName like concat(", quote(_name),",'%')");
                end if;

                IF _SSN != '' THEN
					set @queryStatement = concat(@queryStatement," and customer.Ssn = ",quote(_SSN));
                end if;

                IF _username != '' THEN
					set @queryStatement = concat(@queryStatement," and customer.username = ",quote(_username));
                end if;
                
                 IF _dateOfBirth != '' THEN
					set @queryStatement = concat(@queryStatement," and customer.DateOfBirth = ",quote(_dateOfBirth));
                end if;
                
                IF _phone != '' THEN
					IF length(_phone) > 9 THEN
						set @queryStatement = concat(@queryStatement," and PrimaryPhone.value like concat('%',",quote(_phone),",'%')");
                    ELSE
						set @queryStatement = concat(@queryStatement," and PrimaryPhone.value = ",quote(_phone));
					end if;
                end if;

                IF _email != '' THEN
					set @queryStatement = concat(@queryStatement," and PrimaryEmail.value = ",quote(_email));
                end if;

                IF _companyId != '' THEN
					set @queryStatement = concat(@queryStatement," and customer.Organization_id = ",quote(_companyId));
                end if;

				IF _IDValue != '' THEN
                    IF _IDType = 'ID_DRIVING_LICENSE' THEN
						set @queryStatement = concat(@queryStatement," and (customer.DrivingLicenseNumber = ",quote(_IDValue)," or 
                        (customer.IDType_id = ",quote(_IDType)," and customer.IDValue = ",quote(_IDValue),"))");
                    ELSE
						set @queryStatement = concat(@queryStatement," and (customer.IDType_id = ",quote(_IDType)," and customer.IDValue = ",quote(_IDValue),")");
					end if;
                end if;
                
				IF _TIN != '' THEN
					set @queryStatement = concat(@queryStatement," and organisationmembership.Taxid = ",quote(_TIN));
                end if;
                
                IF _cardorAccountnumber != '' THEN
					set @queryStatement = concat(@queryStatement," and (card.cardNumber = ",quote(_cardorAccountnumber),
                    " or accounts.Account_id = ",quote(_cardorAccountnumber)," or customeraccounts.Account_id = ",quote(_cardorAccountnumber),")");
                end if;
                
                set @queryStatement = concat(@queryStatement,
                ") paginatedCustomers ON (paginatedCustomers.id=customer.id)
			LEFT JOIN customercommunication PrimaryPhone ON (PrimaryPhone.Customer_id=paginatedCustomers.id AND PrimaryPhone.isPrimary=1 AND PrimaryPhone.Type_id='COMM_TYPE_PHONE')
			LEFT JOIN customercommunication PrimaryEmail ON (PrimaryEmail.Customer_id=paginatedCustomers.id AND PrimaryEmail.isPrimary=1 AND PrimaryEmail.Type_id='COMM_TYPE_EMAIL')
			LEFT JOIN customeraddress ON (paginatedCustomers.id=customeraddress.Customer_id AND customeraddress.isPrimary=1 and customeraddress.Type_id='ADR_TYPE_HOME')
			LEFT JOIN address ON (customeraddress.Address_id = address.id)
			LEFT JOIN city ON (city.id = address.City_id)
			LEFT JOIN country ON (city.Country_id = country.id)
			LEFT JOIN customergroup ON (customergroup.Customer_id=paginatedCustomers.id)
			LEFT JOIN membergroup ON (membergroup.id=customergroup.group_id)
            LEFT JOIN organisation company ON (customer.Organization_id = company.id)
            LEFT JOIN organisationemployees ON (organisationemployees.Organization_id = company.id)");

            IF _searchType = 'CUSTOMER_SEARCH' THEN
				set @queryStatement2 = concat(@search_count_statement, @queryStatement);
            	set @queryStatement = concat(@search_select_statement, @queryStatement, " group by paginatedCustomers.id ");
            	IF _sortVariable = "DEFAULT" OR _sortVariable = "" THEN
					set @queryStatement = concat(@queryStatement, " ORDER BY FirstName");
	            ELSEIF _sortVariable != "" THEN
					set @queryStatement = concat(@queryStatement, " ORDER BY ",_sortVariable);
	            end if;
	            IF _sortDirection != "" THEN
					set @queryStatement = concat(@queryStatement, " ",_sortDirection);
	            end if;
	            set @queryStatement = concat(@queryStatement, " LIMIT ",_pageOffset,",",_pageSize);

            ELSEIF _searchType = 'CUSTOMER_SEARCH_TOTAL_COUNT' THEN
            	set @queryStatement = concat(@search_count_statement, @queryStatement);
            END IF;
	END IF;

	PREPARE stmt FROM @queryStatement; EXECUTE stmt; 
     IF _searchType = 'CUSTOMER_SEARCH' OR _searchType = 'GROUP_SEARCH'THEN
          PREPARE stmt FROM @queryStatement2; EXECUTE stmt;
     END If;
    DEALLOCATE PREPARE stmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customer_unread_message_count_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customer_unread_message_count_proc`(
IN _customerId  varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
   SELECT 
        COUNT(`requestmessage`.`id`) AS `messageCount`
    FROM
        (`customerrequest`
        JOIN `requestmessage` ON ((`requestmessage`.`CustomerRequest_id` = `customerrequest`.`id`)))
    WHERE
        (`requestmessage`.`IsRead` = 'FALSE' and `customerrequest`.`softdeleteflag`= 0 and `customerrequest`.`Customer_id`= _customerId 
        and `customerrequest`.`Status_id` != 'SID_DELETED');
  SELECT 
        COUNT(`requestmessage`.`id`) AS `priorityMessageCount`
    FROM
        (`customerrequest`
        JOIN `requestmessage` ON ((`requestmessage`.`CustomerRequest_id` = `customerrequest`.`id`)))
    WHERE
        (`requestmessage`.`IsRead` = 'FALSE' and `customerrequest`.`softdeleteflag`= 0 and `customerrequest`.`Customer_id`= _customerId 
        and `customerrequest`.`Status_id` != 'SID_DELETED' and `requestmessage`.`isPriorityMessage`="1" );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customrole_actionlimits_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customrole_actionlimits_create_proc`(
  IN _queryInput MEDIUMTEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
  IN _customRoleId bigint(20))
BEGIN
      DELETE FROM customroleactionlimits where customroleactionlimits.customRole_id = _customRoleId;
      set @index = 0;
      set @numOfRecords = LENGTH(_queryInput) - LENGTH(REPLACE(_queryInput, '|', '')) + 1;
      insertRecords : LOOP
          set @index = @index + 1;
          IF @index = @numOfRecords + 1 THEN 
            LEAVE insertRecords;
          else
            set @recordsData = SUBSTRING_INDEX(SUBSTRING_INDEX(_queryInput, '|', @index), '|', -1 );
            set @query = concat('INSERT INTO customroleactionlimits(customRole_id,coreCustomerId,contractId,featureId,action_id,account_id,isAllowed, limitGroupId, limitType_id,value) VALUES (',@recordsData,');');
            PREPARE stmt FROM @query; EXECUTE stmt; DEALLOCATE PREPARE stmt;
          END IF;
      END LOOP insertRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `customrole_contract_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `customrole_contract_delete_proc`(in customRoleId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in contractId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in coreCustomerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
begin
       
       SET @contract_statement = 'DELETE FROM contractcustomrole where';
       SET @accounts_statement = 'DELETE FROM customroleaccounts where ';
       SET @excludedaccounts_statement = 'DELETE FROM excludedcustomroleaccounts where ';
       SET @action_statement = 'DELETE FROM customroleactionlimits where ';
       SET @excluded_action_statement = 'DELETE FROM excludedcustomroleactionlimits where ';
       SET @limitgroup_statement = 'DELETE FROM customerlimitgrouplimits where ';
       
       SET @where_clause = '';
       SET @where_clause1 = '';
       SET @where_clause2 = '';
       if(customRoleId != '') THEN
              SET @where_clause = CONCAT(@where_clause , '`customRoleId` = ');
              SET @where_clause = CONCAT(@where_clause , quote(customRoleId));
              SET @where_clause1 = CONCAT(@where_clause1 , '`customRole_id` = ');
              SET @where_clause1 = CONCAT(@where_clause1 , quote(customRoleId));
              SET @where_clause2 = CONCAT(@where_clause2 , '`Customer_id` = ');
              SET @where_clause2 = CONCAT(@where_clause2 , quote(customRoleId));
       
       END IF;
       
       IF(contractId != '') THEN
              IF(@where_clause != '') THEN
                     SET @where_clause = CONCAT(@where_clause , ' AND ');
                     SET @where_clause1 = CONCAT(@where_clause1 , ' AND ');
                     SET @where_clause2 = CONCAT(@where_clause2 , ' AND ');
              END if;
              SET @where_clause = CONCAT(@where_clause , '`contractId` = ');
              SET @where_clause = CONCAT(@where_clause , quote(contractId));
              SET @where_clause1 = CONCAT(@where_clause1 , '`contractId` = ');
              SET @where_clause1 = CONCAT(@where_clause1 , quote(contractId));
              SET @where_clause2 = CONCAT(@where_clause2 , '`contractId` = ');
              SET @where_clause2 = CONCAT(@where_clause2 , quote(contractId));
       END IF;
       
       IF(coreCustomerId != '') THEN
              IF(@where_clause != '') THEN
                     SET @where_clause = CONCAT(@where_clause , ' AND ');
                     SET @where_clause1 = CONCAT(@where_clause1 , ' AND ');
                     SET @where_clause2 = CONCAT(@where_clause2 , ' AND ');
              END if;
              SET @where_clause = CONCAT(@where_clause , '`coreCustomerId` = ');
              SET @where_clause = CONCAT(@where_clause , quote(coreCustomerId));
              SET @where_clause1 = CONCAT(@where_clause1 , '`coreCustomerId` = ');
              SET @where_clause1 = CONCAT(@where_clause1 , quote(coreCustomerId));
              SET @where_clause2 = CONCAT(@where_clause2 , '`coreCustomerId` = ');
              SET @where_clause2 = CONCAT(@where_clause2 , quote(coreCustomerId));
       END IF;
       
       IF(@where_clause != '') THEN     
              SET @contract_statement = CONCAT(@contract_statement , @where_clause);  
              SET @accounts_statement = CONCAT(@accounts_statement , @where_clause);
              SET @excludedaccounts_statement = CONCAT(@excludedaccounts_statement , @where_clause);
              SET @action_statement = CONCAT(@action_statement , @where_clause1);
              SET @excluded_action_statement = CONCAT(@excluded_action_statement , @where_clause1);
              SET @limitgroup_statement = CONCAT(@limitgroup_statement , @where_clause2);    
       
              PREPARE stmt FROM @contract_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
              PREPARE stmt FROM @accounts_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
              PREPARE stmt FROM @excludedaccounts_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
              PREPARE stmt FROM @action_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
              PREPARE stmt FROM @excluded_action_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
              PREPARE stmt FROM @limitgroup_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
       END if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `custom_role_details_fetch_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `custom_role_details_fetch_proc`(
  IN customRoleID TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

    SET @selectDetails = CONCAT("select 
    `customroleactionlimits`.`customRole_id`,
    `customroleactionlimits`.`action_id`,
    `customroleactionlimits`.`account_id`,
    `customroleactionlimits`.`isAllowed`,
    `customroleactionlimits`.`limitType_id`,
    `customroleactionlimits`.`value`,
    `accounts`.`AccountName` as `accountName`,
    `featureaction`.`isAccountLevel`,
    `featureaction`.`Type_id` as `actionType`,
    `featureaction`.`name` as `actionName`,
    `featureaction`.`description` as `actionDescription`,
    `feature`.`name` as `featureName`,
    `feature`.`description` as `featureDescription`,
    `feature`.`id` as `featureId`
     FROM ( `customroleactionlimits`
     LEFT JOIN `accounts` ON ( `customroleactionlimits`.`account_id` = `accounts`.`Account_id`)
     LEFT JOIN `featureaction` ON ( `customroleactionlimits`.`action_id` = `featureaction`.`id`)
     LEFT JOIN `feature` ON ( `featureaction`.`Feature_id` = `feature`.`id` )
     )
     WHERE `customroleactionlimits`.`customRole_id` = ", customRoleID, " ;");

    PREPARE stmt FROM @selectDetails;
    EXECUTE stmt; DEALLOCATE PREPARE stmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `dbpalerts_customercommunication` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `dbpalerts_customercommunication`(_customers TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
select Customer_id, Type_id, Value from customercommunication   where FIND_IN_SET(`customercommunication`.`Customer_id`,_customers) and isPrimary = '1' order by  Type_id; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `dbpalerts_getNotificationId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `dbpalerts_getNotificationId`()
BEGIN
SELECT LAST_INSERT_ID() as lastid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `dbpevents_getCustidFromAccount` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `dbpevents_getCustidFromAccount`(_accounts TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
Select Account_id, User_id, Type_id as accounttype_id from accounts where FIND_IN_SET(`accounts`.`Account_id`,_accounts);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `dbpevents_getCustIdFromCore` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `dbpevents_getCustIdFromCore`(_backendids TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
select BackendId,Customer_id  from backendidentifier where FIND_IN_SET(`backendidentifier`.`BackendId`,_backendids);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `dbpevents_getCustomerData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `dbpevents_getCustomerData`(_customerids TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,_usernames TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
Select id as CustomerId, UserName from customer where FIND_IN_SET(`customer`.`id`,_customerids) or FIND_IN_SET(`customer`.`UserName`,_usernames) ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `dbxcustomeralertentitlement_deletebulk_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `dbxcustomeralertentitlement_deletebulk_proc`( 
IN _deleteRecords TEXT )
BEGIN
  DECLARE index1 INTEGER DEFAULT 0;
 	
  DECLARE EXIT HANDLER for SQLEXCEPTION
    BEGIN
      GET DIAGNOSTICS CONDITION 1 @text = MESSAGE_TEXT;
      SELECT @text as errmsg;
   END;	
 
    set @numOfRecords = LENGTH(_deleteRecords) - LENGTH(REPLACE(_deleteRecords, '|', '')) ;
  deleteRecords : LOOP
     set index1 = index1 + 1;
        IF index1 = @numOfRecords + 1 THEN
            LEAVE deleteRecords;
        else
            set @rowValues = SUBSTRING_INDEX(SUBSTRING_INDEX(_deleteRecords, '|', index1), '|', -1 );
            set @customer_id = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',1 ), '\"', -1 );
            set @alertCategoryId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',2), ',\"', -1 );
            set @alertTypeId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',3), ',\"', -1 );
            set @alertSubTypeId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',4), ',\"', -1 );
            set @accountId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',5), ',\"', -1 );
		    set @accountType = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, ',\"',-1 ), '\"', 1 );
		                   			
		  SET @query  =  CONCAT('delete from `dbxcustomeralertentitlement` where ' ,'`Customer_id` = ',quote(@customer_id),'  and `alertCategoryId` = ',quote(@alertCategoryId),'  and `AlertTypeId` = ',quote(@alertTypeId),'  and `alertSubTypeId` = ',quote(@alertSubTypeId),'  and `AccountId` = ',quote(@accountId),'  and `AccountType` = ',quote(@accountType),' ;');		
	
		 PREPARE stmt FROM @query; EXECUTE stmt; DEALLOCATE PREPARE stmt;	  
			 
        END if;       
    END LOOP deleteRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `dbxcustomeralertentitlement_insertbulk_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `dbxcustomeralertentitlement_insertbulk_proc`(
IN _recordvalues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
begin
	
	DECLARE EXIT HANDLER for SQLEXCEPTION
     BEGIN
      GET DIAGNOSTICS CONDITION 1 @text = MESSAGE_TEXT;
      SELECT @text as errmsg;
     END;

	IF  _recordvalues is not null AND _recordvalues !='' THEN
	         SET @query =  CONCAT('INSERT INTO dbxcustomeralertentitlement(`Customer_id`,`alertCategoryId`,`AlertTypeId`,`alertSubTypeId`,`AccountId`,`AccountType`,`Value1`,`Value2`,`alertRequestId`,`createdby`) VALUES ',_recordvalues,';');
	         PREPARE sql_query FROM @query; EXECUTE sql_query; DEALLOCATE PREPARE sql_query;
	End IF ;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `dbxcustomeralertentitlement_sync` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `dbxcustomeralertentitlement_sync`( 
								in operationType varchar(20) CHARACTER SET UTF8 COLLATE utf8_general_ci,
								in filterValue varchar(255) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
IF operationType = 'edit'  THEN	  
	  Delete from `dbxcustomeralertentitlement` where `alertSubTypeId`= filterValue;	
ELSEIF operationType = 'reassign' THEN
	  Delete from `dbxcustomeralertentitlement` where `AlertTypeId` = filterValue;
end IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `dbxcustomeralertentitlement_updatebulk_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `dbxcustomeralertentitlement_updatebulk_proc`( 
IN _updateRecords TEXT)
begin
	
 DECLARE index1 INTEGER DEFAULT 0;	
 DECLARE EXIT HANDLER for SQLEXCEPTION
    BEGIN
      GET DIAGNOSTICS CONDITION 1 @text = MESSAGE_TEXT;
      SELECT @text as errmsg;
   END;	
 
    set @numOfRecords = LENGTH(_updateRecords) - LENGTH(REPLACE(_updateRecords, '|', ''));
  updateRecords : LOOP
     set index1 = index1 + 1;
        IF index1 = @numOfRecords + 1 THEN
            LEAVE updateRecords;
        else
            set @rowValues = SUBSTRING_INDEX(SUBSTRING_INDEX(_updateRecords, '|', index1), '|', -1 );
            set @numOfParams = LENGTH(@rowValues) - LENGTH(REPLACE(@rowValues, ',', '')) ;
            set @customer_id = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',1 ), '\"', -1 );
            set @alertCategoryId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',2), ',\"', -1 );
            set @alertTypeId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',3), ',\"', -1 );
            set @alertSubTypeId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',4), ',\"', -1 );
            set @accountId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',5), ',\"', -1 );
			set @modifiedby = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',6), ',\"', -1 );
		    set @alertRequestId = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',7), ',\"', -1 );
		  
            set @accountType = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, ',\"',-1 ), '\"', 1 );
		
           	SET @query = CONCAT('update `dbxcustomeralertentitlement` set  `modifiedby` =', quote(@modifiedby));
           
           IF STRCMP(@alertRequestId, 'null') != 0 or STRCMP(@alertRequestId, 'NULL') != 0 Then  
                   SET @query = CONCAT( @query,', `alertRequestId` = ', quote(@alertRequestId));	
			END IF;

           
           IF @numOfParams > 7 THEN 
              set @value1 = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',8), ',\"', -1 );
              SET @query = CONCAT( @query,', `Value1` = ', quote(@value1));	
			END IF;
		
		   IF @numOfParams > 8 THEN
		   set @value2 = SUBSTRING_INDEX(SUBSTRING_INDEX(@rowValues, '\",',9), ',\"', -1 );
		   SET @query = CONCAT(@query,', `Value2` = ', quote(@value2));
		   END IF;
			
		  SET @whereCondition =  CONCAT(' where `Customer_id` = ',quote(@customer_id),'  and `alertCategoryId` = ',quote(@alertCategoryId),'  and `AlertTypeId` = ',quote(@alertTypeId),'  and `alertSubTypeId` = ',quote(@alertSubTypeId),'  and `AccountId` = ',quote(@accountId),'  and `AccountType` = ',quote(@accountType),' ;');		
		  SET @query = CONCAT(@query,'  ' , @whereCondition);
		  
		
          PREPARE stmt FROM @query; EXECUTE stmt; DEALLOCATE PREPARE stmt;
			 				 
        END if;       
    END LOOP updateRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `default_autosync_accounts_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `default_autosync_accounts_create_proc`(
    IN _queryInput LONGTEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
    in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
    SET SESSION group_concat_max_len = 100000000;
    set @index = 0;
    set @numOfRecords = LENGTH(_queryInput) - LENGTH(REPLACE(_queryInput, '|', '')) + 1;
    insertRecords : LOOP
          set @index = @index + 1;
          IF @index = @numOfRecords + 1 THEN 
            LEAVE insertRecords;
          else
            set @recordsData = CONCAT(SUBSTRING_INDEX(SUBSTRING_INDEX(_queryInput, '|', @index), '|', -1 ));
            set @coreCustomerId = SUBSTRING_INDEX(SUBSTRING_INDEX(@recordsData, ':',1), ':', -1 );    
            set @accountId = SUBSTRING_INDEX(SUBSTRING_INDEX(@recordsData, ':',2), ':', -1 );
            set @arrangementId = SUBSTRING_INDEX(SUBSTRING_INDEX(@recordsData, ':',3), ':', -1 );
            set @accountName = SUBSTRING_INDEX(SUBSTRING_INDEX(@recordsData, ':',4), ':', -1 );
            set @accountType = SUBSTRING_INDEX(SUBSTRING_INDEX(@recordsData, ':',5), ':', -1 );
            set @ownerType = SUBSTRING_INDEX(SUBSTRING_INDEX(@recordsData, ':',6), ':', -1 );
            SET @contractId = (select contractcorecustomers.contractId from contractcorecustomers where contractcorecustomers.coreCustomerId = @coreCustomerId);
            SET @typeId = (select accounttype.TypeID from accounttype where accounttype.TypeDescription= @accountType);
            
            SET @contractaccounts = (SELECT contractaccounts.id from contractaccounts where contractaccounts.coreCustomerId = @coreCustomerId
            and contractaccounts.accountId = @accountId);
            if(ISNULL(@contractaccounts)) then
                INSERT INTO contractaccounts(id,contractId,accountId,accountName,typeId,coreCustomerId,ownerType,statusDesc,arrangementId)
                                            VALUES (UUID(),@contractId,@accountId,@accountName,@typeId,@coreCustomerId,@ownerType,'Active',@arrangementId);
            end if;
            
            INSERT INTO customeraccounts(id,Customer_id,Account_id,AccountName,contractId,coreCustomerId,accountType)
                                            VALUES (UUID(),_customerId,@accountId,@accountName,@contractId,@coreCustomerId,@accountType);
            
            call user_account_default_actions_create_proc(_customerId,@accountId,@coreCustomerId,@contractId,'');
           END IF;
    END LOOP insertRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DMS_create_user_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `DMS_create_user_proc`(
  in _username varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, 
  in _group varchar(50)
)
BEGIN DECLARE customerId TEXT(2000);
DECLARE Id TEXT(2000);
DECLARE dmsaddressId VARCHAR(50);
DECLARE tempDayName TEXT(2000);
DECLARE tempStartTime TEXT(2000);
DECLARE tempEndTime TEXT(2000);
DECLARE tempServiceName TEXT(2000);
DECLARE servicesList TEXT(10000);
DECLARE serviceName TEXT(2000);
DECLARE b int(1);
DECLARE pipeFlag int(1);
DECLARE addressId TEXT(2000);
DECLARE customerCommId TEXT(2000);
DECLARE customerReqId TEXT(2000);
DECLARE reqMsgId TEXT(2000);
DECLARE LogID Text(2000);

SELECT customer.id from customer where customer.UserName = _username into customerId ;
IF customerId is null then
	SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 10) INTO customerId;
    INSERT INTO `customer` (
	  `id`, `FirstName`, `MiddleName`, 
	  `LastName`, `UserName`, `Salutation`, 
	  `Gender`, `DateOfBirth`, `Status_id`, 
	  `Ssn`, `MaritalStatus_id`, `SpouseName`, 
	  `EmployementStatus_id`, `IsEnrolledForOlb`, 
	  `IsStaffMember`, `Location_id`, 
	  `PreferredContactMethod`, `PreferredContactTime`, 
	  `createdby`, `modifiedby`
	) 
	VALUES 
	(
		customerId, 'John', '', 
		'Bailey', _username, 'Mr.', 'Male', 
		'1999-11-11', 'SID_CUS_ACTIVE', 
		'123456789', 'SID_SINGLE', '', 'SID_EMPLOYED', 
		'1', '0', 'LID1', 'Call', 'Morning, Afternoon', 
		'Kony User', 'Kony Dev'
	  );
      set customerCommId = concat("CID", customerId);
	INSERT INTO `customercommunication` (
	  `id`, `Type_id`, `Customer_id`, `isPrimary`, 
	  `Value`, `Extension`, `Description`, 
	  `createdby`, `modifiedby`
	) 
	VALUES 
	  (
		customerCommId, 'COMM_TYPE_EMAIL', 
		customerId, '1', 'john.bailey@yahoo.com', 
		'Personal', 'NULL', 'Kony User', 
		'Kony Dev'
	  );
	SELECT SYSDATE() + 2 INTO customerCommId;
	set customerCommId = concat("CID", customerCommId);
	INSERT INTO `customercommunication` (
	  `id`, `Type_id`, `Customer_id`, `isPrimary`, 
	  `Value`, `Extension`, `Description`, 
	  `createdby`, `modifiedby`
	) 
	VALUES 
	  (
		customerCommId, 'COMM_TYPE_PHONE', 
		customerId, '1', '8729899218', 'Personal', 
		'NULL', 'Kony User', 'Kony Dev'
	  );
  
end if;

SELECT concat('Addr1',ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 10000)) into dmsaddressId;
INSERT INTO `address` (`id`, `Region_id`, `City_id`, `addressLine1`,`addressLine2`, `zipCode`, `latitude`, `logitude`, `createdby`, `modifiedby`)
 VALUES (dmsaddressId , 'R57', 'CITY1318', '9225 Bee Caves Rd #300','Fusce Rd.', '20620', '40.7128', '74.0060', 'Kony User', 'Kony Dev');

INSERT INTO `customeraddress` (
  `Customer_id`, `Address_id`, `Type_id`, 
  `isPrimary`, `createdby`, `modifiedby`
) 
VALUES 
  (
    customerId, dmsaddressId, 'ADR_TYPE_WORK', 
    '0', 'Kony User', 'Kony Dev'
  );
  
  SELECT concat('Addr2',ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 10000)) into dmsaddressId;
INSERT INTO `address` (`id`, `Region_id`, `City_id`, `addressLine1`,`addressLine2`, `zipCode`, `latitude`, `logitude`, `createdby`, `modifiedby`)
 VALUES (dmsaddressId , 'R52', 'CITY1289', 'P.O. Box 283 8562','Sit Rd.', '20645', '40.7128', '74.0060', 'Kony User', 'Kony Dev');

INSERT INTO `customeraddress` (
  `Customer_id`, `Address_id`, `Type_id`, 
  `isPrimary`, `createdby`, `modifiedby`
) 
VALUES 
  (
    customerId, dmsaddressId, 'ADR_TYPE_HOME', 
    '1', 'Kony User', 'Kony Dev'
  );

INSERT INTO `customergroup` (
  `Customer_id`, `Group_id`, `createdby`, 
  `modifiedby`, `softdeleteflag`
) 
VALUES 
  (
    customerId, _group, 'Kony User', 
    'Kony Dev', '0'
  );
INSERT INTO `customerproduct` (
  `Customer_id`, `Product_id`, `createdby`, 
  `modifiedby`, `softdeleteflag`
) 
VALUES 
  (
    customerId, 'PRODUCT2', 'Kony User', 
    'Kony Dev', '0'
  );
INSERT INTO `customerproduct` (
  `Customer_id`, `Product_id`, `createdby`, 
  `modifiedby`, `softdeleteflag`
) 
VALUES 
  (
    customerId, 'PRODUCT4', 'Kony User', 
    'Kony Dev', '0'
  );
INSERT INTO `customerproduct` (
  `Customer_id`, `Product_id`, `createdby`, 
  `modifiedby`, `softdeleteflag`
) 
VALUES 
  (
    customerId, 'PRODUCT7', 'Kony User', 
    'Kony Dev', '0'
  );
INSERT INTO `customerproduct` (
  `Customer_id`, `Product_id`, `createdby`, 
  `modifiedby`, `softdeleteflag`
) 
VALUES 
  (
    customerId, 'PRODUCT14', 'Kony User', 
    'Kony Dev', '0'
  );
INSERT INTO `customerproduct` (
  `Customer_id`, `Product_id`, `createdby`, 
  `modifiedby`, `softdeleteflag`
) 
VALUES 
  (
    customerId, 'PRODUCT10', 'Kony User', 
    'Kony Dev', '0'
  );
set 
  customerReqId = concat("REC", customerId);
  
  
INSERT INTO `customerrequest` (
  `id`, `RequestCategory_id`, `Customer_id`, 
  `Priority`, `Status_id`, `RequestSubject`, 
  `AssignedTo`, `Accountid`, `createdby`, 
  `modifiedby`, `softdeleteflag`
) 
VALUES 
  (
    customerReqId, 'RCID_DEPOSITS', customerId, 
    'HIGH', 'SID_OPEN', 'Communication information change', 
    null, '090871', 'KonyUser', 'KonyDev', 
    '0'
  );
set 
  reqMsgId = concat("MSG", customerId);
INSERT INTO `requestmessage` (
  `id`, `CustomerRequest_id`, `MessageDescription`, 
  `RepliedBy`, `RepliedBy_id`, `RepliedBy_Name`,`ReplySequence`, `IsRead`, 
  `createdby`, `modifiedby`, `softdeleteflag`
) 
VALUES 
  (
    reqMsgId, customerReqId, 'What is the expected resolution date?', 
    'CUSTOMER', customerId, 'John bailey', '1', 'TRUE', 'KonyUser', 'konyolbuser', 
    '0'
  );
set 
  reqMsgId = concat("MSG", customerId + 1);
INSERT INTO `requestmessage` (
  `id`, `CustomerRequest_id`, `MessageDescription`, 
  `RepliedBy`, `RepliedBy_id`, `RepliedBy_Name`,`ReplySequence`, `IsRead`, 
  `createdby`, `modifiedby`, `softdeleteflag`
) 
VALUES 
  (
    reqMsgId, customerReqId, 'Can you escalate the request?', 
    'CUSTOMER', customerId, 'John bailey', '2', 'TRUE', 'KonyUser', 'konyolbuser', 
    '0'
  );
set 
  reqMsgId = concat("MSG", customerId + 2);
INSERT INTO `requestmessage` (
  `id`, `CustomerRequest_id`, `MessageDescription`, 
  `RepliedBy`, `RepliedBy_id`, `RepliedBy_Name`,`ReplySequence`, `IsRead`, 
  `createdby`, `modifiedby`, `softdeleteflag`
) 
VALUES 
  (
    reqMsgId, customerReqId, 'Share the current status of the request', 
    'CUSTOMER', customerId, 'John bailey', '3', 'TRUE', 'KonyUser', 'konyolbuser', 
    '0'
  );
set 
  customerReqId = concat("REC", customerId + 1);
INSERT INTO `customerrequest` (
  `id`, `RequestCategory_id`, `Customer_id`, 
  `Priority`, `Status_id`, `RequestSubject`, 
  `AssignedTo`, `Accountid`, `lastupdatedbycustomer`, 
  `createdby`, `createdts`, `lastmodifiedts`, 
  `synctimestamp`, `softdeleteflag`
) 
VALUES 
  (
    customerReqId, 'RCID_CREDITCARD', 
    customerId, 'High', 'SID_INPROGRESS', 
    'Bill Payment Failed', 'UID10', 
    '1234', '1', 'admin2', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    '0'
  );
set 
  reqMsgId = concat("MSG", customerId + 3);
INSERT INTO `requestmessage` (
  `id`, `CustomerRequest_id`, `MessageDescription`, 
  `RepliedBy`,  `RepliedBy_id`, `RepliedBy_Name`,`ReplySequence`, `IsRead`, 
  `createdby`, `modifiedby`, `createdts`, 
  `lastmodifiedts`, `synctimestamp`, 
  `softdeleteflag`
) 
VALUES 
  (
    reqMsgId, customerReqId, '<p><span style=\"color: #000000;\">Hi,</span></p>\n<p><span style=\"color: #000000;\">&nbsp;</span></p>\n<p><span style=\"color: #000000;\">I had scheduled a bill payment to my internet provider- AT&amp;T for $25, this Monday but the payment failed. Can you help me out?</span></p>\n<p><span style=\"color: #000000;\">&nbsp;</span></p>\n<p><span style=\"font-size: 11.0pt; font-family: \'Calibri\',sans-serif; color: #000000;\">Thanks </span></p>', 
    'CUSTOMER', customerId, 'John bailey', '1', 'TRUE', 'jane.doe', 
    'jane.doe', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    '0'
  );
set 
  reqMsgId = concat("MSG", customerId + 4);
INSERT INTO `requestmessage` (
  `id`, `CustomerRequest_id`, `MessageDescription`, 
  `RepliedBy`,  `RepliedBy_id`, `RepliedBy_Name`,`ReplySequence`, `IsRead`, 
  `createdby`, `modifiedby`, `createdts`, 
  `lastmodifiedts`, `synctimestamp`, 
  `softdeleteflag`
) 
VALUES 
  (
    reqMsgId, customerReqId, '<p><span style=\"color: #000000;\">Dear Customer,</span></p>\n<p><span style=\"color: #000000;\">&nbsp;</span></p>\n<p><span style=\"color: #000000;\">Thanks for writing to us. We can see that the payment failed because you had insufficient funds in your account on Monday, 19<sup>th</sup> Feb 2018. You seem to have the required balance at the moment. You can try making this payment now. </span></p>\n<p><span style=\"color: #000000;\">&nbsp;</span></p>\n<p><span style=\"color: #000000;\">Do let us know in case you face any other issues. </span></p>\n<p><span style=\"color: #000000;\">&nbsp;</span></p>\n<p><span style=\"color: #000000;\">Thanks,</span></p>\n<p><span style=\"color: #000000;\">Richard</span></p>\n<p><span style=\"font-size: 11.0pt; font-family: \'Calibri\',sans-serif; color: #000000;\">Kony Bank, Customer Service Team</span></p>', 
    'ADMIN|CSR', 'UID10', 'admin', '2', 'TRUE', 'admin1', 'admin1', 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, '0'
  );
set 
  reqMsgId = concat("MSG", customerId + 5);
INSERT INTO `requestmessage` (
  `id`, `CustomerRequest_id`, `MessageDescription`, 
  `RepliedBy`,  `RepliedBy_id`, `RepliedBy_Name`,`ReplySequence`, `IsRead`, 
  `createdby`, `modifiedby`, `createdts`, 
  `lastmodifiedts`, `synctimestamp`, 
  `softdeleteflag`
) 
VALUES 
  (
    reqMsgId, customerReqId, '<p><span style=\"color: #000000;\">Hi Richard,</span></p><p><span style=\"color: #000000;\">&nbsp;</span></p><p><span style=\"color: #000000;\">Thanks for letting me know. I will go ahead with the payment now. </span></p><p><span style=\"color: #000000;\">&nbsp;</span></p><p><span style=\"font-size: 11.0pt; font-family: \'Calibri\',sans-serif; color: #000000;\">Thanks</span></p>', 
     'CUSTOMER', customerId, 'John bailey', '3', 'TRUE', 'jane.doe', 
    'jane.doe', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    '0'
  );
set 
  customerReqId = concat("REC", customerId + 2);
INSERT INTO `customerrequest` (
  `id`, `RequestCategory_id`, `Customer_id`, 
  `Priority`, `Status_id`, `RequestSubject`, 
  `AssignedTo`, `Accountid`, `lastupdatedbycustomer`, 
  `createdby`, `createdts`, `lastmodifiedts`, 
  `synctimestamp`, `softdeleteflag`
) 
VALUES 
  (
    customerReqId, 'RCID_CREDITCARD', 
    customerId, 'High', 'SID_INPROGRESS', 
    'International Transactions on Credit Card', 
    'UID10', '1234', '0', 'admin2', 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, '0'
  );
set 
  reqMsgId = concat("MSG", customerId + 6);
INSERT INTO `requestmessage` (
  `id`, `CustomerRequest_id`, `MessageDescription`, 
  `RepliedBy`, `RepliedBy_id`, `RepliedBy_Name`,`ReplySequence`, `IsRead`, 
  `createdby`, `modifiedby`, `createdts`, 
  `lastmodifiedts`, `synctimestamp`, 
  `softdeleteflag`
) 
VALUES 
  (
    reqMsgId, customerReqId, ' <p><span style=\"color: #000000;\">Dear Customer,</span></p> <p><span style=\"color: #000000;\">&nbsp;</span></p> <p><span style=\"color: #000000;\">Thank you for informing us about your travel plans. Your card is already enabled for international transactions in Australia. You can use the card at all POS transaction points hassle free. Have a great trip!</span></p> <p><span style=\"color: #000000;\">&nbsp;</span></p> <p><span style=\"font-size: 11.0pt; font-family: \'Calibri\',sans-serif; color: #000000;\">Thanks</span></p> <p><span style=\"font-size: 11.0pt; font-family: \'Calibri\',sans-serif; color: #000000;\">Celeste</span></p> <p><span style=\"font-size: 11.0pt; font-family: \'Calibri\',sans-serif; color: #000000;\">Kony Bank, Customer Service</span></p>', 
    'ADMIN|CSR', 'UID10', 'admin', '2', 'TRUE', 'admin1', 'admin1', 
    '2018-02-26 19:11:15', '2018-02-26 19:11:15', 
    '2018-02-26 19:11:15', '0'
  );
set 
  reqMsgId = concat("MSG", customerId + 7);
INSERT INTO `requestmessage` (
  `id`, `CustomerRequest_id`, `MessageDescription`, 
  `RepliedBy`, `RepliedBy_id`, `RepliedBy_Name`,`ReplySequence`, `IsRead`, 
  `createdby`, `modifiedby`, `createdts`, 
  `lastmodifiedts`, `synctimestamp`, 
  `softdeleteflag`
) 
VALUES 
  (
    reqMsgId, customerReqId, ' <p><span style=\"color: #000000;\">Hi,</span></p> <p><span style=\"color: #000000;\">&nbsp;</span></p> <p><span style=\"color: #000000;\">I will be travelling to Australia from 1<sup>st</sup>-10<sup>th</sup> May 2018 and will be using my Kony Bank Credit Card for the same. Please enable international transactions on this card for this period.</span></p> <p><span style=\"color: #000000;\">&nbsp;</span></p> <p><span style=\"font-size: 11.0pt; font-family: \'Calibri\',sans-serif; color: #000000;\">Thanks</span></p>', 
    'CUSTOMER', customerId, 'John bailey', '2', 'TRUE', 'admin1', 'admin1', 
    '2018-02-26 19:11:15', '2018-02-26 19:11:15', 
    '2018-02-26 19:11:15', '0'
  );
SELECT SYSDATE() + 1 INTO LogID;
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 1,LogID + 1, _username, 'Login', 'Login', 
    'Login with Username/Password', 
    CURRENT_TIMESTAMP, 'Open', 'Web', 
    '10.10.1.1', 'Chrome', 'Windows', 
    '1234', '0', 'olbuser', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 2, LogID + 2,_username, 'Profile', 'Update contact number', 
    'Changed Primary Contact Number', 
    CURRENT_TIMESTAMP, 'Open', 'Web', 
    '10.10.1.2', 'Chrome', 'Windows', 
    '1234', '0', 'olbuser', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 3, LogID + 3,_username, 'Bill Pay', 'Activate Bill Payment Service', 
    'Activate Bill Payment Service', 
    CURRENT_TIMESTAMP, 'Open', 'Web', 
    '10.10.1.3', 'Chrome', 'Windows', 
    '1234', '0', 'olbuser', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 4,LogID + 4, _username, 'Bill Pay', 'Add Payee', 
    'Added Payee CitiBank Credit Card', 
    CURRENT_TIMESTAMP, 'Open', 'Web', 
    '10.10.1.4', 'Chrome', 'Windows', 
    '', '', '', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 5,LogID + 5, _username, 'Bill Pay', 'Pay Bill', 
    'BillPayment to CitiBank Credit Card $450', 
    CURRENT_TIMESTAMP, 'Open', 'Web', 
    '10.10.1.5', 'Chrome', 'Windows', 
    '', '', '', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 6,LogID + 6, _username, 'Logout', 'Logout', 
    'Logout', CURRENT_TIMESTAMP, 
    'Open', 'Web', '10.10.1.6', 'Chrome', 
    'Windows', '1234', '0', 'olbuser', 
    CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 7,LogID + 7, _username, 'Login', 'Login', 
    'Login with FaceID', CURRENT_TIMESTAMP, 
    'Open', 'Mobile', '10.10.1.7', 'Moto', 
    'Android', '1234', '0', 'olbuser', 
    CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 8, LogID + 8,_username, 'Accounts', 'View Accounts', 
    'View Checking Account XX2455', 
    CURRENT_TIMESTAMP, 'Open', 'Mobile', 
    '10.10.1.8', 'Moto', 'Android', '1234', 
    '0', 'olbuser', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 9,LogID + 9, _username, 'Transfers', 'Add Recipient', 
    'Add IntraBank Recipient Tom Brummet', 
    CURRENT_TIMESTAMP, 'Open', 'Mobile', 
    '10.10.1.9', 'Moto', 'Android', '1234', 
    '0', 'olbuser', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 10, LogID + 10,_username, 'Transfers', 
    'IntraBank Fund Transfer', 'IntraBank FT to Tom Brumet $150', 
    CURRENT_TIMESTAMP, 'Open', 'Mobile', 
    '10.10.1.10', 'Moto', 'Android', 
    '1234', '0', 'olbuser', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 11, LogID + 11,_username, 'Accounts', 'View Accounts', 
    'View Checking Account XX2455', 
    CURRENT_TIMESTAMP, 'Open', 'Mobile', 
    '10.10.1.11', 'Moto', 'Android', 
    '1234', '0', 'olbuser', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 12, LogID + 12,_username, 'Messages', 'Send Message', 
    'Send Message subject Bill Payment Failed', 
    CURRENT_TIMESTAMP, 'Open', 'Mobile', 
    '10.10.1.12', 'Moto', 'Android', 
    '1234', '0', 'olbuser', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 13, LogID + 13,_username, 'P2P', 'Send Money', 
    'Send Money to Judy Blume $25', 
    CURRENT_TIMESTAMP, 'Open', 'Mobile', 
    '10.10.1.13', 'Moto', 'Android', 
    '1234', '0', 'olbuser', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 14,LogID + 14, _username, 'Logout', 'Logout', 
    'Logout', CURRENT_TIMESTAMP, 
    'Open', 'Mobile', '10.10.1.14', 'Moto', 
    'Android', '1234', '0', 'olbuser', 
    CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 15,LogID + 15, _username, 'Login', 'Login', 
    'Login with Username/Password', 
    CURRENT_TIMESTAMP, 'Open', 'Web', 
    '10.10.1.15', 'Chrome', 'Windows', 
    '', '', '', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 16, LogID + 16,_username, 'Wire Transfer', 
    'Manage Payee', 'Edit Details for Payee Jane', 
    CURRENT_TIMESTAMP, 'Open', 'Web', 
    '10.10.1.16', 'Chrome', 'Windows', 
    '', '', '', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 17, LogID + 17,_username, 'Wire Transfer', 
    'Wire Transfer', 'Wire Transfer to Jane $200', 
    CURRENT_TIMESTAMP, 'Open', 'Web', 
    '10.10.1.17', 'Chrome', 'Windows', 
    '', '', '', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 18, LogID + 18,_username, 'Accounts', 'View Accounts', 
    'View Checking Account XX2455', 
    CURRENT_TIMESTAMP, 'Open', 'Web', 
    '10.10.1.18', 'Chrome', 'Windows', 
    '', '', '', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 19, LogID + 19,_username, 'Accounts', 'Add External Account', 
    'Add External Account Chase Bank xx7583', 
    CURRENT_TIMESTAMP, 'Open', 'Web', 
    '10.10.1.19', 'Chrome', 'Windows', 
    '', '', '', CURRENT_TIMESTAMP
  );
INSERT INTO `konydbplog`.`customeractivity` 
(`id`, `sessionId`, `username`, `moduleName`, `activityType`, `description`, `eventts`, `status`, `channel`, `ipAddress`, `device`, `operatingSystem`,`referenceId`, `errorCode`, `createdBy`, `createdOn`)
VALUES 
  (
    LogID + 20, LogID + 20,_username, 'Logout', 'Logout', 
    'Logout', CURRENT_TIMESTAMP, 
    'Open', 'Web', '10.10.1.20', 'Chrome', 
    'Windows', '', '', '', CURRENT_TIMESTAMP
  );
INSERT INTO `customernotification` 
VALUES 
  (
    customerId, 'NID1', 0, 'Kony User', 
    'Kony Dev', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    0
  );
INSERT INTO `customernotification` 
VALUES 
  (
    customerId, 'NID10', 0, 'Kony User', 
    'Kony Dev', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    0
  );
INSERT INTO `customernotification` 
VALUES 
  (
    customerId, 'NID12', 0, 'Kony User', 
    'Kony Dev', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    0
  );
INSERT INTO `customernotification` 
VALUES 
  (
    customerId, 'NID13', 0, 'Kony User', 
    'Kony Dev', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    0
  );
INSERT INTO `customernotification` 
VALUES 
  (
    customerId, 'NID14', 0, 'Kony User', 
    'Kony Dev', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    0
  );
INSERT INTO `customernotification` 
VALUES 
  (
    customerId, 'NID2', 0, 'Kony User', 
    'Kony Dev', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    0
  );
INSERT INTO `customernotification` 
VALUES 
  (
    customerId, 'NID3', 0, 'Kony User', 
    'Kony Dev', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    0
  );
INSERT INTO `customernotification` 
VALUES 
  (
    customerId, 'NID4', 0, 'Kony User', 
    'Kony Dev', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    0
  );
INSERT INTO `customernotification` 
VALUES 
  (
    customerId, 'NID5', 0, 'Kony User', 
    'Kony Dev', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    0
  );
INSERT INTO `customernotification` 
VALUES 
  (
    customerId, 'NID6', 0, 'Kony User', 
    'Kony Dev', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    0
  );
INSERT INTO `customernotification` 
VALUES 
  (
    customerId, 'NID7', 0, 'Kony User', 
    'Kony Dev', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    0
  );
INSERT INTO `customernotification` 
VALUES 
  (
    customerId, 'NID8', 0, 'Kony User', 
    'Kony Dev', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    0
  );
INSERT INTO `customernotification` 
VALUES 
  (
    customerId, 'NID9', 0, 'Kony User', 
    'Kony Dev', CURRENT_TIMESTAMP, 
    CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 
    0
  );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `excluded_customeraction_save_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `excluded_customeraction_save_proc`(
  IN _queryInput MEDIUMTEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
      set @index = 0;
      set @numOfRecords = LENGTH(_queryInput) - LENGTH(REPLACE(_queryInput, '|', '')) + 1;
      insertRecords : LOOP
          set @index = @index + 1;
          IF @index = @numOfRecords + 1 THEN 
            LEAVE insertRecords;
          else
            set @recordsData = concat('\'',UUID(),'\'',',', SUBSTRING_INDEX(SUBSTRING_INDEX(_queryInput, '|', @index), '|', -1 ));
             set @query = concat('INSERT INTO excludedcustomeraction(id,RoleType_id,Customer_id,coreCustomerId,contractId,featureId,action_id,account_id) VALUES (',@recordsData,');');
            PREPARE stmt FROM @query; EXECUTE stmt; DEALLOCATE PREPARE stmt;
           END IF;
      END LOOP insertRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `excluded_customrole_actionlimits_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `excluded_customrole_actionlimits_create_proc`(
  IN _queryInput MEDIUMTEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
  IN _customRoleId bigint(20))
BEGIN
      DELETE FROM excludedcustomroleactionlimits where excludedcustomroleactionlimits.customRole_id = _customRoleId;
      set @index = 0;
      set @numOfRecords = LENGTH(_queryInput) - LENGTH(REPLACE(_queryInput, '|', '')) + 1;
      insertRecords : LOOP
          set @index = @index + 1;
          IF @index = @numOfRecords + 1 THEN 
            LEAVE insertRecords;
          else
            set @recordsData = SUBSTRING_INDEX(SUBSTRING_INDEX(_queryInput, '|', @index), '|', -1 );
            set @query = concat('INSERT INTO excludedcustomroleactionlimits(customRole_id,coreCustomerId,contractId,featureId,action_id,account_id) VALUES (',@recordsData,');');
            PREPARE stmt FROM @query; EXECUTE stmt; DEALLOCATE PREPARE stmt;
          END IF;
      END LOOP insertRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `feature_action_limits_update_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `feature_action_limits_update_proc`(
  IN _action varchar(255) CHARACTER SET UTF8 COLLATE utf8_general_ci,
  IN _minTxLimit decimal(20,2),
  IN _maxTxLimit decimal(20,2),
  IN _dailyLimit decimal(20,2),
  IN _weeklyLimit decimal(20,2)
)
BEGIN
  UPDATE actionlimit SET value = _minTxLimit where Action_id = _action AND LimitType_id = 'MIN_TRANSACTION_LIMIT'; 
  UPDATE actionlimit SET value = _maxTxLimit where Action_id = _action AND LimitType_id = 'MAX_TRANSACTION_LIMIT'; 
  UPDATE actionlimit SET value = _dailyLimit where Action_id = _action AND LimitType_id = 'DAILY_LIMIT';
  UPDATE actionlimit SET value = _weeklyLimit where Action_id = _action AND LimitType_id = 'WEEKLY_LIMIT';	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_accountdetails_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_accountdetails_proc`(
    IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _cif VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _accountIds TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci 
    )
BEGIN 
	IF _cif = "" THEN
		SET _cif = "%";
    END IF;
    
	SELECT 
		`customeraccounts`.`Account_id`,
		`customeraccounts`.`AccountName`,
		`customeraccounts`.`accountType`,
		`contractaccounts`.`ownerType` AS `ownershipType`
	FROM (`customeraccounts` AS `customeraccounts`
		LEFT JOIN
		`contractaccounts` AS `contractaccounts`
		ON `customeraccounts`.`Account_id` = `contractaccounts`.`accountId`)
	WHERE 
		`customeraccounts`.`contractId` = `_contractId` AND
		`customeraccounts`.`coreCustomerId` LIKE `_cif` AND
		FIND_IN_SET(`customeraccounts`.`Account_id`,`_accountIds`) > 0
	ORDER BY `customeraccounts`.`contractId`,`customeraccounts`.`coreCustomerId`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_achfiles_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_achfiles_proc`(
	IN _customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _achFile_id VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _featureactionlist TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _queryType VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _filterByParam TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _filterByValue TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _searchString VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _sortByParam VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _sortOrder VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _pageSize VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _pageOffset VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
MAINLABEL:BEGIN
        SET SESSION group_concat_max_len = 100000000;
        
        SET @combinedIds = (select group_concat(id SEPARATOR ",") from customer where combinedUserId = _customerId);

		IF @combinedIds is NULL THEN      
			SET @combinedIds = _customerId;
		ELSE 
			SET @combinedIds = concat(_customerId , "," ,@combinedIds);
        END IF;
		
		SET @isSelfApprovalEnabled = (select isSelfApprovalEnabled from application);
        
		IF @isSelfApprovalEnabled is FALSE THEN
			SET @notCreatedBySelf = concat(" AND NOT FIND_IN_SET(`achfile`.`createdby`, '",@combinedIds,"')");
		ELSE                
			SET @notCreatedBySelf = "";
		END IF;
		
        SET _filterByParam = IF(_filterByParam = NULL , '', _filterByParam);
        SET _filterByValue = IF(_filterByValue = NULL , '', _filterByValue);
        SET _achFile_id = if(_achFile_id = "" OR _achFile_id = NULL, '%', _achFile_id);
       
        SET @numOfParams = 0;
        IF LENGTH(_filterByParam) > 0 THEN
			SET @numOfParams = LENGTH(_filterByParam) - LENGTH(REPLACE(_filterByParam, ',', '')) + 1;
		END IF;
        
        SET @searchQuery = "";
		SET @idx = 1;
		filterParams:LOOP
			IF @idx > @numOfParams THEN 
				LEAVE filterParams;
			END IF;
			
			SET @filterParam = SUBSTRING_INDEX(SUBSTRING_INDEX(_filterByParam, ',', @idx), ',', -1 );
			SET @filterValue = SUBSTRING_INDEX(SUBSTRING_INDEX(_filterByValue, ',', @idx), ',', -1 );
			SET @searchQuery = concat(@searchQuery, " AND (",@filterParam," LIKE '",@filterValue,"' )");
			SET @idx = @idx + 1;
			 
		END LOOP filterParams;
        
        SET @companyId = (select group_concat(concat(contractId,"_",coreCustomerId) SEPARATOR ",") from contractcustomers where customerId =_customerId);
        IF @companyId is NULL THEN      
			SET @companyId = "";
		END IF;
        
        IF _featureactionlist is NULL THEN      
			LEAVE MAINLABEL;
		END IF;
        
        SET _sortByParam = if(_sortByParam = "" OR _sortByParam = NULL, 'createdts', _sortByParam);
		SET _sortOrder = if(_sortOrder = "" OR _sortOrder = NULL, 'DESC', _sortOrder);
        
        SET @customerMatrixIds = (SELECT group_concat(approvalMatrixId SEPARATOR ",") FROM customerapprovalmatrix WHERE FIND_IN_SET(customerId, @combinedIds));
        IF @customerMatrixIds is NULL THEN      
			SET @customerMatrixIds = "";
		END IF;
        
        SET @alreadyApprovedIds = (select GROUP_CONCAT(DISTINCT(requestId) SEPARATOR ",") from bbactedrequest WHERE FIND_IN_SET(createdby, @combinedIds) AND action = 'Approved');
        IF @alreadyApprovedIds is NULL THEN      
			SET @alreadyApprovedIds = "";
		END IF;
        
        SET @approvalRequestIds = (SELECT group_concat(DISTINCT(requestId) SEPARATOR ",") 
        							FROM requestapprovalmatrix
        							INNER JOIN approvalmatrix ON requestapprovalmatrix.approvalMatrixId = approvalmatrix.id
									INNER JOIN approvalrule ON approvalmatrix.approvalruleId = approvalrule.id
	        							WHERE FIND_IN_SET(requestapprovalmatrix.approvalMatrixId,  @customerMatrixIds) 
	        							AND NOT FIND_IN_SET(requestapprovalmatrix.requestId, @alreadyApprovedIds)
	        							AND ((approvalrule.numberOfApprovals = -1 AND requestapprovalmatrix.receivedApprovals < (SELECT COUNT(DISTINCT(customerId)) FROM customerapprovalmatrix WHERE customerapprovalmatrix.approvalMatrixId = requestapprovalmatrix.approvalMatrixId)) 
													OR
												(approvalrule.numberOfApprovals != -1 AND requestapprovalmatrix.receivedApprovals < approvalrule.numberOfApprovals))
									);
        IF @approvalRequestIds is NULL THEN      
			SET @approvalRequestIds = "''";
		END IF;
        
        SET @queryTypecondition = if(_queryType = 'myRequests', concat(" AND FIND_IN_SET(`achfile`.`createdby`, '",@combinedIds,"') AND (`bbrequest`.`status` = 'Pending' OR `bbrequest`.`status` = 'Approved' OR `bbrequest`.`status` = 'Rejected' ) "),
										if(_queryType = 'pendingForMyApprovals', concat(" AND FIND_IN_SET(`achfile`.`requestId`,  \"",@approvalRequestIds,"\")  AND `achfile`.`status` = 'Pending' ", @notCreatedBySelf),
                                        if(_queryType = 'rejected', concat("AND `achfile`.`status` = 'Rejected' "),
                                        if(_achFile_id = '%' , concat(" AND NOT `achfile`.`status` = 'Withdrawn'"),
                                        ''))));
        
        SET @searchQuery = if(_searchString = NULL OR _searchString = "", @searchQuery, 
                concat(@searchQuery, " AND (`achfile`.`achFileName` LIKE '%",_searchString,"%' OR `achfile`.`requestType` LIKE '%",_searchString,"%')"));
        
        SET @paginationQuery = if(_pageOffset = NULL OR _pageOffset = "" OR _pageSize = NULL OR _pageSize = "", '', concat(" LIMIT ",_pageOffset, ", " ,_pageSize)); 
        
        SET @features = (SELECT group_concat(Feature_id SEPARATOR ",") FROM featureaction WHERE FIND_IN_SET(id,_featureactionlist) > 0);
        IF @features is NULL THEN      
			SET @features = "";
		END IF;
        
        SET @createActions = (SELECT group_concat(id SEPARATOR ",") FROM featureaction WHERE FIND_IN_SET(Feature_id, @features) > 0 AND id LIKE "%_UPLOAD");
        IF @createActions is NULL THEN      
			SET @createActions = "";
		END IF;
		
		SET @companyRequestIds = (SELECT group_concat(requestId SEPARATOR ",") FROM bbrequest WHERE FIND_IN_SET(companyId, @companyId) AND FIND_IN_SET(bbrequest.featureActionId, @createActions));
        
        IF @companyRequestIds is NULL THEN      
            SET @companyRequestIds = "";
        END IF;
        
		SET @select_statement = concat("SELECT * FROM
        
        (SELECT 
			`achfile`.`achFile_id` AS `achFile_id`,
			`achfile`.`achFileName` AS `achFileName`,
			`achfile`.`featureActionId` AS `featureActionId`,
			`achfile`.`debitAmount` AS `debitAmount`,
			`achfile`.`approvalAccounts` AS `approvalAccounts`,
			`achfile`.`debitAccounts` AS `debitAccounts`,
			`achfile`.`createdby` AS `createdby`,
			`achfile`.`createdts` AS `createdts`,
			`achfile`.`requestType` AS `requestType`,
			`achfile`.`numberOfCredits` AS `numberOfCredits`,
			`achfile`.`numberOfDebits` AS `numberOfDebits`,
			`achfile`.`numberOfPrenotes` AS `numberOfPrenotes`,
			`achfile`.`requestId` AS `requestId`,
			`achfile`.`contents` AS `contents`,
			`achfile`.`fileSize` AS `fileSize`,
			`achfile`.`softDelete` AS `softDelete`,
			`achfile`.`creditAmount` AS `creditAmount`,
			`achfile`.`numberOfRecords` AS `numberOfRecords`,
			`achfile`.`achFileFormatType_id` AS `achFileFormatType_id`,
            ( CASE 
				WHEN `bbrequest`.`status` is NULL THEN `achfile`.`status`
                ELSE `bbrequest`.`status` 
			END ) AS `status`,
            `achfile`.`companyId` AS `companyId`,
            `achfile`.`confirmationNumber` AS `confirmationNumber`,
			`customer`.`UserName` AS `userName`,
			`achfileformattype`.`fileType` AS `achFileFormatType`,
			`bbrequest`.`createdby` AS `requestCreatedby`,
			(CASE
				WHEN `achfile`.`createdby` IN (",@combinedIds,") THEN 'true'
				ELSE 'false'
			 END) as `amICreator`,
			(CASE 
				WHEN `bbrequest`.`requestId` IN (",@approvalRequestIds,") THEN 'true'
        		ELSE 'false'
	 		END)
	 		 as `amIApprover`
		FROM
			(((`achfile`
			LEFT JOIN `customer` ON (`achfile`.`createdby` = `customer`.`id`))
			LEFT JOIN `achfileformattype` ON (`achfile`.`achFileFormatType_id` = `achfileformattype`.`id`))
			LEFT JOIN `bbrequest` ON (`achfile`.`requestId` = `bbrequest`.`requestId`))
			WHERE `achfile`.`softDelete` = '0'
				AND (FIND_IN_SET(`achfile`.`companyId`, '", @companyId ,"') OR FIND_IN_SET(`achfile`.`createdby`, '",@combinedIds,"'))
				AND `achfile`.`achFile_id` LIKE '",_achFile_id ,"'
                AND FIND_IN_SET(`achfile`.`featureActionId`, \"", @createActions, "\") ",
                @queryTypecondition, " ",
                @searchQuery, " ) AS t1
            LEFT JOIN
            ( 
				SELECT 
					bbrequest.requestId,
					(select count(DISTINCT(createdby)) from bbactedrequest where bbactedrequest.action = 'Approved' AND  bbactedrequest.requestId = bbrequest.requestId) 
							as receivedApprovals,
                    LEAST(
						(SELECT COUNT(DISTINCT(customerId)) FROM customerapprovalmatrix WHERE customerapprovalmatrix.approvalMatrixId in (SELECT approvalMatrixId FROM requestapprovalmatrix where requestapprovalmatrix.requestId = bbrequest.requestId)) 
						, 
						SUM(
							CASE approvalrule.numberOfApprovals
								WHEN -1 THEN (SELECT COUNT(*) FROM customerapprovalmatrix WHERE customerapprovalmatrix.approvalMatrixId = requestapprovalmatrix.approvalMatrixId)
								WHEN NULL OR \"\" THEN 0
								ELSE approvalrule.numberOfApprovals
							END
						) 
					) as requiredApprovals
				FROM
				 bbrequest
				LEFT JOIN requestapprovalmatrix ON (bbrequest.requestId = requestapprovalmatrix.requestId)
				LEFT JOIN approvalmatrix ON (requestapprovalmatrix.approvalMatrixId = approvalmatrix.id)
				LEFT JOIN approvalrule ON (approvalmatrix.approvalruleId = approvalrule.id)
                WHERE FIND_IN_SET(bbrequest.requestId, \"",@companyRequestIds,"\")
				GROUP BY bbrequest.requestId
            ) AS t2
            ON 
            `t1`.`requestId` = `t2`.`requestId`
            ORDER BY ", _sortByParam, " ", _sortOrder , " ",
            @paginationQuery);
            
	PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_achtemplaterecords_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_achtemplaterecords_proc`(
  IN _templateId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
 
	SELECT 
		`bbtemplaterecord`.`templateRecord_id` as `templateRecord_id`,
		`bbtemplaterecord`.`record_Name` AS `record_Name`,
		`bbtemplaterecord`.`toAccountNumber` AS `toAccountNumber`,
		`bbtemplaterecord`.`abatrcNumber` AS `abatrcNumber`,
		`bbtemplaterecord`.`detail_id` AS `detail_id`,
		`bbtemplaterecord`.`amount` AS `amount`,
		`bbtemplaterecord`.`additionalInfo` AS `additionalInfo`,
		`bbtemplaterecord`.`ein` AS `ein`,
		`bbtemplaterecord`.`isZeroTaxDue` AS `isZeroTaxDue`,
		`bbtemplaterecord`.`template_id` AS `template_id`,
		`bbtemplaterecord`.`taxType_id` AS `taxType_id`,
		`bbtemplaterecord`.`templateRequestType_id` AS `templateRequestType_id`,
		`bbtemplaterecord`.`softDelete` AS `softDelete`,
		`bbtemplaterecord`.`toAccountType` AS `toAccountType`,
		`bbtaxtype`.`taxType` AS `taxType`,
		`achaccountstype`.`accountType` AS `accountType`,
		`bbtemplaterequesttype`.`templateRequestTypeName` AS `templateRequestTypeName`
	FROM 
    ((( `bbtemplaterecord`
    LEFT JOIN `bbtaxtype` ON (`bbtemplaterecord`.`taxType_id` = `bbtaxtype`.`id`))
    LEFT JOIN `achaccountstype` ON (`bbtemplaterecord`.`toAccountType` = `achaccountstype`.`id`))
    LEFT JOIN `bbtemplaterequesttype` ON (`bbtemplaterecord`.`templateRequestType_id` = `bbtemplaterequesttype`.`templateRequestType_id`))
    WHERE `bbtemplaterecord`.`softDelete` = '0'
    AND `bbtemplaterecord`.`template_id` = _templateId;
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_achtemplatesubrecords_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_achtemplatesubrecords_proc`(
  IN _templateRecordId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	SELECT 
		`bbtemplatesubrecord`.`templateSubRecord_id` AS `templateSubRecord_id`,
		`bbtemplatesubrecord`.`templateRecord_id` AS `templateRecord_id`,
		`bbtemplatesubrecord`.`taxSubCategory_id` AS `taxSubCategory_id`,
        `bbtemplatesubrecord`.`amount` AS `amount`,
        `bbtemplatesubrecord`.`softDelete` AS `softDelete`,
		`bbtaxsubtype`.`taxSubType` AS `taxSubType`
		 FROM
		(`bbtemplatesubrecord`
		LEFT JOIN `bbtaxsubtype` ON (`bbtemplatesubrecord`.`taxSubCategory_id` = `bbtaxsubtype`.`id`))
        WHERE `bbtemplatesubrecord`.`softDelete` = '0'
		AND `bbtemplatesubrecord`.`templateRecord_id` =  _templateRecordId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_achtransactionrecords_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_achtransactionrecords_proc`(
  IN _transactionId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
 
	SELECT 
		`achtransactionrecord`.`transactionRecord_id` as `transactionRecord_id`,
        `achtransactionrecord`.`toAccountNumber` AS `toAccountNumber`,
        `achtransactionrecord`.`toAccountType` AS `toAccountType`,
		`achtransactionrecord`.`abatrcNumber` AS `abatrcNumber`,
		`achtransactionrecord`.`detail_id` AS `detail_id`,
		`achtransactionrecord`.`amount` AS `amount`,
		`achtransactionrecord`.`additionalInfo` AS `additionalInfo`,
		`achtransactionrecord`.`eIN` AS `eIN`,
		`achtransactionrecord`.`isZeroTaxDue` AS `isZeroTaxDue`,
        `achtransactionrecord`.`taxType_id` AS `taxType_id`,
		`achtransactionrecord`.`transaction_id` AS `transaction_id`,
        `achtransactionrecord`.`softDelete` AS `softDelete`,
		`achtransactionrecord`.`templateRequestType_id` AS `templateRequestType_id`,
        `achtransactionrecord`.`record_Name` AS `record_Name`,
		`bbtaxtype`.`taxType` AS `taxType`,
		`achaccountstype`.`accountType` AS `accountType`,
		`bbtemplaterequesttype`.`templateRequestTypeName` AS `templateRequestTypeName`
	FROM 
    ((( `achtransactionrecord`
    LEFT JOIN `bbtaxtype` ON (`achtransactionrecord`.`taxType_id` = `bbtaxtype`.`id`))
    LEFT JOIN `achaccountstype` ON (`achtransactionrecord`.`toAccountType` = `achaccountstype`.`id`))
    LEFT JOIN `bbtemplaterequesttype` ON (`achtransactionrecord`.`templateRequestType_id` = `bbtemplaterequesttype`.`templateRequestType_id`))
    WHERE `achtransactionrecord`.`softDelete` = '0'
    AND `achtransactionrecord`.`transaction_id` = _transactionId;
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_achtransactionsubrecords_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_achtransactionsubrecords_proc`(
  IN _transactionRecordId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	SELECT 
		`achtransactionsubrecord`.`transcationSubRecord_id` AS `transcationSubRecord_id`,
        `achtransactionsubrecord`.`amount` AS `amount`,
		`achtransactionsubrecord`.`transactionRecord_id` AS `transactionRecord_id`,
		`achtransactionsubrecord`.`taxSubCategory_id` AS `taxSubCategory_id`,
        `achtransactionsubrecord`.`softDelete` AS `softDelete`,
		`bbtaxsubtype`.`taxSubType` AS `taxSubType`
		 FROM
		(`achtransactionsubrecord`
		LEFT JOIN `bbtaxsubtype` ON (`achtransactionsubrecord`.`taxSubCategory_id` = `bbtaxsubtype`.`id`))
        WHERE `achtransactionsubrecord`.`softDelete` = '0'
		AND `achtransactionsubrecord`.`transactionRecord_id` =  _transactionRecordId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_achtransaction_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_achtransaction_proc`(
	IN _customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _transactionId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _featureactionlist TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _queryType VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _filterByParam TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _filterByValue TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _searchString VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _sortByParam VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _sortOrder VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _pageSize VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _pageOffset VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
MAINLABEL:BEGIN
		SET SESSION group_concat_max_len = 100000000;
        
        SET @combinedIds = (select group_concat(id SEPARATOR ",") from customer where combinedUserId = _customerId);

		IF @combinedIds is NULL THEN      
			SET @combinedIds = _customerId;
		ELSE 
			SET @combinedIds = concat(_customerId , "," ,@combinedIds);
        END IF;
        
		SET @isSelfApprovalEnabled = (select isSelfApprovalEnabled from application);
		
		IF @isSelfApprovalEnabled is FALSE THEN
            SET @notCreatedBySelf = concat(" AND NOT FIND_IN_SET(`achtransaction`.`createdby`, '",@combinedIds,"')");
        ELSE
            SET @notCreatedBySelf = "";
		END IF;
		
		SET _filterByParam = IF(_filterByParam = NULL , '', _filterByParam);
        SET _filterByValue = IF(_filterByValue = NULL , '', _filterByValue);
        SET _transactionId = if(_transactionId = "" OR _transactionId = NULL, '%', _transactionId);
        
        SET @numOfParams = 0;
        IF LENGTH(_filterByParam) > 0 THEN
			SET @numOfParams = LENGTH(_filterByParam) - LENGTH(REPLACE(_filterByParam, ',', '')) + 1;
		END IF;
        
        SET @searchQuery = "";
		SET @idx = 1;
		filterParams:LOOP
			IF @idx > @numOfParams THEN 
				LEAVE filterParams;
			END IF;
			
			SET @filterParam = SUBSTRING_INDEX(SUBSTRING_INDEX(_filterByParam, ',', @idx), ',', -1 );
			SET @filterValue = SUBSTRING_INDEX(SUBSTRING_INDEX(_filterByValue, ',', @idx), ',', -1 );
			SET @searchQuery = concat(@searchQuery, " AND (",@filterParam," LIKE '",@filterValue,"' )");
			SET @idx = @idx + 1;
			 
		END LOOP filterParams;
        
        SET @companyId = (select group_concat(concat(contractId,"_",coreCustomerId) SEPARATOR ",") from contractcustomers where customerId =_customerId);
	    IF @companyId is NULL THEN      
			SET @companyId = "";
		END IF;
        
        IF _featureactionlist is NULL THEN      
			LEAVE MAINLABEL;
		END IF;
        
        SET _sortByParam = if(_sortByParam = "" OR _sortByParam = NULL, 'createdts', _sortByParam);
		SET _sortOrder = if(_sortOrder = "" OR _sortOrder = NULL, 'DESC', _sortOrder);
        
        SET @customerMatrixIds = (SELECT group_concat(approvalMatrixId SEPARATOR ",") FROM customerapprovalmatrix where FIND_IN_SET(customerId, @combinedIds));
        IF @customerMatrixIds is NULL THEN      
			SET @customerMatrixIds = "";
		END IF;
        
        SET @alreadyApprovedIds = (select GROUP_CONCAT(DISTINCT(requestId) SEPARATOR ",") from bbactedrequest where FIND_IN_SET(createdby, @combinedIds) AND action = 'Approved');
        IF @alreadyApprovedIds is NULL THEN      
			SET @alreadyApprovedIds = "";
		END IF;
        
        SET @approvalRequestIds = (SELECT group_concat(DISTINCT(requestId) SEPARATOR ",") 
        							FROM requestapprovalmatrix 
        							INNER JOIN approvalmatrix ON requestapprovalmatrix.approvalMatrixId = approvalmatrix.id
									INNER JOIN approvalrule ON approvalmatrix.approvalruleId = approvalrule.id
	        							WHERE FIND_IN_SET(requestapprovalmatrix.approvalMatrixId,  @customerMatrixIds) 
	        							AND NOT FIND_IN_SET(requestapprovalmatrix.requestId, @alreadyApprovedIds)
        								AND ((approvalrule.numberOfApprovals = -1 AND requestapprovalmatrix.receivedApprovals < (SELECT COUNT(DISTINCT(customerId)) FROM customerapprovalmatrix WHERE customerapprovalmatrix.approvalMatrixId = requestapprovalmatrix.approvalMatrixId)) 
												OR
											(approvalrule.numberOfApprovals != -1 AND requestapprovalmatrix.receivedApprovals < approvalrule.numberOfApprovals))
        							);
        IF @approvalRequestIds is NULL THEN      
			SET @approvalRequestIds = "''";
		END IF;
        
        SET @queryTypecondition = if(_queryType = 'myRequests', concat(" AND FIND_IN_SET(`achtransaction`.`createdby`, '",@combinedIds,"') AND (`bbrequest`.`status` = 'Pending' OR `bbrequest`.`status` = 'Approved' OR `bbrequest`.`status` = 'Rejected' ) "),
										if(_queryType = 'pendingForMyApprovals', concat(" AND FIND_IN_SET(`achtransaction`.`requestId`,  \"",@approvalRequestIds,"\")  AND `achtransaction`.`status` = 'Pending' ",@notCreatedBySelf),
                                        if(_queryType = 'rejected', concat(" AND `achtransaction`.`status` = 'Rejected' "),
                                        if(_transactionId = '%' , concat(" AND NOT `achtransaction`.`status` = 'Withdrawn'"),
                                        ''))));
                                        
        SET @validAccountsJoin = if(_queryType = '', concat(" INNER JOIN (SELECT DISTINCT Account_id,
									REPLACE(Action_id, '_VIEW', '_CREATE') as Action_id
									FROM 
									customeraction 
									WHERE Customer_id = ",_customerId,"
									AND isAllowed = '1' 
									AND Account_id is NOT null
									AND Action_id like '%_VIEW') as 
								`can` ON (`achtransaction`.`featureActionId` = `can`.`Action_id` 
											AND `achtransaction`.`fromAccount` = `can`.`Account_id`) "), '');
        
        SET @searchQuery = if(_searchString = NULL OR _searchString = "", @searchQuery, 
								concat(@searchQuery, " AND (`achtransaction`.`templateName` LIKE '%",_searchString,"%' OR `customeraccounts`.`AccountName` LIKE '%",_searchString,"%' OR `bbtemplaterequesttype`.`templateRequestTypeName` LIKE '%",_searchString,"%' OR `achtransaction`.`transaction_id` LIKE '%",_searchString,"%' OR `achtransaction`.`fromAccount` LIKE '%",_searchString,"%' )"));
        
        SET @paginationQuery = if(_pageOffset = NULL OR _pageOffset = "" OR _pageSize = NULL OR _pageSize = "", '', concat(" LIMIT ",_pageOffset, ", " ,_pageSize)); 
        
        SET @features = (SELECT group_concat(Feature_id SEPARATOR ",") FROM featureaction WHERE FIND_IN_SET(id,_featureactionlist) > 0);
        IF @features is NULL THEN      
			SET @features = "";
		END IF;
        
        SET @createActions = (SELECT group_concat(id SEPARATOR ",") FROM featureaction WHERE FIND_IN_SET(Feature_id, @features) > 0 AND id LIKE "%_CREATE");
        IF @createActions is NULL THEN      
			SET @createActions = "";
		END IF;
		
		SET @companyRequestIds = (SELECT group_concat(requestId SEPARATOR ",") FROM bbrequest WHERE FIND_IN_SET(companyId, @companyId) AND FIND_IN_SET(bbrequest.featureActionId, @createActions));
        IF @companyRequestIds is NULL THEN      
            SET @companyRequestIds = "";
        END IF;
        
	    SET @customerAcounts = (SELECT group_concat(Account_id SEPARATOR ",") FROM customeraccounts where FIND_IN_SET(Customer_id, @combinedIds));
	    IF @customerAcounts is NULL THEN      
			SET @customerAcounts = "";
		END IF;
	    
		SET @select_statement = concat("SELECT * FROM
        
        (SELECT 
			`achtransaction`.`transaction_id` AS `transaction_id`,
			`achtransaction`.`fromAccount` AS `fromAccount`,
			`achtransaction`.`effectiveDate` AS `effectiveDate`,
			`achtransaction`.`requestId` AS `requestId`,
			`achtransaction`.`createdby` AS `createdby`,
			`achtransaction`.`createdts` AS `createdts`,
			`achtransaction`.`maxAmount` AS `maxAmount`,
            (CASE 
				WHEN `bbrequest`.`status` is NULL THEN `achtransaction`.`status`
                ELSE `bbrequest`.`status` 
			END) AS `status`,
			`achtransaction`.`transactionType_id` AS `transactionType_id`,
			`achtransaction`.`templateType_id` AS `templateType_id`,
			`achtransaction`.`companyId` AS `companyId`,
			`achtransaction`.`templateRequestType_id` AS `templateRequestType_id`,
			`achtransaction`.`softDelete` AS `softDelete`,
			`achtransaction`.`templateName` AS `templateName`,
			`achtransaction`.`template_id` AS `template_id`,
			`achtransaction`.`totalAmount` AS `totalAmount`,
			`achtransaction`.`featureActionId` AS `featureActionId`,
			`achtransaction`.`confirmationNumber` AS `confirmationNumber`,
			( CASE 
				WHEN `customeraccounts`.`AccountName` is NULL THEN 'AccountName' 
                ELSE `customeraccounts`.`AccountName` 
			END ) AS `accountName`,
			`customer`.`UserName` AS `userName`,
			`bbtransactiontype`.`transactionTypeName` AS `transactionTypeName`,
			`bbtemplatetype`.`templateTypeName` AS `templateTypeName`,
			`bbtemplaterequesttype`.`templateRequestTypeName` AS `templateRequestTypeName`,
			`bbrequest`.`createdby` AS `requestCreatedby`,
			(CASE
				WHEN `achtransaction`.`createdby` IN (",@combinedIds,") THEN 'true'
				ELSE 'false'
			 END) as `amICreator`,
			(CASE 
				WHEN `bbrequest`.`requestId` IN (",@approvalRequestIds,") THEN 'true'
        		ELSE 'false'
	 		END)
	 		 as `amIApprover`
		FROM
			((((((`achtransaction`
			LEFT JOIN `customer` ON (`achtransaction`.`createdby` = `customer`.`id`))
			LEFT JOIN `customeraccounts` ON ((`achtransaction`.`createdby` = `customeraccounts`.`Customer_id`)
				AND (`achtransaction`.`fromAccount` = `customeraccounts`.`Account_id`)))
			LEFT JOIN `bbtransactiontype` ON (`achtransaction`.`transactionType_id` = `bbtransactiontype`.`transactionType_id`))
			LEFT JOIN `bbtemplatetype` ON (`achtransaction`.`templateType_id` = `bbtemplatetype`.`templateType_id`))
			LEFT JOIN `bbtemplaterequesttype` ON (`achtransaction`.`templateRequestType_id` = `bbtemplaterequesttype`.`templateRequestType_id`))"
            ,@validAccountsJoin,
            "
			LEFT JOIN `bbrequest` ON (`achtransaction`.`requestId` = `bbrequest`.`requestId`))
			WHERE `achtransaction`.`softDelete` = '0'
				AND ( FIND_IN_SET(`achtransaction`.`companyId` , '", @companyId ,"') OR FIND_IN_SET(`achtransaction`.`createdby`, '",@combinedIds,"'))
				AND `achtransaction`.`transaction_id` LIKE '",_transactionId ,"'
				AND FIND_IN_SET(`achtransaction`.`fromAccount`, \"", @customerAcounts, "\")
                AND FIND_IN_SET(`achtransaction`.`featureActionId`, \"", @createActions, "\") ",
                @queryTypecondition, " ",
                @searchQuery, " ) AS t1
            LEFT JOIN
            ( 
				SELECT 
					bbrequest.requestId,
                    (select count(DISTINCT(createdby)) from bbactedrequest where bbactedrequest.action = 'Approved' AND  bbactedrequest.requestId = bbrequest.requestId) 
							as receivedApprovals,
                    LEAST(
						(SELECT COUNT(DISTINCT(customerId)) FROM customerapprovalmatrix WHERE customerapprovalmatrix.approvalMatrixId in (SELECT approvalMatrixId FROM requestapprovalmatrix where requestapprovalmatrix.requestId = bbrequest.requestId)) 
						, 
						SUM(
							CASE approvalrule.numberOfApprovals
								WHEN -1 THEN (SELECT COUNT(*) FROM customerapprovalmatrix WHERE customerapprovalmatrix.approvalMatrixId = requestapprovalmatrix.approvalMatrixId)
								WHEN NULL OR \"\" THEN 0
								ELSE approvalrule.numberOfApprovals
							END
						) 
					) as requiredApprovals
				FROM
				 bbrequest
				LEFT JOIN requestapprovalmatrix ON (bbrequest.requestId = requestapprovalmatrix.requestId)
				LEFT JOIN approvalmatrix ON (requestapprovalmatrix.approvalMatrixId = approvalmatrix.id)
				LEFT JOIN approvalrule ON (approvalmatrix.approvalruleId = approvalrule.id)
                WHERE FIND_IN_SET(bbrequest.requestId, \"",@companyRequestIds,"\")
				GROUP BY bbrequest.requestId
            ) AS t2
            ON 
            `t1`.`requestId` = `t2`.`requestId`
            ORDER BY ", _sortByParam, " ", _sortOrder , " ",
            @paginationQuery);
            
	PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_approvalgroups_for_pendingtxn_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_approvalgroups_for_pendingtxn_proc`()
BEGIN
	SELECT signatorygroup.signatoryGroupId
	FROM signatorygroup 
	WHERE FIND_IN_SET(signatorygroup.signatoryGroupId,
	(SELECT group_concat(distinct(REPLACE(REPLACE(REPLACE(pendingGroupList,']',''),'[',''),'"','')))
		FROM signatorygrouprequestmatrix WHERE 
		signatorygrouprequestmatrix.isApproved = false));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_approvalmatrixtemplate_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_approvalmatrixtemplate_proc`(
    IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _cif VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _limitTypeId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _actions TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
    )
BEGIN 
    IF _cif = "" THEN
    SET _cif = "%";
    END IF;
    
    IF _limitTypeId = "" THEN
    SET _limitTypeId = "%";
    END IF;
    
    SET @isGroupMatrix = (select isGroupLevel from approvalmode where contractId = _contractId AND coreCustomerId = _cif);
	IF @isGroupMatrix is null THEN
    SET @isGroupMatrix = 0;
	END IF;
	
    IF @isGroupMatrix = 0 THEN
		SELECT 
			`approvalmatrixtemplate`.`id`,
			`approvalmatrixtemplate`.`contractId`,
			 `approvalmatrixtemplate`.`limitTypeId`,
			 `featureAction`.`id` AS `actionId`,
			 `featureAction`.`name` AS `actionName`,
			 `featureAction`.`description` AS `actionDescription`,
			 `featureAction`.`Feature_id` AS `featureId`,
			 `featureAction`.`Type_id` AS `actionType`,
			 `feature`.`name` AS `featureName`,
			 `feature`.`Status_id` AS `fifeaturestatus`,
			 `approvalRule`.`id` AS `approvalruleId`,
			 `approvalRule`.`numberOfApprovals`,
			 `approvalRule`.`name` AS `approvalRuleName`,
			 `approvalmatrixtemplate`.`lowerlimit`,
			 `approvalmatrixtemplate`.`upperlimit`,
			 `customer`.`id` AS `customerId`,
			 `customer`.`FirstName` AS `firstName`,
			 `customer`.`LastName` AS `lastName`,
			 `contractcorecustomers`.`coreCustomerId` AS `cifId`,
			 `contractcorecustomers`.`coreCustomerName` AS `cifName`,
			 `approvalmatrixtemplate`.`invalid`,
			 `approvalmatrixtemplate`.`isGroupMatrix`
			FROM (((((((`approvalmatrixtemplate` AS `approvalmatrixtemplate`
			LEFT JOIN
			`customerapprovalmatrixtemplate` AS `customerapprovalmatrixtemplate`
			ON `approvalmatrixtemplate`.`id` = `customerapprovalmatrixtemplate`.`approvalMatrixId`)
			LEFT JOIN
			`customer` AS `customer`
			ON `customerapprovalmatrixtemplate`.`customerId` = `customer`.`id`)
			LEFT JOIN
			`featureaction` AS `featureAction`
			ON `approvalmatrixtemplate`.`actionId` = `featureAction`.`id`)
			LEFT JOIN
			`approvalrule` AS `approvalRule`
			ON `approvalmatrixtemplate`.`approvalruleId` = `approvalRule`.`id`) 
		  LEFT JOIN
			`feature` AS `feature`
			ON `featureAction`.`Feature_id` = `feature`.`id`)
		  LEFT JOIN
			`contractfeatures` AS `contractfeatures`
			ON `feature`.`id` = `contractfeatures`.`featureId`
			  and  `approvalmatrixtemplate`.`contractId` = `contractfeatures`.`contractId`
			  and `approvalmatrixtemplate`.`coreCustomerId` = `contractfeatures`.`coreCustomerId`)
		  LEFT JOIN
			`contractcorecustomers` AS `contractcorecustomers`
			ON `approvalmatrixtemplate`.`contractId`  = `contractcorecustomers`.`contractId` AND `approvalmatrixtemplate`.`coreCustomerId` = `contractcorecustomers`.`coreCustomerId`)          
		WHERE 
		`approvalmatrixtemplate`.`contractId` = `_contractId` AND
		`approvalmatrixtemplate`.`coreCustomerId` LIKE `_cif` AND
		FIND_IN_SET(`approvalmatrixtemplate`.`actionId`,`_actions`) > 0 AND 
		`approvalmatrixtemplate`.`limitTypeId` LIKE `_limitTypeId` AND 
		`approvalmatrixtemplate`.`softdeleteflag` = 0 AND
		 `featureAction`.`approveFeatureAction` is not null AND
		 `featureAction`.`approveFeatureAction` != '' AND
		`featureAction`.`status` = 'SID_ACTION_ACTIVE'
			ORDER BY `approvalmatrixtemplate`.`contractId`,`approvalmatrixtemplate`.`coreCustomerId`,`approvalmatrixtemplate`.`limitTypeId`,`approvalmatrixtemplate`.`actionId` ,`approvalmatrixtemplate`.`lowerlimit`;
	
    ELSEIF @isGroupMatrix = 1 THEN
		SELECT 
			`approvalmatrixtemplate`.`id`,
			`approvalmatrixtemplate`.`contractId`,
			 `approvalmatrixtemplate`.`limitTypeId`,
			 `featureAction`.`id` AS `actionId`,
			 `featureAction`.`name` AS `actionName`,
			 `featureAction`.`description` AS `actionDescription`,
			 `featureAction`.`Feature_id` AS `featureId`,
			 `featureAction`.`Type_id` AS `actionType`,
			 `feature`.`name` AS `featureName`,
			 `feature`.`Status_id` AS `fifeaturestatus`,
			 `approvalRule`.`id` AS `approvalruleId`,
			 `approvalRule`.`numberOfApprovals`,
			 `approvalRule`.`name` AS `approvalRuleName`,
			 `approvalmatrixtemplate`.`lowerlimit`,
			 `approvalmatrixtemplate`.`upperlimit`,
			 `signatorygroupmatrixtemplate`.`groupList` AS `groupList`,
			 `signatorygroupmatrixtemplate`.`groupRule` AS `groupRule`,
			 `contractcorecustomers`.`coreCustomerId` AS `cifId`,
			 `contractcorecustomers`.`coreCustomerName` AS `cifName`,
			 `approvalmatrixtemplate`.`invalid`,
			 `approvalmatrixtemplate`.`isGroupMatrix`
			FROM ((((((`approvalmatrixtemplate` AS `approvalmatrixtemplate`
			LEFT JOIN
			`signatorygroupmatrixtemplate` AS `signatorygroupmatrixtemplate`
			ON `approvalmatrixtemplate`.`id` = `signatorygroupmatrixtemplate`.`approvalMatrixId`)
			LEFT JOIN
			`featureaction` AS `featureAction`
			ON `approvalmatrixtemplate`.`actionId` = `featureAction`.`id`)
			LEFT JOIN
			`approvalrule` AS `approvalRule`
			ON `approvalmatrixtemplate`.`approvalruleId` = `approvalRule`.`id`) 
		  LEFT JOIN
			`feature` AS `feature`
			ON `featureAction`.`Feature_id` = `feature`.`id`)
		  LEFT JOIN
			`contractfeatures` AS `contractfeatures`
			ON `feature`.`id` = `contractfeatures`.`featureId`
			  and  `approvalmatrixtemplate`.`contractId` = `contractfeatures`.`contractId`
			  and `approvalmatrixtemplate`.`coreCustomerId` = `contractfeatures`.`coreCustomerId`)
		  LEFT JOIN
			`contractcorecustomers` AS `contractcorecustomers`
			ON `approvalmatrixtemplate`.`contractId`  = `contractcorecustomers`.`contractId` AND `approvalmatrixtemplate`.`coreCustomerId` = `contractcorecustomers`.`coreCustomerId`)          
		WHERE 
		`approvalmatrixtemplate`.`contractId` = `_contractId` AND
		`approvalmatrixtemplate`.`coreCustomerId` LIKE `_cif` AND 
		`approvalmatrixtemplate`.`isGroupMatrix` = 1 AND
		FIND_IN_SET(`approvalmatrixtemplate`.`actionId`,`_actions`) > 0 AND 
		`approvalmatrixtemplate`.`limitTypeId` LIKE `_limitTypeId` AND 
		`approvalmatrixtemplate`.`softdeleteflag` = 0 AND
		 `featureAction`.`approveFeatureAction` is not null AND
		 `featureAction`.`approveFeatureAction` != '' AND
		`featureAction`.`status` = 'SID_ACTION_ACTIVE'
			ORDER BY `approvalmatrixtemplate`.`contractId`,`approvalmatrixtemplate`.`coreCustomerId`,`approvalmatrixtemplate`.`limitTypeId`,`approvalmatrixtemplate`.`actionId` ,`approvalmatrixtemplate`.`lowerlimit`;

    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_approvalqueue_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_approvalqueue_proc`(
	IN _customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _transactionIds VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _requestIds VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _featureactionlist TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
MAINLABEL:BEGIN
        SET SESSION group_concat_max_len = 100000000;
        
        SET @combinedIds = (select group_concat(id SEPARATOR ",") from customer where combinedUserId = _customerId);
		
		IF @combinedIds is NULL THEN      
			SET @combinedIds = _customerId;
		ELSE 
			SET @combinedIds = concat(_customerId , "," ,@combinedIds);
        END IF;

        SET @combinedIds = if(@combinedIds = "" OR @combinedIds = NULL, "''", @combinedIds);
        
        SET _transactionIds = if(_transactionIds = "" OR _transactionIds = NULL, "''", _transactionIds);
        SET _requestIds = if(_requestIds = "" OR _requestIds = NULL, "''", _requestIds);
       
        SET @companyId = (select group_concat(concat(contractId,"_",coreCustomerId) SEPARATOR ",") from contractcustomers where customerId =_customerId);
        IF @companyId is NULL THEN      
			SET @companyId = "";
		END IF;
        
        IF _featureactionlist is NULL THEN      
			LEAVE MAINLABEL;
		END IF;
        
        SET @customerMatrixIds = (SELECT group_concat(approvalMatrixId SEPARATOR ",") FROM customerapprovalmatrix WHERE FIND_IN_SET(customerId, @combinedIds));
        IF @customerMatrixIds is NULL THEN      
			SET @customerMatrixIds = "";
		END IF;
        
        SET @customerGroupIds = (SELECT group_concat(signatoryGroupId SEPARATOR ",") FROM customersignatorygroup WHERE FIND_IN_SET(customerId, @combinedIds));
        IF @customerGroupIds is NULL THEN      
			SET @customerGroupIds = "";
		END IF;
        
        SET @alreadyApprovedIds = (select GROUP_CONCAT(DISTINCT(requestId) SEPARATOR ",") from bbactedrequest WHERE FIND_IN_SET(createdby, @combinedIds) AND NOT action = 'Pending' AND softdeleteflag = 0);
        IF @alreadyApprovedIds is NULL THEN      
			SET @alreadyApprovedIds = "''";
		END IF;
        
        SET @approvalRequestIds = (SELECT group_concat(DISTINCT(requestId) SEPARATOR ",") 
        							FROM requestapprovalmatrix
        							INNER JOIN approvalmatrix ON requestapprovalmatrix.approvalMatrixId = approvalmatrix.id
									INNER JOIN approvalrule ON approvalmatrix.approvalruleId = approvalrule.id
	        							WHERE requestapprovalmatrix.isGroupRule = 0 
                                        AND FIND_IN_SET(requestapprovalmatrix.approvalMatrixId,  @customerMatrixIds) 
	        							AND NOT FIND_IN_SET(requestapprovalmatrix.requestId, @alreadyApprovedIds)
	        							AND ((approvalrule.numberOfApprovals = -1 AND requestapprovalmatrix.receivedApprovals < (SELECT COUNT(DISTINCT(customerId)) FROM customerapprovalmatrix WHERE customerapprovalmatrix.approvalMatrixId = requestapprovalmatrix.approvalMatrixId)) 
													OR
												(approvalrule.numberOfApprovals != -1 AND requestapprovalmatrix.receivedApprovals < approvalrule.numberOfApprovals))
									);
		
		IF @approvalRequestIds is NULL THEN      
			SET @approvalRequestIds = "''";
		END IF;
		
        SET @groupIds = @customerGroupIds;
        do_this: LOOP
			SET @strLen = LENGTH(@groupIds);
				SET @requestIds = (SELECT group_concat(DISTINCT(requestId) SEPARATOR ",") FROM signatorygrouprequestmatrix 
					WHERE NOT FIND_IN_SET(requestId, @approvalRequestIds) AND isApproved = '0' AND FIND_IN_SET( SUBSTRING_INDEX(@groupIds, ',', 1) ,REPLACE(REPLACE(REPLACE(pendingGroupList,'[',''),']',''),' ','')) > 0  
                    AND requestId NOT in (@alreadyApprovedIds) );
				IF @requestIds is NULL THEN      
					SET @requestIds = "''";
				END IF;
				SET @approvalRequestIds = if(@approvalRequestIds = "" OR @approvalRequestIds IS NULL, @requestIds, CONCAT(@approvalRequestIds, CONCAT(',',@requestIds) ));
			SET @SubStrLen = LENGTH(SUBSTRING_INDEX(@groupIds, ',', 1));
			SET @groupIds = MID(@groupIds, @SubStrLen + 2, @strLen);
			IF LENGTH(@groupIds) <= 0 THEN
			  LEAVE do_this;
			END IF;
		END LOOP do_this;
        
        IF @approvalRequestIds is NULL THEN      
			SET @approvalRequestIds = "''";
		END IF;
        
        SET @features = (SELECT group_concat(Feature_id SEPARATOR ",") FROM featureaction WHERE FIND_IN_SET(id,_featureactionlist) > 0);
        IF @features is NULL THEN      
			SET @features = "";
		END IF;
        
        SET @monetaryActions = (SELECT group_concat(id SEPARATOR ",") FROM featureaction WHERE FIND_IN_SET(Feature_id, @features) > 0);
        IF @monetaryActions is NULL THEN      
			SET @monetaryActions = "";
		END IF;
		
		SET @companyRequestIds = (SELECT group_concat(requestId SEPARATOR ",") FROM bbrequest WHERE FIND_IN_SET(companyId, @companyId) AND FIND_IN_SET(bbrequest.featureActionId, @monetaryActions));
        
        IF @companyRequestIds is NULL THEN      
            SET @companyRequestIds = "";
        END IF;
        
        SET @requestIds = if(_requestIds = "''",@companyRequestIds,_requestIds);
        SET @query = if(_transactionIds = "''"
	        ,concat("FIND_IN_SET(bbrequest.requestId, \"",@requestIds,"\") ")
	        ,concat("FIND_IN_SET(bbrequest.transactionId, \"",_transactionIds, "\") AND  FIND_IN_SET(bbrequest.featureActionId, \"",@monetaryActions,"\")") );
        
		SET @select_statement = concat("SELECT 
					bbrequest.requestId,
					bbrequest.transactionId,
					bbrequest.status,
					bbrequest.featureActionId,
                    bbrequest.isGroupMatrix,
                    (CASE
						WHEN `bbrequest`.`createdby` IN (",@combinedIds,") THEN 'true'
						ELSE 'false'
					 END) as `amICreator`,
					(CASE 
						WHEN `bbrequest`.`requestId` IN (",@approvalRequestIds,") THEN 'true'
						ELSE 'false'
					END)
					 as `amIApprover`,
					(CASE
						WHEN `bbrequest`.`requestId` IN (",@alreadyApprovedIds,") THEN 'true'
						ELSE 'false'
					 END) as `actedByMeAlready`,
					(select count(DISTINCT(createdby)) from bbactedrequest where bbactedrequest.action = 'Approved' AND  bbactedrequest.requestId = bbrequest.requestId AND softdeleteflag = '0') 
							as receivedApprovals,
					CASE bbrequest.isGroupMatrix
						WHEN 0 THEN LEAST(
									(SELECT COUNT(DISTINCT(customerId)) FROM customerapprovalmatrix WHERE customerapprovalmatrix.approvalMatrixId in (SELECT approvalMatrixId FROM requestapprovalmatrix where requestapprovalmatrix.requestId = bbrequest.requestId)) 
									, 
									SUM(
										CASE approvalrule.numberOfApprovals
											WHEN -1 THEN (SELECT COUNT(*) FROM customerapprovalmatrix WHERE customerapprovalmatrix.approvalMatrixId = requestapprovalmatrix.approvalMatrixId)
											WHEN NULL OR \"\" THEN 0
											ELSE approvalrule.numberOfApprovals
										END
									) 
								)
                        ELSE NULL
					END as requiredApprovals
				FROM
				 bbrequest
				LEFT JOIN requestapprovalmatrix ON (bbrequest.requestId = requestapprovalmatrix.requestId)
				LEFT JOIN approvalmatrix ON (requestapprovalmatrix.approvalMatrixId = approvalmatrix.id)
				LEFT JOIN approvalrule ON (approvalmatrix.approvalruleId = approvalrule.id)
                WHERE ",@query,"
				GROUP BY bbrequest.requestId"
				);
            
	-- select @select_statement;
	PREPARE stmt FROM @select_statement;
    EXECUTE stmt; 
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_approvers_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_approvers_proc`(
in _requestId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

	SELECT customerapprovalmatrix.customerId as customerId 
    FROM customerapprovalmatrix  WHERE 
    customerapprovalmatrix.approvalMatrixId 
    IN (
		SELECT requestapprovalmatrix.approvalMatrixId 
        FROM requestapprovalmatrix WHERE 
        requestapprovalmatrix.requestId = _requestId
       );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_bbtemplate_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_bbtemplate_proc`(
	IN _customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _templateId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _featureactionlist TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _queryType VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _filterByParam TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _filterByValue TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _searchString VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _sortByParam VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _sortOrder VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _pageSize VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _pageOffset VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
MAINLABEL:BEGIN
		SET SESSION group_concat_max_len = 100000000;
		
		SET @combinedIds = (select group_concat(id SEPARATOR ",") from customer where combinedUserId = _customerId);

		IF @combinedIds is NULL THEN      
			SET @combinedIds = _customerId;
		ELSE 
			SET @combinedIds = concat(_customerId , "," ,@combinedIds);
        END IF;
		
		SET _filterByParam = IF(_filterByParam = NULL , '', _filterByParam);
        SET _filterByValue = IF(_filterByValue = NULL , '', _filterByValue);
		
        SET _templateId = if(_templateId = "" OR _templateId = NULL, '%', _templateId);
        
        SET @numOfParams = 0;
        IF LENGTH(_filterByParam) > 0 THEN
			SET @numOfParams = LENGTH(_filterByParam) - LENGTH(REPLACE(_filterByParam, ',', '')) + 1;
		END IF;
        
        SET @searchQuery = "";
		SET @idx = 1;
		filterParams:LOOP
			IF @idx > @numOfParams THEN 
				LEAVE filterParams;
			END IF;
			
			SET @filterParam = SUBSTRING_INDEX(SUBSTRING_INDEX(_filterByParam, ',', @idx), ',', -1 );
			SET @filterValue = SUBSTRING_INDEX(SUBSTRING_INDEX(_filterByValue, ',', @idx), ',', -1 );
			SET @searchQuery = concat(@searchQuery, " AND (",@filterParam," LIKE '",@filterValue,"' )");
			SET @idx = @idx + 1;
			 
		END LOOP filterParams;
        
        SET @companyId = (select group_concat(concat(contractId,"_",coreCustomerId) SEPARATOR ",") from contractcustomers where customerId =_customerId);
        
        IF @companyId is NULL THEN      
			SET @companyId = "";
		END IF;
        
        IF _featureactionlist is NULL THEN      
			LEAVE MAINLABEL;
		END IF;
        
        SET _sortByParam = if(_sortByParam = "" OR _sortByParam = NULL, 'createdts', _sortByParam);
		SET _sortOrder = if(_sortOrder = "" OR _sortOrder = NULL, 'DESC', _sortOrder);
        
        SET @customerMatrixIds = (SELECT group_concat(approvalMatrixId SEPARATOR ",") FROM customerapprovalmatrix WHERE FIND_IN_SET(customerId, @combinedIds));
        
        IF @customerMatrixIds is NULL THEN      
			SET @customerMatrixIds = "";
		END IF;
        
        SET @alreadyApprovedIds = (select GROUP_CONCAT(DISTINCT(requestId) SEPARATOR ",") from bbactedrequest where FIND_IN_SET(createdby, @combinedIds) AND action = 'Approved');
        
        IF @alreadyApprovedIds is NULL THEN      
			SET @alreadyApprovedIds = "";
		END IF;
        
        SET @approvalRequestIds = (SELECT group_concat(DISTINCT(requestId) SEPARATOR ",") 
        							FROM requestapprovalmatrix
        							INNER JOIN approvalmatrix ON requestapprovalmatrix.approvalMatrixId = approvalmatrix.id
									INNER JOIN approvalrule ON approvalmatrix.approvalruleId = approvalrule.id
        								WHERE FIND_IN_SET(requestapprovalmatrix.approvalMatrixId,  @customerMatrixIds) 
        								AND NOT FIND_IN_SET(requestapprovalmatrix.requestId, @alreadyApprovedIds)
        								AND ((approvalrule.numberOfApprovals = -1 AND requestapprovalmatrix.receivedApprovals < (SELECT COUNT(DISTINCT(customerId)) FROM customerapprovalmatrix WHERE customerapprovalmatrix.approvalMatrixId = requestapprovalmatrix.approvalMatrixId)) 
												OR
											(approvalrule.numberOfApprovals != -1 AND requestapprovalmatrix.receivedApprovals < approvalrule.numberOfApprovals))
        							);
        
        IF @approvalRequestIds is NULL THEN      
			SET @approvalRequestIds = "''";
		END IF;
        
        SET @queryTypecondition = if(_queryType = 'myRequests', concat(" AND FIND_IN_SET(`bbtemplate`.`createdby`, '",@combinedIds,"') AND (`bbrequest`.`status` = 'Pending' OR `bbrequest`.`status` = 'Approved' OR `bbrequest`.`status` = 'Rejected') "),
										if(_queryType = 'pendingForMyApprovals', concat(" AND FIND_IN_SET(`bbtemplate`.`requestId`,  \"",@approvalRequestIds,"\")  AND `bbtemplate`.`status` = 'Pending' "),
                                        if(_templateId = '%' ,concat(" AND NOT `bbtemplate`.`status` = 'Withdrawn'"),
                                        '')));
        
        SET @searchQuery = if(_searchString = NULL OR _searchString = "", @searchQuery, 
								concat(@searchQuery, " AND (`bbtemplate`.`templateName` LIKE '%",_searchString,"%' OR `customeraccounts`.`AccountName` LIKE '%",_searchString,"%' OR `bbtemplaterequesttype`.`templateRequestTypeName` LIKE '%",_searchString,"%' OR `bbtemplate`.`fromAccount` LIKE '%",_searchString,"%')"));
        
        SET @paginationQuery = if(_pageOffset = NULL OR _pageOffset = "" OR _pageSize = NULL OR _pageSize = "", '', concat(" LIMIT ",_pageOffset, ", " ,_pageSize)); 
        
        SET @features = (SELECT group_concat(Feature_id SEPARATOR ",") FROM featureaction WHERE FIND_IN_SET(id,_featureactionlist) > 0);
        
        IF @features is NULL THEN      
			SET @features = "";
		END IF;
        
        SET @createActions = (SELECT group_concat(id SEPARATOR ",") FROM featureaction WHERE FIND_IN_SET(Feature_id, @features) > 0 AND id LIKE "%_CREATE_TEMPLATE");
        
        IF @createActions is NULL THEN      
			SET @createActions = "";
		END IF;
		
		SET @companyRequestIds = (SELECT group_concat(requestId SEPARATOR ",") FROM bbrequest WHERE FIND_IN_SET(companyId, @companyId) AND FIND_IN_SET(bbrequest.featureActionId, @createActions));
        
        IF @companyRequestIds is NULL THEN      
            SET @companyRequestIds = "";
        END IF;
        
        SET @customerAcounts = (SELECT group_concat(Account_id SEPARATOR ",") FROM customeraccounts WHERE FIND_IN_SET(Customer_id, @combinedIds));
	    
	    IF @customerAcounts is NULL THEN      
			SET @customerAcounts = "";
		END IF;
	    
		SET @select_statement = concat("SELECT * FROM
        
        (SELECT 
			`bbtemplate`.`templateId` AS `templateId`,
            `bbtemplate`.`templateName` AS `templateName`,
            `bbtemplate`.`templateDescription` AS `templateDescription`,
			`bbtemplate`.`featureActionId` AS `featureActionId`,
			`bbtemplate`.`fromAccount` AS `fromAccount`,
			`bbtemplate`.`effectiveDate` AS `effectiveDate`,
			`bbtemplate`.`requestId` AS `requestId`,
			`bbtemplate`.`createdby` AS `createdby`,
            `bbtemplate`.`updatedBy` AS `updatedBy`,
			`bbtemplate`.`createdts` AS `createdts`,
			`bbtemplate`.`maxAmount` AS `maxAmount`,
			`bbtemplate`.`status` AS `status`,
			`bbtemplate`.`transactionType_id` AS `transactionType_id`,
			`bbtemplate`.`templateType_id` AS `templateType_id`,
			`bbtemplate`.`companyId` AS `companyId`,
			`bbtemplate`.`templateRequestType_id` AS `templateRequestType_id`,
			`bbtemplate`.`softDelete` AS `softDelete`,
			`bbtemplate`.`totalAmount` AS `totalAmount`,
			( CASE 
				WHEN `customeraccounts`.`AccountName` is NULL THEN 'AccountName' 
                ELSE `customeraccounts`.`AccountName` 
			END ) AS `accountName`,
			`customer`.`UserName` AS `userName`,
			`bbtransactiontype`.`transactionTypeName` AS `transactionTypeName`,
			`bbtemplatetype`.`templateTypeName` AS `templateTypeName`,
			`bbtemplaterequesttype`.`templateRequestTypeName` AS `templateRequestTypeName`,
			`bbrequest`.`createdby` AS `requestCreatedby`,
			(CASE
				WHEN `bbtemplate`.`createdby` IN (",@combinedIds,") THEN 'true'
				ELSE 'false'
			 END) as `amICreator`,
			(CASE 
				WHEN `bbrequest`.`requestId` IN (",@approvalRequestIds,") THEN 'true'
        		ELSE 'false'
	 		END)
	 		 as `amIApprover`
		FROM
			((((((`bbtemplate`
			LEFT JOIN `customer` ON (`bbtemplate`.`createdby` = `customer`.`id`))
			LEFT JOIN `customeraccounts` ON ((`bbtemplate`.`createdby` = `customeraccounts`.`Customer_id`)
				AND (`bbtemplate`.`fromAccount` = `customeraccounts`.`Account_id`)))
			LEFT JOIN `bbtransactiontype` ON (`bbtemplate`.`transactionType_id` = `bbtransactiontype`.`transactionType_id`))
			LEFT JOIN `bbtemplatetype` ON (`bbtemplate`.`templateType_id` = `bbtemplatetype`.`templateType_id`))
			LEFT JOIN `bbtemplaterequesttype` ON (`bbtemplate`.`templateRequestType_id` = `bbtemplaterequesttype`.`templateRequestType_id`))
			LEFT JOIN `bbrequest` ON (`bbtemplate`.`requestId` = `bbrequest`.`requestId`))
			WHERE `bbtemplate`.`softDelete` = '0'
				AND (FIND_IN_SET(`bbtemplate`.`companyId` , '", @companyId ,"') OR FIND_IN_SET(`bbtemplate`.`createdby`, '",@combinedIds,"'))
				AND `bbtemplate`.`templateId` LIKE '",_templateId ,"'
				AND FIND_IN_SET(`bbtemplate`.`fromAccount`, \"", @customerAcounts, "\")
                AND FIND_IN_SET(`bbtemplate`.`featureActionId`, \"", @createActions, "\") ",
                @queryTypecondition, " ",
                @searchQuery, " ) AS t1
            LEFT JOIN
            ( 
				SELECT 
					bbrequest.requestId,
					(select count(DISTINCT(createdby)) from bbactedrequest where bbactedrequest.action = 'Approved' AND  bbactedrequest.requestId = bbrequest.requestId) 
							as receivedApprovals,
                    LEAST(
						(SELECT COUNT(DISTINCT(customerId)) FROM customerapprovalmatrix WHERE customerapprovalmatrix.approvalMatrixId in (SELECT approvalMatrixId FROM requestapprovalmatrix where requestapprovalmatrix.requestId = bbrequest.requestId)) 
						, 
						SUM(
							CASE approvalrule.numberOfApprovals
								WHEN -1 THEN (SELECT COUNT(*) FROM customerapprovalmatrix WHERE customerapprovalmatrix.approvalMatrixId = requestapprovalmatrix.approvalMatrixId)
								WHEN NULL OR \"\" THEN 0
								ELSE approvalrule.numberOfApprovals
							END
						) 
					) as requiredApprovals
				FROM
				 bbrequest
				LEFT JOIN requestapprovalmatrix ON (bbrequest.requestId = requestapprovalmatrix.requestId)
				LEFT JOIN approvalmatrix ON (requestapprovalmatrix.approvalMatrixId = approvalmatrix.id)
				LEFT JOIN approvalrule ON (approvalmatrix.approvalruleId = approvalrule.id)
                WHERE FIND_IN_SET(bbrequest.requestId, \"",@companyRequestIds,"\")
				GROUP BY bbrequest.requestId
            ) AS t2
            ON 
            `t1`.`requestId` = `t2`.`requestId`
            ORDER BY ", _sortByParam, " ", _sortOrder , " ",
            @paginationQuery);
            
	-- select @select_statement;
	PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_bulkwires_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_bulkwires_proc`(
	IN `createdBy` varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `searchString` varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `sortByParam` varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `sortOrder` varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `pageOffset` int,
	IN `pageSize` int,
	IN `bulkWireCategoryFilter` VARCHAR(50),
    IN `fileDomesticView` TINYINT(1),
    IN `fileInternationalView` TINYINT(1),
    IN `templateDomesticView` TINYINT(1),
    IN `templateInternationalView` TINYINT(1)
)
BEGIN
    SET @companyId = (SELECT Organization_Id FROM customer WHERE id = createdby);
    
	SET @isSMEUser = if(@companyId != "" OR @companyId != NULL , true, false);
    
    SET @remove0DomesticFile = concat(" AND `bulkwirefiles`.`noOfDomesticTransactions` <> 0");
    SET @remove0InternationalFile = concat(" AND `bulkwirefiles`.`noOfInternationalTransactions` <> 0");
    SET @remove0DomesticTemplate = concat(" AND `bulkwiretemplate`.`noOfDomesticTransactions` <> 0");
    SET @remove0InternationalTemplate = concat(" AND `bulkwiretemplate`.`noOfInternationalTransactions` <> 0");
    
    SET @fileAddQuery = if(fileDomesticView,@remove0DomesticFile,@remove0InternationalFile);
    SET @fileAdd = if(fileDomesticView && fileInternationalView,"",@fileAddQuery);
    
    SET @templateAddQuery = if(templateDomesticView,@remove0DomesticTemplate,@remove0InternationalTemplate);
    SET @templateAdd = if(templateDomesticView && templateInternationalView,"",@templateAddQuery);
    
    SET @filterRetailFile = concat("`bulkwirefiles`.`createdBy` = ", createdby , " AND `bulkwirefiles`.`softdeleteflag` = 0",@fileAdd);
				
	SET @filterSMEFile = concat("`bulkwirefiles`.`company_id` = ", @companyId  , " AND `bulkwirefiles`.`softdeleteflag` = 0",@fileAdd);

	SET @filterRetailTemplate = concat("`bulkwiretemplate`.`createdBy` = ", createdby , " AND `bulkwiretemplate`.`softdeleteflag` = 0",@templateAdd);
				
	SET @filterSMETemplate = concat("`bulkwiretemplate`.`company_id` = ", @companyId  , " AND `bulkwiretemplate`.`softdeleteflag` = 0",@templateAdd);
	
    SET @getByIdFilterFile = if(@isSMEUser, @filterSMEFile, @filterRetailFile);
    SET @getByIdFilterTemplate = if(@isSMEUser, @filterSMETemplate, @filterRetailTemplate);
    
    SET sortByParam = if(sortByParam = "" OR sortByParam = NULL, 'createdts', sortByParam);
    SET sortOrder = if(sortOrder = "" OR sortOrder = NULL, 'DESC', sortOrder);
    SET sortByParam = if(sortByParam = 'username',concat('firstName ',sortOrder,',lastname'), sortByParam);
    
     SET searchString = if(searchString = "" OR searchString = NULL,"",concat("'%",searchString,"%'"));               
    
    SET @orderBy = concat(" ORDER BY ", sortByParam , " ", sortOrder);
    
    SET @paginationQuery = if((pageOffset != NULL OR pageOffset != "" AND pageSize != NULL OR pageSize != ""),
							concat(" LIMIT ",pageSize, " OFFSET " ,pageOffset),
                            ''); 
    
    
	SET @searchQueryFile = concat("(`bulkwirefiles`.`bulkWireFileName` LIKE ",searchString," OR `bulkwirefiles`.`noOfTransactions` LIKE ",
    searchString," OR `bulkwirefiles`.`noOfDomesticTransactions` LIKE ",searchString," OR `bulkwirefiles`.`noOfInternationalTransactions` LIKE ",searchString," OR  `customer`.`firstname` LIKE ",searchString," OR `customer`.`lastname` LIKE ",searchString,")");
	
    SET @searchQueryTemplate = concat("(`bulkwiretemplate`.`bulkWireTemplateName` LIKE ",searchString," OR `bulkwiretemplate`.`noOfTransactions` LIKE ",
    searchString," OR `bulkwiretemplate`.`noOfDomesticTransactions` LIKE ",searchString," OR `bulkwiretemplate`.`noOfInternationalTransactions` LIKE ",searchString," OR  `customer`.`firstname` LIKE ",searchString," OR `customer`.`lastname` LIKE ",searchString,")");

	SET @onlyFileWithoutSearch = concat(@getByIdFilterFile, @orderBy , @paginationQuery);
	SET @searchFilterFile = concat(@getByIdFilterFile," AND ",@searchQueryFile);
    SET @filterFile = if(searchString = "",@getByIdFilterFile,@searchFilterFile);
    SET @OnlyFile = if(searchString = "",@onlyFileWithoutSearch,@searchFilterFile);
	SET @finalFile = if(bulkWireCategoryFilter = "Files",@OnlyFile,@filterFile);
    
	SET @defaultFilterTemplate = concat(@getByIdFilterTemplate, @orderBy , @paginationQuery);
	SET @searchFilterTemplate = concat(@getByIdFilterTemplate," AND ",@searchQueryTemplate, @orderBy);
    SET @filterTemplate = if(searchString = "",@defaultFilterTemplate,@searchFilterTemplate);

    
    SET @select_files = concat("SELECT
       bulkwirefiles.bulkWireFileID as bulkWireID,
        bulkwirefiles.bulkWireFileName as bulkWireName,
        bulkwirefiles.noOfTransactions,
        bulkwirefiles.noOfDomesticTransactions,
        bulkwirefiles.noOfInternationalTransactions,
        bulkwirefiles.createdts,
        bulkwirefiles.lastmodifiedts,
        bulkwirefiles.lastExecutedOn,
		NULL as defaultFromAccount,
		NULL as defaultCurrency,
        'Files' as bulkWireCategory,
        customer.FirstName as firstname,
        customer.LastName as lastname
     FROM
        (`bulkwirefiles`
        LEFT JOIN `customer` ON (`bulkwirefiles`.`createdBy` = `customer`.`id`))
		WHERE ",@finalFile);
	SET @select_templates = concat("SELECT
	       bulkwiretemplate.bulkWireTemplateID as bulkWireID,
	        bulkwiretemplate.bulkWireTemplateName as bulkWireName,
	        bulkwiretemplate.noOfTransactions,
	        bulkwiretemplate.noOfDomesticTransactions,
	        bulkwiretemplate.noOfInternationalTransactions,
	        bulkwiretemplate.createdts,
	        bulkwiretemplate.lastmodifiedts,
            bulkwiretemplate.lastExecutedOn,
            bulkwiretemplate.defaultFromAccount,
            bulkwiretemplate.defaultCurrency,
	        'Templates' as bulkWireCategory,
	        customer.FirstName as firstname,
	        customer.LastName as lastname
	     FROM
	        (`bulkwiretemplate`
	        LEFT JOIN `customer` ON (`bulkwiretemplate`.`createdBy` = `customer`.`id`))
			WHERE ", @filterTemplate);
			
	IF bulkWireCategoryFilter = 'Files' THEN
		SET @select_statement = @select_files;
	ELSEIF bulkWireCategoryFilter = 'Templates' THEN
		SET @select_statement = @select_templates;
	ELSE
		SET @select_statement = concat(@select_files," UNION ALL ",@select_templates);
	END IF;
	    
	-- select @select_statement;
	PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_bulkwiretemplatelineitems_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_bulkwiretemplatelineitems_proc`(
	IN `bulkWireTemplateID` varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `searchString` VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `sortByParam` varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `sortOrder` varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `groupBy` varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
DECLARE lineItemId,payeeId,swiftCodeVal,intRoutingNumVal,recAccntnumVal,routingNumVal,recNicknameVal VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci;
DECLARE recNameVal,recAddLine1Val,recAddLine2Val,recCityVal,recStateVal,recCountryVal,recBankNameVal,recBankAdd1Val,recBankAdd2Val,recBankCityVal,recBankStateVal  VARCHAR(100) CHARACTER SET UTF8 COLLATE utf8_general_ci;
DECLARE recZipVal,recBankZipVal VARCHAR(20) CHARACTER SET UTF8 COLLATE utf8_general_ci;
DECLARE isPayeeDeleted INTEGER DEFAULT 1;
DECLARE finished INTEGER DEFAULT 0;

DECLARE curTemplateLineItem 
		CURSOR FOR 
			SELECT bulkWireTemplateLineItemID FROM bulkwiretemplatelineitems
            WHERE bulkWireTemplateID = bulkWireTemplateID AND softdeleteflag = 0 AND templateRecipientCategory = "EXISTINGRECIPIENT";

	
	DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;

	OPEN curTemplateLineItem;

	getLineItem: LOOP
		FETCH curTemplateLineItem INTO lineItemId;
		IF finished = 1 THEN 
			LEAVE getLineItem;
		END IF;
	    SELECT bulkwiretemplatelineitems.payeeId INTO payeeId FROM bulkwiretemplatelineitems WHERE bulkwiretemplatelineitems.bulkWireTemplateLineItemID = lineItemId;
		SELECT payee.softDelete INTO isPayeeDeleted FROM payee WHERE payee.Id = payeeId;
		IF isPayeeDeleted = 0 THEN
        SELECT payee.swiftCode,payee.internationalRoutingCode,payee.name,payee.addressLine1,payee.addressLine2,payee.cityName,payee.state,payee.country,payee.zipCode,payee.bankName,payee.bankAddressLine1,payee.bankAddressLine2,payee.bankZip,payee.bankCity,payee.bankState,payee.nickName,payee.accountNumber,payee.routingCode INTO swiftCodeVal,intRoutingNumVal,recNameVal,recAddLine1Val,recAddLine2Val,recCityVal,recStateVal,recCountryVal,recZipVal,recBankNameVal,recBankAdd1Val,recBankAdd2Val,recBankZipVal,recBankCityVal,recBankStateVal,recNicknameVal,recAccntnumVal,routingNumVal FROM payee where payee.Id = payeeId;
		UPDATE bulkwiretemplatelineitems
        SET swiftCode = swiftCodeVal,internationalRoutingNumber = intRoutingNumVal,recipientName = recNameVal,recipientAddressLine1 = recAddLine1Val,recipientAddressLine2  = recAddLine2Val,
            recipientCity =recCityVal,recipientState = recStateVal,recipientCountryName = recCountryVal,recipientZipCode = recZipVal,recipientBankName = recBankNameVal,recipientBankAddress1 = recBankAdd1Val,
            recipientBankAddress2 = recBankAdd2Val,recipientBankZipCode = recBankZipVal,recipientBankcity = recBankCityVal,recipientBankstate = recBankStateVal,accountNickname = recNicknameVal,
			recipientAccountNumber= recAccntnumVal,routingNumber = routingNumVal
        WHERE bulkWireTemplateLineItemID = lineItemId;
        ELSE
        UPDATE bulkwiretemplatelineitems
        SET softdeleteflag = 1 WHERE bulkWireTemplateLineItemID = lineItemId;
        END IF;
	END LOOP getLineItem;
	CLOSE curTemplateLineItem;
   
SET @queryFilter = concat("`bulkwiretemplatelineitems`.`bulkWireTemplateID` = \"", bulkWireTemplateID, "\" AND `bulkwiretemplatelineitems`.`softdeleteflag` = 0 ");
SET sortByParam = if (sortByParam = "" OR sortByParam = NULL, 'recipientName', sortByParam);
SET sortOrder = if (sortOrder = "" OR sortOrder = NULL, 'ASC', sortOrder);
SET searchString = if(searchString = "" OR searchString = NULL,"",concat("'%",searchString,"%'"));   

SET @orderByWithGrouping = concat(" ORDER BY ",groupBy,",",sortByParam," ",sortOrder);
SET @orderByWithoutGrouping = concat(" ORDER BY ",sortByParam," ",sortOrder);
SET @orderBy = if(groupBy = "" OR groupBy = NULL, @orderByWithoutGrouping, @orderByWithGrouping);
SET @searchQuery = concat("(`bulkwiretemplatelineitems`.`recipientName` LIKE ",searchString," OR `bulkwiretemplatelineitems`.`swiftCode` LIKE ",searchString," OR `bulkwiretemplatelineitems`.`recipientAccountNumber` LIKE ",searchString," OR `bulkwiretemplatelineitems`.`routingNumber` LIKE ",searchString," OR `bulkwiretemplatelineitems`.`internationalRoutingNumber` LIKE ",searchString," OR
`bulkwiretemplatelineitems`.`bulkWireTransferType` LIKE ",searchString,"  OR 
`bulkwiretemplatelineitems`.`transactionType` LIKE ",searchString,"  OR `bulkwiretemplatelineitems`.`recipientAddressLine1` LIKE ",searchString,"  OR `bulkwiretemplatelineitems`.`recipientAddressLine2` LIKE ",searchString,"  OR `bulkwiretemplatelineitems`.`recipientCity` LIKE ",searchString,"  OR `bulkwiretemplatelineitems`.`recipientState` LIKE ",searchString,"  OR `bulkwiretemplatelineitems`.`recipientZipCode` LIKE ",searchString,"  OR `bulkwiretemplatelineitems`.`recipientBankName` LIKE ",searchString,"  OR `bulkwiretemplatelineitems`.`recipientBankAddress1` LIKE ",searchString,"  OR
`bulkwiretemplatelineitems`.`recipientBankAddress2` LIKE ",searchString,"  OR `bulkwiretemplatelineitems`.`recipientBankcity` LIKE ",searchString,"  OR
`bulkwiretemplatelineitems`.`recipientBankZipCode` LIKE ",searchString,"  OR `bulkwiretemplatelineitems`.`recipientBankstate` LIKE ",searchString,")");
	
SET @defaultFilter = concat(@queryFilter, @orderBy);

SET @searchFilter = concat(@queryFilter," AND ",@searchQuery,@orderBy);

SET @filter = if(searchString = "",@defaultFilter,@searchFilter);

SET @select_statement = concat("SELECT 
bulkwiretemplatelineitems.bulkWireTemplateLineItemID,
bulkwiretemplatelineitems.swiftCode,
bulkwiretemplatelineitems.recipientCountryName,
bulkwiretemplatelineitems.recipientName,
bulkwiretemplatelineitems.bulkWireTransferType,
bulkwiretemplatelineitems.transactionType,
bulkwiretemplatelineitems.internationalRoutingNumber,
bulkwiretemplatelineitems.recipientAddressLine1,
bulkwiretemplatelineitems.recipientAddressLine2,
bulkwiretemplatelineitems.recipientCity,
bulkwiretemplatelineitems.recipientState,
bulkwiretemplatelineitems.recipientZipCode,
bulkwiretemplatelineitems.recipientBankName,
bulkwiretemplatelineitems.recipientBankAddress1,
bulkwiretemplatelineitems.recipientBankAddress2,
bulkwiretemplatelineitems.recipientBankZipCode,
bulkwiretemplatelineitems.recipientBankcity,
bulkwiretemplatelineitems.recipientBankstate,
bulkwiretemplatelineitems.recipientAccountNumber,
bulkwiretemplatelineitems.routingNumber,
bulkwiretemplatelineitems.templateRecipientCategory,
bulkwiretemplatelineitems.accountNickname,
bulkwiretemplatelineitems.payeeId
FROM `bulkwiretemplatelineitems` WHERE ", @filter);

PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_bulkWireTemplateTransactionsExecution_details_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_bulkWireTemplateTransactionsExecution_details_proc`( 
	in BulkWireTemplateExecution_id varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	in searchString varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	in statusFilter varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	in sortByParam varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	in sortOrder varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `isDomesticPermitted` INT,
	IN `isInternationalPermitted` INT
)
BEGIN

SET @query1= concat("`wiretransfers`.`wireTemplateExecution_id` = ", BulkWireTemplateExecution_id," AND `wiretransfers`.`softdeleteflag` = 0 ");
SET @query2= concat("`wiretransfers`.`wireTemplateExecution_id` = ", BulkWireTemplateExecution_id," AND `wiretransfers`.`status` ='",statusFilter,"' AND `wiretransfers`.`softdeleteflag` = 0 ");
SET @query3= concat("`wiretransfers`.`wireTemplateExecution_id` = ", BulkWireTemplateExecution_id," AND `wiretransfers`.`status` in ('Failed','Denied')"," AND `wiretransfers`.`softdeleteflag` = 0 ");

SET sortByParam =if (sortByParam = "" OR sortByParam = NULL, 'transactionId', sortByParam);
SET sortByParam = if(sortByParam = 'payeeName', 'wiretransfers.payeeName', sortByParam);
SET sortOrder = if (sortOrder = "" OR sortOrder = NULL, 'ASC', sortOrder);
SET searchString = if (searchString = "" OR searchString = NULL, "", concat("'%",searchString,"%'"));

SET @orderBy = concat(" ORDER BY ", sortByParam, " ", sortOrder);
SET @searchQuery = concat("(`wiretransfers`.`amount` LIKE ",searchString," OR `wiretransfers`.`notes` LIKE ",searchString," OR `wiretransfers`.`fromAccountNumber` LIKE ",searchString," OR `wiretransfers`.`payeeAccountNumber` LIKE ",searchString," OR `wiretransfers`.`transactionType` LIKE ",searchString," OR `onetimepayee`.`payeeName` LIKE ",searchString," OR `onetimepayee`.`payeeType` LIKE ",searchString," OR `onetimepayee`.`payeeAddressLine1` LIKE ",searchString," OR `onetimepayee`.`payeeAddressLine2` LIKE ",searchString," OR `onetimepayee`.`cityName` LIKE ",searchString," OR `onetimepayee`.`state` LIKE ",searchString," OR `onetimepayee`.`zipCode` LIKE ",searchString," OR `onetimepayee`.`bankName` LIKE ",searchString," OR `onetimepayee`.`bankAddressLine1` LIKE ",searchString," OR `onetimepayee`.`bankAddressLine2` LIKE ",searchString," OR `onetimepayee`.`bankZip` LIKE ",searchString," OR `onetimepayee`.`bankState` LIKE ",searchString," OR `onetimepayee`.`bankCity` LIKE ",searchString," OR  `onetimepayee`.`routingNumber` LIKE ",searchString," OR `onetimepayee`.`internationalRoutingCode` LIKE ",searchString," OR `onetimepayee`.`swiftCode` LIKE ",searchString,")");
SET @statusQuery = if(statusFilter="Failed", @query3, @query2);
SET @finalQueryFilter = if(statusFilter="", @query1, @statusQuery);
SET @defaultFilter = concat(@finalQueryFilter, @orderBy);
SET @searchFilter = concat(@finalQueryFilter, " AND ", @searchQuery, @orderBy);
SET @filter = if (searchString = "", @defaultFilter, @searchFilter);

SET @DomQuery = CONCAT("SELECT COUNT(*) FROM (`wiretransfers` LEFT JOIN `onetimepayee` ON(`wiretransfers`.`onetime_id` = `onetimepayee`.`onetime_id`)) WHERE onetimepayee.wireAccountType = 'Domestic' AND ", @filter, " INTO @DomCount;");
PREPARE stmt1 FROM @DomQuery; EXECUTE stmt1; DEALLOCATE PREPARE stmt1;

SET @InternationalQuery = CONCAT("SELECT COUNT(*) FROM (`wiretransfers` LEFT JOIN `onetimepayee` ON(`wiretransfers`.`onetime_id` = `onetimepayee`.`onetime_id`)) WHERE onetimepayee.wireAccountType = 'International' AND ", @filter, " INTO @InternationalCount;");
PREPARE stmt2 FROM @InternationalQuery; EXECUTE stmt2; DEALLOCATE PREPARE stmt2;

IF (isDomesticPermitted = 0 AND isInternationalPermitted = 0) THEN
	SELECT "UNAUTHORIZED" AS ErrorMessage;
ELSEIF (isDomesticPermitted = 0 AND @InternationalCount = 0) THEN
	SELECT "UNAUTHORIZED" AS ErrorMessage;
ELSEIF (isInternationalPermitted = 0 AND @DomCount = 0) THEN
	SELECT "UNAUTHORIZED" AS ErrorMessage;
ELSE
	set @select_statement = concat("SELECT 
			wiretransfers.transactionId,
			wiretransfers.confirmationNumber,
			wiretransfers.requestId,
			wiretransfers.status,
			wiretransfers.onetime_id,
			wiretransfers.wireTemplateExecution_id,
			wiretransfers.notes,
			wiretransfers.amount,
			wiretransfers.fromAccountNumber,
			wiretransfers.payeeAccountNumber,
			wiretransfers.transactionType,
			wiretransfers.payeeId,
			wiretransfers.payeeCurrency,
			onetimepayee.payeeName,
			onetimepayee.payeeNickName,
			onetimepayee.payeeType,
			onetimepayee.wireAccountType,
			onetimepayee.swiftCode,
			onetimepayee.routingNumber,
			onetimepayee.zipCode,
			onetimepayee.cityName,
			onetimepayee.state,
			onetimepayee.country,
			onetimepayee.payeeAddressLine1,
			onetimepayee.payeeAddressLine2,
			onetimepayee.bankName,
			onetimepayee.internationalRoutingCode,
			onetimepayee.bankAddressLine1,
			onetimepayee.bankAddressLine2,
			onetimepayee.bankCity,
			onetimepayee.bankState,
			onetimepayee.bankZip
			FROM (`wiretransfers` 
			LEFT JOIN `onetimepayee` ON(`wiretransfers`.`onetime_id` = `onetimepayee`.`onetime_id`)) 
			WHERE ", @filter);
			
	 -- select @select_statement;
		PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
END IF; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_bulkWireTransactionsExecution_details_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_bulkWireTransactionsExecution_details_proc`( 
	in BulkWireFileExecution_id varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	in searchString varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	in statusFilter varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	in sortByParam varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	in sortOrder varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `isDomesticPermitted` INT,
	IN `isInternationalPermitted` INT
)
BEGIN

SET @query1= concat("`wiretransfers`.`wireFileExecution_id` = ", BulkWireFileExecution_id," AND `wiretransfers`.`softdeleteflag` = 0 ");
SET @query2= concat("`wiretransfers`.`wireFileExecution_id` = ", BulkWireFileExecution_id," AND `wiretransfers`.`status` ='",statusFilter,"' AND `wiretransfers`.`softdeleteflag` = 0 ");
SET @query3= concat("`wiretransfers`.`wireFileExecution_id` = ", BulkWireFileExecution_id," AND `wiretransfers`.`status` in ('Failed','Denied')"," AND `wiretransfers`.`softdeleteflag` = 0 ");

SET sortByParam =if (sortByParam = "" OR sortByParam = NULL, 'transactionId', sortByParam);
SET sortOrder = if (sortOrder = "" OR sortOrder = NULL, 'ASC', sortOrder);
SET searchString = if (searchString = "" OR searchString = NULL, "", concat("'%",searchString,"%'"));

SET @orderBy = concat(" ORDER BY ", sortByParam, " ", sortOrder);
SET @searchQuery = concat("(`wiretransfers`.`amount` LIKE ",searchString," OR `wiretransfers`.`notes` LIKE ",searchString," OR `wiretransfers`.`fromAccountNumber` LIKE ",searchString," OR `wiretransfers`.`payeeAccountNumber` LIKE ",searchString," OR `wiretransfers`.`transactionType` LIKE ",searchString," OR `onetimepayee`.`payeeName` LIKE ",searchString," OR `onetimepayee`.`payeeType` LIKE ",searchString," OR `onetimepayee`.`payeeAddressLine1` LIKE ",searchString," OR `onetimepayee`.`payeeAddressLine2` LIKE ",searchString," OR `onetimepayee`.`cityName` LIKE ",searchString," OR `onetimepayee`.`state` LIKE ",searchString," OR `onetimepayee`.`zipCode` LIKE ",searchString," OR `onetimepayee`.`bankName` LIKE ",searchString," OR `onetimepayee`.`bankAddressLine1` LIKE ",searchString," OR `onetimepayee`.`bankAddressLine2` LIKE ",searchString," OR `onetimepayee`.`bankZip` LIKE ",searchString," OR `onetimepayee`.`bankState` LIKE ",searchString," OR `onetimepayee`.`bankCity` LIKE ",searchString," OR  `onetimepayee`.`routingNumber` LIKE ",searchString," OR `onetimepayee`.`internationalRoutingCode` LIKE ",searchString," OR `onetimepayee`.`swiftCode` LIKE ",searchString,")");
SET @statusQuery = if(statusFilter="Failed", @query3, @query2);
SET @finalQueryFilter = if(statusFilter="", @query1, @statusQuery);
SET @defaultFilter = concat(@finalQueryFilter, @orderBy);
SET @searchFilter = concat(@finalQueryFilter, " AND ", @searchQuery, @orderBy);
SET @filter = if (searchString = "", @defaultFilter, @searchFilter);

SET @DomQuery = CONCAT("SELECT COUNT(*) FROM (`wiretransfers` LEFT JOIN `onetimepayee` ON(`wiretransfers`.`onetime_id` = `onetimepayee`.`onetime_id`)) WHERE onetimepayee.wireAccountType = 'Domestic' AND ", @filter, " INTO @DomCount;");
PREPARE stmt1 FROM @DomQuery; EXECUTE stmt1; DEALLOCATE PREPARE stmt1;

SET @InternationalQuery = CONCAT("SELECT COUNT(*) FROM (`wiretransfers` LEFT JOIN `onetimepayee` ON(`wiretransfers`.`onetime_id` = `onetimepayee`.`onetime_id`)) WHERE onetimepayee.wireAccountType = 'International' AND ", @filter, " INTO @InternationalCount;");
PREPARE stmt2 FROM @InternationalQuery; EXECUTE stmt2; DEALLOCATE PREPARE stmt2;

IF (isDomesticPermitted = 0 AND isInternationalPermitted = 0) THEN
	SELECT "UNAUTHORIZED" AS ErrorMessage;
ELSEIF (isDomesticPermitted = 0 AND @InternationalCount = 0) THEN
	SELECT "UNAUTHORIZED" AS ErrorMessage;
ELSEIF (isInternationalPermitted = 0 AND @DomCount = 0) THEN
	SELECT "UNAUTHORIZED" AS ErrorMessage;
ELSE
	set @select_statement = concat("SELECT 
			wiretransfers.transactionId,
			wiretransfers.confirmationNumber,
			wiretransfers.requestId,
			wiretransfers.status,
			wiretransfers.onetime_id,
			wiretransfers.wireFileExecution_id,
			wiretransfers.notes,
			wiretransfers.amount,
			wiretransfers.fromAccountNumber,
			wiretransfers.payeeAccountNumber,
			wiretransfers.transactionType,
			wiretransfers.payeeId,
			wiretransfers.payeeCurrency,
			onetimepayee.payeeName,
			onetimepayee.payeeNickName,
			onetimepayee.payeeType,
			onetimepayee.wireAccountType,
			onetimepayee.swiftCode,
			onetimepayee.routingNumber,
			onetimepayee.zipCode,
			onetimepayee.cityName,
			onetimepayee.state,
			onetimepayee.country,
			onetimepayee.payeeAddressLine1,
			onetimepayee.payeeAddressLine2,
			onetimepayee.bankName,
			onetimepayee.internationalRoutingCode,
			onetimepayee.bankAddressLine1,
			onetimepayee.bankAddressLine2,
			onetimepayee.bankCity,
			onetimepayee.bankState,
			onetimepayee.bankZip
			FROM (`wiretransfers` 
			LEFT JOIN `onetimepayee` ON(`wiretransfers`.`onetime_id` = `onetimepayee`.`onetime_id`)) 
			WHERE ", @filter);
			
	 -- select @select_statement;
		PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
END IF;
  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_bulkwire_filelineitems_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_bulkwire_filelineitems_proc`(
    IN bulkWireFileID varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN searchString VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN sortByParam varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN sortOrder varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET @queryFilter = concat("`bulkwirefilelineitems`.`bulkWireFileID` = \"", bulkWireFileID, "\" AND `bulkwirefilelineitems`.`softdeleteflag` = 0 ");
SET sortByParam = if (sortByParam = "" OR sortByParam = NULL, 'recipientName', sortByParam);
SET sortOrder = if (sortOrder = "" OR sortOrder = NULL, 'ASC', sortOrder);
SET searchString = if(searchString = "" OR searchString = NULL,"",concat("'%",searchString,"%'"));   

SET @orderBy = concat(" ORDER BY bulkWireTransferType,", sortByParam, " ", sortOrder);

SET @searchQuery = concat("(`bulkwirefilelineitems`.`recipientName` LIKE ",searchString," OR  `bulkwirefilelineitems`.`fromAccountNumber` LIKE ",searchString," OR `bulkwirefilelineitems`.`swiftCode` LIKE ",searchString," OR `bulkwirefilelineitems`.`recipientAccountNumber` LIKE ",searchString," OR `bulkwirefilelineitems`.`routingNumber` LIKE ",searchString," OR `bulkwirefilelineitems`.`internationalRoutingNumber` LIKE ",searchString," OR `bulkwirefilelineitems`.`note` LIKE ",searchString,"  OR 
`bulkwirefilelineitems`.`transactionType` LIKE ",searchString,"  OR `bulkwirefilelineitems`.`recipientAddressLine1` LIKE ",searchString,"  OR `bulkwirefilelineitems`.`recipientAddressLine2` LIKE ",searchString,"  OR `bulkwirefilelineitems`.`recipientCity` LIKE ",searchString,"  OR `bulkwirefilelineitems`.`recipientState` LIKE ",searchString,"  OR `bulkwirefilelineitems`.`recipientZipCode` LIKE ",searchString,"  OR `bulkwirefilelineitems`.`recipientBankName` LIKE ",searchString,"  OR `bulkwirefilelineitems`.`recipientBankAddress1` LIKE ",searchString,"  OR
`bulkwirefilelineitems`.`recipientBankAddress2` LIKE ",searchString,"  OR `bulkwirefilelineitems`.`recipientBankcity` LIKE ",searchString,"  OR
`bulkwirefilelineitems`.`recipientBankZipCode` LIKE ",searchString,"  OR `bulkwirefilelineitems`.`recipientBankstate` LIKE ",searchString,")");
	
SET @defaultFilter = concat(@queryFilter, @orderBy);

SET @searchFilter = concat(@queryFilter," AND ",@searchQuery,@orderBy);

SET @filter = if(searchString = "",@defaultFilter,@searchFilter);

SET @select_statement = concat("SELECT * FROM `bulkwirefilelineitems` WHERE ", @filter);
        
 -- select @select_statement;
	PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_bulkwire_files_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_bulkwire_files_proc`(
in createdBy varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in searchString varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in sortByParam varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in sortOrder varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in pageOffset int,
in pageSize int
)
BEGIN
    
    SET @companyId = (select group_concat(concat(contractId,"_",coreCustomerId) SEPARATOR ",") from contractcustomers where customerId =createdby);
        IF @companyId is NULL THEN     
            SET @companyId = "";
        END IF;
		
    SET @companyId = (select concat(@companyId,",",Organization_Id) from customer where id =createdby);
	
	SET @isSMEUser = if(@companyId != "" OR @companyId != NULL , true, false);
    
    SET @filterRetail = concat("`bulkwirefiles`.`createdBy` = ", createdby , " AND `bulkwirefiles`.`softdeleteflag` = 0 ");
				
	SET @filterSME = concat("FIND_IN_SET(`bulkwirefiles`.`company_id`, '", @companyId  ,"')" " AND `bulkwirefiles`.`softdeleteflag` = 0");
    
    SET @getByIdFilter = if(@isSMEUser, @filterSME, @filterRetail);
    
    SET sortByParam = if(sortByParam = "" OR sortByParam = NULL, 'createdts', sortByParam);
	SET sortByParam = if(sortByParam = 'username', 'firstName,lastname', sortByParam);
    SET sortOrder = if(sortOrder = "" OR sortOrder = NULL, 'DESC', sortOrder);
    
     SET searchString = if(searchString = "" OR searchString = NULL,"",concat("'%",searchString,"%'"));               
    
    SET @orderBy = concat(" ORDER BY ", sortByParam , " ", sortOrder);
    
    SET @paginationQuery = if((pageOffset != NULL OR pageOffset != "" AND pageSize != NULL OR pageSize != ""),
							concat(" LIMIT ",pageSize, " OFFSET " ,pageOffset),
                            ''); 
    
    
	SET @searchQuery = concat("(`bulkwirefiles`.`bulkWireFileName` LIKE ",searchString," OR `bulkwirefiles`.`noOfTransactions` LIKE ",
    searchString," OR `bulkwirefiles`.`noOfDomesticTransactions` LIKE ",searchString," OR `bulkwirefiles`.`noOfInternationalTransactions` LIKE ",searchString," OR  `customer`.`firstname` LIKE ",searchString," OR `customer`.`lastname` LIKE ",searchString,")");
    
    SET @defaultFilter = concat(@getByIdFilter, @orderBy , @paginationQuery);
	SET @searchFilter = concat(@getByIdFilter," AND ",@searchQuery, @orderBy);
    SET @filter = if(searchString = "",@defaultFilter,@searchFilter);

    
    SET @select_statement = concat("SELECT 
	    bulkwirefiles.bulkWireFileID,
		bulkwirefiles.bulkWireFileName, 
		bulkwirefiles.noOfTransactions, 
		bulkwirefiles.noOfDomesticTransactions,
		bulkwirefiles.noOfInternationalTransactions,
		bulkwirefiles.createdts,
		bulkwirefiles.lastmodifiedts,
		bulkwirefiles.lastExecutedOn,
        customer.id, 
		customer.FirstName as firstname,
		customer.LastName as lastname
     FROM
        (`bulkwirefiles`
		LEFT JOIN `customer` ON (`bulkwirefiles`.`createdBy` = `customer`.`id`))
		WHERE ", @filter);
    
	-- select @select_statement;
	PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_bulkwire_files_transct_detail_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_bulkwire_files_transct_detail_proc`(
in  bulkWireFileID varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in  searchString varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in  sortByParam varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in  sortOrder varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in  pageOffset int,
in  pageSize int
)
BEGIN
    
    SET @qfilter = concat("`bulkwirefiletransactdetails`.`bulkWireFileID` = \"", bulkWireFileID, "\" AND `bulkwirefiletransactdetails`.`softdeleteflag` = 0 ");
      
    SET sortByParam = if(sortByParam = "" OR sortByParam = NULL, 'transactionDate', sortByParam);
    SET sortByParam = if(sortByParam = 'username', 'firstname,lastname', sortByParam);
    SET sortOrder = if(sortOrder = "" OR sortOrder = NULL, 'DESC', sortOrder);
    
    SET searchString = if(searchString = "" OR searchString = NULL,"",concat("'%",searchString,"%'"));   
    
    SET @orderBy = concat(" ORDER BY ", sortByParam , " ", sortOrder);
    
    SET @paginationQuery = if((pageOffset != NULL OR pageOffset != "" AND pageSize != NULL OR pageSize != ""),
							concat(" LIMIT ",pageSize, " OFFSET " ,pageOffset),
                            ''); 
    
    SET @searchQuery = concat("(`bulkwirefiletransactdetails`.`transactionDate` LIKE ",searchString," OR `bulkwirefiletransactdetails`.`totalCountOfTransactions` LIKE ",searchString," OR  `customer`.`firstname` LIKE ",searchString," OR `customer`.`lastname` LIKE ",searchString,")");
    
	SET @defaultFilter = concat(@qfilter, @orderBy , @paginationQuery);

	SET @searchFilter = concat(@qfilter," AND ",@searchQuery,@orderBy);

    SET @filter = if(searchString = "",@defaultFilter,@searchFilter);

	SET @select_statement = concat("SELECT 
	    bulkwirefiletransactdetails.bulkWireTransactionID,
		bulkwirefiletransactdetails.bulkWireFileID, 
		bulkwirefiletransactdetails.initiatedBy, 
		bulkwirefiletransactdetails.createdts,
		bulkwirefiletransactdetails.lastmodifiedts,
		bulkwirefiletransactdetails.transactionDate, 
		bulkwirefiletransactdetails.totalCountOfTransactions,
		bulkwirefiletransactdetails.totalCountOfDomesticTransactions,
		bulkwirefiletransactdetails.totalCountOfInternationalTransactions,
		customer.FirstName as firstname,
		customer.LastName as lastname
    FROM
        (`bulkwirefiletransactdetails`
		LEFT JOIN `customer` ON (`bulkwirefiletransactdetails`.`initiatedBy` = `customer`.`id`))
		WHERE ", @filter);
		
 -- select @select_statement;
	PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_bulkwire_template_transct_detail_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_bulkwire_template_transct_detail_proc`(
in  bulkWireTemplateID varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in  searchString varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in  sortByParam varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in  sortOrder varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in  pageOffset int,
in  pageSize int
)
BEGIN
    
    SET @qfilter = concat("`bulkwiretemplatetransactdetails`.`bulkWireTemplateID` = \"", bulkWireTemplateID, "\" AND `bulkwiretemplatetransactdetails`.`softdeleteflag` = 0 ");
      
    SET sortByParam = if(sortByParam = "" OR sortByParam = NULL, 'transactionDate', sortByParam);
    SET sortByParam = if(sortByParam = 'username', 'firstname,lastname', sortByParam);
    SET sortOrder = if(sortOrder = "" OR sortOrder = NULL, 'DESC', sortOrder);
    
    SET searchString = if(searchString = "" OR searchString = NULL,"",concat("'%",searchString,"%'"));   
    
    SET @orderBy = concat(" ORDER BY ", sortByParam , " ", sortOrder);
    
    SET @paginationQuery = if((pageOffset != NULL OR pageOffset != "" AND pageSize != NULL OR pageSize != ""),
							concat(" LIMIT ",pageSize, " OFFSET " ,pageOffset),
                            ''); 
    
    SET @searchQuery = concat("(`bulkwiretemplatetransactdetails`.`transactionDate` LIKE ",searchString," OR `bulkwiretemplatetransactdetails`.`totalCountOfTransactions` LIKE ",searchString," OR  `customer`.`firstname` LIKE ",searchString," OR `customer`.`lastname` LIKE ",searchString,")");
    
	SET @defaultFilter = concat(@qfilter, @orderBy , @paginationQuery);

	SET @searchFilter = concat(@qfilter," AND ",@searchQuery,@orderBy);

    SET @filter = if(searchString = "",@defaultFilter,@searchFilter);

	SET @select_statement = concat("SELECT 
	    bulkwiretemplatetransactdetails.bulkWireTransactionID,
		bulkwiretemplatetransactdetails.bulkWireTemplateID, 
		bulkwiretemplatetransactdetails.initiatedBy, 
		bulkwiretemplatetransactdetails.createdts,
		bulkwiretemplatetransactdetails.lastmodifiedts,
		bulkwiretemplatetransactdetails.transactionDate, 
		bulkwiretemplatetransactdetails.totalCountOfTransactions,
		bulkwiretemplatetransactdetails.totalCountOfDomesticTransactions,
		bulkwiretemplatetransactdetails.totalCountOfInternationalTransactions,
		customer.FirstName as firstname,
		customer.LastName as lastname
    FROM
        (`bulkwiretemplatetransactdetails`
		LEFT JOIN `customer` ON (`bulkwiretemplatetransactdetails`.`initiatedBy` = `customer`.`id`))
		WHERE ", @filter);
		
 -- select @select_statement;
	PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_bwfile_domesticInternationalCount_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_bwfile_domesticInternationalCount_proc`(
	IN `_bulkwirefileID` VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
    IF _bulkwirefileID != "" AND _bulkwirefileID IS NOT NULL THEN
		SELECT noOfDomesticTransactions,noOfInternationalTransactions FROM bulkwirefiles WHERE bulkWireFileID = `_bulkwirefileID`;
	ELSE
		SELECT 0 AS noOfDomesticTransactions, 0 AS noOfInternationalTransactions;
	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_bwtemplate_domesticInternationalCount_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_bwtemplate_domesticInternationalCount_proc`(
	IN `_bulkwiretemplateID` VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `_bulkwiretemplatelineitemIDs` TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

	IF _bulkwiretemplatelineitemIDs != "" AND _bulkwiretemplatelineitemIDs IS NOT NULL THEN
		SELECT COUNT(*) FROM bulkwiretemplatelineitems WHERE FIND_IN_SET(bulkWireTemplateLineItemID,_bulkwiretemplatelineitemIDs) AND bulkWireTransferType = 'Domestic' AND softdeleteflag = 0 INTO @DomCount;
		SELECT COUNT(*) FROM bulkwiretemplatelineitems WHERE FIND_IN_SET(bulkWireTemplateLineItemID,_bulkwiretemplatelineitemIDs) AND bulkWireTransferType = 'International' AND softdeleteflag = 0 INTO @InternationalCount;
		SELECT @DomCount AS noOfDomesticTransactions, @InternationalCount AS noOfInternationalTransactions;
		
	ELSEIF _bulkwiretemplateID != "" AND _bulkwiretemplateID IS NOT NULL THEN
		SELECT noOfDomesticTransactions,noOfInternationalTransactions FROM bulkwiretemplate WHERE bulkWireTemplateID = `_bulkwiretemplateID`;
		
	ELSE
		SELECT 0 AS noOfDomesticTransactions, 0 AS noOfInternationalTransactions;
	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_cardtransaction_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_cardtransaction_proc`(
in _cardNumber varchar(50),
in _pageOffset int,
in _pageSize int,
in _sortOrder varchar(50),
in _sortByParam varchar(50)
)
BEGIN
    SET @filter = concat("cardtransaction.cardNumber = ", _cardNumber);
     
    SET _sortByParam = if(_sortByParam = "" OR _sortByParam = NULL, 'transactionDate', _sortByParam);
    SET _sortOrder = if(_sortOrder = "" OR _sortOrder = NULL, 'DESC', _sortOrder);
   
    SET @orderBy = concat(" ORDER BY ", _sortByParam , " ", _sortOrder);
   
    SET @paginationQuery = if((_pageOffset != NULL OR _pageOffset != "" AND _pageSize != NULL OR _pageSize != ""),
concat(" LIMIT ",_pageSize, " OFFSET " ,_pageOffset),
                            '');
   
SET @select_statement = concat("SELECT `cardtransaction`.`transactionDescription`,
    `cardtransaction`.`transactionBalance`,
    `cardtransaction`.`transactionMerchantAddressName`,
    `cardtransaction`.`transactionMerchantCity`,
    `cardtransaction`.`merchantCategory`,
    `cardtransaction`.`transactionStatus`,
    `cardtransaction`.`transactionType`,
    `cardtransaction`.`transactionCategory`,
    `cardtransaction`.`transactionDetailDescription`,
    `cardtransaction`.`transactionIndicator`,
    `cardtransaction`.`transactionDate`,
    `cardtransaction`.`transactionTime`,
    `cardtransaction`.`transactionAmount`,
    `cardtransaction`.`transactionReferenceNumber`,
    `cardtransaction`.`transactionCurrencyCode`,
    `cardtransaction`.`transactionExchangeRate`,
    `cardtransaction`.`exchangeCurrency`,
    `cardtransaction`.`exchangeAmount`,
    `cardtransaction`.`transactionTaxIndicator`,
    `cardtransaction`.`taxPercentage`,
    `cardtransaction`.`transactionTaxAmount`,
    `cardtransaction`.`transactionTerminalID`,
    `cardtransaction`.`cardType`
    FROM cardtransaction cardtransaction
WHERE ", @filter, @orderBy, @paginationQuery);
       
-- select @select_statement;
PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
     
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_customersignatorygroup_details_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_customersignatorygroup_details_proc`(
in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
SELECT 
	`customersignatorygroup`.`customerSignatoryGroupId` AS `customerSignatoryGroupId`,
	`customersignatorygroup`.`signatoryGroupId` AS `signatoryGroupId`,
	`customersignatorygroup`.`customerId` AS `customerId`,
	`signatorygroup`.`coreCustomerId` AS `coreCustomerId`,
	`signatorygroup`.`contractId` AS `contractId`,
	`signatorygroup`.`signatoryGroupName` AS `signatoryGroupName`,
	`signatorygroup`.`signatoryGroupDescription` AS `signatoryGroupDescription`
from 
	`customersignatorygroup`
LEFT JOIN `signatorygroup` ON (`signatorygroup`.`signatoryGroupId` = `customersignatorygroup`.`signatoryGroupId`)
	where 
        `customersignatorygroup`.`customerId` = _customerId;           
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_default_account_actions_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_default_account_actions_proc`(
    IN _userId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, 
    IN _coreCustomerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
    SET SESSION group_concat_max_len = 100000000;

    SET @select_statement = "select group_concat(distinct featureaction.id SEPARATOR ',') as defaultAccountActions from featureaction where 
    (featureaction.isAccountLevel = '1' OR featureaction.isAccountLevel = true) 
    AND featureaction.id IN ";
        
    SET @_contractId = (select contractcorecustomers.contractId from contractcorecustomers where contractcorecustomers.coreCustomerId = _coreCustomerId);
    
    SET @serviceDefinitionId = (SELECT servicedefinitionId from contract WHERE id = @_contractId);
    SET @serviceType = (SELECT serviceType from servicedefinition WHERE id = @serviceDefinitionId);
    SET @_groupId = (select customergroup.Group_id from customergroup where customergroup.Customer_id = _userId
                    and customergroup.coreCustomerId = _coreCustomerId);
                            
    SET @select_statement = CONCAT(@select_statement , "(SELECT actionId FROM servicedefinitionactionlimit
                        WHERE serviceDefinitionId= ",QUOTE(@serviceDefinitionId),"
                        AND servicedefinitionactionlimit.actionId IN ");

    SET @select_statement = CONCAT(@select_statement , " ( SELECT Action_id FROM groupactionlimit WHERE groupactionlimit.Group_id = ",QUOTE(@_groupId),"
                                                                AND groupactionlimit.Action_id IN ");

    SET @select_statement = CONCAT(@select_statement ," ( SELECT actionId  FROM contractactionlimit WHERE 
                                contractId = ",QUOTE(@_contractId),"
                                AND coreCustomerId = ",QUOTE(_coreCustomerId));
    SET @select_statement = CONCAT(@select_statement ,")))");
       
    PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_fi_org_group_level_limits` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_fi_org_group_level_limits`(
IN _groupId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _companyId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

     SET @select = CONCAT(" select 
        `actionlimit`.`Action_id`,
        `actionlimit`.`LimitType_id`,
        `actionlimit`.`value` as `fiValue`,
        `organisationactionlimit`.`value` as `orgValue`,
        `groupactionlimit`.`value` as `groupValue`
         from (`actionlimit` 
         LEFT JOIN `groupactionlimit` ON ( 
                    `actionlimit`.`Action_id` = `groupactionlimit`.`Action_id` AND 
                    `actionlimit`.`LimitType_id` = `groupactionlimit`.`LimitType_id`
                  )
         LEFT JOIN `organisationactionlimit` ON ( 
                    `actionlimit`.`Action_id` =  `organisationactionlimit`.`Action_id` AND
                    `actionlimit`.`LimitType_id` = `organisationactionlimit`.`LimitType_id`
                  ) 
         )
         WHERE 
         `groupactionlimit`.`Group_id` = '", _groupId, "' and ",
         "`organisationactionlimit`.`Organisation_id` = ", _companyId, ";");

         PREPARE stmt FROM @select; 
         EXECUTE stmt; DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_generaltranscation_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_generaltranscation_proc`(
	IN _customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _transactionId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _featureActionId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _featureactionlist TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _queryType VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _filterByParam TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _filterByValue TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _searchString VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _sortByParam VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _sortOrder VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _pageSize VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _pageOffset VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
MAINLABEL:BEGIN
		SET SESSION group_concat_max_len = 100000000;
		
		SET @combinedIds = (select group_concat(id SEPARATOR ",") from customer where combinedUserId = _customerId);

		IF @combinedIds is NULL THEN      
			SET @combinedIds = _customerId;
		ELSE 
			SET @combinedIds = concat(_customerId , "," ,@combinedIds);
        END IF;
		
		SET @isSelfApprovalEnabled = (select isSelfApprovalEnabled from application);
		
		IF @isSelfApprovalEnabled is FALSE THEN
			SET @notCreatedBySelf = concat(" AND NOT FIND_IN_SET(`generaltransaction`.`createdby`, '",@combinedIds,"')");
		ELSE
			SET @notCreatedBySelf = "";
        END IF;
		
		SET _filterByParam = IF(_filterByParam = NULL , '', _filterByParam);
        SET _filterByValue = IF(_filterByValue = NULL , '', _filterByValue);
        
        SET _transactionId = IF(_transactionId = "" OR _transactionId = NULL, '%', _transactionId);
        SET _featureActionId = IF(_transactionId = "%" , '%', _featureActionId);

		SET @numOfParams = 0;
        IF LENGTH(_filterByParam) > 0 THEN
			SET @numOfParams = LENGTH(_filterByParam) - LENGTH(REPLACE(_filterByParam, ',', '')) + 1;
		END IF;
        
        SET @searchQuery = "";
		SET @idx = 1;
		filterParams:LOOP
			IF @idx > @numOfParams THEN 
				LEAVE filterParams;
			END IF;
			
			SET @filterParam = SUBSTRING_INDEX(SUBSTRING_INDEX(_filterByParam, ',', @idx), ',', -1 );
			SET @filterValue = SUBSTRING_INDEX(SUBSTRING_INDEX(_filterByValue, ',', @idx), ',', -1 );
			SET @searchQuery = concat(@searchQuery, " AND (",@filterParam," LIKE '",@filterValue,"' )");
			SET @idx = @idx + 1;
			 
		END LOOP filterParams;

	    SET @companyId = (select group_concat(concat(contractId,"_",coreCustomerId) SEPARATOR ",") from contractcustomers where customerId =_customerId);
	    
	    IF @companyId is NULL THEN      
			SET @companyId = "";
		END IF;
        
        IF _featureactionlist is NULL THEN      
			LEAVE MAINLABEL;
		END IF;
        
        SET _sortByParam = if(_sortByParam = "" OR _sortByParam = NULL, 'createdts', _sortByParam);
		SET _sortOrder = if(_sortOrder = "" OR _sortOrder = NULL, 'DESC', _sortOrder);
        
        SET @customerMatrixIds = (SELECT group_concat(approvalMatrixId SEPARATOR ",") FROM customerapprovalmatrix WHERE FIND_IN_SET(customerId, @combinedIds));
        
        IF @customerMatrixIds is NULL THEN      
			SET @customerMatrixIds = "";
		END IF;
        
        SET @alreadyApprovedIds = (select GROUP_CONCAT(DISTINCT(requestId) SEPARATOR ",") from bbactedrequest WHERE FIND_IN_SET(createdby, @combinedIds) AND action = 'Approved');
        
        IF @alreadyApprovedIds is NULL THEN      
			SET @alreadyApprovedIds = "";
		END IF;
        
        SET @approvalRequestIds = (SELECT group_concat(DISTINCT(requestId) SEPARATOR ",") 
        							FROM requestapprovalmatrix
        							INNER JOIN approvalmatrix ON requestapprovalmatrix.approvalMatrixId = approvalmatrix.id
									INNER JOIN approvalrule ON approvalmatrix.approvalruleId = approvalrule.id
        								WHERE FIND_IN_SET(requestapprovalmatrix.approvalMatrixId,  @customerMatrixIds) 
        								AND NOT FIND_IN_SET(requestapprovalmatrix.requestId, @alreadyApprovedIds)
        								AND ((approvalrule.numberOfApprovals = -1 AND requestapprovalmatrix.receivedApprovals < (SELECT COUNT(DISTINCT(customerId)) FROM customerapprovalmatrix WHERE customerapprovalmatrix.approvalMatrixId = requestapprovalmatrix.approvalMatrixId)) 
												OR
											(approvalrule.numberOfApprovals != -1 AND requestapprovalmatrix.receivedApprovals < approvalrule.numberOfApprovals))
        							);
        
        IF @approvalRequestIds is NULL THEN      
			SET @approvalRequestIds = "''";
		END IF;
        
        SET @queryTypecondition = if(_queryType = 'myRequests', concat(" AND FIND_IN_SET(`generaltransaction`.`createdby`, '",@combinedIds,"') AND (`bbrequest`.`status` = 'Pending' OR `bbrequest`.`status` = 'Approved' OR `bbrequest`.`status` = 'Rejected' ) "),
								  if(_queryType = 'pendingForMyApprovals', concat(" AND FIND_IN_SET(`generaltransaction`.`requestId`,  '",@approvalRequestIds,"')  AND `generaltransaction`.`status` = 'Pending' ",@notCreatedBySelf),
                                        if(_queryType = 'rejected', concat(" AND `generaltransaction`.`status` = 'Rejected' "),
                                        if(_transactionId = '%' , concat(" AND NOT `generaltransaction`.`status` = 'Withdrawn'"),
                                        ''))));
        -- select @queryTypecondition;
        SET @searchQuery = if(_searchString = NULL OR _searchString = "", @searchQuery, 
								concat(@searchQuery, " AND (`generaltransaction`.`payeeId` LIKE '%",_searchString,"%' OR `customeraccounts`.`accountName` LIKE '%",_searchString,"%' OR `feature`.`name` LIKE '%",_searchString,"%' OR `customer`.`userName` LIKE '%",_searchString,"%' )"));
        SET @paginationQuery = if(_pageOffset = NULL OR _pageOffset = "" OR _pageSize = NULL OR _pageSize = "", '', concat(" LIMIT ",_pageOffset, ", " ,_pageSize)); 
        
        SET @features = (SELECT group_concat(Feature_id SEPARATOR ",") FROM featureaction WHERE FIND_IN_SET(id,_featureactionlist) > 0);
        
        IF @features is NULL THEN      
			SET @features = "";
		END IF;
        
        SET @createActions = (SELECT group_concat(id SEPARATOR ",") FROM featureaction WHERE FIND_IN_SET(Feature_id, @features) > 0 AND id LIKE "%_CREATE");
        
        IF @createActions is NULL THEN      
			SET @createActions = "";
		END IF;
		
		SET @companyRequestIds = (SELECT group_concat(requestId SEPARATOR ",") FROM bbrequest WHERE FIND_IN_SET(companyId, @companyId) AND FIND_IN_SET(bbrequest.featureActionId, @createActions));
        
        IF @companyRequestIds is NULL THEN      
            SET @companyRequestIds = "";
        END IF;
        
	    SET @customerAcounts = (SELECT group_concat(Account_id SEPARATOR ",") FROM customeraccounts WHERE FIND_IN_SET(Customer_id, @combinedIds));
	    
        IF @customerAcounts is NULL THEN      
			SET @customerAcounts = "";
		END IF;
        
        SET @accountsQuery = if(@companyId = "" OR @companyId = NULL, '', concat(" AND FIND_IN_SET(`generaltransaction`.`fromAccountNumber`, '", @customerAcounts,"') "));
        SET @companyQuery = if(@companyId = "" OR @companyId = NULL, '', concat(" AND ( FIND_IN_SET(`generaltransaction`.`companyId` , '", @companyId ,"') OR FIND_IN_SET(`generaltransaction`.`createdby`, '",@combinedIds,"'))"));
        
		SET @select_statement = concat("SELECT * FROM
        
        (SELECT 
						`generaltransaction`.`transactionId`,
						`generaltransaction`.`featureActionId`,
						`feature`.`name` as `featureName`,
						`generaltransaction`.`fromAccountNumber`,
						`generaltransaction`.`amount`,
						`generaltransaction`.`requestId`,
						`generaltransaction`.`createdby`,
						`generaltransaction`.`createdts`,
						`generaltransaction`.`frequencyTypeId`,
						`generaltransaction`.`status`,
						`generaltransaction`.`numberOfRecurrences`,
						`generaltransaction`.`payeeId`,
						`generaltransaction`.`companyId`,
						`generaltransaction`.`scheduledDate`,
						( CASE 
							WHEN `customeraccounts`.`AccountName` is NULL THEN 'AccountName' 
			                ELSE `customeraccounts`.`AccountName` 
						END ) AS `accountName`,
						`customer`.`UserName` AS `userName`,
						`bbrequest`.`createdby` AS `requestCreatedby`,
						(CASE
							WHEN `generaltransaction`.`createdby` IN (",@combinedIds,") THEN 'true'
							ELSE 'false'
						 END) as `amICreator`,
						(CASE 
							WHEN `bbrequest`.`requestId` IN (",@approvalRequestIds,") THEN 'true'
			        		ELSE 'false'
				 		END)
				 		 as `amIApprover`

				FROM

				( SELECT 
						`billpaytransfers`.`transactionId` AS `transactionId`,
						`billpaytransfers`.`featureActionId` AS `featureActionId`,
						`billpaytransfers`.`fromAccountNumber` AS `fromAccountNumber`,
						`billpaytransfers`.`amount` AS `amount`,
						`billpaytransfers`.`requestId` AS `requestId`,
						`billpaytransfers`.`createdby` AS `createdby`,
						`billpaytransfers`.`createdts` AS `createdts`,
						`billpaytransfers`.`frequencyTypeId` AS `frequencyTypeId`,
						`billpaytransfers`.`status` AS `status`,
						`billpaytransfers`.`numberOfRecurrences` AS `numberOfRecurrences`,
                        CASE 
							WHEN `billpaytransfers`.`payeeName` IS NULL OR `billpaytransfers`.`payeeName` = '' THEN 
							CASE
								WHEN `billpaytransfers`.`billerId` IS NULL OR `billpaytransfers`.`billerId` = '' THEN 
									CASE 
										WHEN `billpaytransfers`.`payeeId` IS NULL OR `billpaytransfers`.`payeeId` = '' THEN `billpaytransfers`.`toAccountNumber`
										ELSE `billpaytransfers`.`payeeId`
									END
								ELSE `billpaytransfers`.`billerId`
							END
							ELSE `billpaytransfers`.`payeeName`
						END 
                        AS `payeeId`,
						`billpaytransfers`.`companyId` AS `companyId`,
		                `billpaytransfers`.`softdeleteflag` AS `softdeleteflag`,
						`billpaytransfers`.`scheduledDate` AS `scheduledDate`
					FROM
						`billpaytransfers`
				UNION
					SELECT 
						`p2ptransfers`.`transactionId` AS `transactionId`,
						`p2ptransfers`.`featureActionId` AS `featureActionId`,
						`p2ptransfers`.`fromAccountNumber` AS `fromAccountNumber`,
						`p2ptransfers`.`amount` AS `amount`,
						`p2ptransfers`.`requestId` AS `requestId`,
						`p2ptransfers`.`createdby` AS `createdby`,
						`p2ptransfers`.`createdts` AS `createdts`,
						`p2ptransfers`.`frequencyTypeId` AS `frequencyTypeId`,
						`p2ptransfers`.`status` AS `status`,
						`p2ptransfers`.`numberOfRecurrences` AS `numberOfRecurrences`,
						CASE 
							WHEN `p2ptransfers`.`payeeName` IS NULL OR `p2ptransfers`.`payeeName` = '' THEN 
							CASE
								WHEN `p2ptransfers`.`personId` IS NULL OR `p2ptransfers`.`personId` = '' THEN 
									CASE 
										WHEN `p2ptransfers`.`p2pContact` IS NULL OR `p2ptransfers`.`p2pContact` = '' THEN `p2ptransfers`.`toAccountNumber`
										ELSE `p2ptransfers`.`p2pContact`
									END
								ELSE `p2ptransfers`.`personId`
							END
							ELSE `p2ptransfers`.`payeeName`
						END 
                        AS `payeeId`,
						`p2ptransfers`.`companyId` AS `companyId`,
		                `p2ptransfers`.`softdeleteflag` AS `softdeleteflag`,
						`p2ptransfers`.`scheduledDate` AS `scheduledDate`
					FROM
						`p2ptransfers`
				UNION
					SELECT 
						`wiretransfers`.`transactionId` AS `transactionId`,
						`wiretransfers`.`featureActionId` AS `featureActionId`,
						`wiretransfers`.`fromAccountNumber` AS `fromAccountNumber`,
						`wiretransfers`.`amount` AS `amount`,
						`wiretransfers`.`requestId` AS `requestId`,
						`wiretransfers`.`createdby` AS `createdby`,
						`wiretransfers`.`createdts` AS `createdts`,
						null AS `frequencyTypeId`,
						`wiretransfers`.`status` AS `status`,
						null AS `numberOfRecurrences`,
                        CASE 
							WHEN `wiretransfers`.`payeeName` IS NULL OR `wiretransfers`.`payeeName` = '' THEN 
							CASE
								WHEN `wiretransfers`.`payPersonName` IS NULL OR `wiretransfers`.`payPersonName` = '' THEN 
									CASE 
										WHEN `wiretransfers`.`payeeId` IS NULL OR `wiretransfers`.`payeeId` = '' THEN `wiretransfers`.`payeeAccountNumber`
										ELSE `wiretransfers`.`payeeId`
									END
								ELSE `wiretransfers`.`payPersonName`
							END
							ELSE `wiretransfers`.`payeeName`
						END 
                        AS `payeeId`,
						`wiretransfers`.`companyId` AS `companyId`,
		                `wiretransfers`.`softdeleteflag` AS `softdeleteflag`,
						`wiretransfers`.`createdts` AS `scheduledDate`
					FROM
						`wiretransfers`
				UNION
					SELECT 
						`intrabanktransfers`.`transactionId` AS `transactionId`,
						`intrabanktransfers`.`featureActionId` AS `featureActionId`,
						`intrabanktransfers`.`fromAccountNumber` AS `fromAccountNumber`,
						`intrabanktransfers`.`amount` AS `amount`,
						`intrabanktransfers`.`requestId` AS `requestId`,
						`intrabanktransfers`.`createdby` AS `createdby`,
						`intrabanktransfers`.`createdts` AS `createdts`,
						`intrabanktransfers`.`frequencyTypeId` AS `frequencyTypeId`,
						`intrabanktransfers`.`status` AS `status`,
						`intrabanktransfers`.`numberOfRecurrences` AS `numberOfRecurrences`,
                        CASE 
							WHEN `intrabanktransfers`.`payeeName` IS NULL OR `intrabanktransfers`.`payeeName` = '' THEN 
							CASE
								WHEN `intrabanktransfers`.`payPersonName` IS NULL OR `intrabanktransfers`.`payPersonName` = '' THEN 
									CASE 
										WHEN `intrabanktransfers`.`personId` IS NULL OR `intrabanktransfers`.`personId` = '' THEN `intrabanktransfers`.`toAccountNumber`
										ELSE `intrabanktransfers`.`personId`
									END
								ELSE `intrabanktransfers`.`payPersonName`
							END
							ELSE `intrabanktransfers`.`payeeName`
						END 
                        AS `payeeId`,
						`intrabanktransfers`.`companyId` AS `companyId`,
		                `intrabanktransfers`.`softdeleteflag` AS `softdeleteflag`,
						`intrabanktransfers`.`scheduledDate` AS `scheduledDate`
					FROM
						`intrabanktransfers`
				UNION
					SELECT 
						`interbankfundtransfers`.`transactionId` AS `transactionId`,
						`interbankfundtransfers`.`featureActionId` AS `featureActionId`,
						`interbankfundtransfers`.`fromAccountNumber` AS `fromAccountNumber`,
						`interbankfundtransfers`.`amount` AS `amount`,
						`interbankfundtransfers`.`requestId` AS `requestId`,
						`interbankfundtransfers`.`createdby` AS `createdby`,
						`interbankfundtransfers`.`createdts` AS `createdts`,
						`interbankfundtransfers`.`frequencyTypeId` AS `frequencyTypeId`,
						`interbankfundtransfers`.`status` AS `status`,
						`interbankfundtransfers`.`numberOfRecurrences` AS `numberOfRecurrences`,
						CASE 
							WHEN `interbankfundtransfers`.`payeeName` IS NULL OR `interbankfundtransfers`.`payeeName` = '' THEN 
							CASE
								WHEN `interbankfundtransfers`.`payPersonName` IS NULL OR `interbankfundtransfers`.`payPersonName` = '' THEN 
									CASE 
										WHEN `interbankfundtransfers`.`personId` IS NULL OR `interbankfundtransfers`.`personId` = '' THEN `interbankfundtransfers`.`toAccountNumber`
										ELSE `interbankfundtransfers`.`personId`
									END
								ELSE `interbankfundtransfers`.`payPersonName`
							END
							ELSE `interbankfundtransfers`.`payeeName`
						END 
                        AS `payeeId`,
						`interbankfundtransfers`.`companyId` AS `companyId`,
		                `interbankfundtransfers`.`softdeleteflag` AS `softdeleteflag`,
						`interbankfundtransfers`.`scheduledDate` AS `scheduledDate`
					FROM
						`interbankfundtransfers`
				UNION
					SELECT 
						`internationalfundtransfers`.`transactionId` AS `transactionId`,
						`internationalfundtransfers`.`featureActionId` AS `featureActionId`,
						`internationalfundtransfers`.`fromAccountNumber` AS `fromAccountNumber`,
						`internationalfundtransfers`.`amount` AS `amount`,
						`internationalfundtransfers`.`requestId` AS `requestId`,
						`internationalfundtransfers`.`createdby` AS `createdby`,
						`internationalfundtransfers`.`createdts` AS `createdts`,
						`internationalfundtransfers`.`frequencyTypeId` AS `frequencyTypeId`,
						`internationalfundtransfers`.`status` AS `status`,
						`internationalfundtransfers`.`numberOfRecurrences` AS `numberOfRecurrences`,
						CASE 
							WHEN `internationalfundtransfers`.`payeeName` IS NULL OR `internationalfundtransfers`.`payeeName` = '' THEN 
							CASE
								WHEN `internationalfundtransfers`.`payPersonName` IS NULL OR `internationalfundtransfers`.`payPersonName` = '' THEN 
									CASE 
										WHEN `internationalfundtransfers`.`personId` IS NULL OR `internationalfundtransfers`.`personId` = '' THEN `internationalfundtransfers`.`toAccountNumber`
										ELSE `internationalfundtransfers`.`personId`
									END
								ELSE `internationalfundtransfers`.`payPersonName` 
							END
							ELSE `internationalfundtransfers`.`payeeName`
						END 
                        AS `payeeId`,
						`internationalfundtransfers`.`companyId` AS `companyId`,
		                `internationalfundtransfers`.`softdeleteflag` AS `softdeleteflag`,
						`internationalfundtransfers`.`scheduledDate` AS `scheduledDate`
					FROM
						`internationalfundtransfers`
				UNION
					SELECT 
						`ownaccounttransfers`.`transactionId` AS `transactionId`,
						`ownaccounttransfers`.`featureActionId` AS `featureActionId`,
						`ownaccounttransfers`.`fromAccountNumber` AS `fromAccountNumber`,
						`ownaccounttransfers`.`amount` AS `amount`,
						`ownaccounttransfers`.`requestId` AS `requestId`,
						`ownaccounttransfers`.`createdby` AS `createdby`,
						`ownaccounttransfers`.`createdts` AS `createdts`,
						`ownaccounttransfers`.`frequencyTypeId` AS `frequencyTypeId`,
						`ownaccounttransfers`.`status` AS `status`,
						`ownaccounttransfers`.`numberOfRecurrences` AS `numberOfRecurrences`,
						CASE 
							WHEN `ownaccounttransfers`.`payeeName` IS NULL OR `ownaccounttransfers`.`payeeName` = '' THEN 
							CASE
								WHEN `ownaccounttransfers`.`payPersonName` IS NULL OR `ownaccounttransfers`.`payPersonName` = '' THEN 
									CASE 
										WHEN `ownaccounttransfers`.`personId` IS NULL OR `ownaccounttransfers`.`personId` = '' THEN `ownaccounttransfers`.`toAccountNumber`
										ELSE `ownaccounttransfers`.`personId`
									END
								ELSE `ownaccounttransfers`.`payPersonName`
							END
							ELSE `ownaccounttransfers`.`payeeName`
						END 
                        AS `payeeId`,
						`ownaccounttransfers`.`companyId` AS `companyId`,
		                `ownaccounttransfers`.`softdeleteflag` AS `softdeleteflag`,
						`ownaccounttransfers`.`scheduledDate` AS `scheduledDate`
					FROM
						`ownaccounttransfers`
				) AS `generaltransaction`
		        LEFT JOIN `customer` ON (`generaltransaction`.`createdby` = `customer`.`id`)
		        LEFT JOIN `customeraccounts` ON ((`generaltransaction`.`createdby` = `customeraccounts`.`Customer_id`)
						AND (`generaltransaction`.`fromAccountNumber` = `customeraccounts`.`Account_id`))
		        LEFT JOIN `bbrequest` ON (`generaltransaction`.`requestId` = `bbrequest`.`requestId`)
		        LEFT JOIN `featureaction` ON (`generaltransaction`.`featureActionId` = `featureaction`.`id`)
                LEFT JOIN `feature` ON (`featureaction`.`Feature_id` = `feature`.`id`)
        
        	WHERE `generaltransaction`.`softdeleteflag` = 0
				",@companyQuery,"
				AND `generaltransaction`.`transactionId` LIKE '",_transactionId ,"' AND `generaltransaction`.`featureActionId` LIKE '",_featureActionId ,"'
				",@accountsQuery,"
                AND FIND_IN_SET(`generaltransaction`.`featureActionId`, \"", @createActions, "\") ",
                @queryTypecondition, " ",
                @searchQuery, " ) AS t1
            LEFT JOIN
            ( 
				SELECT 
					bbrequest.requestId,
					(select count(DISTINCT(createdby)) from bbactedrequest where bbactedrequest.action = 'Approved' AND  bbactedrequest.requestId = bbrequest.requestId) 
							as receivedApprovals,
                    LEAST(
						(SELECT COUNT(DISTINCT(customerId)) FROM customerapprovalmatrix WHERE customerapprovalmatrix.approvalMatrixId in (SELECT approvalMatrixId FROM requestapprovalmatrix where requestapprovalmatrix.requestId = bbrequest.requestId)) 
						, 
						SUM(
							CASE approvalrule.numberOfApprovals
								WHEN -1 THEN (SELECT COUNT(*) FROM customerapprovalmatrix WHERE customerapprovalmatrix.approvalMatrixId = requestapprovalmatrix.approvalMatrixId)
								WHEN NULL OR \"\" THEN 0
								ELSE approvalrule.numberOfApprovals
							END
						) 
					) as requiredApprovals
				FROM
				 bbrequest
				LEFT JOIN requestapprovalmatrix ON (bbrequest.requestId = requestapprovalmatrix.requestId)
				LEFT JOIN approvalmatrix ON (requestapprovalmatrix.approvalMatrixId = approvalmatrix.id)
				LEFT JOIN approvalrule ON (approvalmatrix.approvalruleId = approvalrule.id)
                WHERE FIND_IN_SET(bbrequest.requestId, \"",@companyRequestIds,"\")
				GROUP BY bbrequest.requestId
            ) AS t2
            ON 
            `t1`.`requestId` = `t2`.`requestId`
            ORDER BY ", _sortByParam, " ", _sortOrder , " ",
            @paginationQuery);
            
	-- select @select_statement;
	PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_nogroup_user_details_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_nogroup_user_details_proc`(
in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _coreCustomerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
SELECT DISTINCT
  `customer`.`id` AS `userId`,
  `customer`.`isCombinedUser` AS `isCombinedUser`,
  `customer`.`UserName` AS `userName`,
  `customer`.`FirstName` AS `firstName`,
  `customer`.`LastName` AS `lastName`,
  `membergroup`.`Name` AS `role`
from
  `customer`
LEFT JOIN `customergroup` ON (`customergroup`.`Customer_id` = `customer`.`id`)
LEFT JOIN `membergroup` ON (`membergroup`.`id` = `customergroup`.`Group_id` )
  where 
      find_in_set(`customer`.`id`,_customerId)   AND  `customergroup`.`coreCustomerId` = _coreCustomerId ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_organisation_customroles_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_organisation_customroles_proc`(
IN organisationId TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN customRoleId TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

  SET @select_roles = CONCAT("SELECT
    `customrole`.`synctimestamp`,
    `customrole`.`status_id`,
    `customrole`.`softdeleteflag`,
    `customrole`.`parent_id`,
    `customrole`.`organization_id`,
    `customrole`.`name`,
    `customrole`.`modifiedby`,
    `customrole`.`lastmodifiedts`,
    `customrole`.`id`,
    `customrole`.`description`,
    `customrole`.`createdts`,
    `customrole`.`createdby`,
    `customer`.`UserName` as `userName`,
    `membergroup`.`Name` as `parentRoleName`,
    `status`.`Description` as `statusValue`
     FROM ( `customrole` 
     LEFT JOIN `customer` ON (`customrole`.`createdby` = `customer`.`id`) 
     LEFT JOIN `status` ON (`customrole`.`status_id` = `status`.`id`)
     LEFT JOIN `membergroup` ON (`customrole`.`parent_id` = `membergroup`.`id`) )
     WHERE `customrole`.`organization_id` = ",organisationId, " and ",
     "`customrole`.`softdeleteflag` = 0");

     IF customRoleId = '' THEN
      SET @select_roles = CONCAT(@select_roles, ";");
     ELSE
      SET @select_roles = CONCAT(@select_roles, " and `customrole`.`id` =", customRoleId,";");
     END IF;
     
     PREPARE stmt FROM @select_roles; 
     EXECUTE stmt; DEALLOCATE PREPARE stmt;
      
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_pending_group_list_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_pending_group_list_proc`(in _requestId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
	SELECT signatorygroup.signatoryGroupId, signatorygroup.signatoryGroupName  
	FROM signatorygroup 
	WHERE FIND_IN_SET(signatorygroup.signatoryGroupId,
	(SELECT group_concat(REPLACE(REPLACE(REPLACE(REPLACE(pendingGroupList,']',''),'[',''),'"',''), ' ', ''))
		FROM signatorygrouprequestmatrix WHERE 
		signatorygrouprequestmatrix.requestId = _requestId AND signatorygrouprequestmatrix.isApproved = false));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_requestapprovalmatrix_details_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_requestapprovalmatrix_details_proc`(
IN _requestId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	
	SELECT bb.requestId AS requestId,
	am.id AS approvalMatrixId,
	ram.id AS requestApprovalMatrixId,
	ram.receivedApprovals AS receivedApprovals,
	IF(ar.numberOfApprovals = -1, na.numberOfApprovals, ar.numberOfApprovals) AS numberOfApprovals,
	cam.customerId AS customerId  
	FROM bbrequest AS bb JOIN 
	requestapprovalmatrix AS ram 
	JOIN approvalmatrix AS am JOIN 
	customerapprovalmatrix AS cam JOIN 
	approvalrule AS ar JOIN
	(SELECT approvalmatrixId, COUNT(DISTINCT(customerId)) AS numberOfApprovals FROM customerapprovalmatrix GROUP BY approvalmatrixId) as na
	WHERE bb.requestId = ram.requestId AND 
	ram.approvalMatrixId = am.id 
	AND cam.approvalMatrixId = am.id 
	AND am.approvalruleId = ar.id 
	AND na.approvalmatrixId = am.id
	AND bb.requestId = _requestId 
	AND cam.customerId = _customerId COLLATE utf8_general_ci;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_request_history_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_request_history_proc`(
  IN _customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
  IN _requestId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
 
	SET @companyId = (select group_concat(concat(contractId,"_",coreCustomerId) SEPARATOR ",") from contractcustomers where customerId =_customerId);
	IF @companyId is NULL THEN
	SET @companyId = "";
	END IF;
    
	SELECT 
		`bbactedrequest`.`approvalId` AS `approvalId`,
		`bbactedrequest`.`requestId` AS `requestId`,
		`bbactedrequest`.`companyId` AS `companyId`,
		`bbactedrequest`.`createdby` AS `requestActedby`,
		`bbactedrequest`.`status` AS `status`,
		`bbactedrequest`.`comments` AS `comments`,
		`bbactedrequest`.`action` AS `action`,
        `bbactedrequest`.`groupName` AS `groupName`,
		`bbactedrequest`.`createdts` AS `actionts`,
		`bbactedrequest`.`softdeleteflag` AS `softdeleteflag`,
        `customer`.`UserName` AS `userName`,
        CASE 
			WHEN `bbactedrequest`.`createdby` IS null THEN "System"
			ELSE 
			concat_ws(
				" ",
				IF(LENGTH(`customer`.`FirstName`),`customer`.`FirstName`,NULL),
				IF(LENGTH(`customer`.`MiddleName`),`customer`.`MiddleName`,NULL),
				IF(LENGTH(`customer`.`LastName`),`customer`.`LastName`,NULL)
			) 
        END AS `customerName`,
        `customer`.`FullName` AS `customerFullName`
	FROM 
    ( `bbactedrequest`
    LEFT JOIN `customer` ON (`bbactedrequest`.`createdby` = `customer`.`id`))
    WHERE `bbactedrequest`.`requestId` = _requestId
    AND  FIND_IN_SET(`bbactedrequest`.`companyId`, @companyId);
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_restrictive_featureactionlimits_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_restrictive_featureactionlimits_proc`(
	IN _locale varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _userId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _serviceDefinitionId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _roleId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _coreCustomerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _accessPolicyIdList varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	SET @select_statement = '';
    SET @action_select_statement = '';
	SET @action_select_statement = ("(SELECT featureaction.id AS actionId FROM featureaction LEFT JOIN feature ON ( feature.id = featureaction.Feature_id )");
	
	IF(_accessPolicyIdList != '') THEN 
		SET @action_select_statement = CONCAT(@action_select_statement , "WHERE FIND_IN_SET(featureaction.accessPolicyId ,",QUOTE(_accessPolicyIdList),")");
	END IF;
	
    SET @action_select_statement = CONCAT(@action_select_statement,")");
    
	IF(_serviceDefinitionId != '') THEN 
		SET @select_statement = CONCAT("(SELECT servicedefinitionactionlimit.actionId AS actionId FROM servicedefinitionactionlimit WHERE servicedefinitionactionlimit.serviceDefinitionId = " , QUOTE(_serviceDefinitionId));
		SET @select_statement = CONCAT(@select_statement , " AND servicedefinitionactionlimit.actionId IN" , @action_select_statement);
        SET @select_statement = CONCAT(@select_statement , ")");
        SET @action_select_statement = @select_statement;
    END IF;	
    IF(_roleId != '') THEN
		SET @select_statement = CONCAT("(SELECT groupactionlimit.Action_id AS actionId FROM groupactionlimit WHERE groupactionlimit.Group_id = ",QUOTE(_roleId));
		SET @select_statement = CONCAT(@select_statement , " AND groupactionlimit.Action_id IN ");
		SET @select_statement = CONCAT(@select_statement , @action_select_statement);
        SET @select_statement = CONCAT(@select_statement , ")");
        SET @action_select_statement = @select_statement;
    END IF;
    IF(_coreCustomerId != '') THEN 
		SET @select_statement = CONCAT("(SELECT contractactionlimit.actionId AS actionId FROM contractactionlimit WHERE contractactionlimit.coreCustomerId = ",QUOTE(_coreCustomerId));
        SET @select_statement = CONCAT(@select_statement , " AND contractactionlimit.actionId IN " , @action_select_statement);
        SET @select_statement = CONCAT(@select_statement , ")");
        SET @action_select_statement = @select_statement;
    END IF;
	IF(_userId != '') THEN 
		SET @select_statement = CONCAT("(SELECT customeraction.Action_id AS actionId FROM customeraction WHERE customeraction.Customer_id = ",QUOTE(_userId));
		IF(_coreCustomerId != '') THEN 
			SET @select_statement = CONCAT(@select_statement , " AND customeraction.coreCustomerId = " , QUOTE(_coreCustomerId));
        END IF;
        SET @select_statement = CONCAT(@select_statement , " AND customeraction.isAllowed = '1' AND customeraction.Action_id IN " , @action_select_statement);
        SET @select_statement = CONCAT(@select_statement , ")");
        SET @action_select_statement = @select_statement;
    END IF;
    SET @select_statement = "( SELECT 
								feature.id AS featureId,
								featuredisplaynamedescription.displayName AS featureName,
                                featuredisplaynamedescription.displayDescription AS featureDescription,
                                feature.Status_id AS featureStatus,
                                featureaction.status AS actionStatus,
                                featureaction.id AS actionId,
                                actiondisplaynamedescription.displayName AS actionName,
                                actiondisplaynamedescription.displayDescription AS actionDescription,
                                featureaction.isAccountLevel AS isAccountLevel,
                                featureaction.Type_id AS typeId,
                                featureaction.limitgroupId as limitGroupId,
                                featureaction.accesspolicyId as accessPolicyId,
                                featureaction.actionlevelId as actionLevelId,
                                null as limitTypeId,
                                null as fiLimitValue";
    if(_serviceDefinitionId != '') THEN
		SET @select_statement = CONCAT(@select_statement , ", null AS serviceLimitValue");
    END IF;
	IF(_roleId != '') THEN 
		SET @select_statement = CONCAT(@select_statement , ", null AS groupLimitValue");
    END IF;  
    IF(_coreCustomerId != '') THEN 
		SET @select_statement = CONCAT(@select_statement , ", null AS coreCustomerLimitValue");
    END IF;
    SET @select_statement = CONCAT(@select_statement , " FROM feature
															LEFT JOIN featuredisplaynamedescription ON ( featuredisplaynamedescription.Feature_id = feature.id)
															LEFT JOIN featureaction ON (featureaction.Feature_id = feature.id)
                                                            LEFT JOIN actiondisplaynamedescription ON (actiondisplaynamedescription.Action_id = featureaction.id)");
	IF(_coreCustomerId != '') THEN 
		SET @select_statement = CONCAT(@select_statement , "LEFT JOIN contractactionlimit ON (contractactionlimit.actionId = featureaction.id)");
    END IF;
	SET @select_statement = CONCAT(@select_statement , " WHERE featureaction.Type_id = 'NON_MONETARY' AND featureaction.id IN " , @action_select_statement);
    SET @select_statement = CONCAT(@select_statement , " AND featuredisplaynamedescription.Locale_id = ",QUOTE(_locale));
    SET @select_statement = CONCAT(@select_statement , " AND actiondisplaynamedescription.Locale_id = ",QUOTE(_locale) ,")");
       
    SET @select_statement = CONCAT(@select_statement , " UNION (SELECT 
								feature.id AS featureId,
								featuredisplaynamedescription.displayName AS featureName,
                                featuredisplaynamedescription.displayDescription AS featureDescription,
                                feature.Status_id AS featureStatus,
                                featureaction.status AS actionStatus,
                                featureaction.id AS actionId,
                                actiondisplaynamedescription.displayName AS actionName,
                                actiondisplaynamedescription.displayDescription AS actionDescription,
                                featureaction.isAccountLevel AS isAccountLevel,
                                featureaction.Type_id AS typeId,
                                featureaction.limitgroupId as limitGroupId,
                                featureaction.accesspolicyId as accessPolicyId,
                                featureaction.actionlevelId as actionLevelId,
                                actionlimit.LimitType_id as limitTypeId,
                                actionlimit.value as fiLimitValue");
	IF(_serviceDefinitionId != '') THEN
		SET @select_statement = CONCAT(@select_statement , ", servicedefinitionactionlimit.value AS serviceLimitValue");
    END IF;
    IF(_roleId != '') THEN 
		SET @select_statement = CONCAT(@select_statement , ", groupactionlimit.value AS groupLimitValue");
    END IF;
    IF(_coreCustomerId != '') THEN 
		SET @select_statement = CONCAT(@select_statement , ", contractactionlimit.value AS coreCustomerLimitValue");
    END IF;
	SET @select_statement = CONCAT(@select_statement , " FROM feature
														LEFT JOIN featuredisplaynamedescription ON ( featuredisplaynamedescription.Feature_id = feature.id)
														LEFT JOIN featureaction ON (featureaction.Feature_id = feature.id)
                                                        LEFT JOIN actionlimit ON (actionlimit.Action_id = featureaction.id)
                                                        LEFT JOIN actiondisplaynamedescription ON (actiondisplaynamedescription.Action_id = featureaction.id)");
    IF(_serviceDefinitionId != '') THEN
		SET @select_statement = CONCAT(@select_statement , " LEFT JOIN servicedefinitionactionlimit ON ( servicedefinitionactionlimit.actionId = actionlimit.Action_id AND 
																										servicedefinitionactionlimit.limitTypeId = actionlimit.LimitType_id)");
                                                                                                        
	END IF;
    IF(_roleId != '') THEN
		SET @select_statement = CONCAT(@select_statement , " LEFT JOIN groupactionlimit ON ( groupactionlimit.Action_id = actionlimit.Action_id AND 
																										groupactionlimit.LimitType_id = actionlimit.LimitType_id)");
	END IF;
    IF(_coreCustomerId != '') THEN
		SET @select_statement = CONCAT(@select_statement , " LEFT JOIN contractactionlimit ON ( contractactionlimit.actionId = actionlimit.Action_id AND 
																										contractactionlimit.limitTypeId = actionlimit.LimitType_id)");
	END IF;
	SET @select_statement = CONCAT(@select_statement , " WHERE featureaction.Type_id = 'MONETARY'");
    IF(_serviceDefinitionId != '') THEN
		SET @select_statement = CONCAT(@select_statement , " AND servicedefinitionactionlimit.serviceDefinitionId = " , QUOTE(_serviceDefinitionId));
	END IF;
    IF(_roleId != '') THEN
		SET @select_statement = CONCAT(@select_statement , " AND groupactionlimit.Group_id = " , QUOTE(_roleId));
	END IF;
    IF(_coreCustomerId != '') THEN
		SET @select_statement = CONCAT(@select_statement , " AND contractactionlimit.coreCustomerId = " , QUOTE(_coreCustomerId));
	END IF;
    SET @select_statement = CONCAT(@select_statement , " AND featureaction.id IN " , @action_select_statement);
    SET @select_statement = CONCAT(@select_statement , " AND featuredisplaynamedescription.Locale_id = ",QUOTE(_locale));
    SET @select_statement = CONCAT(@select_statement , " AND actiondisplaynamedescription.Locale_id = ",QUOTE(_locale) ,")");
   PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_signatorygroups_in_approvalrule_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_signatorygroups_in_approvalrule_proc`()
BEGIN
	SELECT signatorygroup.signatoryGroupId
	FROM signatorygroup 
	WHERE FIND_IN_SET(signatorygroup.signatoryGroupId,
	(SELECT group_concat(distinct(REPLACE(REPLACE(REPLACE(groupList,']',''),'[',''),'"','')))
		FROM signatorygroupmatrix WHERE 
		signatorygroupmatrix.softdeleteflag = false));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_signatorygroups_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_signatorygroups_proc`(
  IN _contractId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
  IN _coreCustomerId longtext CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	DECLARE index1 INTEGER DEFAULT 1;
	IF _contractId = "" THEN
		SET _contractId = "%";
    END IF;

    set @numOfRecords = LENGTH(_coreCustomerId) - LENGTH(REPLACE(_coreCustomerId, ',', '')) + 1;
    set @concatstring = concat ("\"",SUBSTRING_INDEX(SUBSTRING_INDEX(_coreCustomerId, ',', index1), ',', -1 ),"\"");
        customerId :  LOOP
        set index1 = index1 + 1;
        IF index1 = @numOfRecords + 1 THEN
            LEAVE customerId;
        else
            set @customerId = (SUBSTRING_INDEX(SUBSTRING_INDEX(_coreCustomerId, ',', index1), ',', -1 ));
            set @concatstring = Concat(@concatstring, ",\"",@customerId,"\"");
        END IF;
    END LOOP customerId;
	set @execStmt=concat ('select sg.signatoryGroupId, sg.signatoryGroupName, sg.signatoryGroupDescription, cc.coreCustomerId, cc.coreCustomerName,
	c.id contractId, c.name contractName, sg.createdby, sg.createdts, sg.lastmodifiedts, sg.softdeleteflag ,
   ( select count(*) from customersignatorygroup cs1 where cs1.signatoryGroupId=sg.signatoryGroupId  group by cs1.signatoryGroupId ) as noOfUsers
	from contract c 
	left join contractcorecustomers cc on cc.contractId=c.id 
	left join signatorygroup sg  on c.id=sg.contractId and sg.coreCustomerId=cc.coreCustomerId
	where c.id LIKE "',_contractId,'"');
	
    IF _coreCustomerId != "" THEN
		SET  @execStmt =  concat (@execStmt, ' and cc.coreCustomerId IN (', @concatstring,' )');
    END IF;
	
    PREPARE stmt FROM @execStmt;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_signatorygroup_customer_details_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_signatorygroup_customer_details_proc`(
in _signatoryGroupId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
SELECT 
	`customer`.`id` AS `userId`,
    `customer`.`isCombinedUser` AS `isCombinedUser`,
	`customer`.`UserName` AS `userName`,
    `customer`.`FirstName` AS `firstName`,
	`customer`.`LastName` AS `lastName`,
    `membergroup`.`Name` AS `role`,
	`customerimage`.`UserImage` AS `userImage`
from 
	`customersignatorygroup`
LEFT JOIN `customer` ON (`customer`.`id` = `customersignatorygroup`.`customerId`)
LEFT JOIN `signatorygroup` ON (`signatorygroup`.`signatoryGroupId` =  `customersignatorygroup`.`signatoryGroupId`)
LEFT JOIN `customergroup` ON (`customergroup`.`Customer_id` = `customersignatorygroup`.`customerId` AND `customergroup`.`coreCustomerId` = `signatorygroup`.`coreCustomerId`)
LEFT JOIN `membergroup` ON (`membergroup`.`id` = `customergroup`.`Group_id`)
LEFT JOIN `customerimage` ON (`customerimage`.`Customer_id` = `customer`.`id`)
	WHERE 
        `customersignatorygroup`.`signatoryGroupId` = _signatoryGroupId ;           
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_signatorygroup_details_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_signatorygroup_details_proc`(
  IN _signatoryGroupId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	select signatorygroup.signatoryGroupId as `signatoryGroupId`,
	signatorygroup.signatoryGroupName as `signatoryGroupName`,
	signatorygroup.signatoryGroupDescription as `signatoryGroupDescription`,
	signatorygroup.coreCustomerId as `coreCustomerId`,
	contractcorecustomers.coreCustomerName as `coreCustomerName`,
	signatorygroup.createdts as `createdts`,
	signatorygroup.createdby as `createdby`,
	signatorygroup.lastmodifiedts as `lastmodifiedts`,
	customersignatorygroup.customerSignatoryGroupId as `customerSignatoryGroupId`,
	customersignatorygroup.customerId as `customerId`,
	customer.UserName AS `userName`,
     CONCAT(customer.FirstName,
                ' ',
                IFNULL(customer.MiddleName, ''),
                ' ',
                IFNULL(customer.LastName, '')) AS `fullName`,
	membergroup.Name as `customerRole`,
	customersignatorygroup.createdts as `signatoryaddedts`
	  from signatorygroup  
	  left join customersignatorygroup  on signatorygroup.signatoryGroupId=customersignatorygroup.signatoryGroupId
	  left join contractcorecustomers  on contractcorecustomers.coreCustomerId=signatorygroup.coreCustomerId 
	  left join customer on customer.id = customersignatorygroup.customerId
	  left join customergroup on customergroup.Customer_id = customersignatorygroup.customerId and customergroup.coreCustomerId = signatorygroup.coreCustomerId
	  left join membergroup on membergroup.id = customergroup.Group_id
	  where signatorygroup.signatoryGroupId=_signatoryGroupId;  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_unselectedPayees_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_unselectedPayees_proc`(
	IN `_bulkwiretemplateID` VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `User_Id` VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `sortByParam` VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `sortOrder` VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `searchString` VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `isDomesticPermitted` INT,
	IN `isInternationalPermitted` INT
)
proc_label:BEGIN

	SET sortByParam = if(sortByParam = "" OR sortByParam = NULL, 'nickName', sortByParam);
	SET sortOrder = if(sortOrder = "" OR sortOrder = NULL, 'DESC', sortOrder);
	SET @searchAndOrderBy = CONCAT(" ORDER BY ", sortByParam , " ", sortOrder);

	IF searchString != "" OR searchString IS NOT NULL THEN
		SET searchString = CONCAT("'%",searchString,"%'");
		SET @searchQuery = CONCAT(" AND (`name` LIKE ",searchString," OR `bankName` LIKE ",searchString," OR `wireAccountType` LIKE ",searchString," OR `firstName` LIKE ",searchString," OR `lastName` LIKE ",searchString," OR `nickName` LIKE ",searchString,")");
		SET @searchAndOrderBy = CONCAT(@searchQuery, " ", @searchAndOrderBy);
	END IF;
	
	SET @permissionQuery = "";
	
	IF isDomesticPermitted = 0 AND isInternationalPermitted = 1 THEN
		SET @permissionQuery = " AND wireAccountType = 'International' ";
	ELSEIF isDomesticPermitted = 1 AND isInternationalPermitted = 0 THEN
		SET @permissionQuery = " AND wireAccountType = 'Domestic' ";
	ELSEIF isDomesticPermitted = 1 AND isInternationalPermitted = 1 THEN
		SET @permissionQuery = " AND wireAccountType IN ('Domestic', 'International') ";
	ELSE
		SELECT "";
		LEAVE proc_label;
	END IF;
	
	SET @query1 = CONCAT("SELECT * FROM payee WHERE isWiredRecepient = 1 AND User_Id = '",User_Id,"' AND Id NOT IN (SELECT payeeId FROM bulkwiretemplatelineitems WHERE bulkWireTemplateID = '",_bulkwiretemplateID,"' AND templateRecipientCategory = 'EXISTINGRECIPIENT' AND softdeleteflag = 0) ",@permissionQuery, @searchAndOrderBy);
	PREPARE sql_query FROM @query1;
	EXECUTE sql_query;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_user_corecustomer_actions` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_user_corecustomer_actions`(
	IN _userId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _coreCustomerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
     DECLARE _serviceDefinitionId varchar(255) DEFAULT ""  ;
     DECLARE _roleId varchar(255) DEFAULT ""  ;

    SET _serviceDefinitionId = (select GROUP_CONCAT(contract.servicedefinitionId SEPARATOR ",") from contract 
								LEFT JOIN  contractcorecustomers on (contractcorecustomers.contractId = contract.id ) 
                                where contractcorecustomers.coreCustomerId = _coreCustomerId);
	SET _roleId = (select GROUP_CONCAT(customergroup.Group_id SEPARATOR ",") from customergroup where customergroup.Customer_id = _userId
					and customergroup.coreCustomerId = _coreCustomerId );

	SET @action_select_statement = ("(SELECT DISTINCT featureaction.id AS actionId FROM featureaction LEFT JOIN feature ON ( feature.id = featureaction.Feature_id ))");
	IF(_serviceDefinitionId != '') THEN 
		SET @select_statement = CONCAT("(SELECT DISTINCT servicedefinitionactionlimit.actionId AS actionId FROM servicedefinitionactionlimit WHERE servicedefinitionactionlimit.serviceDefinitionId = " , QUOTE(_serviceDefinitionId));
		SET @select_statement = CONCAT(@select_statement , " AND servicedefinitionactionlimit.actionId IN" , @action_select_statement);
        SET @select_statement = CONCAT(@select_statement , ")");
        SET @action_select_statement = @select_statement;
    END IF;	
    IF(_roleId != '') THEN
		SET @select_statement = CONCAT("(SELECT DISTINCT groupactionlimit.Action_id AS actionId FROM groupactionlimit WHERE groupactionlimit.Group_id = ",QUOTE(_roleId));
		SET @select_statement = CONCAT(@select_statement , " AND groupactionlimit.Action_id IN ");
		SET @select_statement = CONCAT(@select_statement , @action_select_statement);
        SET @select_statement = CONCAT(@select_statement , ")");
        SET @action_select_statement = @select_statement;
    END IF;
    IF(_coreCustomerId != '') THEN 
		SET @select_statement = CONCAT("(SELECT DISTINCT contractactionlimit.actionId AS actionId FROM contractactionlimit WHERE contractactionlimit.coreCustomerId = ",QUOTE(_coreCustomerId));
        SET @select_statement = CONCAT(@select_statement , " AND contractactionlimit.actionId IN " , @action_select_statement);
        SET @select_statement = CONCAT(@select_statement , ")");
        SET @action_select_statement = @select_statement;
    END IF;
	IF(_userId != '') THEN 
		SET @select_statement = CONCAT("(SELECT DISTINCT customeraction.Action_id AS actionId FROM customeraction WHERE customeraction.Customer_id = ",QUOTE(_userId));
        IF(_coreCustomerId != '') THEN 
			SET @select_statement = CONCAT(@select_statement , " AND customeraction.coreCustomerId = " , QUOTE(_coreCustomerId));
        END IF;
        SET @select_statement = CONCAT(@select_statement , " AND customeraction.isAllowed = '1' AND customeraction.Action_id IN " , @action_select_statement);
        SET @select_statement = CONCAT(@select_statement , ")");
        SET @action_select_statement = @select_statement;
	END IF;
   PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fetch_wiretransfer_details_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `fetch_wiretransfer_details_proc`(
in _transactionId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _wireFileExecution_id varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	SET @companyId = (select group_concat(concat(contractId,"_",coreCustomerId) SEPARATOR ",") from contractcustomers where customerId =_customerId);
	
	IF _transactionId is NULL THEN
		SET _transactionId = "";
	END IF;
    
    IF _wireFileExecution_id is NULL THEN
		SET _wireFileExecution_id = "";
	END IF;
	
	IF @companyId is NULL THEN   
		SET @companyId = "";
	END IF;
     
	SET @filter = if(_transactionId != "", concat("transactionId = ", _transactionId , " ",@companyId,"')" ), 
					if( _wireFileExecution_id != "", concat("wireFileExecution_id = ", _wireFileExecution_id , " AND FIND_IN_SET(`companyId`, '",@companyId,"')"), '')) ;
                    
	set @select_statement = concat("SELECT 
		wiretransfers.transactionId,
		wiretransfers.featureActionId,
        wiretransfers.confirmationNumber,
        wiretransfers.companyId,
        wiretransfers.createdby,
        wiretransfers.requestId,
        wiretransfers.status,
        wiretransfers.onetime_id,
        wiretransfers.wireFileExecution_id,
        wiretransfers.notes,
        wiretransfers.amount,
        wiretransfers.fromAccountNumber,
        wiretransfers.payeeAccountNumber,
        wiretransfers.transactionType,
        wiretransfers.payeeId,
        wiretransfers.payeeCurrency,
        onetimepayee.payeeName,
		onetimepayee.payeeNickName,
		onetimepayee.payeeType,
		onetimepayee.wireAccountType,
		onetimepayee.swiftCode,
		onetimepayee.routingNumber,
		onetimepayee.zipCode,
		onetimepayee.cityName,
		onetimepayee.state,
		onetimepayee.country,
		onetimepayee.payeeAddressLine1,
		onetimepayee.payeeAddressLine2,
		onetimepayee.bankName,
		onetimepayee.internationalRoutingCode,
		onetimepayee.bankAddressLine1,
		onetimepayee.bankAddressLine2,
		onetimepayee.bankCity,
		onetimepayee.bankState,
		onetimepayee.bankZip
        
    FROM
        (`wiretransfers`
		LEFT JOIN `onetimepayee` ON (`wiretransfers`.`onetime_id` = `onetimepayee`.`onetime_id`))
		WHERE ", @filter);
        
	-- select @select_statement;
	PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
     
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `forex_proc_get` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `forex_proc_get`(IN read_query varchar(20000))
BEGIN
SET @stmt := CONCAT('', read_query);
PREPARE stmt FROM @stmt;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAccountNicknamesByAccountIds` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `getAccountNicknamesByAccountIds`(
	IN `_accountIds` TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `_customerid` TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
SELECT Account_id,NickName FROM customeraccounts WHERE Customer_id=_customerid and FIND_IN_SET(Account_id, _accountIds) COLLATE utf8_general_ci;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAllBackendIdentifiers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `getAllBackendIdentifiers`()
BEGIN
	select `Customer_id` as `customerId`,
			`BackendId` as `backendId`
            from 
			`backendidentifier`;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAllCustomersAlertFrequency_Sp_alertlevel` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `getAllCustomersAlertFrequency_Sp_alertlevel`(startTimePrev varchar(50) CHARACTER SET UTF8, startTimeCurr Varchar(50) CHARACTER SET UTF8, 
endTimePrev Varchar(50) CHARACTER SET UTF8, endTimeCurr Varchar(50) CHARACTER SET UTF8,scheduleDayPrev Varchar(50) CHARACTER SET UTF8,scheduleDayCurr Varchar(50) CHARACTER SET UTF8, 
scheduleDatePrev INT(50) , scheduleDateCurr INT(50) ,isLastDate Varchar(50) CHARACTER SET UTF8,isPrevDateLastDate Varchar(50) CHARACTER SET UTF8)
BEGIN
 DECLARE startTimePrev  varchar(50) CHARACTER SET UTF8 DEFAULT startTimePrev;
 DECLARE startTimeCurr  varchar(50) CHARACTER SET UTF8 DEFAULT startTimeCurr;
 DECLARE endTimePrev varchar(50) CHARACTER SET UTF8 DEFAULT endTimePrev;
 DECLARE endTimeCurr varchar(50) CHARACTER SET UTF8 DEFAULT endTimeCurr;
 DECLARE scheduleDayPrev varchar(50) CHARACTER SET UTF8 DEFAULT scheduleDayPrev;
 DECLARE scheduleDayCurr varchar(50) CHARACTER SET UTF8 DEFAULT scheduleDayCurr;
 DECLARE scheduleDatePrev INT(50) DEFAULT scheduleDatePrev;
 DECLARE scheduleDateCurr INT(50) DEFAULT scheduleDateCurr;
 DECLARE isLastDate Varchar(50) CHARACTER SET UTF8 DEFAULT isLastDate;
 DECLARE isPrevDateLastDate Varchar(50) CHARACTER SET UTF8 DEFAULT isPrevDateLastDate;
 if(isLastDate = 'true') then
 if(isPrevDateLastDate = 'true' ) then
select 
*  
from 
customeralertfrequency 
where 
(customeralertfrequency.frequencyTime>startTimePrev and customeralertfrequency.frequencyTime<=endTimePrev and((customeralertfrequency.frequencyValue=scheduleDayPrev and customeralertfrequency.alertFrequencyId = 'WEEKLY') or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue >=scheduleDatePrev and customeralertfrequency.alertFrequencyId ='MONTHLY')))
 or (customeralertfrequency.frequencyTime>=startTimeCurr and  customeralertfrequency.frequencyTime<=endTimeCurr and((customeralertfrequency.frequencyValue=scheduleDayCurr and customeralertfrequency.alertFrequencyId = 'WEEKLY')  or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue = scheduleDateCurr and customeralertfrequency.alertFrequencyId ='MONTHLY')));
 else
 select 
 *  
 from 
 customeralertfrequency 
 where 
 (customeralertfrequency.frequencyTime>startTimePrev and  customeralertfrequency.frequencyTime<=endTimePrev and((customeralertfrequency.frequencyValue=scheduleDayPrev and customeralertfrequency.alertFrequencyId = 'WEEKLY') or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue =scheduleDatePrev and customeralertfrequency.alertFrequencyId ='MONTHLY'))) 
 or  (customeralertfrequency.frequencyTime>=startTimeCurr and  customeralertfrequency.frequencyTime<=endTimeCurr and((customeralertfrequency.frequencyValue=scheduleDayCurr and customeralertfrequency.alertFrequencyId = 'WEEKLY')  or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue>=scheduleDateCurr and customeralertfrequency.alertFrequencyId ='MONTHLY')));
 end if;
 else
 select 
 *  
 from 
 customeralertfrequency 
 where 
 (customeralertfrequency.frequencyTime>startTimePrev and  customeralertfrequency.frequencyTime<=endTimePrev and((customeralertfrequency.frequencyValue=scheduleDayPrev and customeralertfrequency.alertFrequencyId = 'WEEKLY') or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue = scheduleDatePrev and customeralertfrequency.alertFrequencyId ='MONTHLY'))) 
 or  (customeralertfrequency.frequencyTime>=startTimeCurr and  customeralertfrequency.frequencyTime<=endTimeCurr and((customeralertfrequency.frequencyValue=scheduleDayCurr and customeralertfrequency.alertFrequencyId = 'WEEKLY')  or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue = scheduleDateCurr and customeralertfrequency.alertFrequencyId ='MONTHLY')));
 end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAllCustomersAlertFrequency_Sp_categorylevel` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `getAllCustomersAlertFrequency_Sp_categorylevel`(startTimePrev varchar(50) CHARACTER SET UTF8, startTimeCurr Varchar(50) CHARACTER SET UTF8, 
endTimePrev Varchar(50) CHARACTER SET UTF8, endTimeCurr Varchar(50) CHARACTER SET UTF8,scheduleDayPrev Varchar(50) CHARACTER SET UTF8,scheduleDayCurr Varchar(50) CHARACTER SET UTF8, 
scheduleDatePrev INT(50) , scheduleDateCurr INT(50) ,isLastDate Varchar(50) CHARACTER SET UTF8,isPrevDateLastDate Varchar(50) CHARACTER SET UTF8)
BEGIN
 DECLARE startTimePrev  varchar(50) CHARACTER SET UTF8 DEFAULT startTimePrev;
 DECLARE startTimeCurr  varchar(50) CHARACTER SET UTF8 DEFAULT startTimeCurr;
 DECLARE endTimePrev varchar(50) CHARACTER SET UTF8 DEFAULT endTimePrev;
 DECLARE endTimeCurr varchar(50) CHARACTER SET UTF8 DEFAULT endTimeCurr;
 DECLARE scheduleDayPrev varchar(50) CHARACTER SET UTF8 DEFAULT scheduleDayPrev;
 DECLARE scheduleDayCurr varchar(50) CHARACTER SET UTF8 DEFAULT scheduleDayCurr;
 DECLARE scheduleDatePrev INT(50) DEFAULT scheduleDatePrev;
 DECLARE scheduleDateCurr INT(50) DEFAULT scheduleDateCurr;
 DECLARE isLastDate Varchar(50) CHARACTER SET UTF8 DEFAULT isLastDate;
 DECLARE isPrevDateLastDate Varchar(50) CHARACTER SET UTF8 DEFAULT isPrevDateLastDate;
 if(isLastDate = 'true') then
 if(isPrevDateLastDate = 'true' ) then
select 
 customeralertfrequency.customerId,
 customeralertfrequency.alertCategoryId,
 dbxalerttype.id as alertTypeId,
 customeralertfrequency.accountId,
 alertsubtype.id as alertSubTypeId
 from customeralertfrequency 
 join dbxalerttype on customeralertfrequency.alertCategoryId= dbxalerttype.AlertCategoryId
 join alertsubtype on dbxalerttype.id = alertsubtype.AlertTypeId 
where 
((customeralertfrequency.frequencyTime>startTimePrev and customeralertfrequency.frequencyTime<=endTimePrev and((customeralertfrequency.frequencyValue=scheduleDayPrev and customeralertfrequency.alertFrequencyId = 'WEEKLY') or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue >=scheduleDatePrev and customeralertfrequency.alertFrequencyId ='MONTHLY')))  or (customeralertfrequency.frequencyTime>=startTimeCurr and  customeralertfrequency.frequencyTime<=endTimeCurr and((customeralertfrequency.frequencyValue=scheduleDayCurr and customeralertfrequency.alertFrequencyId = 'WEEKLY')  or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue = scheduleDateCurr and customeralertfrequency.alertFrequencyId ='MONTHLY'))))
and alertsubtype.defaultFrequencyId is not null;
 else
 select 
 customeralertfrequency.customerId,
 customeralertfrequency.alertCategoryId,
 dbxalerttype.id as alertTypeId,
 customeralertfrequency.accountId,
 alertsubtype.id as alertSubTypeId
 from customeralertfrequency 
 join dbxalerttype on customeralertfrequency.alertCategoryId= dbxalerttype.AlertCategoryId
 join alertsubtype on dbxalerttype.id = alertsubtype.AlertTypeId 
 where 
 ((customeralertfrequency.frequencyTime>startTimePrev and  customeralertfrequency.frequencyTime<=endTimePrev and((customeralertfrequency.frequencyValue=scheduleDayPrev and customeralertfrequency.alertFrequencyId = 'WEEKLY') or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue =scheduleDatePrev and customeralertfrequency.alertFrequencyId ='MONTHLY'))) or  (customeralertfrequency.frequencyTime>=startTimeCurr and  customeralertfrequency.frequencyTime<=endTimeCurr and((customeralertfrequency.frequencyValue=scheduleDayCurr and customeralertfrequency.alertFrequencyId = 'WEEKLY')  or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue>=scheduleDateCurr and customeralertfrequency.alertFrequencyId ='MONTHLY'))))
 and alertsubtype.defaultFrequencyId is not null;
 end if;
 else
 select 
customeralertfrequency.customerId,
 customeralertfrequency.alertCategoryId,
 dbxalerttype.id as alertTypeId,
 customeralertfrequency.accountId,
 alertsubtype.id as alertSubTypeId
 from customeralertfrequency 
 join dbxalerttype on customeralertfrequency.alertCategoryId= dbxalerttype.AlertCategoryId
 join alertsubtype on dbxalerttype.id = alertsubtype.AlertTypeId 
 where 
 ((customeralertfrequency.frequencyTime>startTimePrev and  customeralertfrequency.frequencyTime<=endTimePrev and((customeralertfrequency.frequencyValue=scheduleDayPrev and customeralertfrequency.alertFrequencyId = 'WEEKLY') or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue = scheduleDatePrev and customeralertfrequency.alertFrequencyId ='MONTHLY')))  or  (customeralertfrequency.frequencyTime>=startTimeCurr and  customeralertfrequency.frequencyTime<=endTimeCurr and((customeralertfrequency.frequencyValue=scheduleDayCurr and customeralertfrequency.alertFrequencyId = 'WEEKLY')  or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue = scheduleDateCurr and customeralertfrequency.alertFrequencyId ='MONTHLY'))))
 and alertsubtype.defaultFrequencyId is not null;
 end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAllCustomersAlertFrequency_Sp_grouplevel` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `getAllCustomersAlertFrequency_Sp_grouplevel`(startTimePrev varchar(50) CHARACTER SET UTF8, startTimeCurr Varchar(50) CHARACTER SET UTF8, 
endTimePrev Varchar(50) CHARACTER SET UTF8, endTimeCurr Varchar(50) CHARACTER SET UTF8,scheduleDayPrev Varchar(50) CHARACTER SET UTF8,scheduleDayCurr Varchar(50) CHARACTER SET UTF8, 
scheduleDatePrev INT(50) , scheduleDateCurr INT(50) ,isLastDate Varchar(50) CHARACTER SET UTF8,isPrevDateLastDate Varchar(50) CHARACTER SET UTF8)
BEGIN
 DECLARE startTimePrev  varchar(50) CHARACTER SET UTF8 DEFAULT startTimePrev;
 DECLARE startTimeCurr  varchar(50) CHARACTER SET UTF8 DEFAULT startTimeCurr;
 DECLARE endTimePrev varchar(50) CHARACTER SET UTF8 DEFAULT endTimePrev;
 DECLARE endTimeCurr varchar(50) CHARACTER SET UTF8 DEFAULT endTimeCurr;
 DECLARE scheduleDayPrev varchar(50) CHARACTER SET UTF8 DEFAULT scheduleDayPrev;
 DECLARE scheduleDayCurr varchar(50) CHARACTER SET UTF8 DEFAULT scheduleDayCurr;
 DECLARE scheduleDatePrev INT(50) DEFAULT scheduleDatePrev;
 DECLARE scheduleDateCurr INT(50) DEFAULT scheduleDateCurr;
 DECLARE isLastDate Varchar(50) CHARACTER SET UTF8 DEFAULT isLastDate;
 DECLARE isPrevDateLastDate Varchar(50) CHARACTER SET UTF8 DEFAULT isPrevDateLastDate;
 if(isLastDate = 'true') then
 if(isPrevDateLastDate = 'true' ) then
select 
customeralertfrequency.customerId,
customeralertfrequency.alertCategoryId,
customeralertfrequency.alertTypeId, 
customeralertfrequency.accountId,
alertsubtype.id as alertSubTypeId 
from customeralertfrequency
join alertsubtype on customeralertfrequency.alertTypeId = alertsubtype.AlertTypeId 
where 
((customeralertfrequency.frequencyTime>startTimePrev and customeralertfrequency.frequencyTime<=endTimePrev and((customeralertfrequency.frequencyValue=scheduleDayPrev and customeralertfrequency.alertFrequencyId = 'WEEKLY') or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue >=scheduleDatePrev and customeralertfrequency.alertFrequencyId ='MONTHLY')))  or (customeralertfrequency.frequencyTime>=startTimeCurr and  customeralertfrequency.frequencyTime<=endTimeCurr and((customeralertfrequency.frequencyValue=scheduleDayCurr and customeralertfrequency.alertFrequencyId = 'WEEKLY')  or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue = scheduleDateCurr and customeralertfrequency.alertFrequencyId ='MONTHLY'))))
and alertsubtype.defaultFrequencyId is not null;
 else
 select 
customeralertfrequency.customerId,
customeralertfrequency.alertCategoryId,
customeralertfrequency.alertTypeId, 
customeralertfrequency.accountId,
alertsubtype.id as alertSubTypeId 
from customeralertfrequency
join alertsubtype on customeralertfrequency.alertTypeId = alertsubtype.AlertTypeId 
 where 
 ((customeralertfrequency.frequencyTime>startTimePrev and  customeralertfrequency.frequencyTime<=endTimePrev and((customeralertfrequency.frequencyValue=scheduleDayPrev and customeralertfrequency.alertFrequencyId = 'WEEKLY') or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue =scheduleDatePrev and customeralertfrequency.alertFrequencyId ='MONTHLY'))) or  (customeralertfrequency.frequencyTime>=startTimeCurr and  customeralertfrequency.frequencyTime<=endTimeCurr and((customeralertfrequency.frequencyValue=scheduleDayCurr and customeralertfrequency.alertFrequencyId = 'WEEKLY')  or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue>=scheduleDateCurr and customeralertfrequency.alertFrequencyId ='MONTHLY'))))
 and alertsubtype.defaultFrequencyId is not null;
 end if;
 else
 select 
customeralertfrequency.customerId,
customeralertfrequency.alertCategoryId,
customeralertfrequency.alertTypeId, 
customeralertfrequency.accountId,
alertsubtype.id as alertSubTypeId 
from customeralertfrequency
join alertsubtype on customeralertfrequency.alertTypeId = alertsubtype.AlertTypeId 
where 
 ((customeralertfrequency.frequencyTime>startTimePrev and  customeralertfrequency.frequencyTime<=endTimePrev and((customeralertfrequency.frequencyValue=scheduleDayPrev and customeralertfrequency.alertFrequencyId = 'WEEKLY') or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue = scheduleDatePrev and customeralertfrequency.alertFrequencyId ='MONTHLY')))  or  (customeralertfrequency.frequencyTime>=startTimeCurr and  customeralertfrequency.frequencyTime<=endTimeCurr and((customeralertfrequency.frequencyValue=scheduleDayCurr and customeralertfrequency.alertFrequencyId = 'WEEKLY')  or  customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue = scheduleDateCurr and customeralertfrequency.alertFrequencyId ='MONTHLY'))))
 and alertsubtype.defaultFrequencyId is not null;
 end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCoreIdForCustomerIds_Sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `getCoreIdForCustomerIds_Sp`( customerIds TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,backendType TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
 DECLARE customerIds  TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT customerIds;
 DECLARE backendType  TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT backendType;
 if(backendType = 'null')
 then 
  SELECT * FROM backendidentifier where FIND_IN_SET (`backendidentifier`.`Customer_id`,customerIds) and backendidentifier.backendType is NULL;
  else
    SELECT * FROM backendidentifier where FIND_IN_SET (`backendidentifier`.`Customer_id`,customerIds) and FIND_IN_SET(`backendidentifier`.`BackendType`,backendType);
end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCustomerCommunicationData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `getCustomerCommunicationData`(_customers TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
SELECT Customer_id as custid ,Value as value,Type_id as type FROM customercommunication  where isPrimary = 1 
   and  FIND_IN_SET(`customercommunication`.`Customer_id`,_customers) ; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCustomerInfo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `getCustomerInfo`(_customers TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
SELECT id as custid ,FirstName as fname,LastName as lname  , CountryCode as country FROM customer  where FIND_IN_SET(`customer`.`id`,_customers) ; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCustomersAlertFrequency_Sp_alertlevel` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `getCustomersAlertFrequency_Sp_alertlevel`(startTime varchar(50) CHARACTER SET UTF8, endTime Varchar(50) CHARACTER SET UTF8 , scheduleDay varchar(50) CHARACTER SET UTF8 ,scheduleDate INT(50), isLastDate VARCHAR(50) CHARACTER SET UTF8)
BEGIN
 DECLARE startTime  varchar(50) CHARACTER SET UTF8 DEFAULT startTime;
 DECLARE endTime varchar(50) CHARACTER SET UTF8 DEFAULT endTime;
 DECLARE scheduleDay varchar(50) CHARACTER SET UTF8 DEFAULT scheduleDay;
 DECLARE scheduleDate INT(50) DEFAULT scheduleDate;
 DECLARE isLastDate VARCHAR(50) CHARACTER SET UTF8 DEFAULT isLastDate;
 if (isLastDate = 'true')
 then
 select 
 *  
 from customeralertfrequency 
 where 
 customeralertfrequency.frequencyTime>startTime 
 and customeralertfrequency.frequencyTime<=endTime 
 and((customeralertfrequency.frequencyValue=scheduleDay and customeralertfrequency.alertFrequencyId = 'WEEKLY') or customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue >= scheduleDate and customeralertfrequency.alertFrequencyId ='MONTHLY'));
 else
  select
  *  
  from 
  customeralertfrequency 
  where 
  customeralertfrequency.frequencyTime>startTime 
  and customeralertfrequency.frequencyTime<=endTime 
  and((customeralertfrequency.frequencyValue=scheduleDay and customeralertfrequency.alertFrequencyId = 'WEEKLY') or customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue = scheduleDate and customeralertfrequency.alertFrequencyId ='MONTHLY'));
 end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCustomersAlertFrequency_Sp_categorylevel` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `getCustomersAlertFrequency_Sp_categorylevel`(startTime varchar(50) CHARACTER SET UTF8, endTime Varchar(50) CHARACTER SET UTF8 , scheduleDay varchar(50) CHARACTER SET UTF8 ,scheduleDate INT(50), isLastDate VARCHAR(50) CHARACTER SET UTF8)
BEGIN
 DECLARE startTime  varchar(50) CHARACTER SET UTF8 DEFAULT startTime;
 DECLARE endTime varchar(50) CHARACTER SET UTF8 DEFAULT endTime;
 DECLARE scheduleDay varchar(50) CHARACTER SET UTF8 DEFAULT scheduleDay;
 DECLARE scheduleDate INT(50) DEFAULT scheduleDate;
 DECLARE isLastDate VARCHAR(50) CHARACTER SET UTF8 DEFAULT isLastDate;
 if (isLastDate = 'true')
 then
 select 
 customeralertfrequency.customerId,
 customeralertfrequency.alertCategoryId,
 dbxalerttype.id as alertTypeId,
 customeralertfrequency.accountId,
 alertsubtype.id as alertSubTypeId
 from customeralertfrequency 
 join dbxalerttype on customeralertfrequency.alertCategoryId= dbxalerttype.AlertCategoryId
 join alertsubtype on dbxalerttype.id = alertsubtype.AlertTypeId 
 where 
 customeralertfrequency.frequencyTime>startTime 
 and  customeralertfrequency.frequencyTime<=endTime 
 and ((customeralertfrequency.frequencyValue=scheduleDay and customeralertfrequency.alertFrequencyId = 'WEEKLY') or customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue >= scheduleDate and customeralertfrequency.alertFrequencyId ='MONTHLY'))
 and alertsubtype.defaultFrequencyId is not null;
 else
  select 
  customeralertfrequency.customerId,
  customeralertfrequency.alertCategoryId,
  dbxalerttype.id as alertTypeId, 
  customeralertfrequency.accountId,
  alertsubtype.id as alertSubTypeId 
  from 
  customeralertfrequency 
  join dbxalerttype on customeralertfrequency.alertCategoryId = dbxalerttype.AlertCategoryId 
  join alertsubtype on dbxalerttype.id = alertsubtype.AlertTypeId 
  where 
  customeralertfrequency.frequencyTime>startTime 
  and customeralertfrequency.frequencyTime<=endTime 
  and((customeralertfrequency.frequencyValue=scheduleDay and customeralertfrequency.alertFrequencyId = 'WEEKLY') or customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue = scheduleDate and customeralertfrequency.alertFrequencyId ='MONTHLY'))
  and alertsubtype.defaultFrequencyId is not null ;
 end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCustomersAlertFrequency_Sp_grouplevel` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `getCustomersAlertFrequency_Sp_grouplevel`(startTime varchar(50) CHARACTER SET UTF8, endTime Varchar(50) CHARACTER SET UTF8 , scheduleDay varchar(50) CHARACTER SET UTF8 ,scheduleDate INT(50), isLastDate VARCHAR(50) CHARACTER SET UTF8)
BEGIN
 DECLARE startTime  varchar(50) CHARACTER SET UTF8 DEFAULT startTime;
 DECLARE endTime varchar(50) CHARACTER SET UTF8 DEFAULT endTime;
 DECLARE scheduleDay varchar(50) CHARACTER SET UTF8 DEFAULT scheduleDay;
 DECLARE scheduleDate INT(50) DEFAULT scheduleDate;
 DECLARE isLastDate VARCHAR(50) CHARACTER SET UTF8 DEFAULT isLastDate;
 if (isLastDate = 'true')
 then
 select 
 customeralertfrequency.customerId,
 customeralertfrequency.alertCategoryId,
 customeralertfrequency.alertTypeId, 
 customeralertfrequency.accountId,
 alertsubtype.id as alertSubTypeId 
 from customeralertfrequency
 join alertsubtype on customeralertfrequency.alertTypeId = alertsubtype.AlertTypeId 
 where 
 customeralertfrequency.frequencyTime>startTime 
 and customeralertfrequency.frequencyTime<=endTime 
 and((customeralertfrequency.frequencyValue=scheduleDay and customeralertfrequency.alertFrequencyId = 'WEEKLY') or customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue >= scheduleDate and customeralertfrequency.alertFrequencyId ='MONTHLY'))
 and alertsubtype.defaultFrequencyId is not null;
 else
  select 
  customeralertfrequency.customerId,
  customeralertfrequency.alertCategoryId,
  customeralertfrequency.alertTypeId, 
  customeralertfrequency.accountId,
  alertsubtype.id as alertSubTypeId 
  from 
  customeralertfrequency 
  join alertsubtype on customeralertfrequency.alertTypeId = alertsubtype.AlertTypeId 
  where 
  customeralertfrequency.frequencyTime>startTime 
  and customeralertfrequency.frequencyTime<=endTime 
  and((customeralertfrequency.frequencyValue=scheduleDay and customeralertfrequency.alertFrequencyId = 'WEEKLY') or customeralertfrequency.alertFrequencyId = 'DAILY' or (customeralertfrequency.frequencyValue = scheduleDate and customeralertfrequency.alertFrequencyId ='MONTHLY'))
  and alertsubtype.defaultFrequencyId is not null;
 end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getCustomersIdFromCoreId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `getCustomersIdFromCoreId`(_corecustomers  TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
SELECT Customer_id as custid ,BackendId as corecustid FROM backendidentifier where FIND_IN_SET (`backendidentifier`.`BackendId`,_corecustomers) ; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getRequestApprovers_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `getRequestApprovers_proc`(
	IN _requestId TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _status TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

	IF _status IS NULL OR _status = '' THEN
		SET @isGroupMatrix = (select isGroupMatrix from bbrequest where requestId = _requestId);
		IF @isGroupMatrix = '1' THEN
			select _requestId AS requestId,csg.customerId AS approvers, c.FirstName, c.LastName FROM customersignatorygroup as csg 
			LEFT JOIN customer AS c ON (c.id = csg.customerId)
			where FIND_IN_SET(csg.signatoryGroupId,
			(SELECT group_concat(REPLACE(REPLACE(REPLACE(pendingGroupList,']',''),'[',''),'"',''))
				FROM signatorygrouprequestmatrix WHERE 
				signatorygrouprequestmatrix.requestId = _requestId AND signatorygrouprequestmatrix.isApproved = false)); 
        ELSE
            SELECT bb.requestId,cam.customerId AS approvers, c.FirstName, c.LastName FROM bbrequest AS bb JOIN requestapprovalmatrix AS ram JOIN customerapprovalmatrix AS cam JOIN customer AS c WHERE bb.requestId = ram.requestId AND ram.approvalMatrixId = cam.approvalMatrixId AND cam.customerId = c.id AND bb.requestId = _requestId GROUP BY approvers;
		END IF;
	ELSE
		SELECT bb.createdby AS approvers, c.FirstName, c.LastName FROM bbactedrequest AS bb JOIN customer AS c WHERE bb.createdby = c.id AND requestId = _requestId AND status = _status GROUP BY approvers;
	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_actions_with_approvefeatureaction_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_actions_with_approvefeatureaction_proc`(
IN _featureActions TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET @actionsList = (SELECT group_concat(id SEPARATOR ",") from featureaction WHERE 
                      FIND_IN_SET(id,_featureActions) AND
					(!ISNULL(featureaction.approveFeatureAction) || featureaction.approveFeatureAction != ''));

select @actionsList As actions;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_AlertTypes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_AlertTypes`( in _alertTypes text CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN

SELECT `dbxalerttype`.`id` as `id`,
		`dbxalerttype`.`AttributeId` as `attributeId`,
        `dbxalerttype`.`AlertConditionId` as `alertConditionId`,
        `dbxalerttype`.`Value1` as `value1`,
        `dbxalerttype`.`Value2` as `value2`,
        `dbxalerttype`.`IsGlobal` as `isGlobal`
        from  `dbxalerttype`
        where  FIND_IN_SET(`dbxalerttype`.`id` ,_alertTypes);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_alert_sub_types` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_alert_sub_types`(in _alertTypes text CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
select `alertsubtype`.`id` as `alertSubType`
	from `alertsubtype` 
    where FIND_IN_SET(`alertsubtype`.`AlertTypeId` ,_alertTypes);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_associated_contractaccounts_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_associated_contractaccounts_proc`(
IN _accountIdList TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

	SET SESSION group_concat_max_len = 1000000;
    
	SET @accountIdList = (SELECT group_concat(accountId SEPARATOR ',') from contractaccounts WHERE FIND_IN_SET(accountId,_accountIdList));
    SET @excludedaccountIdList = (SELECT group_concat(accountId SEPARATOR ',') from excludedcontractaccounts WHERE FIND_IN_SET(accountId COLLATE utf8_general_ci,_accountIdList));

	select @accountIdList As accountIdList;
    select @excludedaccountIdList As excludedaccountIdList;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_backendidentifiers_for_customerids` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_backendidentifiers_for_customerids`(in _customerIds text CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
select `backendidentifier`.`Customer_id` as `customerId`,
	`backendidentifier`.`BackendId` as `backendId`
    from `backendidentifier`
    where FIND_IN_SET(`backendidentifier`.`Customer_id`, _customerIds);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_customerAlertEntitlementForAlertTypes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_customerAlertEntitlementForAlertTypes`(in _alertTypes text CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN

select `dbxcustomeralertentitlement`.`AlertTypeId` as `alertTypeId`,
		`dbxcustomeralertentitlement`.`Customer_id` as `customerId`,
        `dbxcustomeralertentitlement`.`value1` as `value1`,
        `dbxcustomeralertentitlement`.`value2` as `value2`,
        `dbxcustomeralertentitlement`.`AccountId` as `accountId`
        from `dbxcustomeralertentitlement`
        where FIND_IN_SET(`dbxcustomeralertentitlement`.`AlertTypeId` ,_alertTypes);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_customerids_for_backendidentifiers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_customerids_for_backendidentifiers`(in _backendIds text CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
select `backendidentifier`.`Customer_id` as `customerId`,
	`backendidentifier`.`BackendId` as `backendId`
    from `backendidentifier`
    where FIND_IN_SET(`backendidentifier`.`BackendId`, _backendIds);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_customer_applicantinfo_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_customer_applicantinfo_proc`(IN `Customer_id` varchar(20000))
BEGIN
Select `customer`.`id` AS Customer_id,
`customer`.`FirstName` AS FirstName,
`customer`.`MiddleName` AS MiddleName,
`customer`.`LastName` AS LastName,
`customer`.`DateOfBirth` AS DateOfBirth,
`customer`.`Ssn` AS Ssn,
`customer`.`PreferredContactMethod` AS PreferredContactMethod,
(Select `customercommunication`.`Value` from customercommunication where `customercommunication`.Type_id = 'COMM_TYPE_PHONE' AND `customercommunication`.`isPrimary`=1 AND BINARY `customercommunication`.`Customer_id`= BINARY `Customer_id` limit 1) as Phone,
(Select `customercommunication`.`Value` from customercommunication where `customercommunication`.Type_id = 'COMM_TYPE_EMAIL' AND `customercommunication`.`isPrimary`=1 AND BINARY `customercommunication`.`Customer_id`= BINARY `Customer_id` limit 1) as Email
FROM
	customer customer
WHERE
	BINARY customer.id=BINARY `Customer_id`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_customer_employement_details_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_customer_employement_details_proc`(IN Customer_id varchar(20000))
BEGIN
SELECT 
`employementdetails`.`Customer_id` AS Customer_id,
`employementdetails`.`EmploymentType` AS EmploymentType,
`employementdetails`.`CurrentEmployer` AS CurrentEmployer,
`employementdetails`.`Designation` AS Designation,
`employementdetails`.`PayPeriod` AS PayPeriod,
`employementdetails`.`GrossIncome` AS GrossIncome,
`employementdetails`.`WeekWorkingHours` AS WeekWorkingHours,
`employementdetails`.`EmploymentStartDate` AS EmployementStartDate,
`employementdetails`.`PreviousEmployer` AS PreviousEmployer,
`employementdetails`.`OtherEmployementType` AS OtherEmployementType,
`employementdetails`.`OtherEmployementDescription` AS OtherEmployementDescription,
`employementdetails`.`PreviousDesignation` AS PreviousDesignation,
`othersourceofincome`.`SourceType` AS OtherIncomeSourceType,
`othersourceofincome`.`PayPeriod` AS OtherIncomeSourcePayPeriod,
`othersourceofincome`.`GrossIncome` AS OtherGrossIncomeValue,
`othersourceofincome`.`WeekWorkingHours` AS OtherIncomeSourceWorkingHours,
`othersourceofincome`.`SourceOfIncomeDescription` AS OtherSourceOfIncomeDescription,
`othersourceofincome`.`SourceOfIncomeName` AS OtherSourceOfIncomeName,
`customeraddress`.`Address_id` AS Address_id,
`customeraddress`.`Type_id` AS Type_id,
`address`.`Region_id` AS Region_id,
`address`.`City_id` AS City_id,
`address`.`AddressLine1` AS AddressLine1,
`address`.`AddressLine2` AS AddressLine2,
`address`.`AddressLine3` AS AddressLine3,
`address`.`zipCode` AS ZipCode,
`address`.`state` AS State,
`address`.`cityName` AS city,
`address`.`country` AS Country
FROM employementdetails employementdetails LEFT JOIN
		othersourceofincome othersourceofincome ON employementdetails.Customer_id = othersourceofincome.Customer_id
LEFT JOIN
    customeraddress customeraddress ON (customeraddress.Customer_id = employementdetails.Customer_id AND customeraddress.Type_id="ADR_TYPE_WORK")
JOIN
    address address ON address.id=customeraddress.Address_id
WHERE
	employementdetails.Customer_id=Customer_id limit 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_entitlements_for_customerids_and_alerttypes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_entitlements_for_customerids_and_alerttypes`(in _alertTypes text CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _customerIds text CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN

select `dbxcustomeralertentitlement`.`AlertTypeId` as `alertTypeId`,
		`dbxcustomeralertentitlement`.`Customer_id` as `customerId`,
        `dbxcustomeralertentitlement`.`value1` as `value1`,
        `dbxcustomeralertentitlement`.`value2` as `value2`,
        `dbxcustomeralertentitlement`.`AccountId` as `accountId`
        from `dbxcustomeralertentitlement`
        where FIND_IN_SET(`dbxcustomeralertentitlement`.`AlertTypeId` ,_alertTypes) and 
         FIND_IN_SET(`dbxcustomeralertentitlement`.`Customer_id` ,_customerIds);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_filtered_companies_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_filtered_companies_proc`(
in _searchText varchar(60) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	select * from organisation where name like Concat('%', _searchText,'%') order by name limit 20;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_filtered_locations_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_filtered_locations_proc`(
in _searchText varchar(60) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	select * from location where DisplayName like Concat('%', _searchText,'%') order by DisplayName limit 20;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_monetary_actions_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_monetary_actions_proc`(
IN _featureActions TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET @monetaryActionList = (SELECT group_concat(id SEPARATOR ",") from featureaction WHERE (Type_id = "MONETARY" AND FIND_IN_SET(id,_featureActions)));

select @monetaryActionList As monetaryActions;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_organisation_employees_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_organisation_employees_proc`(
IN _organisationId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _filterColumnName VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _filterColumnValue VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
SET @org_employees_list = (SELECT group_concat(Customer_id SEPARATOR ",") FROM organisationemployees WHERE (Organization_id = _organisationId)); 
SET @org_employees_list = IF(@org_employees_list is null, '', @org_employees_list); 

IF _filterColumnName = "Ssn" THEN
      SET @customersList = (SELECT group_concat(id SEPARATOR ",") FROM customer where FIND_IN_SET(id,@org_employees_list) AND FIND_IN_SET(Ssn,_filterColumnValue));
      SET @customersList = IF(@customersList is null, '', @customersList); 
END IF;

SELECT @customersList AS employeesList;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_reports_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_reports_proc`(
IN p_userId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN p_roleId varchar(250) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

select * from report rp where rp.createdby=p_userId
UNION
SELECT rp.* from report rp,sharedreport sr
where sr.reportId=rp.id
and sr.userid=p_userId
union
SELECT rp.* from report rp,sharedreport sr
where sr.reportId = rp.id
and FIND_IN_SET( sr.roleId,p_roleId) order by createdts desc;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_signatorygroup_approvers_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_signatorygroup_approvers_proc`(
in _groupList text
)
BEGIN
    set @execStmt  = concat("select customerId from customersignatorygroup where FIND_IN_SET(signatoryGroupId,'" , REPLACE(REPLACE(REPLACE(_groupList, '[', ''),']',''),' ','') , "')");
    PREPARE stmt FROM @execStmt;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_validcorecustomerslist_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_validcorecustomerslist_proc`(
IN _coreCustomersCSV TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET @list = (SELECT _coreCustomersCSV);
SET @validcorecustomersCSV = '';

iterator: LOOP
  IF LENGTH(TRIM(@list)) = 0 OR @list IS NULL THEN
    LEAVE iterator;
  END IF;
    SET @next = SUBSTRING_INDEX(@list,',',1);
    SET @nextlen = LENGTH(@next);
    SET @value = TRIM(@next);
    SET @customers = (SELECT group_concat(id SEPARATOR ",") from contractcorecustomers WHERE (coreCustomerId=@value));
    IF ISNULL(@customers)>0 OR @customers="" THEN 
        IF @validcorecustomersCSV='' THEN
           SET @validcorecustomersCSV = @value;
		ELSE 
           SET @validcorecustomersCSV = CONCAT(@validcorecustomersCSV,",",@value);
	    END IF;
	END IF;
    SET @list = INSERT(@list,1,@nextlen + 1,'');
END LOOP;

select @validcorecustomersCSV As validCustomers;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_valid_customeraccounts_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_valid_customeraccounts_get_proc`(
	IN _customeraccountsCSV TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _customerId TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET @list = (SELECT _customeraccountsCSV);
SET @validcustomeraccountsCSV = '';

iterator: LOOP
  IF LENGTH(TRIM(@list)) = 0 OR @list IS NULL THEN
    LEAVE iterator;
  END IF;
    SET @next = SUBSTRING_INDEX(@list,',',1);
    SET @nextlen = LENGTH(@next);
    SET @value = TRIM(@next);
    SET @accountIdList = (SELECT GROUP_CONCAT(customeraccounts.Account_id SEPARATOR ",") from customeraccounts WHERE (Account_id=@value) and Customer_id = _customerId);
    IF ISNULL(@accountIdList)>0 OR @accountIdList="" THEN 
        IF @validcustomeraccountsCSV='' THEN
           SET @validcustomeraccountsCSV = @value;
		ELSE 
           SET @validcustomeraccountsCSV = CONCAT(@validcustomeraccountsCSV,",",@value);
	    END IF;
	END IF;
    SET @list = INSERT(@list,1,@nextlen + 1,'');
END LOOP;

select @validcustomeraccountsCSV As validAccounts;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_valid_orgaccounts_list_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `get_valid_orgaccounts_list_proc`(
IN _accountsList TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET SESSION group_concat_max_len = 4294967295;
SET @list = (SELECT _accountsList);
SET @NONDBXAccounts = '';
SET @tableAccountsList = (SELECT group_concat(Account_id SEPARATOR ",") from accounts);

iterator: LOOP
  IF LENGTH(TRIM(@list)) = 0 OR @list IS NULL THEN
    LEAVE iterator;
  END IF;
    SET @next = SUBSTRING_INDEX(@list,',',1);
    SET @nextlen = LENGTH(@next);
    SET @value = TRIM(@next);
    IF NOT find_in_set(@value,@tableAccountsList) THEN 
        IF @NONDBXAccounts='' THEN
           SET @NONDBXAccounts = @value;
		ELSE 
           SET @NONDBXAccounts = CONCAT(@NONDBXAccounts,",",@value);
	    END IF;
	END IF;
    SET @list = INSERT(@list,1,@nextlen + 1,'');
END LOOP;

SET @DBXAccounts = (SELECT group_concat(Account_id SEPARATOR ",") from accounts WHERE FIND_IN_SET(Account_id,_accountsList)
    AND (ISNULL(Organization_id)>0 OR Organization_id=""));
    
    
IF(ISNULL(@DBXAccounts) OR @DBXAccounts='' ) THEN
SELECT @NONDBXAccounts AS accountsList;
ELSEIF(ISNULL(@NONDBXAccounts) OR @NONDBXAccounts='') THEN
SELECT @DBXAccounts AS accountsList;
ELSE 
SELECT CONCAT(@DBXAccounts,",",@NONDBXAccounts) AS accountsList;
END IF;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `group_actions_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `group_actions_proc`(
in _groupId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _actionType varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _actionId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _isOnlyPremissions varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

if _isOnlyPremissions = 'true' then
	select DISTINCT `groupactionlimit`.`Action_id` AS `actionId` FROM `groupactionlimit` where `groupactionlimit`.`Group_id` = _groupId;
else
	SET @select_statement = concat("SELECT
		`groupactionlimit`.`Group_id` AS `groupId`,
		`groupactionlimit`.`LimitType_id` AS `limitTyeId`,
		`groupactionlimit`.`value` AS `value`,
		`featureaction`.`id` AS `actionId`,
		`featureaction`.`Type_id` AS `actionType`,
		`featureaction`.`name` AS `actionName`,
		`featureaction`.`description` AS `actionDescription`,	
		`feature`.`id` AS `featureId`
	FROM
		(`groupactionlimit`
		LEFT JOIN `featureaction` ON (`featureaction`.`id` = `groupactionlimit`.`Action_id`)
		LEFT JOIN `feature` ON (`feature`.`id` = `featureaction`.`Feature_id`))
		where `feature`.`Status_id` = 'SID_FEATURE_ACTIVE' and `groupactionlimit`.`Group_id`=",quote(_groupId));

		IF (_actionType != '' ) THEN
			set @select_statement =  concat(@select_statement ," and `featureaction`.`Type_id` = ",quote(_actionType));
		END IF;
		
		 IF (_actionId != '' ) THEN
			set @select_statement =  concat(@select_statement ," and `featureaction`.`id` = ",quote(_actionId));
		END IF;
        PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `group_actions_remove_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `group_actions_remove_proc`(
  IN _action varchar(255) CHARACTER SET UTF8 COLLATE utf8_general_ci,
  IN _customerIdValues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
  
)
BEGIN
  DECLARE index1 INTEGER DEFAULT 0;
	set @numOfRecords = LENGTH(_customerIdValues) - LENGTH(REPLACE(_customerIdValues, ',', '')) + 1;
  deleteRecords : LOOP 
     set index1 = index1 + 1;
		IF index1 = @numOfRecords + 1 THEN 
			LEAVE deleteRecords;
		else
		    set @customerValues = SUBSTRING_INDEX(SUBSTRING_INDEX(_customerIdValues, ',', index1), ',', -1 );
			set @customerId = SUBSTRING_INDEX(@customerValues,'.',1);
			set @coreCustomerId = SUBSTRING_INDEX(@customerValues,'.',-1);
			delete from customeraction where Customer_id = @customerId AND coreCustomerId = @coreCustomerId AND Action_id = _action;
		END IF;
		
	END LOOP deleteRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `group_limits_update_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `group_limits_update_proc`(
  IN _action varchar(255) CHARACTER SET UTF8 COLLATE utf8_general_ci,
  IN _maxTxLimit decimal(20,2),
  IN _dailyLimit decimal(20,2),
  IN _weeklyLimit decimal(20,2),
  IN _customerIdValues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
  
)
BEGIN
  DECLARE index1 INTEGER DEFAULT 0;
	set @numOfRecords = LENGTH(_customerIdValues) - LENGTH(REPLACE(_customerIdValues, ',', '')) + 1;
  updateRecords : LOOP 
     set index1 = index1 + 1;
		IF index1 = @numOfRecords + 1 THEN 
			LEAVE updateRecords;
		else
		    set @customerValues = SUBSTRING_INDEX(SUBSTRING_INDEX(_customerIdValues, ',', index1), ',', -1 );
			set @customerId = SUBSTRING_INDEX(@customerValues,'.',1);
			set @coreCustomerId = SUBSTRING_INDEX(@customerValues,'.',-1);
			UPDATE customeraction SET value = _maxTxLimit where Customer_id = @customerId AND coreCustomerId = @coreCustomerId AND Action_id = _action AND LimitType_id = 'MAX_TRANSACTION_LIMIT' AND value > _maxTxLimit; 
			UPDATE customeraction SET value = _dailyLimit where Customer_id = @customerId AND coreCustomerId = @coreCustomerId AND Action_id = _action AND LimitType_id = 'DAILY_LIMIT' AND value > _dailyLimit;
			UPDATE customeraction SET value = _weeklyLimit where Customer_id = @customerId AND coreCustomerId = @coreCustomerId AND Action_id = _action AND LimitType_id = 'WEEKLY_LIMIT' AND value > _weeklyLimit; 
		END IF;
		
	END LOOP updateRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `increment_receivedapprovals_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `increment_receivedapprovals_proc`(
IN _requestId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _approvalMatrixId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	UPDATE requestapprovalmatrix SET `receivedApprovals` = `receivedApprovals` + 1 WHERE `requestId` = _requestId AND `approvalMatrixId` = _approvalMatrixId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `infinityuser_contractdetails_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `infinityuser_contractdetails_get_proc`(
in _id varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	select
		contract.id AS contractId,
        contract.name AS contractName,
        contract.servicedefinitionId AS servicedefinitionId,
        servicedefinition.name AS serviceDefinitionName,
        membergrouptype.description AS serviceDefinitionType,
        contractcorecustomers.coreCustomerId AS coreCustomerId,
		contractcorecustomers.isPrimary AS isPrimary,
        customergroup.Group_id AS userRole,
        membergroup.name AS userRoleName,
        membergroup.Description AS userRoleDescription,
        "true" AS isAssociated
	FROM
		contract
        LEFT JOIN servicedefinition ON (servicedefinition.id = contract.servicedefinitionId)
        LEFT JOIN membergrouptype ON (membergrouptype.id = servicedefinition.serviceType)
        LEFT JOIN contractcorecustomers ON (contractcorecustomers.contractId = contract.id)
        LEFT JOIN customergroup ON ( customergroup.contractId = contract.id AND customergroup.coreCustomerId = contractcorecustomers.coreCustomerId)
        LEFT JOIN membergroup ON (membergroup.id = customergroup.Group_id)
        LEFT JOIN contractcustomers ON (contractcustomers.contractId = contractcorecustomers.contractId)
	WHERE
		customergroup.Customer_id = _id
        AND contractcustomers.customerId = _id
	UNION
    select
		contract.id AS contractId,
        contract.name AS contractName,
        contract.servicedefinitionId AS servicedefinitionId,
        servicedefinition.name AS serviceDefinitionName,
        membergrouptype.description AS serviceDefinitionType,
        contractcorecustomers.coreCustomerId AS coreCustomerId,
		contractcorecustomers.isPrimary AS isPrimary,
        NULL AS userRole,
        NULL AS userRoleName,
        NULL AS userRoleDescription,
        "false" AS isAssociated
	FROM
		contract
        LEFT JOIN servicedefinition ON (servicedefinition.id = contract.servicedefinitionId)
        LEFT JOIN membergrouptype ON (membergrouptype.id = servicedefinition.serviceType)
        LEFT JOIN contractcorecustomers ON (contractcorecustomers.contractId = contract.id)
	WHERE
		contractcorecustomers.contractId IN ( select contractcustomers.contractId FROM contractcustomers WHERE contractcustomers.customerId = _id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `infinity_customer_from_servicedefinition_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `infinity_customer_from_servicedefinition_proc`(
IN _servicedefinitionIds varchar(500) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
SET @contractIdList = (SELECT group_concat(id SEPARATOR ",") FROM contract WHERE 
						 FIND_IN_SET(servicedefinitionId, _servicedefinitionIds) );
SELECT DISTINCT customerId  FROM contractcustomers 
WHERE customerId IS NOT NULL AND FIND_IN_SET(contractId, @contractIdList) ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `internalpermissionaction_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `internalpermissionaction_delete_proc`(
in _permissionId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
DELETE FROM `permissionaction` where permissionId = _permissionId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `internal_user_access_on_given_customer_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `internal_user_access_on_given_customer_proc`(
IN _roleIds varchar(500) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _customerId  varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _customerUsername  varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

DECLARE isCustomerAccessible VARCHAR(6);
if(_customerId = '' and _customerUsername != '') THEN
	SET _customerId = (SELECT id from customer where UserName = _customerUsername);
END IF;

SET isCustomerAccessible = (_customerId in (SELECT distinct cg.Customer_id from userrolecustomerrole uc, customergroup cg 
where FIND_IN_SET(uc.UserRole_id, _roleIds) 
AND uc.CustomerRole_id=cg.Group_id));

IF ISNULL(isCustomerAccessible) or isCustomerAccessible = 0 THEN
	SET isCustomerAccessible = 'false';
ELSEIF(isCustomerAccessible = 1) THEN
	SET isCustomerAccessible = 'true';
END IF;

SELECT isCustomerAccessible;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `internal_user_access_to_customer_on_servicedefinition_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `internal_user_access_to_customer_on_servicedefinition_proc`(
IN _roleIds VARCHAR(500) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _customerId  VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _customerUsername  VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

DECLARE isCustomerAccessible VARCHAR(6);
SET SESSION group_concat_max_len = 100000000;

IF(_customerId = '' AND _customerUsername != '') THEN
	SET _customerId = (SELECT id FROM customer WHERE UserName = _customerUsername);
END IF;

SET @servicedefinitionIds = (SELECT group_concat(distinct servicedefinitionId SEPARATOR ",")   
							FROM userroleservicedefinition 
                            WHERE FIND_IN_SET(UserRole_id, _roleIds));

SET @contractIdList = (SELECT group_concat(id SEPARATOR ",") FROM contract WHERE FIND_IN_SET(servicedefinitionId, @servicedefinitionIds));

SET isCustomerAccessible = (_customerId IN (SELECT custId FROM 
(SELECT DISTINCT customerId  AS custId FROM contractcustomers 
WHERE customerId IS NOT NULL AND FIND_IN_SET(contractId, @contractIdList)
UNION ALL
SELECT DISTINCT coreCustomerId  AS custId FROM contractcustomers 
WHERE coreCustomerId IS NOT NULL AND FIND_IN_SET(contractId, @contractIdList) ) T));

IF ISNULL(isCustomerAccessible) OR isCustomerAccessible = 0 THEN
	SET isCustomerAccessible = 'false';
ELSEIF(isCustomerAccessible = 1) THEN
	SET isCustomerAccessible = 'true';
END IF;

SELECT isCustomerAccessible;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `lead_archive_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `lead_archive_proc`(
    IN _leadId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _leadClosureReason varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _modifiedby varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
#Proc to Faclitate Archiving and Purging of a specifc lead.
#Author : Aditya Mankal (KH2322) 

#NOT Found Handlers - START
DECLARE lead_note_cursor_done BOOLEAN DEFAULT FALSE;  
#NOT Found Handlers - END

DECLARE curr_leadnote_id VARCHAR(50) DEFAULT '';
DECLARE lead_note_count INT(10) DEFAULT 0;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET lead_note_cursor_done=TRUE;

  INSERT INTO archivedlead SELECT *FROM lead where id=_leadId;
	UPDATE archivedlead SET `Status_id`='SID_ARCHIVED' , `closureReason` = _leadClosureReason, `lastmodifiedts`= CURRENT_TIMESTAMP, `modifiedby` = _modifiedby WHERE id=_leadId;
	SELECT _leadId;
			
		LEADNOTEBLOCK: BEGIN
			DECLARE leadnote_cursor CURSOR FOR SELECT id FROM leadnote WHERE lead_Id = _leadId;
			   OPEN leadnote_cursor;
				   leadnote_loop:REPEAT 
					FETCH leadnote_cursor INTO curr_leadnote_id;
					IF NOT lead_note_cursor_done THEN
						INSERT INTO archivedleadnote SELECT *FROM leadnote where id = curr_leadnote_id;	
						DELETE FROM leadnote where id = curr_leadnote_id;
					END IF;	
				  UNTIL lead_note_cursor_done 
				END REPEAT leadnote_loop;
				SET lead_note_cursor_done=FALSE;
				CLOSE leadnote_cursor;
		END LEADNOTEBLOCK;	
		DELETE FROM lead where id = _leadId;
       
	SET lead_note_cursor_done=FALSE;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `lead_assign_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `lead_assign_proc`(
IN _leadIds  varchar(10000) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _csrID  varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _modifiedby varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	SET @updateStatement = CONCAT("UPDATE lead SET csr_id = '",_csrID,"', modifiedby = '",_modifiedby,"', lastmodifiedts = CURRENT_TIMESTAMP where id in (",func_escape_input_for_in_operator(_leadIds),")");
	PREPARE stmt FROM @updateStatement;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `lead_auto_archive_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `lead_auto_archive_proc`()
BEGIN
#Proc to Faclitate Archiving and Purging of Leads.
#Author : Aditya Mankal (KH2322) 

#NOT Found Handlers - START
DECLARE lead_cursor_done BOOLEAN DEFAULT FALSE;
DECLARE lead_note_cursor_done BOOLEAN DEFAULT FALSE;  
#NOT Found Handlers - END

DECLARE curr_lead_id VARCHAR(50) DEFAULT '';
DECLARE curr_leadnote_id VARCHAR(50) DEFAULT '';
DECLARE lead_note_count INT(10) DEFAULT 0;
DECLARE lead_cursor CURSOR FOR SELECT id FROM lead WHERE TIMESTAMPDIFF(SQL_TSI_SECOND ,lastmodifiedts, now())>= ((select PropertyValue from systemconfiguration where PropertyName='LEAD_AUTO_ARCHIVE_PERIOD_IN_DAYS')*24*60*60);
DECLARE CONTINUE HANDLER FOR NOT FOUND SET lead_cursor_done = TRUE,lead_note_cursor_done=TRUE;

	OPEN lead_cursor;
	lead_loop:REPEAT 
	FETCH lead_cursor INTO curr_lead_id;
	IF NOT lead_cursor_done THEN
		INSERT INTO archivedlead SELECT *FROM lead where id=curr_lead_id;
		UPDATE archivedlead SET `Status_id`='SID_ARCHIVED',  `closureReason` = 'Automatically archived' WHERE id=curr_lead_id;
		SELECT curr_lead_id;
			
			LEADNOTEBLOCK: BEGIN
			  DECLARE leadnote_cursor CURSOR FOR SELECT id FROM leadnote WHERE lead_Id = curr_lead_id;
			    OPEN leadnote_cursor;
				   leadnote_loop:REPEAT 
					FETCH leadnote_cursor INTO curr_leadnote_id;
					IF NOT lead_note_cursor_done THEN
						INSERT INTO archivedleadnote SELECT *FROM leadnote where id = curr_leadnote_id;	
						DELETE FROM leadnote where id = curr_leadnote_id;
					END IF;	
				  UNTIL lead_note_cursor_done 
				  END REPEAT leadnote_loop;
				  SET lead_cursor_done=FALSE;
				  SET lead_note_cursor_done=FALSE;
				CLOSE leadnote_cursor;
			END LEADNOTEBLOCK;
			
		 DELETE FROM lead where id = curr_lead_id;
        END IF;
	UNTIL lead_cursor_done 
	END REPEAT lead_loop;
	SET lead_cursor_done=FALSE;
	SET lead_note_cursor_done=FALSE;
	CLOSE lead_cursor;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `lead_notes_search_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `lead_notes_search_proc`(
IN _leadId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _leadStatusId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _sortOrder varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _sortCriteria varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _offset BIGINT,
IN _recordsPerPage BIGINT
)
BEGIN

SET @tableName="leadnote";
 
IF _leadStatusId = 'SID_ARCHIVED' THEN
	SET @tableName="archivedleadnote";
END IF;

SET @queryStatement =CONCAT( "
SELECT  ",@tableName,".`id` AS `id`,",
@tableName,".`lead_Id` AS `lead_Id`,",
@tableName,".`note` AS `note`,
`su1`.`FirstName` AS `createdByFirstName`,
`su1`.`MiddleName` AS `createdByMiddleName`,
`su1`.`LastName` AS `createdByLastName`,
`su2`.`FirstName` AS `modifiedbyFirstName`,
`su2`.`MiddleName` AS `modifiedbyMiddleName`,
`su2`.`LastName` AS `modifiedbyLastName`,",
@tableName,".`createdby` AS `createdby`,",
@tableName,".`modifiedby` AS `modifiedby`,",
@tableName,".`createdts` AS `createdts`,",
@tableName,".`lastmodifiedts` AS `lastmodifiedts`,",
@tableName,".`synctimestamp` AS `synctimestamp`,",
@tableName,".`softdeleteflag` AS `softdeleteflag`
FROM (`",@tableName,"`
        LEFT JOIN `systemuser` su1 ON `",@tableName,"`.`createdby` = `su1`.`id`
        LEFT JOIN `systemuser` su2 ON `",@tableName,"`.`modifiedby` = `su2`.`id`
 ) where true");

		IF _leadId != '' THEN
		SET @queryStatement = concat(@queryStatement," and ",@tableName,".lead_Id = ", quote(_leadId));
        END IF;
        
		IF _sortOrder != 'asc' OR  _sortOrder != 'desc' THEN
		SET _sortOrder = "desc";
		END IF;
		
        IF _sortCriteria = '' THEN
		SET _sortCriteria ="lastmodifiedts";
		END IF;
        SET @queryStatement = concat(@queryStatement," order by ",@tableName,".",_sortCriteria," ",_sortOrder);
		SET @queryStatement = concat(@queryStatement," LIMIT ",_recordsPerPage," OFFSET ",_offset);

		-- select @queryStatement;
		 PREPARE stmt FROM @queryStatement;
		 EXECUTE stmt;
		 DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `lead_search_count_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `lead_search_count_proc`(
IN _productId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _productType varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _leadType varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _statusIds varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _assignedCSRID varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _modifiedStartDate varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _modifiedEndDate varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _phoneNumber varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _emailAddress varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET @tableName="lead";
 
IF _statusIds = 'SID_ARCHIVED' THEN
	SET @tableName="archivedlead";
END IF;

 SET @queryStatement =CONCAT( "
 SELECT 
count(",@tableName,".`id`) AS `count` FROM
        (`",@tableName,"`
        LEFT JOIN `systemuser` su1 ON `",@tableName,"`.`csr_id` = `su1`.`id`
		LEFT JOIN `product` ON `",@tableName,"`.`product_id` = `product`.`id`
        LEFT JOIN `systemuser` su2 ON `",@tableName,"`.`createdby` = `su2`.`id`
		LEFT JOIN `systemuser` su3 ON `",@tableName,"`.`modifiedby` = `su3`.`id`) where true");
        
        IF _productId != '' THEN
		SET @queryStatement = concat(@queryStatement," and ",@tableName,".product_id = ", quote(_productId));
        END IF;
        
		IF _productType != '' THEN
		SET @queryStatement = concat(@queryStatement," and ", "product.Type_id` = ", quote(_productType));
        END IF;
        
         IF _customerId != '' THEN
		SET @queryStatement = concat(@queryStatement," and ",@tableName,".customerId = ", quote(_customerId));
        END IF;
        
		IF _assignedCSRID != '' THEN
		SET @queryStatement = concat(@queryStatement," and ",@tableName,".csr_id = ", quote(_assignedCSRID));
		END IF;
        
         IF _phoneNumber != '' THEN
		SET @queryStatement = concat(@queryStatement," and ",@tableName,".phoneNumber like concat(", quote(_phoneNumber),",'%')");
		END IF;
        
        IF _emailAddress != '' THEN
		SET @queryStatement = concat(@queryStatement," and ",@tableName,".email = ", quote(_emailAddress));
		END IF;
		
		IF _leadType = 'CUSTOMER' THEN
		SET @queryStatement = concat(@queryStatement," and ",@tableName,".isCustomer = 1");
        ELSEIF _leadType = 'NON-CUSTOMER' THEN
        	set @queryStatement = concat(@queryStatement," and ",@tableName,".isCustomer = 0");
		END IF;
        
		IF _statusIds != '' THEN
		SET @queryStatement = concat(@queryStatement," and ",@tableName,".status_id in  (", func_escape_input_for_in_operator(_statusIds),")");
		END IF;
        
		IF _modifiedStartDate != '' AND _modifiedEndDate != '' THEN
		SET @queryStatement = concat(@queryStatement," and DATE(",@tableName,".lastmodifiedts) >=",quote(DATE(_modifiedStartDate)), " and  DATE(",@tableName,".lastmodifiedts) <=",quote(DATE(_modifiedEndDate)));
		END IF;

		-- select @queryStatement;
		PREPARE stmt FROM @queryStatement;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `lead_search_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `lead_search_proc`(
IN _productId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _leadId  varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _productType varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _leadType varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _statusIds varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _assignedCSRID varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _modifiedStartDate varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _modifiedEndDate varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _phoneNumber varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _emailAddress varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _sortOrder varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _sortCriteria varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _offset BIGINT,
IN _recordsPerPage BIGINT
)
BEGIN

SET @tableName="lead";
 
IF _statusIds = 'SID_ARCHIVED' THEN
	SET @tableName="archivedlead";
END IF;

 SET @queryStatement =CONCAT( "
 SELECT 
",@tableName,".`id` AS `id`,
",@tableName,".`firstName` AS `firstName`,
",@tableName,".`middleName` AS `middleName`,
",@tableName,".`lastName` AS `lastName`,
",@tableName,".`salutation` AS `salutation`,
",@tableName,".`isCustomer` AS `isCustomer`,
",@tableName,".`customerId` AS `customerId`,
`customer`.`CustomerType_id` AS `customerType`,
",@tableName,".`product_id` AS `productId`,
`product`.`name` AS `productName`,
`product`.`Type_id` AS `productType`,
",@tableName,".`csr_id` AS `csrId`,
`su1`.`FirstName` AS `assignedToFirstName`,
`su1`.`MiddleName` AS `assignedToMiddleName`,
`su1`.`LastName` AS `assignedToLastName`,
",@tableName,".`status_id` AS `statusId`,
",@tableName,".`closureReason` AS `closureReason`,
",@tableName,".`countryCode` AS `countryCode`,
",@tableName,".`phoneNumber` AS `phoneNumber`,
",@tableName,".`extension` AS `extension`,
",@tableName,".`email` AS `email`,
",@tableName,".`createdby` AS `createdby`,
`su2`.`FirstName` AS `createdByFirstName`,
`su2`.`MiddleName` AS `createdByMiddleName`,
`su2`.`LastName` AS `createdByLastName`,
`su3`.`FirstName` AS `modifiedbyFirstName`,
`su3`.`MiddleName` AS `modifiedbyMiddleName`,
`su3`.`LastName` AS `modifiedbyLastName`,
",@tableName,".`modifiedby` AS `modifiedby`,
",@tableName,".`createdts` AS `createdts`,
",@tableName,".`lastmodifiedts` AS `lastmodifiedts`,
",@tableName,".`synctimestamp` AS `synctimestamp`,
",@tableName,".`softdeleteflag` AS `softdeleteflag`
    FROM
        (`",@tableName,"`
        LEFT JOIN `systemuser` su1 ON `",@tableName,"`.`csr_id` = `su1`.`id`
		LEFT JOIN `product` ON `",@tableName,"`.`product_id` = `product`.`id`
        LEFT JOIN `systemuser` su2 ON `",@tableName,"`.`createdby` = `su2`.`id`
		LEFT JOIN `systemuser` su3 ON `",@tableName,"`.`modifiedby` = `su3`.`id`
        LEFT JOIN `customer` ON `",@tableName,"`.`customerId` = `customer`.`id`) where true");
        
		IF _leadId != '' THEN
			SET @queryStatement = concat(@queryStatement," and ",@tableName,".id = ", quote(_leadId));
		END IF;
		
		IF _productId != '' THEN
			SET @queryStatement = concat(@queryStatement," and ",@tableName,".product_id = ", quote(_productId));
		END IF;
        
		IF _productType != '' THEN
			SET @queryStatement = concat(@queryStatement," and ", "product.Type_id` = ", quote(_productType));
		END IF;
        
		IF _customerId != '' THEN
			SET @queryStatement = concat(@queryStatement," and ",@tableName,".customerId = ", quote(_customerId));
		END IF;
        
		IF _assignedCSRID != '' THEN
			SET @queryStatement = concat(@queryStatement," and ",@tableName,".csr_id = ", quote(_assignedCSRID));
		END IF;
        
		IF _phoneNumber != '' THEN
			SET @queryStatement = concat(@queryStatement," and ",@tableName,".phoneNumber like concat(", quote(_phoneNumber),",'%')");
		END IF;
        
		IF _emailAddress != '' THEN
			SET @queryStatement = concat(@queryStatement," and ",@tableName,".email = ", quote(_emailAddress));
		END IF;
		
		IF _leadType = 'CUSTOMER' THEN
				SET @queryStatement = concat(@queryStatement," and ",@tableName,".isCustomer = 1");
		ELSEIF _leadType = 'NON-CUSTOMER' THEN
				SET @queryStatement = concat(@queryStatement," and ",@tableName,".isCustomer = 0");
		END IF;
        
		IF _statusIds != '' THEN
			SET @queryStatement = concat(@queryStatement," and ",@tableName,".status_id in  (", func_escape_input_for_in_operator(_statusIds),")");
		END IF;
        
		IF _modifiedStartDate != '' AND _modifiedEndDate != '' THEN
			SET @queryStatement = concat(@queryStatement," and DATE(",@tableName,".lastmodifiedts) >=",quote(DATE(_modifiedStartDate)), " and  DATE(",@tableName,".lastmodifiedts) <=",quote(DATE(_modifiedEndDate)));
		END IF;
		
		IF _sortOrder != 'asc' AND _sortOrder != 'desc' THEN
			SET _sortOrder = "desc";
		END IF;
        
		IF _sortCriteria = '' THEN
			SET _sortCriteria ="lastmodifiedts";
		END IF;
	
   SET @queryStatement = concat(@queryStatement," order by `",_sortCriteria,"` ",_sortOrder);
		SET @queryStatement = concat(@queryStatement," LIMIT ",_recordsPerPage," OFFSET ",_offset);
		PREPARE stmt FROM @queryStatement;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `limits_update_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `limits_update_proc`(
  IN _action varchar(255) CHARACTER SET UTF8 COLLATE utf8_general_ci,
  IN _maxTxLimit decimal(20,2),
  IN _dailyLimit decimal(20,2),
  IN _weeklyLimit decimal(20,2)
)
BEGIN
  UPDATE groupactionlimit SET value = _maxTxLimit where Action_id = _action AND LimitType_id = 'MAX_TRANSACTION_LIMIT' AND value > _maxTxLimit; 
  UPDATE groupactionlimit SET value = _dailyLimit where Action_id = _action AND LimitType_id = 'DAILY_LIMIT' AND value > _dailyLimit;
  UPDATE groupactionlimit SET value = _weeklyLimit where Action_id = _action AND LimitType_id = 'WEEKLY_LIMIT' AND value > _weeklyLimit;
  UPDATE servicedefinitionactionlimit SET value = _maxTxLimit where actionId = _action AND limitTypeId = 'MAX_TRANSACTION_LIMIT' AND value > _maxTxLimit; 
  UPDATE servicedefinitionactionlimit SET value = _dailyLimit where actionId = _action AND limitTypeId = 'DAILY_LIMIT' AND value > _dailyLimit;
  UPDATE servicedefinitionactionlimit SET value = _weeklyLimit where actionId = _action AND limitTypeId = 'WEEKLY_LIMIT' AND value > _weeklyLimit; 
  UPDATE contractactionlimit SET value = _maxTxLimit where actionId = _action AND limitTypeId = 'MAX_TRANSACTION_LIMIT' AND value > _maxTxLimit; 
  UPDATE contractactionlimit SET value = _dailyLimit where actionId = _action AND limitTypeId = 'DAILY_LIMIT' AND value > _dailyLimit;
  UPDATE contractactionlimit SET value = _weeklyLimit where actionId = _action AND limitTypeId = 'WEEKLY_LIMIT' AND value > _weeklyLimit; 
  UPDATE customeraction SET value = _maxTxLimit where Action_id = _action AND LimitType_id = 'MAX_TRANSACTION_LIMIT' AND value > _maxTxLimit; 
  UPDATE customeraction SET value = _dailyLimit where Action_id = _action AND LimitType_id = 'DAILY_LIMIT' AND value > _dailyLimit;
  UPDATE customeraction SET value = _weeklyLimit where Action_id = _action AND LimitType_id = 'WEEKLY_LIMIT' AND value > _weeklyLimit; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Location_address_suggestions_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `Location_address_suggestions_proc`(
  in _currLatitude TEXT(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, 
  in _currLongitude TEXT(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, 
  in _radius float(50) ,
  in _var TEXT(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN DECLARE rowLatitide TEXT(2000);
DECLARE rowLongitude TEXT(2000);
DECLARE b int(1);
DECLARE pipeFlag int(1);
set _var = concat('%',_var,'%');
set _radius= _radius/1000;
SELECT 
  location.id AS locationId, 
  (
    SELECT 
      GROUP_CONCAT(
        DISTINCT CONCAT(
          UCASE(
            LEFT(dayschedule.weekdayname, 1)
          ), 
          LCASE(
            SUBSTRING(dayschedule.weekdayname, 2)
          ), 
          ':', 
          SUBSTRING(dayschedule.StartTime, 1, 5), 
          '-', 
          SUBSTRING(dayschedule.endTime, 1, 5)
        ) SEPARATOR ' || '
      ) 
    FROM 
      dayschedule, 
      location 
    WHERE 
      dayschedule.WorkSchedule_id = location.WorkSchedule_id 
      and location.id = locationId
  ) AS workingHours,
  location.name AS informationTitle,
  location.Description AS description,
  location.phoneNumber AS phone, 
  location.emailId AS email, 
  CASE `location`.`softdeleteflag` WHEN 'SID_ACTIVE' THEN 'OPEN' ELSE 'CLOSED' END AS status, 
  CASE `location`.`type_id` WHEN 'ATM' THEN 'ATM' ELSE 'BRANCH'  END AS type,
  location.status_id AS isVisible,
  (SELECT 
                GROUP_CONCAT(`facility`.`name`
                        ORDER BY `facility`.`id` ASC
                        SEPARATOR '||')
            FROM
                (`facility`
                JOIN `locationfacility`)
            WHERE
                ((`facility`.`id` = CONVERT( `locationfacility`.`facility_id` USING UTF8))
                    AND (CONVERT( `locationfacility`.`Location_id` USING UTF8) =`location`.`id`))) AS  services, 
  (
    SELECT 
      address.cityName
    FROM 
      address
    WHERE 
      address.id = location.Address_id
  ) AS city,
  (SELECT 
                region.Name
            FROM
                region
            WHERE
                (region.id = address.Region_id)) AS Region,
	(SELECT 
                country.Name
            FROM
                country
            WHERE
                (country.id = (SELECT 
                        region.Country_id
                    FROM region WHERE
                        (region.id = address.Region_id)))) AS country,
  address.addressLine1 AS addressLine1, 
  address.addressLine2 AS addressLine2, 
  address.addressLine3 AS addressLine3, 
  address.zipCode AS zipCode,
  address.latitude AS latitude, 
  address.logitude AS longitude, 
  (
    6371 * 2 * ASIN(
      SQRT(
        POWER(
          SIN(
            (
              latitude - ABS(_currLatitude)
            ) * PI() / 180 / 2
          ), 
          2
        ) + COS(
          latitude * PI() / 180
        ) * COS(
          ABS(_currLatitude) * PI() / 180
        ) * POWER(
          SIN(
            (Logitude - _currLongitude) * PI() / 180 / 2
          ), 
          2
        )
      )
    )
  ) AS distance 
FROM 
  address, 
  location 
WHERE 
  address.id = location.address_id 
HAVING 
  distance <= _radius AND (city like _var or type like _var or Region like _var  or informationTitle like _var or phone like _var or country like _var) AND isVisible= 'SID_ACTIVE'  ;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `location_details_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `location_details_proc`(
  in _locationId char(50)
)
BEGIN DECLARE workingHours TEXT(2000);
DECLARE tempDayName TEXT(2000);
DECLARE tempStartTime TEXT(2000);
DECLARE tempEndTime TEXT(2000);
DECLARE servicesList TEXT(10000);
DECLARE currencyList TEXT(10000);

DECLARE b int(1);
DECLARE pipeFlag int(1);
DECLARE cur_1 CURSOR FOR 
select 
  dayschedule.WeekDayName, 
  dayschedule.StartTime, 
  dayschedule.EndTime 
from 
  dayschedule, 
  location 
WHERE 
  location.WorkSchedule_id = dayschedule.WorkSchedule_id 
  and location.id = _locationId;
DECLARE serviceCursor CURSOR FOR 
SELECT 
  service.name 
from 
  service, 
  locationservice 
WHERE 
  service.id = locationservice.Service_id 
  and locationservice.Location_id = _locationId;

DECLARE currencyCursor CURSOR FOR 
SELECT 
  currency.code 
from 
  currency, 
  locationcurrency 
WHERE 
  currency.code = locationcurrency.currency_code 
  and locationcurrency.Location_id = _locationId;
  
DECLARE CONTINUE HANDLER FOR NOT FOUND 
SET 
  b = 1;
set 
  @a = "";
set 
  @wh = "";
set 
  @pipeFlag = 0;
OPEN cur_1;
REPEAT 
set 
  tempDayName = "";
set 
  tempStartTime = "";
set 
  tempEndTime = "";
FETCH cur_1 INTO tempDayName, 
tempStartTime, 
tempEndTime;
if tempDayName <> "" then if @pipeFlag = 0 then 
set 
  @a = CONCAT(
    UCASE(
      LEFT(tempDayName, 1)
    ), 
    LCASE(
      SUBSTRING(tempDayName, 2)
    ), 
    ": ", 
    SUBSTRING(tempStartTime, 1, 5), 
    "AM", 
    " - ", 
    SUBSTRING(tempEndTime, 1, 5), 
    "PM"
  );
else 
set 
  @a = CONCAT(
    " || ", 
    UCASE(
      LEFT(tempDayName, 1)
    ), 
    LCASE(
      SUBSTRING(tempDayName, 2)
    ), 
    ": ", 
    SUBSTRING(tempStartTime, 1, 5), 
    "AM", 
    " - ", 
    SUBSTRING(tempEndTime, 1, 5), 
    "PM"
  );
end if;
set 
  @pipeFlag = 1;
set 
  @a = CONCAT(
    " || ", tempDayName, ": ", tempStartTime, 
    "AM", " - ", tempEndTime, "PM"
  );
set 
  @wh = CONCAT(@wh, " ", @a, "");
end if;
UNTIL b = 1 END REPEAT;
CLOSE cur_1;
set 
  workingHours = @wh;
set 
  servicesList = (
    SELECT 
      GROUP_CONCAT(
        service.name 
        ORDER BY 
          service.name ASC SEPARATOR ' || '
      ) as services 
    FROM 
      service, 
      locationservice 
    WHERE 
      service.id = locationservice.Service_id 
      and locationservice.Location_id = _locationId
  );  
set 
  currencyList = (
    SELECT 
      GROUP_CONCAT(
        currency.code 
        ORDER BY 
          currency.code ASC SEPARATOR ' , '
      ) as currencies 
    FROM 
      currency, 
      locationcurrency 
    WHERE 
      currency.code = locationcurrency.currency_code 
      and locationcurrency.Location_id = _locationId
  );  
SELECT 
  `location`.`id` AS locationId, 
  `location`.`Type_id` AS type, 
  `location`.`Name` AS informationTitle, 
  CASE `location`.`status_id` WHEN 'SID_ACTIVE' THEN 'OPEN' ELSE 'CLOSED' END AS status, 
  `location`.`DisplayName` AS displayName, 
  `location`.`Description` AS description, 
  `location`.`PhoneNumber` AS phone, 
  `location`.`EmailId` AS email, 
  `location`.`WorkingDays` AS workingDays, 
  `location`.`IsMainBranch` AS isMainBranch, 
  `location`.`MainBranchCode` AS mainBranchCode, 
  `address`.`AddressLine1` AS addressLine1, 
  `address`.`AddressLine2` AS addressLine2, 
  `address`.`AddressLine3` AS addressLine3, 
  `address`.`Latitude` AS latitude, 
  `address`.`Logitude` AS longitude, 
  workingHours AS workingHours,
  servicesList AS services, 
  currencyList AS currencies 
FROM 
  (
    (
      `location` 
      JOIN `address` ON (
        (
          `location`.`address_id` = `address`.`id`
        )
      )
    )
  ) 
WHERE 
  `location`.`id` = `_locationId`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `location_range_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `location_range_proc`(
  in _currLatitude TEXT(50), 
  in _currLongitude TEXT(50), 
  in _radius float(50) )
BEGIN DECLARE rowLatitide TEXT(2000);
DECLARE rowLongitude TEXT(2000);
DECLARE b int(1);
DECLARE pipeFlag int(1);
set _radius=_radius/1000;
SELECT 
  location.id AS locationId, 
  (
    SELECT 
      GROUP_CONCAT(
        DISTINCT CONCAT(
          UCASE(
            LEFT(dayschedule.weekdayname, 1)
          ), 
          LCASE(
            SUBSTRING(dayschedule.weekdayname, 2)
          ), 
          ':', 
          SUBSTRING(dayschedule.StartTime, 1, 5), 
          '-', 
          SUBSTRING(dayschedule.endTime, 1, 5)
        ) SEPARATOR ' || '
      ) 
    FROM 
      dayschedule, 
      location 
    WHERE 
      dayschedule.WorkSchedule_id = location.WorkSchedule_id 
      and location.id = locationId
  ) AS workingHours, 
  location.name AS informationTitle, 
  location.phoneNumber AS phone, 
  location.emailId AS email, 
  CASE `location`.`softdeleteflag` WHEN '0' THEN 'OPEN' ELSE 'CLOSED' END AS status, 
  CASE `location`.`type_id` WHEN 'ATM' THEN 'ATM' ELSE 'BRANCH'  END AS type,
  location.status_id AS isVisible,
 (SELECT 
                GROUP_CONCAT(`facility`.`name`
                        ORDER BY `facility`.`id` ASC
                        SEPARATOR '||')
            FROM
                (`facility`
                JOIN `locationfacility`)
            WHERE
                ((`facility`.`id` = CONVERT( `locationfacility`.`facility_id` USING UTF8))
                    AND (CONVERT( `locationfacility`.`Location_id` USING UTF8) =`location`.`id`))) AS  services, 
  (
    SELECT 
      address.cityName
    FROM 
      address
    WHERE 
      address.id = location.Address_id
  ) AS city, 
  address.addressLine1 AS addressLine1, 
  address.addressLine2 AS addressLine2, 
  address.addressLine3 AS addressLine3, 
  address.zipCode AS zipCode, 
  address.latitude AS latitude, 
  address.logitude AS longitude, 
  (
    6371 * 2 * ASIN(
      SQRT(
        POWER(
          SIN(
            (
              latitude - ABS(_currLatitude)
            ) * PI() / 180 / 2
          ), 
          2
        ) + COS(
          latitude * PI() / 180
        ) * COS(
          ABS(_currLatitude) * PI() / 180
        ) * POWER(
          SIN(
            (Logitude - _currLongitude) * PI() / 180 / 2
          ), 
          2
        )
      )
    )
  ) AS distance 
FROM 
  address, 
  location 
WHERE 
  address.id = location.address_id 
HAVING 
  distance <= _radius AND isVisible= 'SID_ACTIVE';
  
  
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `location_search_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `location_search_proc`(
  in _searchKeyword TEXT(1000)
)
BEGIN DECLARE _next TEXT DEFAULT NULL;
DECLARE _nextlen INT DEFAULT NULL;
DECLARE _value TEXT DEFAULT NULL;
DECLARE sql1 TEXT (200000);
DECLARE sqlFrontPart TEXT (200000);
DECLARE sqlresult TEXT (200000) default null;
declare counter int(50);
set 
  @sqlFrontPart = 'SELECT * FROM locationdetails_view where isVisible = "SID_ACTIVE" AND';
set 
  @counter = 1;
iterator : LOOP IF LENGTH(
  TRIM(_searchKeyword)
) = 0 
OR _searchKeyword IS NULL THEN LEAVE iterator;
END IF;
SET 
  _next = SUBSTRING_INDEX(_searchKeyword, ',', 1);
SET 
  _nextlen = LENGTH(_next);
SET 
  _value = LCASE(
    TRIM(_next)
  );
set 
  @sql1 = null;
set 
  @var = null;
set 
  @var = REPLACE (
    trim(_value), 
    ' ', 
    '%'
  );
set 
  @sql1 = concat(
    '( 
 LOWER(city) like (', '"', '%', 
    @var, '%', '"', ') 
  or LOWER(informationTitle) like (', '"', '%', 
    @var, '%', '"', ') 
  or LOWER(name) like (', '"', '%', 
    @var, '%', '"', ') 
  or LOWER(Description) like (', '"', '%', 
    @var, '%', '"', ') 
  or LOWER(Code) like (', '"', '%', 
    @var, '%', '"', ') 
  or LOWER(phoneNumber) like (', '"', '%', 
    @var, '%', '"', ') 
  or LOWER(email) like (', '"', '%', 
    @var, '%', '"', ') 
  or LOWER(status) like (', '"', '%', 
    @var, '%', '"', ') 
  or LOWER(type) like (', '"', '%', 
    @var, '%', '"', ') 
  or LOWER(services) like (', '"', '%', 
    @var, '%', '"', ') 
  or LOWER(region) like (', '"', '%', 
    @var, '%', '"', ')
  or LOWER(addressLine1) like (', 
    '"', '%', @var, '%', '"', ') 
  or LOWER(addressLine2) like (', 
    '"', '%', @var, '%', '"', ') 
  or LOWER(addressLine3) like (', 
    '"', '%', @var, '%', '"', ') 
  or LOWER(country) like (', 
    '"', '%', @var, '%', '"', ') 
  or LOWER(zipcode) like (', 
    '"', '%', @var, '%', '"', '))'
  );
if @counter = 1 then 
set 
  @sqlresult = @sql1;
else 
set 
  @sqlresult = concat(@sql1, ' AND ', @sqlresult);
end if;
set 
  @counter = @counter + 1;

SET 
  _searchKeyword = INSERT(_searchKeyword, 1, _nextlen + 1, '');
END LOOP;

set 
  @sqlresult = concat(
    '(', @sqlFrontPart, @sqlresult, ')'
  );
PREPARE stmt1 
from 
  @sqlresult;
EXECUTE stmt1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `manageapprovalmatrix_update_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `manageapprovalmatrix_update_proc`(
	IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _cifList longtext CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN _isDisabledflag VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	DECLARE index1 INTEGER DEFAULT 0;
	DECLARE mod_contractId VARCHAR(60);
    set mod_contractId=concat("\"",_contractId,"\"");
	set @numOfRecords = LENGTH(_cifList) - LENGTH(REPLACE(_cifList, ',', '')) + 1;
	insertRecords : LOOP
		set index1 = index1 + 1;
		IF index1 = @numOfRecords + 1 THEN 
			LEAVE insertRecords;
		else
			set @recordsData = concat("\"",SUBSTRING_INDEX(SUBSTRING_INDEX(_cifList, ',', index1), ',', -1 ),"\"");
			set @cquery= concat('select count(*) from manageapprovalmatrix where contractId=',mod_contractId,' and coreCustomerId=',@recordsData,'INTO @count');
            PREPARE count_query FROM @cquery;
			EXECUTE count_query;
			if @count = 0 THEN
				set @query = concat('INSERT INTO manageapprovalmatrix(contractId,coreCustomerId,isDisabled) VALUES (',mod_contractId,',',@recordsData,',',_isDisabledflag,');');
				PREPARE sql_query FROM @query;
				EXECUTE sql_query;				
			else
				set @query2=concat('UPDATE manageapprovalmatrix set isDisabled=',_isDisabledflag,' where contractId=',mod_contractId,' and coreCustomerId=',@recordsData);
                PREPARE update_query FROM @query2;
				EXECUTE update_query;
			END IF;
	    END IF;
	END LOOP insertRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `membership_customer_search_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `membership_customer_search_proc`(
in _id varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _name varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _email varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _phone varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _dateOfBirth varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _status varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _city varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _country varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _zipCode varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

		SET @select_statement = concat("select membership.id , membership.industry,membership.name , membership.firstName , membership.lastName , membership.phone , membership.isBusinessType , membership.taxId , membership.faxId , membership.email , address.addressLine1 ,address.addressLine2 , address.cityName , address.country , address.zipCode , address.state from membership LEFT JOIN address on (membership.addressId = address.id) where membership.id is not null");

	IF(_id != "") THEN
		SET @select_statement = concat(@select_statement , " and membership.id = ",quote(_id));
    END IF;
    
    IF(_name != "") THEN
		SET @select_statement = concat(@select_statement , " and membership.name like '%",_name,"%'");
    END IF;
    
    IF(_email != "") THEN
		SET @select_statement = concat(@select_statement , " and membership.email = ",quote(_email));
    END IF;
    
    IF(_phone != "") THEN
		SET @select_statement = concat(@select_statement , " and membership.phone = ",quote(_phone));
    END IF;
    
    IF(_dateOfBirth != "") THEN
		SET @select_statement = concat(@select_statement , " and membership.dateOfBirth = ",quote(_dateOfBirth));
    END IF;
		
	SET @address_select_statement = "";
	IF(_city != "" OR _country!= "" OR _zipCode!= "") THEN
    
		SET @address_select_statement = concat("select address.id from address where address.id is not null");
        IF(_city != "") THEN
			SET @address_select_statement = concat(@address_select_statement , " and address.cityName = ",quote(_city));
        END IF;
        
        IF(_country != "") THEN
			SET @address_select_statement = concat(@address_select_statement , " and address.country = ",quote(_country));
        END IF;
        
        IF(_zipCode != "") THEN
			SET @address_select_statement = concat(@address_select_statement , " and address.zipCode = ",quote(_zipCode));
        END IF;
        
    END IF;
    
	IF(@address_select_statement != "") THEN 
		SET @select_statement = concat(@select_statement , " and membership.addressId in (", @address_select_statement , ")");
    END IF;
    
    PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
      
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `membership_relative_customer_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `membership_relative_customer_get_proc`(
in _id varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

    select  membership.id , 
			membership.name , 
            membership.firstName , 
            membership.lastName , 
            membership.phone , 
            membership.email , 
			membership.dateOfBirth, 
			membership.taxId,
			membership.faxId,
            membership.industry, 
			membership.isBusinessType,
            membershiprelation.relationshipId,
            membershiprelation.relationshipName,
            address.addressLine1 ,
			address.addressLine2 , 
            address.cityName , 
            address.country , 
            address.zipCode , 
            address.state 
			from 
            membership 
			LEFT JOIN address on (membership.addressId = address.id)
            JOIN membershiprelation on (membershiprelation.relatedMebershipId COLLATE utf8_general_ci = membership.id)
			where membership.id in (select membershiprelation.relatedMebershipId COLLATE utf8_general_ci from membershiprelation where membershiprelation.membershipId COLLATE utf8_general_ci= _id)
            and membershiprelation.membershipId COLLATE utf8_general_ci = _id;
      
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `mfa_c360_feature_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `mfa_c360_feature_get_proc`()
BEGIN
SELECT DISTINCT feature.id AS feature_id, feature.name AS feature_name,feature.App_id as App_id
	FROM feature 
	JOIN featureaction ON (feature.id = featureaction.Feature_id)
	WHERE featureaction.isMFAApplicable = 1 and feature.Status_id = "SID_FEATURE_ACTIVE"
	ORDER BY feature.id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `new_procedure` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `new_procedure`()
BEGIN
DECLARE offsetCustom int default 0;
DECLARE total int default 0;
SET offsetCustom = offset_in *10;

SET total = (SELECT ceil(count(*) / 10) as Total FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('S1') and IsPrimaryCustomer = 1);

SELECT total, ApplicationID, CustName, "Branch", ModifiedDate,(CASE
WHEN Status = "S1" THEN "Under Supervision"         
ELSE " "
END) as Status, BranchNameIdValue,
COALESCE(SubmittedDate,CreatedDate) AS "CreatedDate" FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('S1') and IsPrimaryCustomer = 1 ORDER BY COALESCE(SubmittedDate,CreatedDate)  DESC LIMIT 10
OFFSET offsetCustom;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `organisation_actions_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `organisation_actions_create_proc`(
IN _features TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _organisationType VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _organisationId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
 DECLARE finished INTEGER DEFAULT 0 ;
 DECLARE featureActionId varchar(255) CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT "" ;
 DECLARE actionslist TEXT DEFAULT "" ;
 DECLARE limitId varchar(255) CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT "";
 DECLARE entryStatus INTEGER DEFAULT 0 ;

 DECLARE actions CURSOR 
		FOR (select id from featureaction where FIND_IN_SET(Feature_id ,@features_list) COLLATE utf8_general_ci );
DECLARE limits CURSOR 
		FOR (select LimitType_id from actionlimit where Action_id COLLATE utf8_general_ci = featureActionId COLLATE utf8_general_ci);
 
 DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;
 
SET @features_list = (select group_concat(feature.id SEPARATOR ",") from feature JOIN featureroletype ON (featureroletype.Feature_id =feature.id  and featureroletype.RoleType_id = _organisationType) 
                     where (FIND_IN_SET(feature.id,_features)) OR feature.isPrimary = '1' OR feature.isPrimary = true) ; 

SET @features_list = IF(@features_list is null, '', @features_list) COLLATE utf8_general_ci;

OPEN actions; 
getAction: LOOP
        SET entryStatus = 0;
        FETCH actions INTO featureActionId;
        IF finished = 1 THEN 
            LEAVE getAction;
	    else
            OPEN limits; 
			getlimit: LOOP
            FETCH limits INTO limitId;
			IF finished = 1 THEN 
               LEAVE getlimit;
			else
               SET @limitvalue = (SELECT value FROM actionlimit WHERE Action_id  = featureActionId COLLATE utf8_general_ci 
               AND LimitType_id = limitId COLLATE utf8_general_ci );
	       
			   SET @id = (SELECT LEFT(UUID(), 50));
			   INSERT IGNORE INTO organisationactionlimit(id,Organisation_id,Action_id,LimitType_id,value) VALUES
		        (@id,_organisationId,featureActionId,limitId,@limitvalue);
                SET entryStatus = 1;
			   ITERATE  getlimit;
			END IF;
                END LOOP getlimit;
                CLOSE limits;
                
            SET finished = 0;
            IF entryStatus = 0 THEN
                  SET @orgIds = (SELECT group_concat(DISTINCT organisationactionlimit.Organisation_id SEPARATOR ",") from organisationactionlimit
                  where Organisation_id = _organisationId AND Action_id = featureActionId);
                 IF isnull(@orgIds) OR @orgIds = "" THEN
                     SET @id = (SELECT LEFT(UUID(), 50));
					 INSERT IGNORE INTO organisationactionlimit(id,Organisation_id,Action_id) VALUES
					  (@id,_organisationId,featureActionId);
				 END IF;
            END IF;
			set actionslist = CONCAT(featureActionId,",",actionslist) COLLATE utf8_general_ci;
			ITERATE  getAction;
        END IF;
END LOOP getAction;
CLOSE actions;
  
SET actionslist = (select SUBSTRING(actionslist FROM 1 FOR (CHAR_LENGTH(actionslist)-1)));

select actionslist;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `organisation_actions_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `organisation_actions_delete_proc`(
IN _features TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _organisationType VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _organisationId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
 DECLARE finished INTEGER DEFAULT 0;
 DECLARE featureActionId varchar(255) DEFAULT "";
 DECLARE actionslist TEXT DEFAULT "";

 DECLARE actions CURSOR 
		FOR (select id from featureaction where FIND_IN_SET(Feature_id,@features_list));
 DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;
 
SET @features_list = (select group_concat(feature.id SEPARATOR ",") from feature JOIN featureroletype ON (featureroletype.Feature_id COLLATE utf8_general_ci =feature.id COLLATE utf8_general_ci and featureroletype.RoleType_id COLLATE utf8_general_ci = _organisationType COLLATE utf8_general_ci) 
                      where (FIND_IN_SET(feature.id COLLATE utf8_general_ci,_features COLLATE utf8_general_ci))) ; 
SET @features_list = IF(@features_list is null, '', @features_list);

SET @customer_list = (select group_concat(Customer_id SEPARATOR ",") from organisationemployees where (Organization_id = _organisationId));  

SET @customer_list = IF(@customer_list is null, '', @customer_list);

OPEN actions; 
 getAction: LOOP
        FETCH actions INTO featureActionId;
        IF finished = 1 THEN 
            LEAVE getAction;
	    else
			DELETE FROM organisationactionlimit WHERE (Organisation_id COLLATE utf8_general_ci = _organisationId COLLATE utf8_general_ci AND
            Action_id COLLATE utf8_general_ci = featureActionId COLLATE utf8_general_ci);
			set actionslist = CONCAT(featureActionId,",",actionslist);
			ITERATE  getAction;
        END IF;
END LOOP getAction;
CLOSE actions;

SET actionslist = (select SUBSTRING(actionslist FROM 1 FOR (CHAR_LENGTH(actionslist)-1)));

delete from customeraction where (RoleType_id =_organisationType and FIND_IN_SET(Action_id COLLATE utf8_general_ci,actionslist COLLATE utf8_general_ci) and FIND_IN_SET(Customer_id COLLATE utf8_general_ci,@customer_list COLLATE utf8_general_ci));
    
select actionslist;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `organisation_actions_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `organisation_actions_get_proc`(
IN _organisationId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
SET @actionList = (SELECT group_concat(DISTINCT organisationactionlimit.Action_id SEPARATOR ",") from organisationactionlimit 
                     where Organisation_id COLLATE utf8_general_ci = _organisationId);

SELECT @actionList AS actionslist;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `organisation_customeraccounts_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `organisation_customeraccounts_delete_proc`(
IN _accounts TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _organisationId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET @orgemployees_list = (select group_concat(organisationemployees.Customer_id SEPARATOR ",") from organisationemployees 
                      where Organization_id COLLATE utf8_general_ci = _organisationId); 
SET @orgemployees_list = IF(@orgemployees_list is null, '', @orgemployees_list);  

select @orgemployees_list;

DELETE FROM `customeraction` 
	WHERE  FIND_IN_SET(`customeraction`.`Account_id`,_accounts) 
    AND FIND_IN_SET(`customeraction`.`Customer_id` ,@orgemployees_list);
		
DELETE FROM `customeraccounts` 
	WHERE  FIND_IN_SET(`customeraccounts`.`Account_id`,_accounts) 
    AND FIND_IN_SET(`customeraccounts`.`Customer_id` ,@orgemployees_list);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `organisation_details_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `organisation_details_get_proc`(
IN _orgId TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _orgName TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _orgEmail TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _orgTaxId TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
DECLARE orgIdCSV TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT "" ;
IF _orgName != ''then
   SET orgIdCSV = (SELECT group_concat(DISTINCT organisation.id SEPARATOR ",") FROM organisation WHERE Name LIKE concat('%',_orgName,'%'));
END IF;

IF _orgEmail != '' then
  IF orgIdCSV = '' THEN
      SET orgIdCSV = (SELECT group_concat(DISTINCT organisationcommunication.Organization_id SEPARATOR ",") FROM organisationcommunication WHERE Value LIKE concat('%',_orgEmail,'%') AND Type_id = 'COMM_TYPE_EMAIL');
  ELSE
     SET orgIdCSV = concat(orgIdCSV,',',(SELECT group_concat(DISTINCT organisationcommunication.Organization_id SEPARATOR ",") FROM organisationcommunication WHERE Value LIKE concat('%',_orgEmail,'%') AND Type_id = 'COMM_TYPE_EMAIL' 
                    AND FIND_IN_SET(organisationcommunication.Organization_id,orgIdCSV)));
  END IF;
END IF;

IF _orgTaxId  != '' then
  IF orgIdCSV = '' THEN
     SET orgIdCSV = (SELECT group_concat(DISTINCT organisationmembership.Organization_id SEPARATOR ",") FROM organisationmembership WHERE Taxid LIKE concat('%',_orgTaxId,'%'));
  ELSE
     SET orgIdCSV = concat(orgIdCSV,',',(SELECT group_concat(DISTINCT organisationmembership.Organization_id SEPARATOR ",") FROM organisationmembership WHERE Taxid LIKE concat('%',_orgTaxId,'%') 
                    AND FIND_IN_SET(organisationmembership.Organization_id,orgIdCSV)));
  END IF;
END IF;

IF _orgId != '' then
  SET orgIdCSV = _orgId;
END IF;

SELECT DISTINCT `organisation`.`id` AS `id`,
        `organisation`.`Name` AS `Name`,
        `organisation`.`Type_Id` AS `TypeId`,
        `organisation`.`StatusId` AS `orgStatus`,
        `organisation`.`FaxId` AS `faxId`,
        `phone`.`value` AS `Phone`,
        `email`.`value` AS `Email`,
        `address`.`cityName` AS `cityName`,
        `address`.`addressLine1` AS `addressLine1`,
        `address`.`addressLine2` AS `addressLine2`,
        `address`.`zipCode` AS `zipCode`,
        `address`.`id` AS `addressId`,
        `address`.`state` AS `state`,
        `address`.`country` AS `country`,
        `organisationaddress`.`IsPrimary` AS `IsPrimary`,
        `businesstype`.`name` AS `businessType`,
        `businesstype`.`id` AS `businessTypeId`
    FROM
      (((`organisation`
        LEFT JOIN `organisationcommunication` phone ON ((FIND_IN_SET(organisation.id ,orgIdCSV)) AND phone.Type_id= 'COMM_TYPE_PHONE' AND (`organisation`.`id` = `phone`.`Organization_id`))
		LEFT JOIN `organisationcommunication` email ON ((FIND_IN_SET(organisation.id ,orgIdCSV)) AND email.Type_id= 'COMM_TYPE_EMAIL' AND (`organisation`.`id` = `email`.`Organization_id`))
        LEFT JOIN `organisationaddress` ON ((FIND_IN_SET(organisationaddress.Organization_id ,orgIdCSV)) AND (`organisation`.`id` = `organisationaddress`.`Organization_id`)))
        LEFT JOIN `address` ON ((`organisationaddress`.`Address_id` = `address`.`id`)))
        LEFT JOIN `businesstype` ON ((`businesstype`.`id` = `organisation`.`BusinessType_id`))) where FIND_IN_SET(organisation.id ,orgIdCSV);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `organisation_features_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `organisation_features_create_proc`(
IN _features TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _organisationType VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _organisationId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
 DECLARE finished INTEGER DEFAULT 0;
 DECLARE featureId varchar(255) DEFAULT "";
 DECLARE featuresList TEXT DEFAULT "";

 DECLARE features CURSOR 
		FOR (select id from feature where FIND_IN_SET(id,@features_list));
 DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;
 
SET @features_list = (select group_concat(feature.id SEPARATOR ",") from feature JOIN featureroletype ON (featureroletype.Feature_id =feature.id  and featureroletype.RoleType_id = _organisationType) 
                      where (FIND_IN_SET(feature.id,_features)) OR feature.isPrimary = '1' OR feature.isPrimary = true) ; 
SET @features_list = IF(@features_list is null, '', @features_list);

OPEN features; 
 getFeature: LOOP
        FETCH features INTO featureId;
        IF finished = 1 THEN 
            LEAVE getFeature;
	    else
            SET @id = (SELECT LEFT(UUID(), 50));
			INSERT IGNORE INTO organisationfeatures(id,organisationId,featureId) VALUES
		    (@id,_organisationId,featureId);
			set featuresList = CONCAT(featureId,",",featuresList);
			ITERATE  getFeature;
        END IF;
END LOOP getFeature;
CLOSE features;
  
SET featuresList = (select SUBSTRING(featuresList FROM 1 FOR (CHAR_LENGTH(featuresList)-1)));
select featuresList;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `organisation_features_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `organisation_features_delete_proc`(
IN _features TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _organisationType VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _organisationId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
 DECLARE finished INTEGER DEFAULT 0 ;
 DECLARE featureId varchar(255) DEFAULT "" ;
 DECLARE featuresList TEXT DEFAULT "" ;

 DECLARE features CURSOR 
		FOR (select id from feature where FIND_IN_SET(id,@features_list));
 DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;
 
SET @features_list = (select group_concat(feature.id SEPARATOR ",") from feature JOIN featureroletype ON (featureroletype.Feature_id =feature.id  and featureroletype.RoleType_id = _organisationType) 
                      where (FIND_IN_SET(feature.id,_features))) ; 
SET @features_list = IF(@features_list is null, '', @features_list);

OPEN features; 
 getFeature: LOOP
        FETCH features INTO featureId;
        IF finished = 1 THEN 
            LEAVE getFeature;
	    else
			DELETE FROM organisationfeatures WHERE (organisationfeatures.organisationId COLLATE utf8_general_ci =_organisationId COLLATE utf8_general_ci AND 
            organisationfeatures.featureId COLLATE utf8_general_ci = featureId COLLATE utf8_general_ci);
			set featuresList = CONCAT(featureId,",",featuresList);
			ITERATE  getFeature;
        END IF;
END LOOP getFeature;
CLOSE features;
  
SET featuresList = (select SUBSTRING(featuresList FROM 1 FOR (CHAR_LENGTH(featuresList)-1)));
select featuresList;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `organisation_features_suspend_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `organisation_features_suspend_proc`(
IN _features TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _organisationType VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _organisationId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
 DECLARE finished INTEGER DEFAULT 0;
 DECLARE featureId varchar(255) CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT "";
 DECLARE featuresList TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci DEFAULT "";

 DECLARE features CURSOR 
		FOR (select id from feature where FIND_IN_SET(id,@features_list));
 DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;
        
UPDATE organisationfeatures SET featureStatus = NULL WHERE organisationId = _organisationId;

SET @features_list = (select group_concat(feature.id SEPARATOR ",") from feature JOIN featureroletype ON (featureroletype.Feature_id =feature.id and featureroletype.RoleType_id = _organisationType) 
                      where (FIND_IN_SET(feature.id,_features))) ; 
SET @features_list = IF(@features_list is null, '', @features_list);

OPEN features; 
 getFeature: LOOP
        FETCH features INTO featureId;
        IF finished = 1 THEN 
            LEAVE getFeature;
	    else
			UPDATE organisationfeatures set featureStatus = "SID_FEATURE_SUSPENDED" WHERE (organisationfeatures.organisationId COLLATE utf8_general_ci =_organisationId AND 
            organisationfeatures.featureId COLLATE utf8_general_ci = featureId COLLATE utf8_general_ci);
			set featuresList = CONCAT(featureId,",",featuresList);
			ITERATE  getFeature;
        END IF;
END LOOP getFeature;
CLOSE features;
  
SET featuresList = (select SUBSTRING(featuresList FROM 1 FOR (CHAR_LENGTH(featuresList)-1)));
select featuresList;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `organization_actions_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `organization_actions_proc`(
in _organizationId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _actionType varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _actionId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET @select_statement = concat("SELECT
	`feature`.`id` AS `featureId`,
    `feature`.`name` AS `featureName`,
    `feature`.`description` AS `featureDescription`,
    `feature`.`Status_id` AS `fiFeatureStatus`,
    `organisationfeatures`.`featureStatus` AS `orgFeatureStatus`,
    `featureaction`.`Type_id` AS `actionType`,
    `featureaction`.`id` AS `actionId`,
    `featureaction`.`name` AS `actionName`,
    `featureaction`.`description` AS `actionDescription`,	
    `featureaction`.`isAccountLevel` AS `isAccountLevel`,
	`actionlimit`.`LimitType_id` AS `limitTypeId`,
    `organisationactionlimit`.`value` AS `orgLimitValue`,
    `actionlimit`.`value` AS `fiLimitValue`
	FROM
		(`organisationactionlimit`
		LEFT JOIN `featureaction` ON (`featureaction`.`id` = `organisationactionlimit`.`Action_id`)
		LEFT JOIN `feature` ON (`feature`.`id` = `featureaction`.`Feature_id`)
        LEFT JOIN `organisationfeatures` ON (`organisationfeatures`.`featureId` = `feature`.`id`)
        LEFT JOIN `actionlimit` ON (`actionlimit`.`Action_id` = `organisationactionlimit`.`Action_id` and `actionlimit`.`LimitType_id` = `organisationactionlimit`.`LimitType_id`))
	where `organisationactionlimit`.`Organisation_id` = ",quote(_organizationId)," and `organisationfeatures`.`organisationId` = ",quote(_organizationId),"");
    
	IF (_actionType != '' ) THEN
		set @select_statement =  concat(@select_statement ," and `featureaction`.`Type_id` = ",quote(_actionType));
    END IF;   
    
    IF (_actionId != '' ) THEN
		set @select_statement =  concat(@select_statement ," and `featureaction`.`id` = ",quote(_actionId));
    END IF;
        
PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `permissionlegalentity_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `permissionlegalentity_delete_proc`(
in _permissionId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
DELETE FROM `permissionlegalentity` where permissionId = _permissionId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `prospect_securityattributes_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `prospect_securityattributes_get_proc`(in _userId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN

SET SESSION group_concat_max_len = 10000000;

SET @userAssociatedGroups =  (SELECT group_concat(distinct customergroup.Group_id SEPARATOR ",") FROM customergroup WHERE customergroup.Customer_id = _userId);


SET @actionsAtGroups = (SELECT group_concat(distinct groupactionlimit.Action_id SEPARATOR ",") FROM groupactionlimit WHERE FIND_IN_SET(groupactionlimit.Group_id,@userAssociatedGroups));


SET @activeFeaturesAtFI = (SELECT group_concat(distinct feature.id SEPARATOR ",") FROM feature WHERE Status_id = 'SID_FEATURE_ACTIVE'); 

SET @intersectedUserActions = (SELECT group_concat(distinct featureaction.id SEPARATOR ",") FROM featureaction WHERE 
                                featureaction.status = 'SID_ACTION_ACTIVE' AND
                                FIND_IN_SET(featureaction.id,@actionsAtGroups) AND
                                FIND_IN_SET(featureaction.Feature_id,@activeFeaturesAtFI));
                               
SELECT @intersectedUserActions AS actions;

SET @intersectedUserFeatures = (SELECT group_concat(distinct featureaction.Feature_id SEPARATOR ",") FROM featureaction WHERE 
                                  FIND_IN_SET(featureaction.id,@intersectedUserActions));
								  
SELECT @intersectedUserFeatures AS features;
                       
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reports_messages_received` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `reports_messages_received`(
  in from_date VARCHAR(19), 
  in _todate VARCHAR(19), 
  in category_id VARCHAR(50), 
  in csr_name VARCHAR(50)
)
BEGIN 
set 
  @queryStatement = "
  SELECT
    count(id) messages_received_count 
  FROM
    requestmessage 
  where
    createdby not in 
    (
      select
        Username 
      from
        systemuser
    )
    ";
if from_date != '' 
and from_date != '' then 
set 
  @queryStatement = concat(
    @queryStatement, 
    "
    and createdts >= ", 
    quote(STR_TO_DATE(from_date, '%m/%d/%Y %H:%i:%s')), 
    " and createdts <= ", 
    quote(STR_TO_DATE(_todate, '%m/%d/%Y %H:%i:%s'))
  );
end if;
if category_id != '' then 
set 
  @queryStatement = concat(
    @queryStatement, " 
    and CustomerRequest_id in 
    (
      select
        id 
      from
        customerrequest 
      where
        RequestCategory_id = ", 
    quote(category_id), "
    )
    "
  );
end if;
if csr_name != '' then 
set 
  @queryStatement = concat(
    @queryStatement, " 
    and RepliedBy_id = ", 
    quote(csr_name)
  );
end if;
PREPARE stmt 
FROM 
  @queryStatement;
EXECUTE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reports_messages_sent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `reports_messages_sent`(
  in from_date VARCHAR(19), 
  in _todate VARCHAR(19), 
  in category_id VARCHAR(50), 
  in csr_name VARCHAR(50)
)
BEGIN 
set 
  @queryStatement = "
    SELECT
      count(id) messages_sent_count 
    FROM
      requestmessage 
    where
      createdby in 
      (
        select
          Username 
        from
          systemuser
      )
      ";
if from_date != '' 
and from_date != '' then 
set 
  @queryStatement = concat(
    @queryStatement, 
    "
      and createdts >= ", 
    quote(STR_TO_DATE(from_date, '%m/%d/%Y %H:%i:%s')), 
    " and createdts <= ", 
    quote(STR_TO_DATE(_todate, '%m/%d/%Y %H:%i:%s'))
  );
end if;
if category_id != '' then 
set 
  @queryStatement = concat(
    @queryStatement, " 
      and CustomerRequest_id in 
      (
        select
          id 
        from
          customerrequest 
        where
          RequestCategory_id = ", 
    quote(category_id), "
      )
      "
  );
end if;
if csr_name != '' then 
set 
  @queryStatement = concat(
    @queryStatement, " 
      and RepliedBy_id = ", 
    quote(csr_name)
  );
end if;
PREPARE stmt 
FROM 
  @queryStatement;
EXECUTE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reports_threads_averageage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `reports_threads_averageage`(
  in from_date VARCHAR(19), 
  in _todate VARCHAR(19), 
  in category_id VARCHAR(50), 
  in csr_name VARCHAR(50)
)
BEGIN 
set 
  @queryStatement = "
      select
        AVG(thread_life_records.thread_life) threads_averageage_count 
      from
        (
          select
            DATEDIFF(MAX(createdts), MIN(createdts)) thread_life 
          from
            requestmessage 
          where
            true ";
if from_date != '' 
and from_date != '' then 
set 
  @queryStatement = concat(
    @queryStatement, 
    " 
            and createdts >= ", 
    quote(STR_TO_DATE(from_date, '%m/%d/%Y %H:%i:%s')), 
    " and createdts <= ", 
    quote(STR_TO_DATE(_todate, '%m/%d/%Y %H:%i:%s'))

  );
end if;
if category_id != '' then 
set 
  @queryStatement = concat(
    @queryStatement, " 
            and CustomerRequest_id in 
            (
              select
                id 
              from
                customerrequest 
              where
                RequestCategory_id = ", 
    quote(category_id), "
            )
            "
  );
end if;
if csr_name != '' then 
set 
  @queryStatement = concat(
    @queryStatement, " 
            and RepliedBy_id = ", 
    quote(csr_name)
  );
end if;
set 
  @queryStatement = concat(
    @queryStatement, " 
          group by
            CustomerRequest_id) thread_life_records"
  );
PREPARE stmt 
FROM 
  @queryStatement;
EXECUTE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reports_threads_new` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `reports_threads_new`(
  in from_date VARCHAR(19), 
  in _todate VARCHAR(19), 
  in category_id VARCHAR(50), 
  in csr_name VARCHAR(50)
)
BEGIN 
set 
  @queryStatement = "
            SELECT
              count(id) threads_new_count 
            from
              customerrequest 
            where
              Status_id = 'SID_OPEN'";
if from_date != '' 
and from_date != '' then 
set 
  @queryStatement = concat(
    @queryStatement, 
    " 
              and createdts >= ", 
    quote(STR_TO_DATE(from_date, '%m/%d/%Y %H:%i:%s')), 
    " and createdts <= ", 
    quote(STR_TO_DATE(_todate, '%m/%d/%Y %H:%i:%s'))
  );
end if;
if category_id != '' then 
set 
  @queryStatement = concat(
    @queryStatement, " 
              and RequestCategory_id = ", 
    quote(category_id)
  );
end if;
if csr_name != '' then 
set 
  @queryStatement = concat(
    @queryStatement, " 
              and AssignedTo = ", 
    quote(csr_name)
  );
end if;
PREPARE stmt 
FROM 
  @queryStatement;
EXECUTE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `reports_threads_resolved` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `reports_threads_resolved`(
  in from_date VARCHAR(19), 
  in to_date VARCHAR(19), 
  in category_id VARCHAR(50), 
  in csr_name VARCHAR(50)
)
BEGIN 
set 
  @queryStatement = "
              SELECT
                count(id) threads_resolved_count 
              from
                customerrequest 
              where
                Status_id = 'SID_RESOLVED'";
if from_date != '' 
and from_date != '' then 
set 
  @queryStatement = concat(
    @queryStatement, 
    " 
                and createdts >= ", 
    quote(STR_TO_DATE(from_date, '%m/%d/%Y %H:%i:%s')), 
    " and createdts <= ", 
    quote(STR_TO_DATE(to_date, '%m/%d/%Y %H:%i:%s'))
  );
end if;
if category_id != '' then 
set 
  @queryStatement = concat(
    @queryStatement, " 
                and RequestCategory_id = ", 
    quote(category_id)
  );
end if;
if csr_name != '' then 
set 
  @queryStatement = concat(
    @queryStatement, " 
                and AssignedTo = ", 
    quote(csr_name)
  );
end if;
PREPARE stmt 
FROM 
  @queryStatement;
EXECUTE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `requestmessages_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `requestmessages_proc`(
in _customerRequestID varchar(50)  CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _fetchDraftMessages tinyint(1) 
)
BEGIN

    SELECT 
        `requestmessage`.`id` AS `Message_id`,
        (SELECT 
                COUNT(`messageattachment`.`id`)
            FROM
                `messageattachment`
            WHERE
                (`messageattachment`.`RequestMessage_id` = `requestmessage`.`id`)) AS `totalAttachments`,
        `messageattachment`.`id` AS `Messageattachment_id`,
        `messageattachment`.`Media_id` AS `Media_id`,
        `media`.`Name` AS `Media_Name`,
        `media`.`Type` AS `Media_Type`,
        `media`.`Size` AS `Media_Size`,
        `requestmessage`.`CustomerRequest_id` AS `CustomerRequest_id`,
        `requestmessage`.`RepliedBy` AS `RepliedBy`,
        `requestmessage`.`MessageDescription` AS `MessageDescription`,
        `requestmessage`.`ReplySequence` AS `ReplySequence`,
        `requestmessage`.`IsRead` AS `IsRead`,
        `requestmessage`.`createdby` AS `createdby`,
        `requestmessage`.`RepliedBy_Name` AS `createdby_name`,
        `requestmessage`.`modifiedby` AS `modifiedby`,
        `requestmessage`.`createdts` AS `createdts`,
        `requestmessage`.`lastmodifiedts` AS `lastmodifiedts`,
        `requestmessage`.`synctimestamp` AS `synctimestamp`,
        `requestmessage`.`softdeleteflag` AS `softdeleteflag`
    FROM
        ((`requestmessage`
        LEFT JOIN `messageattachment` ON ((`requestmessage`.`id` = `messageattachment`.`RequestMessage_id`)))
        LEFT JOIN `media` ON ((`messageattachment`.`Media_id` = `media`.`id`)))
	WHERE `requestmessage`.`CustomerRequest_id` = _customerRequestID and If(_fetchDraftMessages = 1, true, 
    `requestmessage`.`IsRead` != 'draft' )
    ORDER BY `requestmessage`.`ReplySequence` asc;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `retail_accounts_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `retail_accounts_proc`(
in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
SET @username = (SELECT UserName from customer where id=_customerId);
IF @username is null THEN 
  SET @username = "";
END IF;
SELECT Account_id,AccountName FROM accounts WHERE JSON_VALID(AccountHolder) AND (JSON_EXTRACT(AccountHolder, "$.username") =@username);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `rolePermissionDelete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `rolePermissionDelete_proc`(
IN _roleId VARCHAR(50) CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI, 
IN _PermissionIds VARCHAR(5000) CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI
)
BEGIN
DECLARE caid VARCHAR(50);
DECLARE isEnabled VARCHAR(10);
DECLARE finished INTEGER DEFAULT 0 ;
DECLARE caids CURSOR FOR (SELECT c.id, c.isEnabled FROM compositeaction c WHERE FIND_IN_SET(`Permission_id` COLLATE UTF8_GENERAL_CI, _PermissionIds));
DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;
SET SQL_SAFE_UPDATES = 0;
OPEN caids; 
MANAGECAIDS : LOOP
FETCH caids INTO caid, isEnabled;
IF finished = 1 THEN 
    LEAVE MANAGECAIDS;
END IF;
SET @caid_count =(SELECT COUNT(*) FROM compositeaction c,  rolepermission rp WHERE rp.Role_id COLLATE UTF8_GENERAL_CI = _roleId
AND rp.Permission_id COLLATE UTF8_GENERAL_CI =c.Permission_id COLLATE UTF8_GENERAL_CI
AND NOT FIND_IN_SET(rp.Permission_id COLLATE UTF8_GENERAL_CI ,_PermissionIds)
AND c.id COLLATE UTF8_GENERAL_CI=caid COLLATE UTF8_GENERAL_CI);

IF @caid_count=0 THEN
DELETE FROM rolecompositeaction WHERE Role_id COLLATE UTF8_GENERAL_CI =_roleId AND CompositeAction_id COLLATE UTF8_GENERAL_CI=caid COLLATE UTF8_GENERAL_CI;
END IF;
END LOOP MANAGECAIDS;
CLOSE caids;

DELETE FROM rolepermission WHERE Role_id COLLATE UTF8_GENERAL_CI=_roleId AND FIND_IN_SET(Permission_id COLLATE UTF8_GENERAL_CI,_PermissionIds);
SET SQL_SAFE_UPDATES = 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `rolepermissionou_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `rolepermissionou_delete_proc`(
in _roleId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
DELETE FROM rolepermissionou where rolepermissionou.roleId = _roleId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `rolescompositeactions_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `rolescompositeactions_proc`(
IN _roleIds varchar(500) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _isEnabled TINYINT(1))
BEGIN
SELECT 
    `CompositeAction_id`,CASE WHEN `isEnabled` = '1' THEN 'true' ELSE 'false' END AS `isEnabled`
FROM
    `rolecompositeaction`
WHERE
    FIND_IN_SET(`role_id`, _roleIds)
        AND `isEnabled` = ifnull(_isEnabled,1) 
UNION SELECT 
    `CompositeAction_id`, CASE WHEN `isEnabled` = '1' THEN 'true' ELSE 'false' END AS `isEnabled`
FROM
    `rolecompositeaction`
WHERE
    FIND_IN_SET(`role_id`, _roleIds)
        AND `CompositeAction_id` NOT IN (SELECT 
            `CompositeAction_id`
        FROM
            `rolecompositeaction`
        WHERE
            FIND_IN_SET(`role_id`, _roleIds)
                AND `isEnabled` = ifnull(_isEnabled,1))
        AND `isEnabled` = ifnull(_isEnabled,0) ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `servicedefinition_action_limits_update_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `servicedefinition_action_limits_update_proc`(
  IN _action varchar(255) CHARACTER SET UTF8 COLLATE utf8_general_ci,
  IN _maxTxLimit decimal(20,2),
  IN _dailyLimit decimal(20,2),
  IN _weeklyLimit decimal(20,2),
  IN _contractIdValues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
  DECLARE index1 INTEGER DEFAULT 0;
  set @numOfRecords = LENGTH(_contractIdValues) - LENGTH(REPLACE(_contractIdValues, ',', '')) + 1;
   updateRecords : LOOP 
     set index1 = index1 + 1;
		IF index1 = @numOfRecords + 1 THEN 
			LEAVE updateRecords;
  		else
		   set @contractId = SUBSTRING_INDEX(SUBSTRING_INDEX(_contractIdValues, ',', index1), ',', -1 );
           UPDATE contractactionlimit SET value = _maxTxLimit where contractId = @contractId AND actionId = _action AND limitTypeId = 'MAX_TRANSACTION_LIMIT' AND value > _maxTxLimit; 
           UPDATE contractactionlimit SET value = _dailyLimit where contractId = @contractId AND actionId = _action AND limitTypeId = 'DAILY_LIMIT' AND value > _dailyLimit;
           UPDATE contractactionlimit SET value = _weeklyLimit where contractId = @contractId AND actionId = _action AND limitTypeId = 'WEEKLY_LIMIT' AND value > _weeklyLimit; 
           UPDATE customeraction SET value = _maxTxLimit where contractId = @contractId AND Action_id = _action AND LimitType_id = 'MAX_TRANSACTION_LIMIT' AND value > _maxTxLimit; 
           UPDATE customeraction SET value = _dailyLimit where contractId = @contractId AND Action_id = _action AND LimitType_id = 'DAILY_LIMIT' AND value > _dailyLimit;
           UPDATE customeraction SET value = _weeklyLimit where contractId = @contractId AND Action_id = _action AND LimitType_id = 'WEEKLY_LIMIT' AND value > _weeklyLimit; 
		   END IF;
	END LOOP updateRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `servicedefinition_defaultgroup_update_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `servicedefinition_defaultgroup_update_proc`(
 IN _serviceDefinitionId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
 IN _groupId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
 IN _isDefault varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
 )
BEGIN
 IF(_isDefault = '0') THEN
     UPDATE groupservicedefinition SET isDefaultGroup = false where serviceDefinitionId = _serviceDefinitionId AND Group_id = _groupId ;
 ELSE
     UPDATE groupservicedefinition SET isDefaultGroup = false where serviceDefinitionId = _serviceDefinitionId;
     UPDATE groupservicedefinition SET isDefaultGroup = true where serviceDefinitionId = _serviceDefinitionId AND Group_id = _groupId;     
 END IF;
 END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `servicedefinition_delete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `servicedefinition_delete_proc`(
	IN _servicedefinitionid VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
DELETE FROM `groupservicedefinition` WHERE  serviceDefinitionId = _servicedefinitionid;
DELETE FROM `servicedefinitionactionlimit` WHERE serviceDefinitionId = _servicedefinitionid;
DELETE FROM `userroleservicedefinition`  WHERE serviceDefinitionId = _servicedefinitionid;
DELETE FROM `servicedefinition`  WHERE id = _servicedefinitionid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `servicedefinition_remove_actions_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `servicedefinition_remove_actions_proc`(
  IN _action varchar(255) CHARACTER SET UTF8 COLLATE utf8_general_ci,
  IN _contractIdValues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
  DECLARE index1 INTEGER DEFAULT 0;
  set @numOfRecords = LENGTH(_contractIdValues) - LENGTH(REPLACE(_contractIdValues, ',', '')) + 1;
   deleteRecords : LOOP 
     set index1 = index1 + 1;
		IF index1 = @numOfRecords + 1 THEN 
			LEAVE deleteRecords;
  		else
		   set @contractId = SUBSTRING_INDEX(SUBSTRING_INDEX(_contractIdValues, ',', index1), ',', -1 );
		   delete from contractactionlimit where contractId = @contractId AND actionId = _action;
		   delete from customeraction where contractId = @contractId AND Action_id = _action;
		   END IF;
	END LOOP deleteRecords;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `signatorygroup_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `signatorygroup_create_proc`(
    IN signatoryGroupValues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN customerSignatoryGroupValues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
DECLARE index1 INTEGER DEFAULT 0;
set @matrixRecord = SUBSTRING_INDEX(signatoryGroupValues, ',', 1 );
set @matrixComma = REPLACE(@matrixRecord, ';', ',');
set @query = concat('INSERT INTO signatorygroup(signatoryGroupId,signatoryGroupName,signatoryGroupDescription,coreCustomerId,contractId,createdby) values (',@matrixComma,');');
prepare sql_query from @query;
execute sql_query;   
IF customerSignatoryGroupValues IS NOT NULL AND customerSignatoryGroupValues != '' THEN
 set @length1 = LENGTH(customerSignatoryGroupValues) - LENGTH(REPLACE(customerSignatoryGroupValues, ',', ''));
 set index1 = 0;
 addSignatories: LOOP
 set index1 = index1 + 1;
   IF index1 = @length1 + 1 THEN
    LEAVE addSignatories;
   ELSE
    set @signatory = SUBSTRING_INDEX(SUBSTRING_INDEX(customerSignatoryGroupValues, ',', index1), ',', -1);
    set @signatoriesComma = REPLACE(@signatory, ';', ',');
    set @query = concat('INSERT INTO customersignatorygroup(customerSignatoryGroupId, signatoryGroupId, customerId, createdby) values (',@signatoriesComma,');');
	prepare sql_query from @query;
    execute sql_query; 
	ITERATE addSignatories;
    END IF;
    END LOOP addSignatories;
END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `signatorygroup_update_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `signatorygroup_update_proc`(
   IN _sigGroupValues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
   IN _newSigValues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,
   IN _deleteSigValues TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
DECLARE index1 INTEGER DEFAULT 0;
set @sigGroupId = SUBSTRING_INDEX(_sigGroupValues, ';', 1 );
set @SigGroupName = SUBSTRING_INDEX(SUBSTRING_INDEX(_sigGroupValues, ';', 2 ),';',-1);
set @SigGroupDes = SUBSTRING_INDEX(SUBSTRING_INDEX(_sigGroupValues, ';', 3 ),';',-1);
set @sigCreatedBy = SUBSTRING_INDEX(_sigGroupValues, ';', -1 );
IF @SigGroupName IS NOT NULL AND @SigGroupName != '' THEN
set @query = concat('UPDATE signatorygroup SET signatoryGroupName = ',@SigGroupName,' WHERE signatoryGroupId = ',@sigGroupId,'');
prepare sql_query from @query;
execute sql_query;
END IF;
IF @sigGroupDes IS NOT NULL AND @sigGroupDes != '' THEN
set @query = concat('UPDATE signatorygroup SET signatoryGroupDescription = ',@sigGroupDes,',lastmodifiedts = CURRENT_TIMESTAMP, modifiedby = ',@sigCreatedBy,'  WHERE signatoryGroupId = ',@sigGroupId,'');
prepare sql_query from @query;
execute sql_query;
END IF;
IF _newSigValues IS NOT NULL AND _newSigValues != '' THEN
set @length1 = LENGTH(_newSigValues) - LENGTH(REPLACE(_newSigValues, ',', ''))+1;
set index1 = 0;
addSignatories: LOOP
set index1 = index1 + 1;
IF index1 = @length1 + 1 THEN
LEAVE addSignatories;
ELSE
set @signatory = SUBSTRING_INDEX(SUBSTRING_INDEX(_newSigValues, ',', index1), ',', -1);
set @signatoriesComma = REPLACE(@signatory, ';', ',');
set @query = concat('INSERT INTO customersignatorygroup(customerSignatoryGroupId, signatoryGroupId, customerId, createdby) values (',@signatoriesComma,');');
prepare sql_query from @query;
execute sql_query;
ITERATE addSignatories;
END IF;
END LOOP addSignatories;
END IF;
IF _deleteSigValues IS NOT NULL AND _deleteSigValues != '' THEN
set @length1 = LENGTH(_deleteSigValues) - LENGTH(REPLACE(_deleteSigValues, ',', ''))+1;
set index1 = 0;
deleteSignatories: LOOP
set index1 = index1 + 1;
IF index1 = @length1 + 1 THEN
LEAVE deleteSignatories;
ELSE
set @signatory = SUBSTRING_INDEX(SUBSTRING_INDEX(_deleteSigValues, ',', index1), ',', -1);
set @sigGroupId = SUBSTRING_INDEX(@signatory, ';', 1 );
set @custId = SUBSTRING_INDEX(SUBSTRING_INDEX(@signatory, ';', 2 ),';',-1);
set @query = concat('DELETE FROM customersignatorygroup WHERE signatoryGroupId =',@sigGroupId,'AND customerId=',@custId,'');
prepare sql_query from @query;
execute sql_query;
ITERATE deleteSignatories;
END IF;
END LOOP deleteSignatories;
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_deleteCoApplicantData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_deleteCoApplicantData`(IN sectionsToDelete varchar(200), IN applicationID varchar(100))
BEGIN
SET @FirstPart = "update questionresponse set softdeleteflag =1 where QuerySectionQuestion_id in (select id from querysectionquestion where QuerySection_id IN (";
SET @Query = CONCAT(@FirstPart, sectionsToDelete, ")) AND questionresponse.QueryResponse_id = \'" ,applicationID, "\';");
PREPARE queryStatement FROM @Query;
EXECUTE queryStatement;
DEALLOCATE PREPARE queryStatement;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getAllApplications` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_getAllApplications`(
    IN `User_id` VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, IN `Status_id` VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
IF Status_id = 'PENDING' THEN

(SELECT
    queryresp.id AS id,
    queryresp.QueryDefinition_id AS QueryDefinition_id,
    queryresp.Customer_id AS User_id,
    queryresp.CoBorrower_id AS CoBorrower_id,
    queryresp.createdts AS StartDate,
    queryresp.lastmodifiedts AS LastEditedDate,
    queryresp.LoanProduct_id AS LoanProduct_id,
    queryresp.Application_id AS Application_id,
    loanProd.LoanType_id AS LoanType_id,
    queryresp.Status_id AS Status,
    querycoborrower.CoBorrower_Type AS CoBorrower_Type,
    (SELECT
            CAST(CONCAT('[',
                            GROUP_CONCAT(JSON_OBJECT('QuerySectionQuestion_id',
                                        questResp.QuerySectionQuestion_id,
                                        'ResponseValue',
                                        questResp.ResponseValue)
                                SEPARATOR ','),
                            ']')
                    AS CHAR CHARSET UTF8MB4)
        FROM
            `questionresponse` questResp
        WHERE
            questResp.queryresponse_id = queryresp.id
                 AND `questResp`.`QuerySectionQuestion_id` IN ('VA_LOAN_AMOUNT' , 'VA_COBORROWER',
                'CCA_CARDLIMIT',
                'PA_LOANAMOUNT',
                'PA_COBORROWER')) AS `QuestionResponse`
FROM
    `queryresponse` queryresp
        JOIN
    loanproduct loanProd ON queryresp.LoanProduct_id = loanProd.id
        LEFT JOIN
    querycoborrower querycoborrower ON queryresp.id = querycoborrower.queryresponse_id
WHERE
    (`queryresp`.`Customer_id` = User_id COLLATE utf8_unicode_ci)
        AND `queryresp`.`QueryDefinition_id` IN ('PERSONAL_APPLICATION' , 'VEHICLE_APPLICATION',
        'CREDIT_CARD_APPLICATION') AND `queryresp`.softdeleteflag IS FALSE) UNION ALL (SELECT
    queryresp.id AS id,
    queryresp.QueryDefinition_id AS QueryDefinition_id,
    queryresp.Customer_id AS User_id,
    queryresp.CoBorrower_id AS CoBorrower_id,
    queryresp.createdts AS StartDate,
    queryresp.lastmodifiedts AS LastEditedDate,
    queryresp.LoanProduct_id AS LoanProduct_id,
    queryresp.Application_id AS Application_id,
    loanProd.LoanType_id AS LoanType_id,
    queryresp.Status_id AS STATUS,
    querycoborrower.CoBorrower_Type AS CoBorrower_Type,
    (SELECT
            CAST(CONCAT('[',
                            GROUP_CONCAT(JSON_OBJECT('QuerySectionQuestion_id',
                                        questResp.QuerySectionQuestion_id,
                                        'ResponseValue',
                                        questResp.ResponseValue)
                                SEPARATOR ','),
                            ']')
                    AS CHAR CHARSET UTF8MB4)
        FROM
            `questionresponse` questResp
        WHERE
            questResp.queryresponse_id = queryresp.id
                AND `questResp`.`QuerySectionQuestion_id` IN ('VA_LOAN_AMOUNT', 'VA_COBORROWER',
                'CCA_CARDLIMIT',
                'PA_LOANAMOUNT',
                'PA_COBORROWER')) AS `QuestionResponse`
FROM
    `queryresponse` queryresp
        JOIN
    loanproduct loanProd ON queryresp.LoanProduct_id = loanProd.id
        LEFT JOIN
    querycoborrower querycoborrower ON queryresp.id = querycoborrower.queryresponse_id
WHERE
    (`queryresp`.`CoBorrower_id` = User_id COLLATE utf8_unicode_ci)
        AND `queryresp`.`QueryDefinition_id` IN ('PERSONAL_APPLICATION' , 'VEHICLE_APPLICATION',
        'CREDIT_CARD_APPLICATION') AND `queryresp`.softdeleteflag IS FALSE) ORDER BY LastEditedDate DESC;
ELSE
           (SELECT
    queryresp.id AS id,
    queryresp.QueryDefinition_id AS QueryDefinition_id,
    queryresp.Customer_id AS User_id,
    queryresp.CoBorrower_id AS CoBorrower_id,
    queryresp.createdts AS StartDate,
    losapps.lastupdatedts AS LastEditedDate,
    queryresp.LoanProduct_id AS LoanProduct_id,
    queryresp.Application_id AS Application_id,
    loanProd.LoanType_id AS LoanType_id,
    queryresp.Status_id AS Status,
    querycoborrower.CoBorrower_Type AS CoBorrower_Type,
    (SELECT
            CAST(CONCAT('[',
                            GROUP_CONCAT(JSON_OBJECT('QuerySectionQuestion_id',
                                        questResp.QuerySectionQuestion_id,
                                        'ResponseValue',
                                        questResp.ResponseValue)
                                SEPARATOR ','),
                            ']')
                    AS CHAR CHARSET UTF8MB4)
        FROM
            `questionresponse` questResp
        WHERE
            questResp.queryresponse_id = queryresp.id
                 AND `questResp`.`QuerySectionQuestion_id` IN ('VA_LOAN_AMOUNT' , 'VA_COBORROWER',
                'CCA_CARDLIMIT',
                'PA_LOANAMOUNT',
                'PA_COBORROWER')) AS `QuestionResponse`
FROM
    `queryresponse` queryresp
        JOIN
    loanproduct loanProd ON queryresp.LoanProduct_id = loanProd.id
        LEFT JOIN
    querycoborrower querycoborrower ON queryresp.id = querycoborrower.queryresponse_id
		LEFT JOIN
	losapplications losapps ON queryresp.id = losapps.queryresponse_id
WHERE
    (`queryresp`.`Customer_id` = User_id COLLATE utf8_unicode_ci)
    AND `queryresp`.`Status_id` = Status_id
        AND `queryresp`.`QueryDefinition_id` IN ('PERSONAL_APPLICATION' , 'VEHICLE_APPLICATION',
        'CREDIT_CARD_APPLICATION') AND `queryresp`.softdeleteflag IS FALSE) UNION ALL (SELECT
    queryresp.id AS id,
    queryresp.QueryDefinition_id AS QueryDefinition_id,
    queryresp.Customer_id AS User_id,
    queryresp.CoBorrower_id AS CoBorrower_id,
    queryresp.createdts AS StartDate,
    losapps.lastupdatedts AS LastEditedDate,
    queryresp.LoanProduct_id AS LoanProduct_id,
    queryresp.Application_id AS Application_id,
    loanProd.LoanType_id AS LoanType_id,
    queryresp.Status_id AS STATUS,
    querycoborrower.CoBorrower_Type AS CoBorrower_Type,
    (SELECT
            CAST(CONCAT('[',
                            GROUP_CONCAT(JSON_OBJECT('QuerySectionQuestion_id',
                                        questResp.QuerySectionQuestion_id,
                                        'ResponseValue',
                                        questResp.ResponseValue)
                                SEPARATOR ','),
                            ']')
                    AS CHAR CHARSET UTF8MB4)
        FROM
            `questionresponse` questResp
        WHERE
            questResp.queryresponse_id = queryresp.id
                AND `questResp`.`QuerySectionQuestion_id` IN ('VA_LOAN_AMOUNT', 'VA_COBORROWER',
                'CCA_CARDLIMIT',
                'PA_LOANAMOUNT',
                'PA_COBORROWER')) AS `QuestionResponse`
FROM
    `queryresponse` queryresp
        JOIN
    loanproduct loanProd ON queryresp.LoanProduct_id = loanProd.id
        LEFT JOIN
    querycoborrower querycoborrower ON queryresp.id = querycoborrower.queryresponse_id
		LEFT JOIN
	losapplications losapps ON queryresp.id = losapps.queryresponse_id
WHERE
    (`queryresp`.`CoBorrower_id` = User_id COLLATE utf8_unicode_ci)
    AND `queryresp`.`Status_id` = Status_id
        AND `queryresp`.`QueryDefinition_id` IN ('PERSONAL_APPLICATION' , 'VEHICLE_APPLICATION',
        'CREDIT_CARD_APPLICATION') AND `queryresp`.softdeleteflag IS FALSE) ORDER BY LastEditedDate DESC;
        END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getAPRs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_getAPRs`()
BEGIN

SELECT 

    loantype.id AS LoanType_id,

    loantype.APRValue AS APRValue,

	(select cast(concat('[',group_concat(json_object('LoanProduct_id',lp.id , 'APR',lp.APR, 'LoanType_id',lp.LoanType_id )separator ','),']') as char charset utf8mb4)from `loanproduct` lp where lp.loantype_id =loantype.id) AS LoanProduct

FROM

    loantype loantype;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getCustomerPrequalifyPackage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_getCustomerPrequalifyPackage`(IN Customer_id varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN

SELECT 
cpp.id as id, 
cpp.Customer_id as Customer_id,
cpp.PrequalifyPackage_id as PrequalifyPackage_id,
cpp.createdby as createdby,
cpp.modifiedby as modifiedby,
cpp.createdts as createdts,
cpp.lastmodifiedts as lastmodifiedts,
cpp.synctimestamp as synctimestamp,
cpp.softdeleteflag as softdeleteflag,
(select cast(concat('[',group_concat(json_object('id',pp.id ,'LoanType_id',pp.LoanType_id,'LoanProduct_id',pp.LoanProduct_id,'Name',pp.Name,'Code',pp.Code,'Description',pp.Description,'LoanAmount',pp.LoanAmount,'LoanTerms',pp.LoanTerms, 'APR',pp.APR,'Rate',pp.rate,'MonthlyPayment',pp.MonthlyPayment,'AnnualFee',pp.AnnualFee,'BenfitsandRewards',pp.BenfitsandRewards,'TransferInformation',pp.TransferInformation,'PrequalifyCondition',pp.PrequalifyCondition,'createdby',pp.createdby,'modifiedby',pp.modifiedby,'createdts',pp.createdts,'lastmodifiedts',pp.lastmodifiedts,'synctimestamp',pp.synctimestamp,'softdeleteflag',pp.softdeleteflag,
'LoanProduct', cast((select concat('[',group_concat(json_object('id',lp.id,'LoanType_id',lp.LoanType_id,'MinLimitAmount',lp.MinLimitAmount,'MaxLimitAmount',lp.MaxLimitAmount,'Name',lp.Name,'Description',lp.Description,'Code',lp.Code,'AtAGlance',lp.AtAGlance,'APR',lp.APR,'AnnualFee',lp.AnnualFee,'Rewards',lp.Rewards,'createdby',lp.createdby,'modifiedby',lp.modifiedby,'createdts',lp.createdts,'lastmodifiedts',lp.lastmodifiedts,'synctimestamp',lp.synctimestamp,'softdeleteflag',lp.softdeleteflag) separator ','),']')from loanproduct lp where lp.id = pp.LoanProduct_id) as json))separator ','),']') as char charset utf8mb4)from `prequalifypackage` pp where pp.id =cpp.PrequalifyPackage_id) AS `PrequalifyPackage`
FROM customerprequalifypackage cpp where cpp.Customer_id = Customer_id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getDecisionFailureData` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_getDecisionFailureData`(IN id_list varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, IN job_id varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, IN top INT, IN skip INT)
BEGIN
IF id_list IS NOT NULL THEN 
SELECT df.id,df.job_id,df.decision_id,df.failureTriggerJob_id,df.exception,cdd.Customer_id,cdd.CreditScore,cdd.EmploymentType,cdd.AnnualIncome,cdd.AccountBalance,cdd.Age,cdd.PrequalifyScore FROM decisionfailure df join customerdpdata cdd on baseAttributeValue=Customer_id where find_in_set(df.id , id_list) order by df.createdts,df.id LIMIT top OFFSET skip;
ELSEIF job_id IS NOT NULL THEN 
SELECT df.id,df.job_id,df.decision_id,cdd.Customer_id,df.exception,cdd.CreditScore,cdd.EmploymentType,cdd.AnnualIncome,cdd.AccountBalance,cdd.Age,cdd.PrequalifyScore FROM decisionfailure df join customerdpdata cdd on baseAttributeValue=Customer_id where df.job_id=job_id order by df.createdts,df.id LIMIT top OFFSET skip;
ELSE
SELECT df.id,df.job_id,df.decision_id,df.failureTriggerJob_id,df.exception,cdd.Customer_id,cdd.CreditScore,cdd.EmploymentType,cdd.AnnualIncome,cdd.AccountBalance,cdd.Age,cdd.PrequalifyScore FROM decisionfailure df join customerdpdata cdd on baseAttributeValue=Customer_id where failureTriggerJob_id is null order by df.createdts,df.id LIMIT top OFFSET skip;
END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getLoanAnswers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_getLoanAnswers`(IN QueryResponseID varchar(100) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
SELECT
      querySecQue.abstractName AS Question_FriendlyName,
      questionResp.id AS QuestionResponse_id,
      questionResp.ResponseValue AS Value,
      questionResp.OptionItem_id AS OptionItem_id 
   FROM
      questionresponse questionResp 
      INNER JOIN
         querysectionquestion querySecQue 
         ON querySecQue.id = questionResp.QuerySectionQuestion_id 
   WHERE
      questionResp.QueryResponse_id = QueryResponseID 
      AND questionResp.softdeleteflag is not true;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getLoanAnswersSectionwise` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_getLoanAnswersSectionwise`(IN QueryResponseID varchar(100) , IN QuerySection_id varchar(100))
BEGIN
SELECT
      querySecQue.abstractName AS Question_FriendlyName,
      questionResp.id AS QuestionResponse_id,
      questionResp.ResponseValue AS Value,
      questionResp.OptionItem_id AS OptionItem_id 
   FROM
      questionresponse questionResp 
      INNER JOIN
         querysectionquestion querySecQue 
         ON querySecQue.id = questionResp.QuerySectionQuestion_id 
   WHERE
      questionResp.QueryResponse_id = QueryResponseID AND questionResp.QuerySection_id = (SELECT id from querysection where abstractname=QuerySection_id AND QueryDefinition_id=questionResp.QueryDefinition_id)
      AND questionResp.softdeleteflag is not true;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getQueryAnswers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_getQueryAnswers`(IN id varchar(50))
BEGIN

SET @FirstPart = "SELECT 
	queryresp.id as id, 
	queryresp.QueryDefinition_id as QueryDefinition_id,
	queryresp.Customer_id as Customer_id,
	queryresp.LoanProduct_id as LoanProduct_id,
	queryresp.Status_id as Status_id,
	queryresp.SubmitDate as SubmitDate,
	queryresp.ClosingDate as ClosingDate,
	queryresp.OverallPercentageCompletion as OverallPercentageCompletion,
	queryresp.createdby as createdby,
	queryresp.modifiedby as modifiedby,
	queryresp.createdts as createdts,
	queryresp.lastmodifiedts as lastmodifiedts,
	queryresp.synctimestamp as synctimestamp,
	queryresp.softdeleteflag as softdeleteflag,
	(select cast(concat('[',group_concat(json_object(
	'id',questResp.id ,
	'QueryDefinition_id',questResp.QueryDefinition_id,
	'QueryResponse_id',questResp.QueryResponse_id,
	'QuestionDefinition_id',questResp.QuestionDefinition_id,
	'QuerySection_id',questResp.QuerySection_id,
	'ArrayIndex',questResp.ArrayIndex,
	'ResponseValue',questResp.ResponseValue,
	'Unit',questResp.Unit,
	'createdby',questResp.createdby,
	'modifiedby',questResp.modifiedby,
	'createdts',questResp.createdts,
	'lastmodifiedts',questResp.lastmodifiedts,
	'synctimestamp',questResp.synctimestamp,
	'softdeleteflag',questResp.softdeleteflag) separator ','),']') as char charset utf8mb4)
	FROM `questionresponse` questResp WHERE questResp.QueryResponse_id = queryresp.id) AS `QuestionResponse`,
	(select cast(concat('[',group_concat(json_object(
	'id',cqss.id ,
	'User_id',cqss.User_id,
	'QueryResponse_id',cqss.QueryResponse_id,
	'QuerySection_id',cqss.QuerySection_id,
	'Status',cqss.Status,
	'PercentageCompletion',cqss.PercentageCompletion,
    'LastQuerySectionQuestion_id',cqss.LastQuerySectionQuestion_id,
	'createdby',cqss.createdby,
	'modifiedby',cqss.modifiedby,
	'createdts',cqss.createdts,
	'lastmodifiedts',cqss.lastmodifiedts,
	'synctimestamp',cqss.synctimestamp,
	'softdeleteflag',cqss.softdeleteflag) separator ','),']') as char charset utf8mb4)
	FROM `customerquerysectionstatus` cqss WHERE cqss.QueryResponse_id = queryresp.id) AS `CustomerQuerySectionStatus`
	FROM `queryresponse` queryresp
WHERE queryresp.id = ";
SET @Query = CONCAT(@FirstPart, "'", id, "';");
PREPARE queryStatement FROM @Query;
EXECUTE queryStatement;
DEALLOCATE PREPARE queryStatement;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getQueryDefinition` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_getQueryDefinition`(IN id varchar(500), IN Parent_id varchar(500))
BEGIN
SET @FirstPart = "SELECT 
    `queryDef`.`id` AS QueryDefinition_id,
    `queryDef`.`Name` AS QueryDefinition_Name,
    `querySecQue`.`id` AS QuerySectionQuestion_id,
    `querySecQue`.`ParentQuerySectionQuestion_id` AS ParentQuerySectionQuestion_id,
    `querySecQue`.`ParentQuestionOptionValue` AS ParentQuestionOptionValue,
    `querySecQue`.`Sequence` AS QuerySectionQuestion_Sequence,
    `querySecQue`.`IsRequired` AS QuerySectionQuestion_IsRequired,
    `querySec`.`id` AS QuerySection_id,
    `querySec`.`Name` AS QuerySection_Name,
    `querySec`.`Sequence` AS QuerySection_Sequence,
	`querySec`.`Parent_id` AS QuerySection_Parent_id,
    `querySec`.`ApplicantAllowedTo` AS QuerySection_ApplicantAllowedTo,
    `querySec`.`IndCoApplicantAllowedTo` AS QuerySection_IndCoApplicantAllowedTo,
    `querySec`.`JointCoApplicantAllowedTo` AS QuerySection_JointCoApplicantAllowedTo,
	`questionDef`.`Name` AS QuestionDefinition_Name,
    `questionDef`.`Label` AS QuestionDefinition_Label,
    `questionDef`.`OtherLabel` AS QuestionDefinition_OtherLabel,
    `questionDef`.`id` AS QuestionDefinition_id,
    `optiongroup`.`Name` AS OptionGroup_Name,
    `optiongroup`.`id` AS OptionGroup_id,
    `optionitem`.`id` AS Optionitem_id,
    `optionitem`.`DefaultValue` AS Optionitem_DefaultValue,
    `optionitem`.`Sequence` AS Optionitem_Sequence
FROM
    querydefinition queryDef
        LEFT JOIN
    querysectionquestion querySecQue ON querySecQue.QueryDefinition_id = queryDef.id
        LEFT JOIN
    querysection querySec ON querySec.id = querySecQue.QuerySection_id
        LEFT JOIN
    questiondefinition questionDef ON querySecQue.QuestionDefinition_id = questionDef.id
        LEFT JOIN
    optiongroup optiongroup ON questionDef.OptionGroup_id = optiongroup.id
        LEFT JOIN
    optionitem optionitem ON optiongroup.id = optionitem.OptionGroup_id
WHERE
    queryDef.id = ";
SET @SecondPart = CONCAT(@FirstPart, "'", id, "'", " AND querySec.softdeleteflag = FALSE AND querySecQue.softdeleteflag = FALSE ");
IF Parent_id = 'COAPPLICANT' THEN
    SET @ThirdPart = CONCAT(@SecondPart, "AND querySec.Parent_id IN ('COAPPLICANT') ");
ELSE 
	IF Parent_id = 'APPLICANT' THEN
		SET @ThirdPart = CONCAT(@SecondPart, "AND querySec.Parent_id IN ('APPLICANT', 'GENERAL') ");
	ELSE 
		SET @ThirdPart = CONCAT(@SecondPart, "");
	END IF;
END IF;
SET @Query = CONCAT(@ThirdPart, "ORDER BY QuerySection_Sequence,QuerySectionQuestion_Sequence,Optionitem_Sequence");
PREPARE queryStatement FROM @Query;
EXECUTE queryStatement;
DEALLOCATE PREPARE queryStatement;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getQueryResponse` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_getQueryResponse`(IN id varchar(50))
BEGIN

SET @FirstPart = "SELECT 
queryresp.id as id, 
queryresp.QueryDefinition_id as QueryDefinition_id,
queryresp.User_id as User_id,
queryresp.LoanProduct_id as LoanProduct_id,
queryresp.Status_id as Status_id,
queryresp.SubmitDate as SubmitDate,
queryresp.ClosingDate as ClosingDate,
queryresp.OverallPercentageCompletion as OverallPercentageCompletion,
queryresp.createdby as createdby,
queryresp.modifiedby as modifiedby,
queryresp.createdts as createdts,
queryresp.lastmodifiedts as lastmodifiedts,
queryresp.synctimestamp as synctimestamp,
queryresp.softdeleteflag as softdeleteflag,
(select cast(concat('[',group_concat(json_object(
'id',cqss.id ,
'User_id',cqss.User_id,
'QueryResponse_id',cqss.QueryResponse_id,
'QuerySection_id',cqss.QuerySection_id,
'Status',cqss.Status,
'PercentageCompletion',cqss.PercentageCompletion,
'createdby',cqss.createdby,
'modifiedby',cqss.modifiedby,
'createdts',cqss.createdts,
'lastmodifiedts',cqss.lastmodifiedts,
'synctimestamp',cqss.synctimestamp,
'softdeleteflag',cqss.softdeleteflag) separator ','),']') as char charset utf8mb4)
FROM `customerquerysectionstatus` cqss WHERE cqss.QueryResponse_id = queryresp.id) AS `CustomerQuerySectionStatus`
FROM `queryresponse` queryresp
WHERE queryresp.id = ";
SET @Query = CONCAT(@FirstPart, "'", id, "';");
PREPARE queryStatement FROM @Query;
EXECUTE queryStatement;
DEALLOCATE PREPARE queryStatement;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getQueryResponseApplicationList` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_getQueryResponseApplicationList`(IN id varchar(20000) CHARACTER SET UTF8 COLLATE utf8_general_ci,IN Status_id varchar(20000) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
SELECT 
    queryresp.id AS id,
    queryresp.QueryDefinition_id AS QueryDefinition_id,
    queryresp.OverallPercentageCompletion AS progress,
    queryresp.Status_id AS Status_id,
    queryresp.createdts AS createdTS,
    queryresp.lastmodifiedts AS modifiedTS,
    loantype.description AS loantype,
    queryresp.CoBorrower_id as CoBorrower_id, 
    (SELECT 
            CAST(CONCAT('[',
                            GROUP_CONCAT(JSON_OBJECT('id',
                                        questResp.id,
                                        'QuestionDefinition_id',
                                        questResp.QuestionDefinition_id,
                                        'QueryResponse_id',
                                        questResp.QueryResponse_id,
                                        'ResponseValue',
                                        questResp.ResponseValue,
                                        'Unit',
                                        questResp.Unit,
                                        'QuerySectionQuestion_id',
                                        questResp.QuerySectionQuestion_id)
                                SEPARATOR ','),
                            ']')
                    AS CHAR CHARSET UTF8MB4)
        FROM
            `questionresponse` questResp
        WHERE
            questResp.QueryResponse_id = queryresp.id) AS `QuestionResponse`
FROM
    `queryresponse` queryresp
        LEFT JOIN
    loanproduct loanproduct ON loanproduct.id = queryresp.LoanProduct_id
        LEFT JOIN
    loantype loantype ON loantype.id = loanproduct.LoanType_id
WHERE
    (queryresp.Customer_id = id or queryresp.CoBorrower_id= id)
        AND queryresp.Status_id = Status_id
        AND (queryresp.QueryDefinition_id = 'PERSONAL_APPLICATION'
        OR queryresp.QueryDefinition_id = 'CREDIT_CARD_APPLICATION'
        OR queryresp.QueryDefinition_id = 'VEHICLE_APPLICATION')
ORDER BY queryresp.lastmodifiedts DESC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getQuestionOptions` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_getQuestionOptions`(IN `QueryDefinitionID` varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
SELECT
      querySec.Name AS QuerySection_Name,
      querySec.abstractname AS QuerySection_FriendlyName,
      querySec.Parent_id AS QuerySection_Parent_id,
      querySec.ApplicantAllowedTo AS QuerySection_ApplicantAllowedTo,
      querySec.IndCoApplicantAllowedTo AS QuerySection_IndCoApplicantAllowedTo,
      querySec.JointCoApplicantAllowedTo AS QuerySection_JointCoApplicantAllowedTo,
      ifNULL(( 
      SELECT
         CAST(CONCAT('[', GROUP_CONCAT(JSON_OBJECT('Question_FriendlyName', querysectionquestion.abstractName, 'ParentQuestion_FriendlyName', querysectionquestion.ParentQuerySectionQuestion_AbstractName, 'ParentQuestion_Value', querysectionquestion.ParentQuestionOptionValue, 'OptionItems', 
         (
            SELECT
               CAST(CONCAT('[', GROUP_CONCAT(JSON_OBJECT( 'OptionItem_id', optionitem.id, 'OptionItem_DefaultValue', optionitem.DefaultValue,'OptionItem_Note',optionitem.Label ) separator ','), ']') AS JSON) 
            FROM
               optionitem optionitem 
            WHERE
               optionitem.OptionGroup_id in 
               (
                  SELECT
                     OptionGroup_id 
                  FROM
                     questiondefinition 
                  WHERE
                     questiondefinition.id = querysectionquestion.QuestionDefinition_id 
               )
            ORDER BY
               optionitem.Sequence 
         )
) SEPARATOR ','), ']') AS CHAR CHARSET UTF8MB4) 
      FROM
         `querysectionquestion` querysectionquestion 
      WHERE
         querySec.id = querysectionquestion.QuerySection_id ), '') AS `QuestionDefinition` 
      FROM
         querysection querySec 
      WHERE
         querySec.QueryDefinition_id = QueryDefinitionID 
         AND querySec.softdeleteflag = FALSE 
      ORDER BY
         querySec.Sequence;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getQuestionOptionsForSection` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_getQuestionOptionsForSection`(IN `QueryDefinitionID` varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, IN `QuerySectionID` varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
SELECT
      querySec.Name AS QuerySection_Name,
      querySec.abstractname AS QuerySection_FriendlyName,
      querySec.Parent_id AS QuerySection_Parent_id,
      querySec.ApplicantAllowedTo AS QuerySection_ApplicantAllowedTo,
      querySec.IndCoApplicantAllowedTo AS QuerySection_IndCoApplicantAllowedTo,
      querySec.JointCoApplicantAllowedTo AS QuerySection_JointCoApplicantAllowedTo,
      ifNULL(( 
      SELECT
         CAST(CONCAT('[', GROUP_CONCAT(JSON_OBJECT('Question_FriendlyName', querysectionquestion.abstractName, 'ParentQuestion_FriendlyName', querysectionquestion.ParentQuerySectionQuestion_AbstractName, 'ParentQuestion_Value', querysectionquestion.ParentQuestionOptionValue, 'OptionItems', 
         (
            SELECT
               CAST(CONCAT('[', GROUP_CONCAT(JSON_OBJECT( 'OptionItem_id', optionitem.id, 'OptionItem_DefaultValue', optionitem.DefaultValue,'OptionItem_Note',optionitem.Label  ) separator ','), ']') AS JSON) 
            FROM
               optionitem optionitem 
            WHERE
               optionitem.OptionGroup_id in 
               (
                  SELECT
                     OptionGroup_id 
                  FROM
                     questiondefinition 
                  WHERE
                     questiondefinition.id = querysectionquestion.QuestionDefinition_id 
               )
            ORDER BY
               optionitem.Sequence 
         )
) SEPARATOR ','), ']') AS CHAR CHARSET UTF8MB4) 
      FROM
         `querysectionquestion` querysectionquestion 
      WHERE
         querySec.id = querysectionquestion.QuerySection_id ), '') AS `QuestionDefinition` 
      FROM
         querysection querySec 
      WHERE
         querySec.QueryDefinition_id = QueryDefinitionID
		 AND querySec.abstractname = QuerySectionID
         AND querySec.softdeleteflag = FALSE 
      ORDER BY
         querySec.Sequence;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getQuestionResponse` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_getQuestionResponse`(

	IN `QueryDefinition_id` VARCHAR(500)



,

	IN `User_id` VARCHAR(50)



)
BEGIN

 

SET @basequery = "SELECT 

    quesresp.id as id,

    quesresp.QueryResponse_id as QueryResponse_id,

    quesresp.QueryDefinition_id as QueryDefinition_id,

    quesresp.QuestionDefinition_id as QuestionDefinition_id,

    quesresp.QuerySection_id as QuerySection_id,

    quesresp.ArrayIndex as ArrayIndex,

    quesresp.ResponseValue as ResponseValue,

    quesresp.Unit as Unit,

    quesresp.createdby as createdby,

    quesresp.modifiedby as modifiedby,

    quesresp.createdts as createdts,

    quesresp.lastmodifiedts as lastmodifiedts,

    quesresp.synctimestamp as synctimestamp,

    quesresp.softdeleteflag as softdeleteflag    

    FROM questionresponse quesresp 

    WHERE QueryResponse_id = (SELECT id from queryresponse queryresp WHERE queryresp.QueryDefinition_id = ";

    

    SET @SubQueryFirst = "queryresp.Customer_id = ";

    SET @SubQuerySecond = "ORDER BY queryresp.lastmodifiedts DESC ";

SET @MainQuery = CONCAT(@basequery, "'", QueryDefinition_id, "'", " AND ", @SubQueryFirst, "'", User_id, "'", @SubQuerySecond, "limit 1)");

 

PREPARE queryStatement FROM @MainQuery;

EXECUTE queryStatement;

DEALLOCATE PREPARE queryStatement; 

 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getSectionLoanAnswers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_getSectionLoanAnswers`(IN QueryResponseID varchar(100) CHARACTER SET UTF8 COLLATE utf8_general_ci, IN QuerySection_id varchar(100) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
SELECT
      querySecQue.abstractName AS Question_FriendlyName,
      questionResp.id AS QuestionResponse_id,
      questionResp.ResponseValue AS Value,
      questionResp.OptionItem_id AS OptionItem_id 
   FROM
      questionresponse questionResp 
      INNER JOIN
         querysectionquestion querySecQue 
         ON querySecQue.id = questionResp.QuerySectionQuestion_id 
   WHERE
      questionResp.QueryResponse_id = QueryResponseID AND questionResp.QuerySection_id = (SELECT id from querysection where abstractname=QuerySection_id AND QueryDefinition_id=questionResp.QueryDefinition_id)
      AND questionResp.softdeleteflag is not true;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_CreateCustomerJoint` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_CreateCustomerJoint`( IN customerJson JSON,
                                    IN length INT )
BEGIN
	DECLARE p_id varchar(50) default "0";
    DECLARE p_ApplicationID varchar(100) default "0";
    DECLARE p_CustomerType varchar(200) default "0";
    DECLARE p_AccountType varchar(200) default "0";
    DECLARE p_IdDocumentType varchar(50) default "0";
    DECLARE p_IdDocumentTypeValue varchar(50) default "0";
    DECLARE p_IdDocumentNumber varchar(50) default "0";
    DECLARE p_NtbEtb varchar(45) default "0";
    DECLARE p_ATA varchar(45) default "0";
    DECLARE p_PageName varchar(100) default "0";
    DECLARE P_IsPrimaryCustomer tinyint(4) default 0;
    DECLARE P_processInstanceId varchar(50) default "0";
    DECLARE p_taskInstanceId varchar(50) default "0";
	DECLARE p_CreatedDate varchar(100) default "0";
    DECLARE p_CreatedBy tinyint(4) default 0;
    DECLARE counter smallint default 0;
    DECLARE p_Id1 varchar(50) default "0";
    DECLARE p_Id2 varchar(50) default "0";
    DECLARE p_Id3 varchar(50) default "0";
    DECLARE p_Id4 varchar(50) default "0";
    
    
    
	start transaction;
    set autocommit = 0;
    
    WHILE counter < length DO
    -- Extract the JSON value
    -- SELECT JSON_VALUE(customerJson, CONCAT('$[', counter, '].id')) INTO p_id;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].ApplicationID'))) INTO p_ApplicationID;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].CustomerType'))) INTO p_CustomerType;
	SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].AccountType'))) INTO p_AccountType;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].IdDocumentType'))) INTO p_IdDocumentType;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].IdDocumentTypeValue'))) INTO p_IdDocumentTypeValue;
	SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].IdDocumentNumber'))) INTO p_IdDocumentNumber;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].Id1'))) INTO p_Id1;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].Id2'))) INTO p_Id2;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].Id3'))) INTO p_Id3;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].Id4'))) INTO p_Id4;
	SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].NtbEtb'))) INTO p_NtbEtb;
	SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].ATA'))) INTO p_ATA;
	SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].PageName'))) INTO p_PageName;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].isPrimaryCustomer'))) INTO P_IsPrimaryCustomer;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].CreatedDate'))) INTO p_CreatedDate;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].CreatedBy'))) INTO p_CreatedBy;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].processInstanceId'))) INTO P_processInstanceId;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].taskInstanceId'))) INTO p_taskInstanceId;
   

IF (counter = 0 and length > 1)then
    BEGIN
      set P_IdDocumentNumber = p_Id1; -- Setting first CNIC
    --  select P_IdDocumentNumber;
    END;
ELSEIF (counter = 1 and length > 1)then
    BEGIN
      set P_IdDocumentNumber = p_Id2; -- Setting Second CNIC
    --  select P_IdDocumentNumber;
    END;
ELSEIF (counter = 2 and length >2 )then
    BEGIN
      set P_IdDocumentNumber = p_Id3; -- Setting Third CNIC if Length is 3
    --  select P_IdDocumentNumber;
    END;    
ELSEIF (counter = 3 and length > 3)then 
   BEGIN
      set P_IdDocumentNumber = p_Id4; -- Setting 4th CNIC if Length is 4
     -- select P_IdDocumentNumber;
   END;
END IF;

   INSERT INTO dbxdb.UBL_CLM_CUSTOMER_DATA(id,ApplicationID,CustomerType,AccountType,IdDocumentType,IdDocumentTypeValue,IdDocumentNumber,NtbEtb,ATA,PageName,Status, IsPrimaryCustomer,processInstanceId, taskInstanceId ,CreatedDate, CreatedBy)
   VALUES(CONCAT(p_ApplicationID,counter),p_ApplicationID,p_CustomerType,p_AccountType,p_IdDocumentType,p_IdDocumentTypeValue,p_IdDocumentNumber,p_NtbEtb,p_ATA,p_PageName,"M0", P_IsPrimaryCustomer, P_processInstanceId, p_taskInstanceId ,p_CreatedDate, p_CreatedBy);
   
    SET counter = counter + 1;
   -- select counter;
    END WHILE;
    COMMIT;
    
    
    SELECT id from dbxdb.UBL_CLM_CUSTOMER_DATA where ApplicationID= (SELECT p_ApplicationID);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_Pagination` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_Pagination`( IN offset_in INT)
BEGIN
SELECT ApplicationID, CustName, "Branch", ModifiedDate,(CASE
WHEN Status = "M0" THEN "Pending at Maker"
WHEN Status = "M1" THEN "Forward to Checker"
WHEN Status = "C0" THEN "Pending at Checker"
WHEN Status = "C1" THEN "Forward to Back Office"
WHEN Status = "R0" THEN "Reject by Checker"             
ELSE " "
END) as Status, BranchNameIdValue, IdDocumentTypeValue,IdDocumentNumber,
CreatedDate, CustClientCategoryIdValue, AccountType FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('M0','M1', 'R0') and IsPrimaryCustomer = 1 ORDER BY CreatedDate DESC LIMIT 10
OFFSET offset_in;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_PaginationChecker` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_PaginationChecker`( IN offset_in INT, IN processInstanceIds varchar(5000))
BEGIN

DECLARE offsetCustom int default 0;
DECLARE total int default 0;


SET total = (SELECT ceil(count(*) / 10) as Total FROM dbxdb.UBL_CLM_CUSTOMER_DATA
where   Status in ('M1','C0','C1') and IsPrimaryCustomer = 1 );

CASE WHEN offset_in is NULL THEN
SELECT total,ApplicationID, CustName, "Branch", ModifiedDate,(CASE
WHEN Status = "M0" THEN "Pending at Maker"
WHEN Status = "M1" THEN "Forward to Checker"
WHEN Status = "C0" THEN "Pending at Checker"
WHEN Status = "C1" THEN "Forward to Back Office"
WHEN Status = "R0" THEN "Reject by Checker"
ELSE " "
END) as Status, BranchNameIdValue, IdDocumentTypeValue,IdDocumentNumber,
CreatedDate AS "CreatedDate", CustClientCategoryIdValue, AccountType FROM dbxdb.UBL_CLM_CUSTOMER_DATA
where   Status in ('M1','C0','C1') and
 IsPrimaryCustomer = 1 ORDER BY CreatedDate DESC ;

ELSE
SET offsetCustom = offset_in *10;
SELECT total,ApplicationID, CustName, "Branch", ModifiedDate,(CASE
WHEN Status = "M0" THEN "Pending at Maker"
WHEN Status = "M1" THEN "Forward to Checker"
WHEN Status = "C0" THEN "Pending at Checker"
WHEN Status = "C1" THEN "Forward to Back Office"
WHEN Status = "R0" THEN "Reject by Checker"
ELSE " "
END) as Status, BranchNameIdValue, IdDocumentTypeValue,IdDocumentNumber,
CreatedDate AS "CreatedDate", CustClientCategoryIdValue, AccountType FROM dbxdb.UBL_CLM_CUSTOMER_DATA
where   Status in ('M1','C0','C1')   and
 IsPrimaryCustomer = 1 ORDER BY CreatedDate  DESC LIMIT 10
OFFSET offsetCustom;
END CASE;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_PaginationDiscrepancy` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_PaginationDiscrepancy`( IN offset_in INT)
BEGIN
DECLARE offsetCustom int default 0;
DECLARE total int default 0;
SET offsetCustom = offset_in *10;

SET total = (SELECT ceil(count(*) / 10) as Total FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('DP') and IsPrimaryCustomer = 1);

SELECT total, ApplicationID, CustName,CustomerType,ProductIdValue, custAccountNumber,AccountType, "Branch", ModifiedDate,(CASE
WHEN Status = "DP" THEN "AOF Under Discrepancy"         
ELSE " "
END) as Status, BranchNameIdValue,
COALESCE(SubmittedDate,CreatedDate) AS "CreatedDate" FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('DP') and IsPrimaryCustomer = 1 ORDER BY COALESCE(SubmittedDate,CreatedDate)  DESC LIMIT 10
OFFSET offsetCustom;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_PaginationInputter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_PaginationInputter`( IN offset_in INT)
BEGIN
DECLARE offsetCustom int default 0;
DECLARE total int default 0;
SET offsetCustom = offset_in *10;

SET total = (SELECT ceil(count(*) / 10) as Total FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('IP') and IsPrimaryCustomer = 1);

SELECT total, ApplicationID, CustName,CustomerType,ProductIdValue, custAccountNumber,AccountType, "Branch", ModifiedDate,(CASE
WHEN Status = "IP" THEN "AOF Under Inputter"         
ELSE " "
END) as Status, BranchNameIdValue,
COALESCE(SubmittedDate,CreatedDate) AS "CreatedDate" FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('IP') and IsPrimaryCustomer = 1 ORDER BY COALESCE(SubmittedDate,CreatedDate)  DESC LIMIT 10
OFFSET offsetCustom;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_PaginationMaker` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_PaginationMaker`( IN offset_in INT,  IN processInstanceIds varchar(900))
BEGIN 
DECLARE offsetCustom int default 0;
DECLARE total int default 0;


SET total = (SELECT ceil(count(*) / 10) as Total FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('M0','M1', 'R0') and IsPrimaryCustomer = 1 );
CASE WHEN offset_in is NULL THEN
SELECT total,ApplicationID, CustName, "Branch", ModifiedDate, processInstanceId,(CASE
WHEN Status = "M0" THEN "Pending at Maker"
WHEN Status = "M1" THEN "Forward to Checker"
WHEN Status = "C0" THEN "Pending at Checker"
WHEN Status = "C1" THEN "Forward to Back Office"
WHEN Status = "R0" THEN "Rejected by Checker"             
ELSE " "
END) as Status, BranchNameIdValue, IdDocumentTypeValue,IdDocumentNumber,
CreatedDate as "CreatedDate", CustClientCategoryIdValue, AccountType FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('M0','M1', 'R0')  
and IsPrimaryCustomer = 1 ORDER BY CreatedDate DESC ;


ELSE
SET offsetCustom = offset_in *10;
SELECT total,ApplicationID, CustName, "Branch", ModifiedDate,(CASE
WHEN Status = "M0" THEN "Pending at Maker"
WHEN Status = "M1" THEN "Forward to Checker"
WHEN Status = "C0" THEN "Pending at Checker"
WHEN Status = "C1" THEN "Forward to Back Office"
WHEN Status = "R0" THEN "Rejected by Checker"             
ELSE " "
END) as Status, BranchNameIdValue, IdDocumentTypeValue,IdDocumentNumber,
CreatedDate  as "CreatedDate", CustClientCategoryIdValue, AccountType FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('M0','M1', 'R0')  
and IsPrimaryCustomer = 1 ORDER BY CreatedDate DESC  LIMIT 10
OFFSET offsetCustom;
END CASE;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_PaginationMakerDescriment` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_PaginationMakerDescriment`()
BEGIN 

DECLARE total int default 0;


SET total = (SELECT ceil(count(*) / 10) as Total FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('DC') and IsPrimaryCustomer = 1 );

SELECT total,ApplicationID, CustName, "Branch", ModifiedDate,(CASE
WHEN Status = "DC" THEN "Marked as Discrepant"
ELSE " "
END) as Status, BranchNameIdValue, IdDocumentTypeValue,IdDocumentNumber,
CreatedDate as "CreatedDate", CustClientCategoryIdValue, AccountType FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('DC') 
 and IsPrimaryCustomer = 1 ORDER BY CreatedDate DESC  
;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_PaginationScruitny` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_PaginationScruitny`( IN offset_in INT)
BEGIN
DECLARE offsetCustom int default 0;
DECLARE total int default 0;
SET offsetCustom = offset_in *10;

SET total = (SELECT ceil(count(*) / 10) as Total FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('C1') and IsPrimaryCustomer = 1);

SELECT total, ApplicationID, CustName,CustomerType,ProductIdValue, custAccountNumber,AccountType, "Branch", ModifiedDate,(CASE
WHEN Status = "C1" THEN "AOF Under Scrutiny"         
ELSE " "
END) as Status, BranchNameIdValue,
COALESCE(SubmittedDate,CreatedDate) AS "CreatedDate" FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('C1') and IsPrimaryCustomer = 1 ORDER BY COALESCE(SubmittedDate,CreatedDate)  DESC LIMIT 10
OFFSET offsetCustom;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_PaginationSupervision` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_PaginationSupervision`( IN offset_in INT)
BEGIN
DECLARE offsetCustom int default 0;
DECLARE total int default 0;
SET offsetCustom = offset_in *10;

SET total = (SELECT ceil(count(*) / 10) as Total FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('SP') and IsPrimaryCustomer = 1);

SELECT total, ApplicationID, CustName,CustomerType,ProductIdValue, custAccountNumber,AccountType, "Branch", ModifiedDate,(CASE
WHEN Status = "SP" THEN "Under Supervision"         
ELSE " "
END) as Status, BranchNameIdValue,
COALESCE(SubmittedDate,CreatedDate) AS "CreatedDate" FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('SP') and IsPrimaryCustomer = 1 ORDER BY COALESCE(SubmittedDate,CreatedDate)  DESC LIMIT 10
OFFSET offsetCustom;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_tabledata` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `sp_update_tabledata`(IN tablename varchar(255),IN primarykeyfield varchar(255),IN primarykeyvalue varchar(255),IN fieldname varchar(255),IN fieldvalue varchar(255))
BEGIN
Set @MainQuery = CONCAT("update ",tablename," set ",fieldname,"=",fieldvalue," where ",primarykeyfield,"=","'",primarykeyvalue,"'");
PREPARE queryStatement FROM @MainQuery;
EXECUTE queryStatement;
DEALLOCATE PREPARE queryStatement;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `subscriber_getAlertSubtypePreferences` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `subscriber_getAlertSubtypePreferences`(alerttypes TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
Select id as alertsubtypeid ,AlertTypeId as alerttypeid ,  attributeId as attributeid, alertConditionId as alertconditionid, value1, value2, isGlobal as isglobal
 from alertsubtype where FIND_IN_SET(`alertsubtype`.`AlertTypeId`,alerttypes)  ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `subscriber_getCoreIdFromDbxIds` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `subscriber_getCoreIdFromDbxIds`(_dbxids TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN

select BackendId,Customer_id  from backendidentifier where FIND_IN_SET(`backendidentifier`.`Customer_id`,_dbxids) and BackendType is null;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `subscriber_getCoreIdFromDbxIds_coreSpecific` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `subscriber_getCoreIdFromDbxIds_coreSpecific`(_dbxids TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, _coretype TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
select BackendId,Customer_id  from backendidentifier where FIND_IN_SET(`backendidentifier`.`Customer_id`,_dbxids) and BackendType = _coretype ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `subscriber_getCustIdFromCore` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `subscriber_getCustIdFromCore`(_backendids TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
select BackendId,Customer_id  from backendidentifier where FIND_IN_SET(`backendidentifier`.`BackendId`,_backendids);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `subscriber_getEntitleMentsNocustomer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `subscriber_getEntitleMentsNocustomer`(alerttypes TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
SELECT Customer_id as customerid , AlertTypeId as alerttypeid , alertSubTypeId as alertsubtypeid , AccountId as accountid, AccountType as accounttype, Value1 as value1, Value2 as value2 FROM dbxcustomeralertentitlement where   FIND_IN_SET(`dbxcustomeralertentitlement`.`AlertTypeId`,alerttypes)  ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `subscriber_getEntitleMentsWithcustomer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `subscriber_getEntitleMentsWithcustomer`(alerttypes TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci,custids TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
SELECT Customer_id as customerid , AlertTypeId as alerttypeid , alertSubTypeId as alertsubtypeid , AccountId as accountid, AccountType as accounttype, Value1 as value1, Value2 as value2  FROM dbxcustomeralertentitlement where   FIND_IN_SET(`dbxcustomeralertentitlement`.`AlertTypeId`,alerttypes)  and
   FIND_IN_SET(`dbxcustomeralertentitlement`.`Customer_id`,custids);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `systemroles_permission_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `systemroles_permission_proc`(
IN _roleIds varchar(100) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
 SELECT distinct
        `p`.`id` AS `id`,
        `p`.`Name` AS `name`,
        `p`.`Status_id` AS `status`,
        `p`.`PermissionValue` AS `PermissionValue`,
        `p`.`isComposite` AS `isComposite`,
        `rp`.`Role_id` AS `Role_id`,
		 CAST(`p`.`softdeleteflag` AS unsigned) AS `softdeleteflag`
    FROM
    `rolepermission` `rp` , `permission` `p`
    WHERE `p`.`id` = `rp`.`Permission_id` AND `p`.`Status_id`='SID_ACTIVE' AND FIND_IN_SET(`rp`.`Role_id`,_roleIds) ORDER BY `id`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `systemuser_permission_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `systemuser_permission_proc`(
IN _userId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
 SELECT 
        `up`.`Permission_id` AS `id`,
        `p`.`Name` AS `name`,
        `p`.`Status_id` AS `status`,
        `p`.`PermissionValue` as `PermissionValue`,
        `p`.`softdeleteflag` AS `softdeleteflag`
    FROM
        (`userpermission` `up`
        JOIN `permission` `p` ON (`up`.`Permission_id` = `p`.`id`)) where `up`.`User_id`= _userId and `p`.`Status_id`='SID_ACTIVE'
    UNION SELECT 
        `p`.`id` AS `id`,
        `p`.`Name` AS `name`,
        `p`.`Status_id` AS `status`,
        `p`.`PermissionValue` as `PermissionValue`,
        `p`.`softdeleteflag` AS `softdeleteflag`
    FROM
        (`userrole` `ur` JOIN `role` `r` ON (`r`.`id` = `ur`.`Role_id`)
        JOIN `rolepermission` `rp` ON (`ur`.`Role_id` = `rp`.`Role_id`)
        JOIN `permission` `p` ON (`rp`.`Permission_id` = `p`.`id`)) where `ur`.`User_id`= _userId and `p`.`Status_id`='SID_ACTIVE' ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `test` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `test`()
BEGIN 
DECLARE List1 varchar(100);
SET List1 = 'M1,C0, C1';
SELECT ApplicationID, CustName, "Branch", ModifiedDate,(CASE
WHEN Status = "M0" THEN "Pending at Maker"
WHEN Status = "M1" THEN "Forward to Checker"
WHEN Status = "C0" THEN "Pending at Checker"
WHEN Status = "C1" THEN "Forward to Back Office"
WHEN Status = "R0" THEN "Reject by Checker"
ELSE " "
END) as Status, BranchNameIdValue, IdDocumentTypeValue,IdDocumentNumber,
COALESCE(SubmittedDate,CreatedDate) AS "CreatedDate", CustClientCategoryIdValue, AccountType FROM dbxdb.UBL_CLM_CUSTOMER_DATA
where   FIND_IN_SET(Status, List1) and IsPrimaryCustomer = 1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `transaction_proc_get` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `transaction_proc_get`(in transactions_query varchar(20000))
BEGIN
SET @stmt := CONCAT('', transactions_query);
PREPARE stmt FROM @stmt;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_bbactedrequest_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `update_bbactedrequest_proc`(
IN _requestId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
update bbactedrequest set softdeleteflag=1 where requestId=_requestId ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Update_BulkWireTemplateRecipient_Count` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `Update_BulkWireTemplateRecipient_Count`(
	IN `_userID` varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `_orgID` varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
	IN `_payeeID` varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
proc_label: BEGIN

	IF (_userID IS NULL OR _userID = "") AND (_orgID IS NULL OR _orgID = "") THEN
		SELECT "Both user_id and company_id cannot be empty" AS Response;
		LEAVE proc_label;
	END IF;
	IF _payeeID IS NULL OR _payeeID = "" THEN
		SELECT "payeeId cannot be empty" AS Response;
		LEAVE proc_label;
	END IF;
	
	IF _orgID IS NOT NULL AND _orgID != "" THEN
		SET @filterLineItemQuery = CONCAT("WHERE payeeId = '",_payeeID,"' AND bulkWireTemplateID IN (SELECT bulkWireTemplateID FROM bulkwiretemplate WHERE company_id = '",_orgID,"')");
		SET @filterTemplateQuery = CONCAT("WHERE payeeId = '",_payeeID,"') AND company_id = '",_orgID,"'");
	ELSE
		SET @filterLineItemQuery = CONCAT("WHERE payeeId = '",_payeeID,"' AND createdby = '",_userID,"'");
		SET @filterTemplateQuery = CONCAT("WHERE payeeId = '",_payeeID,"' AND createdby = '",_userID,"')");
	END IF;
	
	SELECT bulkWireTransferType FROM bulkwiretemplatelineitems WHERE payeeId = _payeeID LIMIT 1 INTO @transferType;
	
	IF @transferType = "Domestic" THEN
		SET @updateTransferTypeTransactions = "UPDATE bulkwiretemplate SET noOfDomesticTransactions = noOfDomesticTransactions - 1 ";
	ELSE
		SET @updateTransferTypeTransactions = "UPDATE bulkwiretemplate SET noOfInternationalTransactions = noOfInternationalTransactions - 1 ";
	END IF;
	
	SET @updatelineitemQuery = CONCAT("UPDATE bulkwiretemplatelineitems SET softdeleteflag = 1 ",@filterLineItemQuery);
	
	SET @updateTemplateQuery = CONCAT("UPDATE bulkwiretemplate SET noOfTransactions = noOfTransactions - 1 WHERE bulkWireTemplateID IN (SELECT bulkWireTemplateID FROM bulkwiretemplatelineitems ",@filterTemplateQuery);
	
	SET @updateTransferTypeTemplateQuery = CONCAT(@updateTransferTypeTransactions," WHERE bulkWireTemplateID IN (SELECT bulkWireTemplateID FROM bulkwiretemplatelineitems ",@filterTemplateQuery);
	
	PREPARE sql_query FROM @updatelineitemQuery;
	EXECUTE sql_query;
	
	PREPARE sql_query1 FROM @updateTemplateQuery;
	EXECUTE sql_query1;
	
	PREPARE sql_query2 FROM @updateTransferTypeTemplateQuery;
	EXECUTE sql_query2;
	
	SELECT "SUCCESS" AS Response;
	
	

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_requestapprovalmatrix_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `update_requestapprovalmatrix_proc`(
IN _requestApprovalMatrixId VARCHAR(250) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	UPDATE requestapprovalmatrix SET `receivedApprovals` = `receivedApprovals` + 1 WHERE FIND_IN_SET(id , _requestApprovalMatrixId);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_signatorygroup_for_user_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `update_signatorygroup_for_user_proc`(
IN _coreCustomerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _contractId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _customerId longtext CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _signatorygroupId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN
DECLARE index1 INTEGER DEFAULT 0;
set @length = LENGTH(_customerId) - LENGTH(REPLACE(_customerId, ',', '')) + 1;
users: LOOP
	set index1 = index1 + 1;
	IF index1 = @length + 1 THEN 
		LEAVE users;
	else
		set @cusRecord = SUBSTRING_INDEX( SUBSTRING_INDEX(_customerId, ',', index1), ',', -1 );
		set @customerId = REPLACE(@cusRecord, ';', ',');

		delete from customersignatorygroup where customerId = @customerId and signatoryGroupId 
		in (select signatoryGroupId from signatorygroup where coreCustomerId=_coreCustomerId and contractId=_contractId);

		if(_signatorygroupId !='') THEN
			INSERT INTO customersignatorygroup(customerSignatoryGroupId, signatoryGroupId, customerId, createdby) values (UUID(),_signatorygroupId,@customerId,@customerId);		
		END IF;
		
		ITERATE  users;
	END IF;
END LOOP users;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_transaction_status_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `update_transaction_status_proc`(
    IN _featureId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _status VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _confirmationNumber VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
	)
BEGIN
	SET @isSuccess = "true";
	CASE _featureId
		WHEN  'ACH_COLLECTION' THEN 
			UPDATE `achtransaction` SET `status` = _status WHERE `confirmationNumber` = _confirmationNumber;
        WHEN  'ACH_PAYMENT' THEN 
			UPDATE `achtransaction` SET `status` = _status WHERE `confirmationNumber` = _confirmationNumber;
		WHEN 'ACH_FILES' THEN   
			UPDATE `achfile` SET `status` = _status WHERE `confirmationNumber` = _confirmationNumber;
		WHEN 'BILL_PAY' THEN   
			UPDATE `billpaytransfers` SET `status` = _status WHERE `confirmationNumber` = _confirmationNumber;
		WHEN 'DOMESTIC_WIRE_TRANSFER' THEN   
			UPDATE `wiretransfers` SET `status` = _status WHERE `confirmationNumber` = _confirmationNumber;
		WHEN 'INTERNATIONAL_WIRE_TRANSFER' THEN   
			UPDATE `wiretransfers` SET `status` = _status WHERE `confirmationNumber` = _confirmationNumber;
		WHEN 'INTERNATIONAL_ACCOUNT_FUND_TRANSFER' THEN   
			UPDATE `internationalfundtransfers` SET `status` = _status WHERE `confirmationNumber` = _confirmationNumber;
		WHEN 'INTER_BANK_ACCOUNT_FUND_TRANSFER' THEN   
			UPDATE `interbankfundtransfers` SET `status` = _status WHERE `confirmationNumber` = _confirmationNumber;
		WHEN 'INTRA_BANK_FUND_TRANSFER' THEN   
			UPDATE `intrabanktransfers` SET `status` = _status WHERE `confirmationNumber` = _confirmationNumber;
		WHEN 'P2P' THEN   
			UPDATE `p2ptransfers` SET `status` = _status WHERE `confirmationNumber` = _confirmationNumber;
		WHEN 'TRANSFER_BETWEEN_OWN_ACCOUNT' THEN   
			UPDATE `ownaccounttransfers` SET `status` = _status WHERE `confirmationNumber` = _confirmationNumber;
		ELSE
			SET @isSuccess = "false";
    END CASE;
    SELECT @isSuccess AS isSuccess;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_user_recent_currency` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `update_user_recent_currency`(
  IN _customerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
  IN _currencyCode VARCHAR(10) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

DECLARE rowWithGivenCustomerAndCurrency INT DEFAULT 0;
DECLARE lengthOfRecentCurrencies INT DEFAULT 0;
DECLARE recentCurrencyToDelete VARCHAR(60);

SELECT COUNT(*) INTO rowWithGivenCustomerAndCurrency FROM 
recentcurrencies rc WHERE rc.customerId = _customerId AND rc.quoteCurrencyCode = _currencyCode;

SELECT COUNT(*) INTO lengthOfRecentCurrencies FROM 
recentcurrencies rc WHERE rc.customerId = _customerId ;

SELECT  recentcurrencies.id INTO recentCurrencyToDelete FROM recentcurrencies  WHERE recentcurrencies.customerId = _customerId
													ORDER BY recentcurrencies.createdts asc limit 1;
IF rowWithGivenCustomerAndCurrency >= 1 THEN
	DELETE FROM recentcurrencies  WHERE recentcurrencies.customerId = _customerId AND recentcurrencies.quoteCurrencyCode = _currencyCode;
ELSEIF lengthOfRecentCurrencies >= 5 THEN
	DELETE FROM recentcurrencies  WHERE recentcurrencies.id = recentCurrencyToDelete COLLATE utf8_general_ci;
END IF;

INSERT INTO recentcurrencies (`id`, `customerId`, `quoteCurrencyCode`) VALUES (concat( _currencyCode, _customerId ), _customerId, _currencyCode);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `useraccounts_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `useraccounts_create_proc`(
IN _userId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _accountsCSV TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _coreCustomerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
DECLARE accountID varchar(255) ;
DECLARE finished INTEGER DEFAULT 0 ;

DECLARE accountData CURSOR FOR (SELECT contractaccounts.accountId FROM contractaccounts WHERE contractId = _contractId
        AND coreCustomerId = _coreCustomerId AND find_in_set(contractaccounts.accountId,_accountsCSV) );
	
DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;
         
OPEN accountData; 
getAccount : LOOP
fetch accountData into accountID; 
     IF finished = 1 THEN 
	     LEAVE getAccount;
	 ELSE
         SET @id = (SELECT LEFT(UUID(), 50));
         set @accounttypeid = (select contractaccounts.typeId from contractaccounts where contractaccounts.accountId COLLATE utf8_general_ci =accountID);
         set @accounttypename = (select accounttype.TypeDescription from accounttype where accounttype.TypeID COLLATE utf8_general_ci =@accounttypeid);
		 INSERT INTO `customeraccounts` (`id`, `Customer_id`, `Account_id`,`contractId`, `coreCustomerId`,`accountType`)
            VALUES (@id, _userId, accountID, _contractId, _coreCustomerId,@accounttypename);
      END IF;
      ITERATE  getAccount;
END LOOP getAccount;
CLOSE accountData;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `useractions_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `useractions_create_proc`(
IN _userId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _accountsCSV TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _coreCustomerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,  
IN _groupId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

 DECLARE finished INTEGER DEFAULT 0 ;
 DECLARE featureActionId varchar(255) DEFAULT "" ;
 DECLARE actionslist TEXT DEFAULT "" ;
 DECLARE limitId varchar(255) DEFAULT ""  ;
 DECLARE entryStatus INTEGER DEFAULT 0 ;
 DECLARE accountId varchar(255) DEFAULT "" ;
 DECLARE actualLimitId varchar(255) DEFAULT "" ;

DECLARE accounts CURSOR
         FOR (SELECT customeraccounts.Account_id FROM customeraccounts WHERE contractId = _contractId AND coreCustomerId = _coreCustomerId 
         AND Customer_id = _userId AND FIND_IN_SET(Account_id,_accountsCSV));
DECLARE actions CURSOR 
      FOR (select id from featureaction where FIND_IN_SET(id,@validActionsList) AND (featureaction.isAccountLevel = '1' OR featureaction.isAccountLevel = true ));
DECLARE nonaccountlevelactions CURSOR 
      FOR (select id from featureaction where FIND_IN_SET(id,@validActionsList) AND (featureaction.isAccountLevel = '0' OR featureaction.isAccountLevel = false ));
DECLARE limits CURSOR 
        FOR (select LimitType_id from actionlimit where Action_id COLLATE utf8_general_ci = featureActionId COLLATE utf8_general_ci );
DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;

SET SESSION group_concat_max_len = 100000000;

IF(ISNULL(_accountsCSV) OR _accountsCSV='' ) THEN
SET _accountsCSV = (SELECT group_concat(distinct customeraccounts.Account_id SEPARATOR ",") FROM customeraccounts WHERE customeraccounts.Customer_id = _userId
              AND customeraccounts.contractId = _contractId AND  customeraccounts.coreCustomerId = _coreCustomerId );
END IF;
 
SET @serviceDefinitionId = (SELECT servicedefinitionId from contract WHERE id = _contractId);
SET @serviceType = (SELECT serviceType from servicedefinition WHERE id = @serviceDefinitionId);
IF(ISNULL(_groupId) OR _groupId='' ) THEN
SET _groupId = (SELECT Group_id FROM groupservicedefinition WHERE serviceDefinitionId = @serviceDefinitionId 
                       AND (isDefaultGroup = true OR isDefaultGroup = '1'));
END IF;
                        
SET @validFIActions = (SELECT group_concat(distinct id SEPARATOR ",") FROM featureaction);

SET @validServiceDefinitionActions = (SELECT group_concat(distinct actionId SEPARATOR ",") FROM servicedefinitionactionlimit
                        WHERE serviceDefinitionId= @serviceDefinitionId AND FIND_IN_SET(actionId,@validFIActions));

SET _groupId = (SELECT Group_id FROM groupservicedefinition WHERE serviceDefinitionId = @serviceDefinitionId 
                        AND  Group_id = _groupId );


SET @validGroupActions = (SELECT group_concat(distinct Action_id SEPARATOR ",") FROM groupactionlimit WHERE Group_id = _groupId 
                                AND FIND_IN_SET(Action_id,@validServiceDefinitionActions));
                                
SET @validActionsList = (SELECT group_concat(distinct actionId SEPARATOR ",") FROM contractactionlimit WHERE contractId = _contractId
                                AND coreCustomerId = _coreCustomerId AND FIND_IN_SET(actionId,@validGroupActions));
  

OPEN accounts; 
getAccount : LOOP
FETCH accounts INTO accountId;
IF finished = 1 THEN 
    LEAVE getAccount;
ELSE
  OPEN actions; 
  getAction: LOOP
        SET entryStatus = 0;
        FETCH actions INTO featureActionId;
        SET @featureId = (SELECT Feature_id FROM featureaction WHERE id = featureActionId COLLATE utf8_general_ci);
        IF finished = 1 THEN 
            LEAVE getAction;
        else
            OPEN limits; 
            getlimit: LOOP
            FETCH limits INTO limitId;
            IF finished = 1 THEN 
               LEAVE getlimit;
            else
               SET @limitvalue = (SELECT value FROM contractactionlimit WHERE actionId = featureActionId  COLLATE utf8_general_ci
               AND limitTypeId = limitId COLLATE utf8_general_ci AND contractId = _contractId AND coreCustomerId = _coreCustomerId);
    
               if (limitId='MAX_TRANSACTION_LIMIT') THEN 
               SET actualLimitId = 'AUTO_DENIED_TRANSACTION_LIMIT';
               ELSEIF (limitId='MIN_TRANSACTION_LIMIT') THEN 
               SET actualLimitId = 'PRE_APPROVED_TRANSACTION_LIMIT';
               ELSEIF (limitId='DAILY_LIMIT') THEN 
               SET actualLimitId = 'PRE_APPROVED_DAILY_LIMIT';
                SET @id = (SELECT LEFT(UUID(), 50));
                INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,contractId,coreCustomerId,featureId,Action_id,Account_id,isAllowed,LimitType_id,value) VALUES
                (@id,@serviceType,_userId,_contractId,_coreCustomerId,@featureId,featureActionId,accountId,true,actualLimitId,0.00);
                SET actualLimitId = 'AUTO_DENIED_DAILY_LIMIT';
               ELSEIF (limitId='WEEKLY_LIMIT') THEN 
               SET actualLimitId = 'PRE_APPROVED_WEEKLY_LIMIT';
                SET @id = (SELECT LEFT(UUID(), 50));
               INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,contractId,coreCustomerId,featureId,Action_id,Account_id,isAllowed,LimitType_id,value) VALUES
                (@id,@serviceType,_userId,_contractId,_coreCustomerId,@featureId,featureActionId,accountId,true,actualLimitId,0.00);
               SET actualLimitId = 'AUTO_DENIED_WEEKLY_LIMIT';
               END IF;
               
               SET @id = (SELECT LEFT(UUID(), 50));
               INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,contractId,coreCustomerId,featureId,Action_id,Account_id,isAllowed,LimitType_id,value) VALUES
               (@id,@serviceType,_userId,_contractId,_coreCustomerId,@featureId,featureActionId,accountId,true,actualLimitId,@limitvalue);
                
                SET @id = (SELECT LEFT(UUID(), 50));
                INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,contractId,coreCustomerId,featureId,Action_id,Account_id,isAllowed,LimitType_id,value) VALUES
                (@id,@serviceType,_userId,_contractId,_coreCustomerId,@featureId,featureActionId,accountId,true,limitId,@limitvalue);
                  
                SET entryStatus = 1;
               ITERATE  getlimit;
            END IF;
                END LOOP getlimit;
                CLOSE limits;
                
            SET finished = 0;
            IF entryStatus = 0 THEN
                  SET @id = (SELECT LEFT(UUID(), 50));
                   INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,contractId,coreCustomerId,featureId,Action_id,Account_id,isAllowed) VALUES
                (@id,@serviceType,_userId,_contractId,_coreCustomerId,@featureId,featureActionId,accountId,true);
            END IF;
            ITERATE  getAction;
        END IF;
  END LOOP getAction;
  CLOSE actions;
  SET finished = 0;
  ITERATE  getAccount;
 END IF;
END LOOP getAccount;
CLOSE accounts;

 
SET finished = 0;
OPEN nonaccountlevelactions; 
  getAction: LOOP
        FETCH nonaccountlevelactions INTO featureActionId;
        SET @featureId = (SELECT Feature_id FROM featureaction WHERE id = featureActionId COLLATE utf8_general_ci);
        SELECT @featureId;
        IF finished = 1 THEN 
            LEAVE getAction;
        else
                   SET @id = (SELECT LEFT(UUID(), 50));
                   INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,contractId,coreCustomerId,featureId,Action_id,isAllowed) VALUES
                (@id,@serviceType,_userId,_contractId,_coreCustomerId,@featureId,featureActionId,true);
        END IF;
        ITERATE  getAction;
END LOOP getAction;
CLOSE nonaccountlevelactions;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `userdelinking_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `userdelinking_proc`(
IN _newUser VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _combinedUser VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
	SET SQL_SAFE_UPDATES = 0;
	
    SET @groupId = (select Group_id from `customergroup` join `membergroup` on (`customergroup`.`Group_id`  = `membergroup`.`id` and `membergroup`.`Type_id` ='TYPE_ID_RETAIL') where `Customer_id`= _combinedUser );
    DELETE FROM `customergroup` WHERE `Customer_id`= _combinedUser and `Group_id` = @groupId;
    INSERT INTO `customergroup` (`Customer_id`, `Group_id`) VALUES (_newUser, @groupId);    
    UPDATE `payee` SET `User_Id` = _newUser WHERE `User_Id` = _combinedUser AND `organizationId` IS NULL;
    UPDATE `externalaccount` SET `User_id` = _newUser WHERE `User_id` = _combinedUser AND `organizationId` IS NULL;
    UPDATE `billpaypayee` SET `customerId` = _newUser WHERE `customerId` = _combinedUser AND `isBusinessPayee` = '0' AND `companyId` IS NULL;
    UPDATE `wiretransferspayee` SET `customerId` = _newUser WHERE `customerId` = _combinedUser AND `isBusinessPayee` = '0' AND `companyId` IS NULL;
    UPDATE `interbankpayee` SET `customerId` = _newUser WHERE `customerId` = _combinedUser AND `isBusinessPayee` = '0' AND `companyId` IS NULL;
    UPDATE `intrabankpayee` SET `customerId` = _newUser WHERE `customerId` = _combinedUser AND `isBusinessPayee` = '0' AND `companyId` IS NULL;
    UPDATE `internationalpayee` SET `customerId` = _newUser WHERE `customerId` = _combinedUser AND `isBusinessPayee` = '0' AND `companyId` IS NULL;
    
    SET SQL_SAFE_UPDATES = 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `userlinking_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `userlinking_proc`(
    IN _combinedUser VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _otherUser VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    IN _isCombinedUserBusiness VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
DECLARE success_flag INT(8);

	SET SESSION group_concat_max_len = 100000000;
	SET SQL_SAFE_UPDATES = 0;
	
	UPDATE customerrequest SET Customer_id = _combinedUser
    WHERE Customer_id = _otherUser;
	UPDATE cardaccountrequest SET Customer_id = _combinedUser
    WHERE Customer_id = _otherUser;
	UPDATE notificationcardinfo SET Customer_id = _combinedUser
    WHERE Customer_id = _otherUser;
	SET @deviceIds = (select group_concat(id SEPARATOR ",") from customerdevice where Customer_id = _combinedUser);
    UPDATE customerdevice SET Customer_id = _combinedUser
    WHERE Customer_id = _otherUser AND NOT FIND_IN_SET(id, @deviceIds);
    
	UPDATE `payee` SET `User_Id` = _combinedUser WHERE `User_Id` = _otherUser;
    UPDATE `externalaccount` SET `User_id` = _combinedUser WHERE `User_id` = _otherUser;
    UPDATE `billpaypayee` SET `customerId` = _combinedUser WHERE `customerId` = _otherUser;
    UPDATE `wiretransferspayee` SET `customerId` = _combinedUser WHERE `customerId` = _otherUser;
    UPDATE `interbankpayee` SET `customerId` = _combinedUser WHERE `customerId` = _otherUser;
    UPDATE `intrabankpayee` SET `customerId` = _combinedUser WHERE `customerId` = _otherUser;
	UPDATE `internationalpayee` SET `customerId` = _combinedUser WHERE `customerId` = _otherUser;
	IF _isCombinedUserBusiness = 'false' THEN
		UPDATE customeraction SET Customer_id = _combinedUser
		WHERE Customer_id = _otherUser;	
    END IF;
    
    SET SQL_SAFE_UPDATES = 1;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `userPermissionDelete_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `userPermissionDelete_proc`(
IN _userId VARCHAR(50) CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI, 
IN _PermissionIds VARCHAR(5000) CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI
)
BEGIN
DECLARE caid VARCHAR(50);
DECLARE isEnabled VARCHAR(10);
DECLARE finished INTEGER DEFAULT 0 ;
DECLARE caids CURSOR FOR (SELECT c.id FROM compositeaction c WHERE FIND_IN_SET(`Permission_id`, _PermissionIds));
DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;

OPEN caids; 
MANAGECAIDS : LOOP
FETCH caids INTO caid;
IF finished = 1 THEN 
	LEAVE MANAGECAIDS;
END IF;
SET @caid_count =(SELECT COUNT(*) FROM compositeaction c,  userpermission up WHERE up.User_id = _userId
AND up.Permission_id=c.Permission_id
AND NOT FIND_IN_SET(up.Permission_id,_PermissionIds)
AND c.id=caid);

IF @caid_count=0 THEN 
DELETE FROM usercompositeaction WHERE User_id=_userId AND CompositeAction_id=caid;
END IF;
END LOOP MANAGECAIDS;
CLOSE caids;
DELETE FROM userpermission WHERE User_id=_userId AND FIND_IN_SET(Permission_id,_PermissionIds);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_accountactions_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `user_accountactions_get_proc`(
in _userId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _coreCustomerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN

SET SESSION group_concat_max_len = 100000000;
SET @contractId = (SELECT contractcorecustomers.contractId FROM contractcorecustomers WHERE contractcorecustomers.coreCustomerId = _coreCustomerId );
SET @serviceDefinitionId = (SELECT contract.servicedefinitionId FROM contract WHERE contract.id = @contractId );
SET @groupId = (SELECT customergroup.Group_id FROM customergroup WHERE customergroup.contractId = @contractId 
                 AND customergroup.coreCustomerId = _coreCustomerId AND customergroup.Customer_id = _userId);
SET @groupId = (SELECT groupservicedefinition.Group_id FROM groupservicedefinition WHERE groupservicedefinition.serviceDefinitionId = @serviceDefinitionId
                 AND groupservicedefinition.Group_id = @groupId); 
IF isnull(@groupId) THEN
  SET @groupId = '';
END IF;

SET @validFIAccountLevelActions = (SELECT group_concat(distinct featureaction.id SEPARATOR ",") FROM featureaction WHERE 
                                    (featureaction.isAccountLevel = '1' OR featureaction.isAccountLevel = true) AND 
                                    featureaction.status = 'SID_ACTION_ACTIVE');
SET @validServiceDefinitinActions = (SELECT group_concat(distinct servicedefinitionactionlimit.actionId SEPARATOR ",") FROM servicedefinitionactionlimit WHERE
                                servicedefinitionactionlimit.serviceDefinitionId = @serviceDefinitionId AND
                                FIND_IN_SET(servicedefinitionactionlimit.actionId, @validFIAccountLevelActions)); 
SET @validGroupActions = (SELECT group_concat(distinct groupactionlimit.Action_id SEPARATOR ",") FROM groupactionlimit WHERE
                                groupactionlimit.Group_id = @groupId AND
                                FIND_IN_SET(groupactionlimit.Action_id, @validServiceDefinitinActions)); 
SET @validUserActions = (SELECT group_concat(distinct customeraction.Action_id SEPARATOR ",") FROM customeraction WHERE
                                customeraction.Customer_id = _userId AND
                                customeraction.coreCustomerId = _coreCustomerId AND 
                                customeraction.contractId = @contractId AND 
                                (customeraction.isAllowed = '1' OR customeraction.isAllowed = true) AND
                                FIND_IN_SET(customeraction.Action_id, @validGroupActions)); 
                                
SET @actionCondition = concat("FIND_IN_SET(`customeraction`.`Action_id`, '",@validUserActions,"')");

set @select_statement = concat("SELECT 
        `customeraction`.`Customer_id` AS `Customer_id`,
         `customeraction`.`contractId` AS `contractId`,
		 `customeraction`.`coreCustomerId` AS `coreCustomerId`,
        `customeraction`.`Account_id` AS `Account_id`,
        `customeraction`.`featureId` AS `featureId`,
        `customeraction`.`Action_id` AS `Action_id`,
        `customeraction`.`RoleType_id` AS `RoleType_id`,
    	`customeraction`.`LimitType_id` AS `LimitType_id`,
        `customeraction`.`value` AS `value`
    FROM
        (`customeraction`)
  	where `customeraction`.`Customer_id` = ", quote(_userId)," and ",@actionCondition ," and `customeraction`.`coreCustomerId` = ", quote(_coreCustomerId));
     
PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_account_default_actions_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `user_account_default_actions_create_proc`(
IN _userId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _accountsCSV TEXT CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _coreCustomerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,  
IN _groupId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

 DECLARE finished INTEGER DEFAULT 0 ;
 DECLARE featureActionId varchar(255) DEFAULT "" ;
 DECLARE actionslist TEXT DEFAULT "" ;
 DECLARE limitId varchar(255) DEFAULT ""  ;
 DECLARE entryStatus INTEGER DEFAULT 0 ;
 DECLARE accountId varchar(255) DEFAULT "" ;
 DECLARE actualLimitId varchar(255) DEFAULT "" ;

DECLARE accounts CURSOR
         FOR (SELECT customeraccounts.Account_id FROM customeraccounts WHERE contractId = _contractId AND coreCustomerId = _coreCustomerId 
         AND Customer_id = _userId AND FIND_IN_SET(Account_id,_accountsCSV));
DECLARE actions CURSOR 
      FOR (select id from featureaction where FIND_IN_SET(id,@validActionsList) AND (featureaction.isAccountLevel = '1' OR featureaction.isAccountLevel = true ));
DECLARE limits CURSOR 
        FOR (select LimitType_id from actionlimit where Action_id COLLATE utf8_general_ci = featureActionId COLLATE utf8_general_ci );
DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET finished = 1;

SET SESSION group_concat_max_len = 100000000;

IF(ISNULL(_accountsCSV) OR _accountsCSV='' ) THEN
SET _accountsCSV = (SELECT group_concat(distinct customeraccounts.Account_id SEPARATOR ",") FROM customeraccounts WHERE customeraccounts.Customer_id = _userId
              AND customeraccounts.contractId = _contractId AND  customeraccounts.coreCustomerId = _coreCustomerId );
END IF;
 
SET @serviceDefinitionId = (SELECT servicedefinitionId from contract WHERE id = _contractId);
SET @serviceType = (SELECT serviceType from servicedefinition WHERE id = @serviceDefinitionId);
IF(ISNULL(_groupId) OR _groupId='' ) THEN
SET _groupId = (SELECT Group_id FROM groupservicedefinition WHERE serviceDefinitionId = @serviceDefinitionId 
                       AND (isDefaultGroup = true OR isDefaultGroup = '1'));
END IF;
                        
SET @validFIActions = (SELECT group_concat(distinct id SEPARATOR ",") FROM featureaction);

SET @validServiceDefinitionActions = (SELECT group_concat(distinct actionId SEPARATOR ",") FROM servicedefinitionactionlimit
                        WHERE serviceDefinitionId= @serviceDefinitionId AND FIND_IN_SET(actionId,@validFIActions));

SET _groupId = (SELECT Group_id FROM groupservicedefinition WHERE serviceDefinitionId = @serviceDefinitionId 
                        AND  Group_id = _groupId );


SET @validGroupActions = (SELECT group_concat(distinct Action_id SEPARATOR ",") FROM groupactionlimit WHERE Group_id = _groupId 
                                AND FIND_IN_SET(Action_id,@validServiceDefinitionActions));
                                
SET @validActionsList = (SELECT group_concat(distinct actionId SEPARATOR ",") FROM contractactionlimit WHERE contractId = _contractId
                                AND coreCustomerId = _coreCustomerId AND FIND_IN_SET(actionId,@validGroupActions));
  

OPEN accounts; 
getAccount : LOOP
FETCH accounts INTO accountId;
IF finished = 1 THEN 
    LEAVE getAccount;
ELSE
  OPEN actions; 
  getAction: LOOP
        SET entryStatus = 0;
        FETCH actions INTO featureActionId;
        SET @featureId = (SELECT Feature_id FROM featureaction WHERE id = featureActionId COLLATE utf8_general_ci);
       SET @limitGroupId = (SELECT limitgroupId FROM featureaction WHERE id = featureActionId COLLATE utf8_general_ci);
        IF finished = 1 THEN 
            LEAVE getAction;
        else
            OPEN limits; 
            getlimit: LOOP
            FETCH limits INTO limitId;
            IF finished = 1 THEN 
               LEAVE getlimit;
            else
               SET @limitvalue = (SELECT value FROM contractactionlimit WHERE actionId = featureActionId  COLLATE utf8_general_ci
               AND limitTypeId = limitId COLLATE utf8_general_ci AND contractId = _contractId AND coreCustomerId = _coreCustomerId);
    
               if (limitId='MAX_TRANSACTION_LIMIT') THEN 
               	SET actualLimitId = 'PRE_APPROVED_TRANSACTION_LIMIT';
				SET @id = (SELECT LEFT(UUID(), 50));
				INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,contractId,coreCustomerId,featureId,Action_id,Account_id,isAllowed,limitGroupId,LimitType_id,value) VALUES
				(@id,@serviceType,_userId,_contractId,_coreCustomerId,@featureId,featureActionId,accountId,true,@limitGroupId,actualLimitId,0.00);
               	SET actualLimitId = 'AUTO_DENIED_TRANSACTION_LIMIT';
               ELSEIF (limitId='DAILY_LIMIT') THEN 
               	SET actualLimitId = 'PRE_APPROVED_DAILY_LIMIT';
                SET @id = (SELECT LEFT(UUID(), 50));
                INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,contractId,coreCustomerId,featureId,Action_id,Account_id,isAllowed,limitGroupId,LimitType_id,value) VALUES
                (@id,@serviceType,_userId,_contractId,_coreCustomerId,@featureId,featureActionId,accountId,true,@limitGroupId,actualLimitId,0.00);
                SET actualLimitId = 'AUTO_DENIED_DAILY_LIMIT';
               ELSEIF (limitId='WEEKLY_LIMIT') THEN 
               	SET actualLimitId = 'PRE_APPROVED_WEEKLY_LIMIT';
                SET @id = (SELECT LEFT(UUID(), 50));
               	INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,contractId,coreCustomerId,featureId,Action_id,Account_id,isAllowed,limitGroupId,LimitType_id,value) VALUES
                (@id,@serviceType,_userId,_contractId,_coreCustomerId,@featureId,featureActionId,accountId,true,@limitGroupId,actualLimitId,0.00);
               	SET actualLimitId = 'AUTO_DENIED_WEEKLY_LIMIT';
               END IF;
               
              if (limitId!='MIN_TRANSACTION_LIMIT') THEN
               SET @id = (SELECT LEFT(UUID(), 50));
               INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,contractId,coreCustomerId,featureId,Action_id,Account_id,isAllowed,limitGroupId,LimitType_id,value) VALUES
               (@id,@serviceType,_userId,_contractId,_coreCustomerId,@featureId,featureActionId,accountId,true,@limitGroupId,actualLimitId,@limitvalue);
                
                SET @id = (SELECT LEFT(UUID(), 50));
                INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,contractId,coreCustomerId,featureId,Action_id,Account_id,isAllowed,limitGroupId,LimitType_id,value) VALUES
                (@id,@serviceType,_userId,_contractId,_coreCustomerId,@featureId,featureActionId,accountId,true,@limitGroupId,limitId,@limitvalue);
				END IF;
                SET entryStatus = 1;
               ITERATE  getlimit;
            END IF;
                END LOOP getlimit;
                CLOSE limits;
                
            SET finished = 0;
            IF entryStatus = 0 THEN
                  SET @id = (SELECT LEFT(UUID(), 50));
                   INSERT IGNORE INTO customeraction(id,RoleType_id,Customer_id,contractId,coreCustomerId,featureId,Action_id,Account_id,isAllowed) VALUES
                (@id,@serviceType,_userId,_contractId,_coreCustomerId,@featureId,featureActionId,accountId,true);
            END IF;
            ITERATE  getAction;
        END IF;
  END LOOP getAction;
  CLOSE actions;
  SET finished = 0;
  ITERATE  getAccount;
 END IF;
END LOOP getAccount;
CLOSE accounts;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_associated_corecustomeraccounts_info` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `user_associated_corecustomeraccounts_info`(
    in customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
    
    SET @implictCIF = (SELECT GROUP_CONCAT(contractcustomers.coreCustomerId SEPARATOR ",")
    FROM contractcustomers
    WHERE contractcustomers.customerId = customerId
    AND contractcustomers.autoSyncAccounts = '1' );
    
    SELECT contractcorecustomers.coreCustomerId , contractcorecustomers.contractId  from 
    contractcorecustomers where FIND_IN_SET(contractcorecustomers.coreCustomerId , @implictCIF) > 0;
    

    IF( @implictCIF) THEN
    
        SELECT customeraccounts.Account_id AS nonCIFAccounts
        FROM customeraccounts 
        WHERE customeraccounts.Customer_id = customerId
        AND FIND_IN_SET(customeraccounts.coreCustomerId , @implictCIF) = 0 ;
        
        SELECT contractaccounts.accountId AS contractaccounts
        FROM contractaccounts
        WHERE FIND_IN_SET(contractaccounts.coreCustomerId , @implictCIF) > 0 ; 
        
        SELECT excludedcontractaccounts.accountId AS excludedcontractaccounts
        FROM excludedcontractaccounts
        WHERE FIND_IN_SET(excludedcontractaccounts.coreCustomerId COLLATE utf8_general_ci , @implictCIF) > 0 ; 
        
        SELECT customeraccounts.Account_id AS customeraccounts
        FROM customeraccounts
        WHERE FIND_IN_SET(customeraccounts.coreCustomerId , @implictCIF) > 0 
        AND customeraccounts.Customer_id = customerId; 
        
        SELECT excludedcustomeraccounts.Account_id AS excludedcustomeraccounts
        FROM excludedcustomeraccounts
        WHERE FIND_IN_SET(excludedcustomeraccounts.coreCustomerId , @implictCIF) > 0 
        AND excludedcustomeraccounts.Customer_id = customerId; 
    ELSE
        SELECT customeraccounts.Account_id AS nonCIFAccounts
        FROM customeraccounts 
        WHERE customeraccounts.Customer_id = customerId;
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_customers_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `user_customers_proc`(
    in _customerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
    in _coreCustomerId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
    SET @select_statement = ("(SELECT 
        `contractcustomers`.`customerId` AS `customerId`,
        `contractcustomers`.`coreCustomerId` AS `coreCustomerId`,
        `contractcustomers`.`contractId` AS `contractId`,
        `contractcustomers`.`autoSyncAccounts` AS `autoSyncAccounts`,
        `contract`.`name` AS `contractName`,
        `contractcustomers`.`isPrimary` AS `isPrimary`,
        `contractcorecustomers`.`coreCustomerName` AS `coreCustomerName`,
        `contractcorecustomers`.`isBusiness` AS `isBusiness`,
        `contract`.`servicedefinitionId` AS `serviceDefinitionId`,
        `servicedefinition`.`name` AS `serviceDefinitionName`,
        `membergrouptype`.`description` AS `serviceDefinitionType`,
        `membergroup`.`id` AS `roleId`,
        `membergroup`.`Name` AS `userRole`
    FROM
        ((((((`contractcustomers`
        LEFT JOIN `contractcorecustomers` ON (((`contractcorecustomers`.`contractId` = `contractcustomers`.`contractId`)
            AND (`contractcorecustomers`.`coreCustomerId` = `contractcustomers`.`coreCustomerId`))))
        LEFT JOIN `contract` ON ((`contract`.`id` = `contractcorecustomers`.`contractId`)))
        LEFT JOIN `servicedefinition` ON ((`servicedefinition`.`id` = `contract`.`servicedefinitionId`)))
        LEFT JOIN `membergrouptype` ON ((`membergrouptype`.`id` = `servicedefinition`.`serviceType`)))
        LEFT JOIN `customergroup` ON (((`customergroup`.`Customer_id` = `contractcustomers`.`customerId`)
            AND (`customergroup`.`contractId` = `contractcustomers`.`contractId`)
            AND (`customergroup`.`coreCustomerId` = `contractcustomers`.`coreCustomerId`))))
        LEFT JOIN `membergroup` ON ((`membergroup`.`id` = `customergroup`.`Group_id`)))");
    SET @isWhereAppened = false;
    SET @shouldAndAppend = false;
    IF(_customerId != '') THEN
        IF(!@isWhereAppened) THEN
            SET @select_statement = CONCAT(@select_statement , " where");
            SET @isWhereAppened = true;
            SET @shouldAndAppend = true;
        END IF;
      set @select_statement =  concat(@select_statement ," `contractcustomers`.`customerId` = ",quote(_customerId));
    END IF;
    IF(_coreCustomerId != '') THEN
        IF(!@isWhereAppened) THEN
            SET @select_statement = CONCAT(@select_statement , " where");
            SET @shouldAndAppend = true;
        END IF;
        IF(@isWhereAppened and @shouldAndAppend) THEN
            SET @select_statement = CONCAT(@select_statement , " and");
            SET @shouldAndAppend = true;
        END IF;
      set @select_statement =  concat(@select_statement ," `contractcustomers`.`coreCustomerId` = ",quote(_coreCustomerId));
    END IF;
    set @select_statement =  concat(@select_statement ,");");
    PREPARE stmt FROM @select_statement; EXECUTE stmt; DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_limitgroup_limits_create_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `user_limitgroup_limits_create_proc`(
IN _userId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci, 
IN _coreCustomerId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
IN _contractId VARCHAR(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN

SET SESSION group_concat_max_len = 100000000;

SET @singlePaymentsActions = (SELECT group_concat(distinct featureaction.id SEPARATOR ",") from featureaction where
                             featureaction.limitgroupId = 'SINGLE_PAYMENT');
                             
SET @bulkPaymentsActions = (SELECT group_concat(distinct featureaction.id SEPARATOR ",") from featureaction where
                             featureaction.limitgroupId = 'BULK_PAYMENT');

SET @max_per_transaction_single_payment = (SELECT MAX(value) FROM customeraction WHERE 
                                         customeraction.contractId = _contractId AND
                                         customeraction.coreCustomerId = _coreCustomerId AND
                                         customeraction.Customer_id = _userId AND
                                         FIND_IN_SET(customeraction.Action_id,@singlePaymentsActions) AND
                                         customeraction.LimitType_id = 'AUTO_DENIED_TRANSACTION_LIMIT' AND
                                         !isnull(customeraction.Account_id) AND
                                         !isnull(customeraction.value));

SET @max_per_transaction_bulk_payment = (SELECT MAX(value) FROM customeraction WHERE 
                                         customeraction.contractId = _contractId AND
                                         customeraction.coreCustomerId = _coreCustomerId AND
                                         customeraction.Customer_id = _userId AND
                                         FIND_IN_SET(customeraction.Action_id,@bulkPaymentsActions) AND
                                         customeraction.LimitType_id = 'AUTO_DENIED_TRANSACTION_LIMIT' AND
                                         !isnull(customeraction.Account_id) AND
                                         !isnull(customeraction.value));
                                         
SET @max_daily_limit_single_payment = (SELECT SUM(value) FROM customeraction WHERE 
                                         customeraction.contractId = _contractId AND
                                         customeraction.coreCustomerId = _coreCustomerId AND
                                         customeraction.Customer_id = _userId AND
                                         FIND_IN_SET(customeraction.Action_id,@singlePaymentsActions) AND
                                         customeraction.LimitType_id = 'AUTO_DENIED_DAILY_LIMIT' AND
                                         !isnull(customeraction.Account_id) AND
                                         !isnull(customeraction.value));
                                         
SET @max_daily_limit_bulk_payment = (SELECT SUM(value) FROM customeraction WHERE 
                                         customeraction.contractId = _contractId AND
                                         customeraction.coreCustomerId = _coreCustomerId AND
                                         customeraction.Customer_id = _userId AND
                                         FIND_IN_SET(customeraction.Action_id,@bulkPaymentsActions) AND
                                         customeraction.LimitType_id = 'AUTO_DENIED_DAILY_LIMIT' AND
                                         !isnull(customeraction.Account_id) AND
                                         !isnull(customeraction.value));
                                         
SET @max_weekly_limit_single_payment = (SELECT SUM(value) FROM customeraction WHERE 
                                         customeraction.contractId = _contractId AND
                                         customeraction.coreCustomerId = _coreCustomerId AND
                                         customeraction.Customer_id = _userId AND
                                         FIND_IN_SET(customeraction.Action_id,@singlePaymentsActions) AND
                                         customeraction.LimitType_id = 'AUTO_DENIED_WEEKLY_LIMIT' AND
                                         !isnull(customeraction.Account_id) AND
                                         !isnull(customeraction.value));
                                         
SET @max_weekly_limit_bulk_payment = (SELECT SUM(value) FROM customeraction WHERE 
                                         customeraction.contractId = _contractId AND
                                         customeraction.coreCustomerId = _coreCustomerId AND
                                         customeraction.Customer_id = _userId AND
                                         FIND_IN_SET(customeraction.Action_id,@bulkPaymentsActions) AND
                                         customeraction.LimitType_id = 'AUTO_DENIED_WEEKLY_LIMIT' AND
                                         !isnull(customeraction.Account_id) AND
                                         !isnull(customeraction.value));
                                         

IF(@max_per_transaction_single_payment != 0) THEN 						
INSERT INTO `customerlimitgrouplimits` (`id`, `Customer_id`, `contractId`, `coreCustomerId`, `limitGroupId`, `LimitType_id`, `value`) VALUES 
(LEFT(UUID(), 50), _userId, _contractId, _coreCustomerId, 'SINGLE_PAYMENT', 'MAX_TRANSACTION_LIMIT', @max_per_transaction_single_payment);
END IF;

IF ( @max_per_transaction_bulk_payment != 0) THEN
INSERT INTO `customerlimitgrouplimits` (`id`, `Customer_id`, `contractId`, `coreCustomerId`, `limitGroupId`, `LimitType_id`, `value`) VALUES 
(LEFT(UUID(), 50), _userId, _contractId, _coreCustomerId, 'BULK_PAYMENT', 'MAX_TRANSACTION_LIMIT', @max_per_transaction_bulk_payment);
END IF;

IF ( @max_daily_limit_single_payment != 0) THEN
INSERT INTO `customerlimitgrouplimits` (`id`, `Customer_id`, `contractId`, `coreCustomerId`, `limitGroupId`, `LimitType_id`, `value`) VALUES 
(LEFT(UUID(), 50), _userId, _contractId, _coreCustomerId, 'SINGLE_PAYMENT', 'DAILY_LIMIT', @max_daily_limit_single_payment);
END IF;

IF ( @max_daily_limit_bulk_payment != 0) THEN
INSERT INTO `customerlimitgrouplimits` (`id`, `Customer_id`, `contractId`, `coreCustomerId`, `limitGroupId`, `LimitType_id`, `value`) VALUES 
(LEFT(UUID(), 50), _userId, _contractId, _coreCustomerId, 'BULK_PAYMENT', 'DAILY_LIMIT', @max_daily_limit_bulk_payment);
END IF;

IF ( @max_weekly_limit_single_payment != 0) THEN
INSERT INTO `customerlimitgrouplimits` (`id`, `Customer_id`, `contractId`, `coreCustomerId`, `limitGroupId`, `LimitType_id`, `value`) VALUES 
(LEFT(UUID(), 50), _userId, _contractId, _coreCustomerId, 'SINGLE_PAYMENT', 'WEEKLY_LIMIT', @max_weekly_limit_single_payment);
END IF;

IF ( @max_weekly_limit_bulk_payment != 0) THEN
INSERT INTO `customerlimitgrouplimits` (`id`, `Customer_id`, `contractId`, `coreCustomerId`, `limitGroupId`, `LimitType_id`, `value`) VALUES 
(LEFT(UUID(), 50), _userId, _contractId, _coreCustomerId, 'BULK_PAYMENT', 'WEEKLY_LIMIT', @max_weekly_limit_bulk_payment);
END IF;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_securityattributes_get_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `user_securityattributes_get_proc`(
in _userId varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci)
BEGIN

SET SESSION group_concat_max_len = 10000000;

SET @userAssociatedCoreCustomers =  (SELECT group_concat(distinct contractcustomers.coreCustomerId SEPARATOR ",") FROM contractcustomers WHERE contractcustomers.customerId = _userId);

SET @userAssociatedContracts =  (SELECT group_concat(distinct contractcustomers.contractId SEPARATOR ",") FROM contractcustomers WHERE contractcustomers.customerId = _userId);

SET @userAssociatedServiceDefinitions = (SELECT group_concat(distinct contract.servicedefinitionId SEPARATOR ",") FROM contract WHERE FIND_IN_SET(contract.id,@userAssociatedContracts) 
                                        AND contract.statusId = 'SID_CONTRACT_ACTIVE');

SET @userAssociatedGroups =  (SELECT group_concat(distinct customergroup.Group_id SEPARATOR ",") FROM customergroup WHERE customergroup.Customer_id = _userId);

SET @actionsAtCoreCustomers = (SELECT group_concat(distinct contractactionlimit.actionId SEPARATOR ",") FROM contractactionlimit WHERE FIND_IN_SET(contractactionlimit.coreCustomerId,@userAssociatedCoreCustomers));

SET @newActionsAtCoreCustomers = (SELECT group_concat(distinct contractactionlimit.actionId SEPARATOR ",") FROM contractactionlimit WHERE FIND_IN_SET(contractactionlimit.coreCustomerId,@userAssociatedCoreCustomers) and contractactionlimit.isNewAction = '1');

SET @actionsAtServiceDefinitions = (SELECT group_concat(distinct servicedefinitionactionlimit.actionId SEPARATOR ",") FROM servicedefinitionactionlimit WHERE FIND_IN_SET(servicedefinitionactionlimit.serviceDefinitionId,@userAssociatedServiceDefinitions));

SET @actionsAtGroups = (SELECT group_concat(distinct groupactionlimit.Action_id SEPARATOR ",") FROM groupactionlimit WHERE FIND_IN_SET(groupactionlimit.Group_id,@userAssociatedGroups));

SET @newActionsAtGroups = (SELECT group_concat(distinct groupactionlimit.Action_id SEPARATOR ",") FROM groupactionlimit WHERE FIND_IN_SET(groupactionlimit.Group_id,@userAssociatedGroups) and groupactionlimit.isNewAction = '1');

SET @actionsAtuser = (SELECT group_concat(distinct customeraction.Action_id SEPARATOR ",") FROM customeraction WHERE customeraction.Customer_id = _userId 
                     AND (customeraction.isAllowed ='1' OR customeraction.isAllowed = true));
                     
SET @activeFeaturesAtFI = (SELECT group_concat(distinct feature.id SEPARATOR ",") FROM feature WHERE Status_id = 'SID_FEATURE_ACTIVE'); 

SET @intersectedUserActions = (SELECT group_concat(distinct featureaction.id SEPARATOR ",") FROM featureaction WHERE 
                                featureaction.status = 'SID_ACTION_ACTIVE' AND
                                FIND_IN_SET(featureaction.id,@actionsAtuser) AND
                                FIND_IN_SET(featureaction.id,@actionsAtGroups) AND
                                FIND_IN_SET(featureaction.id,@actionsAtServiceDefinitions) AND
                                FIND_IN_SET(featureaction.id,@actionsAtCoreCustomers) AND 
                                FIND_IN_SET(featureaction.Feature_id,@activeFeaturesAtFI));
                               
SET @intersectedUserActions = (SELECT group_concat(distinct featureaction.id SEPARATOR ",") FROM featureaction WHERE 
                                featureaction.status = 'SID_ACTION_ACTIVE' AND(
                                FIND_IN_SET(featureaction.id,@intersectedUserActions) OR
                                FIND_IN_SET(featureaction.id,@newActionsAtCoreCustomers) OR
                                FIND_IN_SET(featureaction.id,@newActionsAtGroups)) AND
                                FIND_IN_SET(featureaction.id,@actionsAtServiceDefinitions) AND 
                                FIND_IN_SET(featureaction.Feature_id,@activeFeaturesAtFI));                               
                                
SELECT @intersectedUserActions AS actions;

SET @intersectedUserFeatures = (SELECT group_concat(distinct featureaction.Feature_id SEPARATOR ",") FROM featureaction WHERE 
                                  FIND_IN_SET(featureaction.id,@intersectedUserActions));
                                  
SELECT @intersectedUserFeatures AS features;
                                  
                       
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `verify_user_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `verify_user_proc`(
in _phone varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _email varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _dateOfBirth varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _backendIdentifiers varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci,
in _backendType varchar(50) CHARACTER SET UTF8 COLLATE utf8_general_ci
)
BEGIN
SET @usersList = (select group_concat(backendidentifier.Customer_id SEPARATOR ",") from backendidentifier where BackendType=_backendType  
									 AND FIND_IN_SET(backendidentifier.BackendId ,_backendIdentifiers) );

(SELECT 
        `customer`.`id` AS `id`,
        `customer`.`FirstName` AS `FirstName`,
        `customer`.`MiddleName` AS `MiddleName`,
        `customer`.`LastName` AS `LastName`,
        `customer`.`UserName` AS `UserName`,
        `customer`.`Gender` AS `Gender`,
        `customer`.`DateOfBirth` AS `DateOfBirth`,
        `customer`.`Ssn` AS `Ssn`,
        `customer`.`Status_id` AS `Status_id`,
        `customer`.`CustomerType_id` AS `CustomerType_id`
    FROM
        (`customer`
        LEFT JOIN `customercommunication` `primaryphone` ON ((`primaryphone`.`Customer_id` = `customer`.`id`)
            AND (`primaryphone`.`Value` = _phone)
            AND (`primaryphone`.`Type_id` = 'COMM_TYPE_PHONE'))
        LEFT JOIN `customercommunication` `primaryemail` ON ((`primaryemail`.`Customer_id` = `customer`.`id`)
            AND (`primaryemail`.`Value` = _email)
            AND (`primaryemail`.`Type_id` = 'COMM_TYPE_EMAIL')))
	where
		`customer`.`DateOfBirth` = _dateOfBirth
		and `primaryphone`.`Customer_id` = `primaryemail`.`Customer_id`) union SELECT 
        `customer`.`id` AS `id`,
        `customer`.`FirstName` AS `FirstName`,
        `customer`.`MiddleName` AS `MiddleName`,
        `customer`.`LastName` AS `LastName`,
        `customer`.`UserName` AS `UserName`,
        `customer`.`Gender` AS `Gender`,
        `customer`.`DateOfBirth` AS `DateOfBirth`,
        `customer`.`Ssn` AS `Ssn`,
        `customer`.`Status_id` AS `Status_id`,
		`customer`.`CustomerType_id` AS `CustomerType_id`
    FROM `customer` where FIND_IN_SET(customer.id ,@usersList);
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `weekend_proc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`infinity`@`%` PROCEDURE `weekend_proc`()
BEGIN
set @startDate=CURDATE();
set @count = 0;
while @count < 100 do
if(WEEKDAY(@startDate) = 6 or WEEKDAY(@startDate) = 5) then
insert into holidays(`holidayDate`,`createdBy`,`modifiedby`) values(@startDate,'priya','priya');
set @count = @count+1;
end if;
set @startDate=DATE_ADD(@startDate, INTERVAL 1 DAY);
end while;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-15 13:47:26
