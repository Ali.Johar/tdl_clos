/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_CreateCustomerJoint` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_CreateCustomerJoint`( IN customerJson JSON,
                                    IN length INT )
BEGIN
	DECLARE p_id varchar(50) default "0";
    DECLARE p_ApplicationID varchar(100) default "0";
    DECLARE p_CustomerType varchar(200) default "0";
    DECLARE p_AccountType varchar(200) default "0";
    DECLARE p_IdDocumentType varchar(50) default "0";
    DECLARE p_IdDocumentTypeValue varchar(50) default "0";
    DECLARE p_IdDocumentNumber varchar(50) default "0";
    DECLARE p_NtbEtb varchar(45) default "0";
    DECLARE p_ATA varchar(45) default "0";
    DECLARE p_PageName varchar(100) default "0";
    DECLARE P_IsPrimaryCustomer tinyint(4) default 0;
    DECLARE P_processInstanceId varchar(50) default "0";
    DECLARE p_taskInstanceId varchar(50) default "0";
	DECLARE p_CreatedDate varchar(100) default "0";
    DECLARE p_CreatedBy tinyint(4) default 0;
    DECLARE counter smallint default 0;
    DECLARE p_Id1 varchar(50) default "0";
    DECLARE p_Id2 varchar(50) default "0";
    DECLARE p_Id3 varchar(50) default "0";
    DECLARE p_Id4 varchar(50) default "0";
    
    
    
	start transaction;
    set autocommit = 0;
    
    WHILE counter < length DO
    -- Extract the JSON value
    -- SELECT JSON_VALUE(customerJson, CONCAT('$[', counter, '].id')) INTO p_id;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].ApplicationID'))) INTO p_ApplicationID;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].CustomerType'))) INTO p_CustomerType;
	SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].AccountType'))) INTO p_AccountType;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].IdDocumentType'))) INTO p_IdDocumentType;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].IdDocumentTypeValue'))) INTO p_IdDocumentTypeValue;
	SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].IdDocumentNumber'))) INTO p_IdDocumentNumber;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].Id1'))) INTO p_Id1;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].Id2'))) INTO p_Id2;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].Id3'))) INTO p_Id3;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].Id4'))) INTO p_Id4;
	SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].NtbEtb'))) INTO p_NtbEtb;
	SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].ATA'))) INTO p_ATA;
	SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].PageName'))) INTO p_PageName;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].isPrimaryCustomer'))) INTO P_IsPrimaryCustomer;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].CreatedDate'))) INTO p_CreatedDate;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].CreatedBy'))) INTO p_CreatedBy;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].processInstanceId'))) INTO P_processInstanceId;
    SELECT JSON_UNQUOTE(JSON_EXTRACT(customerJson, CONCAT('$[', counter, '].taskInstanceId'))) INTO p_taskInstanceId;
   

IF (counter = 0 and length > 1)then
    BEGIN
      set P_IdDocumentNumber = p_Id1; -- Setting first CNIC
    --  select P_IdDocumentNumber;
    END;
ELSEIF (counter = 1 and length > 1)then
    BEGIN
      set P_IdDocumentNumber = p_Id2; -- Setting Second CNIC
    --  select P_IdDocumentNumber;
    END;
ELSEIF (counter = 2 and length >2 )then
    BEGIN
      set P_IdDocumentNumber = p_Id3; -- Setting Third CNIC if Length is 3
    --  select P_IdDocumentNumber;
    END;    
ELSEIF (counter = 3 and length > 3)then 
   BEGIN
      set P_IdDocumentNumber = p_Id4; -- Setting 4th CNIC if Length is 4
     -- select P_IdDocumentNumber;
   END;
END IF;

   INSERT INTO dbxdb.UBL_CLM_CUSTOMER_DATA(id,ApplicationID,CustomerType,AccountType,IdDocumentType,IdDocumentTypeValue,IdDocumentNumber,NtbEtb,ATA,PageName,Status, IsPrimaryCustomer,processInstanceId, taskInstanceId ,CreatedDate, CreatedBy)
   VALUES(CONCAT(p_ApplicationID,counter),p_ApplicationID,p_CustomerType,p_AccountType,p_IdDocumentType,p_IdDocumentTypeValue,p_IdDocumentNumber,p_NtbEtb,p_ATA,p_PageName,"M0", P_IsPrimaryCustomer, P_processInstanceId, p_taskInstanceId ,p_CreatedDate, p_CreatedBy);
   
    SET counter = counter + 1;
   -- select counter;
    END WHILE;
    COMMIT;
    
    
    SELECT id from dbxdb.UBL_CLM_CUSTOMER_DATA where ApplicationID= (SELECT p_ApplicationID);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_Pagination` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_Pagination`( IN offset_in INT)
BEGIN
SELECT ApplicationID, CustName, "Branch", ModifiedDate,(CASE
WHEN Status = "M0" THEN "Pending at Maker"
WHEN Status = "M1" THEN "Forward to Checker"
WHEN Status = "C0" THEN "Pending at Checker"
WHEN Status = "C1" THEN "Forward to Back Office"
WHEN Status = "R0" THEN "Reject by Checker"             
ELSE " "
END) as Status, BranchNameIdValue, IdDocumentTypeValue,IdDocumentNumber,
CreatedDate, CustClientCategoryIdValue, AccountType FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('M0','M1', 'R0') and IsPrimaryCustomer = 1 ORDER BY CreatedDate DESC LIMIT 10
OFFSET offset_in;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_PaginationChecker` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_PaginationChecker`( IN offset_in INT, IN processInstanceIds varchar(5000))
BEGIN

DECLARE offsetCustom int default 0;
DECLARE total int default 0;


SET total = (SELECT ceil(count(*) / 10) as Total FROM dbxdb.UBL_CLM_CUSTOMER_DATA
where   Status in ('M1','C0','C1') and IsPrimaryCustomer = 1 );

CASE WHEN offset_in is NULL THEN
SELECT total,ApplicationID, CustName, "Branch", ModifiedDate,(CASE
WHEN Status = "M0" THEN "Pending at Maker"
WHEN Status = "M1" THEN "Forward to Checker"
WHEN Status = "C0" THEN "Pending at Checker"
WHEN Status = "C1" THEN "Forward to Back Office"
WHEN Status = "R0" THEN "Reject by Checker"
ELSE " "
END) as Status, BranchNameIdValue, IdDocumentTypeValue,IdDocumentNumber,
CreatedDate AS "CreatedDate", CustClientCategoryIdValue, AccountType FROM dbxdb.UBL_CLM_CUSTOMER_DATA
where   Status in ('M1','C0','C1') and
 IsPrimaryCustomer = 1 ORDER BY CreatedDate DESC ;

ELSE
SET offsetCustom = offset_in *10;
SELECT total,ApplicationID, CustName, "Branch", ModifiedDate,(CASE
WHEN Status = "M0" THEN "Pending at Maker"
WHEN Status = "M1" THEN "Forward to Checker"
WHEN Status = "C0" THEN "Pending at Checker"
WHEN Status = "C1" THEN "Forward to Back Office"
WHEN Status = "R0" THEN "Reject by Checker"
ELSE " "
END) as Status, BranchNameIdValue, IdDocumentTypeValue,IdDocumentNumber,
CreatedDate AS "CreatedDate", CustClientCategoryIdValue, AccountType FROM dbxdb.UBL_CLM_CUSTOMER_DATA
where   Status in ('M1','C0','C1')   and
 IsPrimaryCustomer = 1 ORDER BY CreatedDate  DESC LIMIT 10
OFFSET offsetCustom;
END CASE;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_PaginationDiscrepancy` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_PaginationDiscrepancy`( IN offset_in INT)
BEGIN
DECLARE offsetCustom int default 0;
DECLARE total int default 0;
SET offsetCustom = offset_in *10;

SET total = (SELECT ceil(count(*) / 10) as Total FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('DP') and IsPrimaryCustomer = 1);

SELECT total, ApplicationID, CustName,CustomerType,ProductIdValue, custAccountNumber,AccountType, "Branch", ModifiedDate,(CASE
WHEN Status = "DP" THEN "AOF Under Discrepancy"         
ELSE " "
END) as Status, BranchNameIdValue,
COALESCE(SubmittedDate,CreatedDate) AS "CreatedDate" FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('DP') and IsPrimaryCustomer = 1 ORDER BY COALESCE(SubmittedDate,CreatedDate)  DESC LIMIT 10
OFFSET offsetCustom;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_PaginationInputter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_PaginationInputter`( IN offset_in INT)
BEGIN
DECLARE offsetCustom int default 0;
DECLARE total int default 0;
SET offsetCustom = offset_in *10;

SET total = (SELECT ceil(count(*) / 10) as Total FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('IP') and IsPrimaryCustomer = 1);

SELECT total, ApplicationID, CustName,CustomerType,ProductIdValue, custAccountNumber,AccountType, "Branch", ModifiedDate,(CASE
WHEN Status = "IP" THEN "AOF Under Inputter"         
ELSE " "
END) as Status, BranchNameIdValue,
COALESCE(SubmittedDate,CreatedDate) AS "CreatedDate" FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('IP') and IsPrimaryCustomer = 1 ORDER BY COALESCE(SubmittedDate,CreatedDate)  DESC LIMIT 10
OFFSET offsetCustom;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_PaginationMaker` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_PaginationMaker`( IN offset_in INT,  IN processInstanceIds varchar(900))
BEGIN 
DECLARE offsetCustom int default 0;
DECLARE total int default 0;


SET total = (SELECT ceil(count(*) / 10) as Total FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('M0','M1', 'R0') and IsPrimaryCustomer = 1 );
CASE WHEN offset_in is NULL THEN
SELECT total,ApplicationID, CustName, "Branch", ModifiedDate, processInstanceId,(CASE
WHEN Status = "M0" THEN "Pending at Maker"
WHEN Status = "M1" THEN "Forward to Checker"
WHEN Status = "C0" THEN "Pending at Checker"
WHEN Status = "C1" THEN "Forward to Back Office"
WHEN Status = "R0" THEN "Rejected by Checker"             
ELSE " "
END) as Status, BranchNameIdValue, IdDocumentTypeValue,IdDocumentNumber,
CreatedDate as "CreatedDate", CustClientCategoryIdValue, AccountType FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('M0','M1', 'R0')  
and IsPrimaryCustomer = 1 ORDER BY CreatedDate DESC ;


ELSE
SET offsetCustom = offset_in *10;
SELECT total,ApplicationID, CustName, "Branch", ModifiedDate,(CASE
WHEN Status = "M0" THEN "Pending at Maker"
WHEN Status = "M1" THEN "Forward to Checker"
WHEN Status = "C0" THEN "Pending at Checker"
WHEN Status = "C1" THEN "Forward to Back Office"
WHEN Status = "R0" THEN "Rejected by Checker"             
ELSE " "
END) as Status, BranchNameIdValue, IdDocumentTypeValue,IdDocumentNumber,
CreatedDate  as "CreatedDate", CustClientCategoryIdValue, AccountType FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('M0','M1', 'R0')  
and IsPrimaryCustomer = 1 ORDER BY CreatedDate DESC  LIMIT 10
OFFSET offsetCustom;
END CASE;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_PaginationMakerDescriment` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_PaginationMakerDescriment`()
BEGIN 

DECLARE total int default 0;


SET total = (SELECT ceil(count(*) / 10) as Total FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('DC') and IsPrimaryCustomer = 1 );

SELECT total,ApplicationID, CustName, "Branch", ModifiedDate,(CASE
WHEN Status = "DC" THEN "Marked as Discrepant"
ELSE " "
END) as Status, BranchNameIdValue, IdDocumentTypeValue,IdDocumentNumber,
CreatedDate as "CreatedDate", CustClientCategoryIdValue, AccountType FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('DC') 
 and IsPrimaryCustomer = 1 ORDER BY CreatedDate DESC  
;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_PaginationScruitny` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_PaginationScruitny`( IN offset_in INT)
BEGIN
DECLARE offsetCustom int default 0;
DECLARE total int default 0;
SET offsetCustom = offset_in *10;

SET total = (SELECT ceil(count(*) / 10) as Total FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('C1') and IsPrimaryCustomer = 1);

SELECT total, ApplicationID, CustName,CustomerType,ProductIdValue, custAccountNumber,AccountType, "Branch", ModifiedDate,(CASE
WHEN Status = "C1" THEN "AOF Under Scrutiny"         
ELSE " "
END) as Status, BranchNameIdValue,
COALESCE(SubmittedDate,CreatedDate) AS "CreatedDate" FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('C1') and IsPrimaryCustomer = 1 ORDER BY COALESCE(SubmittedDate,CreatedDate)  DESC LIMIT 10
OFFSET offsetCustom;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_UBL_CLM_PaginationSupervision` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`developer`@`%` PROCEDURE `sp_UBL_CLM_PaginationSupervision`( IN offset_in INT)
BEGIN
DECLARE offsetCustom int default 0;
DECLARE total int default 0;
SET offsetCustom = offset_in *10;

SET total = (SELECT ceil(count(*) / 10) as Total FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('SP') and IsPrimaryCustomer = 1);

SELECT total, ApplicationID, CustName,CustomerType,ProductIdValue, custAccountNumber,AccountType, "Branch", ModifiedDate,(CASE
WHEN Status = "SP" THEN "Under Supervision"         
ELSE " "
END) as Status, BranchNameIdValue,
COALESCE(SubmittedDate,CreatedDate) AS "CreatedDate" FROM dbxdb.UBL_CLM_CUSTOMER_DATA 
where   Status in ('SP') and IsPrimaryCustomer = 1 ORDER BY COALESCE(SubmittedDate,CreatedDate)  DESC LIMIT 10
OFFSET offsetCustom;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_tabledata` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;